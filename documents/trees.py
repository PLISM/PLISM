import sys, os, json


with open('trees.json') as f:
    data = json.load(f)

if not os.path.exists("trees"):
        os.makedirs("trees")
for module_name, module in data.items():
    fout = open(f"trees/{module_name}.md", "w")
    def write(s):
            if s:
                fout.write(f"\n{s}\n\n") if s.startswith("#") else fout.write(f"{s}  \n")
    write("[[_TOC_]]")
    for tree_title, tree in module.items():
        write(f"# *tree* {tree_title}")
        write("Name: `{}`".format(tree["name"]))
        write(tree["description"])
        write("**Each entry corresponds to {}.**".format(tree["entry"]))
        for detail in tree["detail"]:
            write(f"- {detail}")
        fout.write("\n\n")
        fout.write("| type | name | description |\n")
        fout.write("| :--- | :--- | :--- |\n")
        for branch_name, branch in tree["branches"].items():
            #write("#### *{0}* `{1}`".format(branch["type"], branch_name))
            #write(branch["description"])
            fout.write("| `{0}` | `{1}` | {2} |\n".format(branch["type"], branch_name, branch["description"]))
        fout.write("\n\n")
    fout.close()
