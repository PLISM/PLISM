import os, sys, glob
import xml.etree.ElementTree as ET 

filelist = glob.glob("../source/CellPropertyGenerator/**/*Module.cxx", recursive=True)
filelist += glob.glob("../source/HitSummarizer/**/*Module.cxx", recursive=True)
filelist += glob.glob("../source/SequenceGenerator/**/*Module.cxx", recursive=True)
filelist += glob.glob("../source/Reconstructor/**/*Module.cxx", recursive=True)
filelist += glob.glob("../source/SCconditionSimulator/**/*Module.cxx", recursive=True)

class module:
    def __init__(self, namespace, name) -> None:
        self.namespace = namespace
        self.name = name
        self.classes = {}

class cppclass:
    def __init__(self, name, display) -> None:
        self.name = name
        self.display = display
        self.attributes = {}
        self.funcs = {}
        self.consts = []
        self.description = ""

class constructor:
    def __init__(self, name) -> None:
        self.name = name
        self.description = ""
        self.args = [] # element is argument

class attribute:
    def __init__(self, name, display) -> None:
        self.type = ""
        self.name = name
        self.display = display
        self.description = ""
        self.initializer = ""

class function:
    def __init__(self, name, display) -> None:
        self.type = ""
        self.name = name
        self.display = display
        self.description = ""
        self.args = [] # element is argument

class argument:
    def __init__(self, type, name) -> None:
        self.type = type
        self.name = name
        self.description = ""

def error(file, line, message):
    print("ERROR: Unknown format")
    print(message)
    print(f"File: {file}")
    print(f"Line: {line}")
    sys.exit(1)

def getClassDic(l):
    out = l[0].classes
    if len(l) > 0:
        for m in l[1:]:
            out.update(m.classes)
    return out

modules = []
for f in filelist:
    fin = open(f, "r")
    for line in fin:
        line = line.strip()
        display = lambda : line.split("\"")[1]
        if line.startswith("using namespace"): # name space
            namespace = line.split()[-1].split(";")[0]
        elif line.startswith("PYBIND11_MODULE"): # module
            name = line.split("(")[1].split(",")[0]
            modules.append(module(namespace, name))
        elif line.startswith("pybind11::class_<"): # class
            classname = line.split("<")[1].split(">")[0]
            modules[-1].classes[classname] = cppclass(classname, display())
        elif line.startswith(".def_readwrite(\""): # attribute
            name = line.split(":")[-1].split(")")[0]
            modules[-1].classes[classname].attributes[name] = attribute(name, display())
        elif line.startswith(".def(\""): # function
            name = line.split(":")[-1].split(")")[0]
            modules[-1].classes[classname].funcs[name] = function(name, display())
    fin.close()

def rm_angblackets(nodes):
    s = str(ET.tostring(nodes[0]))
    for x in ["para", "name", "type", "initializer", "declname"]:
        if f"<{x}>" in s:
            s = s.split(f"<{x}>")[1].split(f"</{x}>")[0].strip()
    s = s.replace("amp;", "&").replace("gt;", ">").replace("lt;", "<")
    refs = [x.text for x in nodes[0].findall("ref")]
    outs = []
    tmp = s
    if "<ref " in s:
        while True:
            if "<ref " in tmp:
                outs.append(tmp.split("<ref ")[0])
                tmp = "<ref ".join(tmp.split("<ref ")[1:])
                outs.append(refs[0])
                refs = refs[1:]
                outs.append(s.split("</ref>")[1])
                tmp = "</ref>".join(tmp.split("</ref>")[1:])
            else:
                break
        return "".join(outs)
    else:
        return s

def unique_text(nodes):
    if len(nodes) != 1:
        print(f"ERROR {nodes}")
        sys.exit(1)
    return rm_angblackets(nodes)

def lt2_text(nodes):
    if len(nodes) == 0:
        return ""
    elif len(nodes) == 1:
        return rm_angblackets(nodes)
    print(f"ERROR {nodes}")
    sys.exit(1)

def typefix(t):
    t = t.replace("const ", "")
    t = t.replace("* ", "").replace(" *", "").replace("*", "")
    t = t.replace("& ", "").replace(" &", "").replace("&", "")
    t = t.replace("< ", "<").replace(" <", "<")
    t = t.replace("> ", ">").replace(" >", ">")
    t = t.replace("<", "[").replace(">", "]")
    t = t.replace("std::", "")
    t = t.replace("vector", "list").replace("double", "float")
    t = t.replace("Int_t", "int").replace("Bool_t", "bool").replace("Float_t", "float").replace("Double_t", "float")
    return t

for m in modules:
    for c in m.classes.values():
        def fill(filename):
            tree = ET.parse(filename)
            root = tree.getroot()
            if not c.description:
                briefdescription = lt2_text(root.findall("./compounddef/briefdescription/para"))
                detaileddescription = lt2_text(root.findall("./compounddef/detaileddescription/para"))
                c.description = f"{briefdescription}  \n{detaileddescription}"
            sectiondef = root.findall("./compounddef/sectiondef")
            for section in sectiondef:
                if section.get("kind") == "public-attrib":
                    memberdef = section.findall("./memberdef")
                    for mem in memberdef:
                        name = unique_text(mem.findall("./name"))
                        if name in c.attributes.keys():
                            c.attributes[name].type = typefix(unique_text(mem.findall("./type")))
                            c.attributes[name].description = lt2_text(mem.findall("./briefdescription/para"))
                            c.attributes[name].initializer = lt2_text(mem.findall("./initializer"))
                            if c.attributes[name].initializer.startswith("{") and c.attributes[name].initializer.endswith("}"):
                                c.attributes[name].initializer = c.attributes[name].initializer[1:-1]
                            c.attributes[name].initializer = c.attributes[name].initializer.replace("true", "True").replace("false", "False")
                            if c.attributes[name].initializer == "":
                                if c.attributes[name].type == "bool":
                                    c.attributes[name].initializer = "False"
                                elif c.attributes[name].type.startswith("list"):
                                    c.attributes[name].initializer = "[]"
                                elif c.attributes[name].type == "string":
                                    c.attributes[name].initializer = "\"\""
                                else:
                                    c.attributes[name].initializer = "0"
                elif section.get("kind") == "public-func":
                    memberdef = section.findall("./memberdef")
                    for mem in memberdef:
                        name = unique_text(mem.findall("./name"))
                        if name in c.funcs.keys():
                            c.funcs[name].type = typefix(unique_text(mem.findall("./type")))
                            c.funcs[name].description = lt2_text(mem.findall("./briefdescription/para"))
                            params = mem.findall("./param")
                            paradescs = mem.findall("./detaileddescription/para/parameterlist/parameteritem/parameterdescription/para")
                            for param, paradesc in zip(params, paradescs):
                                type = typefix(unique_text(param.findall("./type")))
                                declname = unique_text(param.findall("./declname"))
                                description = paradesc.text.strip()
                                c.funcs[name].args.append(argument(type, declname))
                                c.funcs[name].args[-1].description = description
                        elif name == c.name: # constructor
                            c.consts.append(constructor(name))
                            c.consts[-1].description = lt2_text(mem.findall("./briefdescription/para"))
                            params = mem.findall("./param")
                            paradescs = mem.findall("./detaileddescription/para/parameterlist/parameteritem/parameterdescription/para")
                            for param, paradesc in zip(params, paradescs):
                                type = typefix(unique_text(param.findall("./type")))
                                declname = unique_text(param.findall("./declname"))
                                description = paradesc.text.strip()
                                c.consts[-1].args.append(argument(type, declname))
                                c.consts[-1].args[-1].description = description
            basecompoundref = root.findall("./compounddef/basecompoundref")
            for base in basecompoundref:
                ifnametmp = base.get("refid")
                fill(f"Doxydir/xml/{ifnametmp}.xml")
        fill(f"Doxydir/xml/class{m.namespace}_1_1{c.name}.xml")


if not os.path.exists("reference"):
        os.makedirs("reference")
for m in modules:
    fout = open(f"reference/{m.name}.md", "w")
    def write(s):
        if s:
            fout.write(f"\n{s}\n\n") if s.startswith("#") else fout.write(f"{s}  \n")
    write("[[_TOC_]]")
    for c in sorted(m.classes.values(), key=lambda x : x.display):
        write(f"# *class* {c.display}")
        write(c.description)
        write("## Constructors")
        for ct in sorted(c.consts, key=lambda x : len(x.args)):
            write(f"### `{c.display}`")
            write(ct.description)
            if len(ct.args):
                write("**Arguments**:")
                for i, a in enumerate(ct.args, 1):
                    #write(f"##### {i}. *{a.type}* `{a.name}`")
                    #write(a.description)
                    write(f"{i}. ***{a.type}*** - {a.description}")
            else:
                write("There is no argument.")
        write("## Member functions")
        for func in sorted(c.funcs.values(), key=lambda x : x.display):
            write(f"### *{func.type}* `{func.display}`")
            write(func.description)
            if len(func.args):
                write("**Arguments**:")
                for i, a in enumerate(func.args, 1):
                    #write(f"##### {i}. *{a.type}* `{a.name}`")
                    #write(a.description)
                    write(f"{i}. ***{a.type}*** - {a.description}")
            else:
                write("There is no argument.")
        write("## Attributes")
        for p in sorted(c.attributes.values(), key=lambda x : x.display):
            write(f"### *{p.type}* `{p.display}`")
            write(f"Default: `{p.initializer}`")
            write(p.description)
    fout.close()
