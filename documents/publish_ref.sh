wikidir=~/PLISM.wiki

cp reference/CellPropertyGeneratorModule.md $wikidir/CellPropertyGeneratorModule/Reference.md
cp reference/HitSummarizerModule.md	      $wikidir/HitSummarizerModule/Reference.md
cp reference/ReconstructorModule.md	      $wikidir/ReconstructorModule/Reference.md
cp reference/DigitizerModule.md	          $wikidir/DigitizerModule/Reference.md
cp reference/OFCCalibratorModule.md         $wikidir/OFCCalibratorModule/Reference.md
cp reference/SCconditionSimulatorModule.md         $wikidir/SCconditionSimulator/Reference.md

cp trees/CellPropertyGeneratorModule.md $wikidir/CellPropertyGeneratorModule/Output-trees.md
cp trees/HitSummarizerModule.md	      $wikidir/HitSummarizerModule/Output-trees.md
cp trees/ReconstructorModule.md	      $wikidir/ReconstructorModule/Output-trees.md
cp trees/DigitizerModule.md	          $wikidir/DigitizerModule/Output-trees.md
cp trees/OFCCalibratorModule.md         $wikidir/OFCCalibratorModule/Output-trees.md
cp trees/SCconditionSimulatorModule.md         $wikidir/SCconditionSimulatiorModule/Output-trees.md
