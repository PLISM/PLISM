
import sqlite3, os
import numpy as np

target = ["SC_ONL_ID", "DET"]
condition = "(FTNAME like 'I%' or FTNAME like 'A%' or FTNAME like 'C%') and (DET = 0) or (DET = 1)"
connection = sqlite3.connect(os.environ['LITDB'])
cursor = connection.cursor()
l = []
for row in cursor.execute("select " + ",".join(target) + " from LARID where " + condition):
    #if row[1]==0: print(row)
    l.append(row[0])

l = list(set(l))
a = np.array(l)
np.save("EMSCIDlist", a)

