import ROOT as root
import atlasplots as aplt
import math, sys
import numpy as np
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

EMSCIDlist = set(np.load("EMSCIDlist.npy"))

fin = root.TFile('../storage/CellProperties/CellProperty_test.root', "read")
tin = fin.Get("CellProperty")
fout = root.TFile('canvas.root', "recreate")

g_pedADC = [root.TGraph() for i in range(4)]
g_pedRMS = [root.TGraph() for i in range(4)]
g_LSB = [root.TGraph() for i in range(4)]

chids = set()
nEntries = tin.GetEntries()
for i in range(0, nEntries):
    tin.GetEntry(i)

    if tin.eta < 0:
        continue
    if tin.phi < 1.6 or tin.phi > 1.7:
        continue
    if not (tin.channelId in EMSCIDlist):
        continue
    if tin.channelId in chids:
        continue
    if tin.layer < 0 or tin.layer > 4:
        print("ERROR: Unknown layer")
        sys.exit()

    chids.add(tin.channelId)

    g_pedADC[tin.layer].SetPoint(g_pedADC[tin.layer].GetN(), tin.eta, tin.pedADC)
    g_pedRMS[tin.layer].SetPoint(g_pedRMS[tin.layer].GetN(), tin.eta, tin.noise)
    g_LSB[tin.layer].SetPoint(g_LSB[tin.layer].GetN(), tin.eta, tin.LSB)

colors = [807, 64, 847, 52]
markerstyles = [23, 22, 21, 20]
labels = ["Presampler", "Front layer", "Middle layer", "Back layer"]

fig_pedADC, ax_pedADC = aplt.subplots(1, 1)
fig_pedADC.canvas.SetCanvasSize(1400, 600)
for i in range(4):
    ax_pedADC.plot(g_pedADC[i], options="p", markercolor=colors[i], markerstyle=markerstyles[i], label=labels[i], labelfmt="p")
ax_pedADC.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
ax_pedADC.set_xlabel("#eta")
ax_pedADC.set_ylabel("Pedestal [ADC]")
ax_pedADC.set_xlim(0, 3.3)
ax_pedADC.set_ylim(0, 1100)
line = root.TLine(2.5, ax_pedADC.get_ylim()[0], 2.5, ax_pedADC.get_ylim()[1])
line.SetLineColor(root.kGray+1)
line.SetLineStyle(2)
line.Draw()
ax_pedADC.text(0.2, 0.86, "#phi = 1.62", size=22, align=13)
ax_pedADC.text(0.2, 0.86, "(#Delta#phi = 0.1)", size=22, align=13)
ax_pedADC.text(0.2, 0.86, "#phi = 1.66", size=22, align=13)
ax_pedADC.text(0.2, 0.86, "(#Delta#phi = 0.2)", size=22, align=13)
fig_pedADC.canvas.SetName("pedADC")
fig_pedADC.canvas.Write()

fig_pedRMS, ax_pedRMS = aplt.subplots(1, 1)
fig_pedRMS.canvas.SetCanvasSize(1400, 600)
for i in range(4):
    ax_pedRMS.plot(g_pedRMS[i], options="p", markercolor=colors[i], markerstyle=markerstyles[i], label=labels[i], labelfmt="p")
ax_pedRMS.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
ax_pedRMS.set_xlabel("#eta")
ax_pedRMS.set_ylabel("Pedestal RMS [ADC]")
ax_pedRMS.set_xlim(0, 3.3)
ax_pedRMS.set_ylim(0, 1.2)
line = root.TLine(2.5, ax_pedRMS.get_ylim()[0], 2.5, ax_pedRMS.get_ylim()[1])
line.SetLineColor(root.kGray+1)
line.SetLineStyle(2)
line.Draw()
ax_pedRMS.text(0.2, 0.86, "#phi = 1.62", size=22, align=13)
ax_pedRMS.text(0.2, 0.86, "(#Delta#phi = 0.1)", size=22, align=13)
ax_pedRMS.text(0.2, 0.86, "#phi = 1.66", size=22, align=13)
ax_pedRMS.text(0.2, 0.86, "(#Delta#phi = 0.2)", size=22, align=13)
fig_pedRMS.canvas.SetName("pedRMS")
fig_pedRMS.canvas.Write()

fig_LSB, ax_LSB = aplt.subplots(1, 1)
fig_LSB.canvas.SetCanvasSize(1400, 600)
for i in range(4):
    ax_LSB.plot(g_LSB[i], options="p", markercolor=colors[i], markerstyle=markerstyles[i], label=labels[i], labelfmt="p")
ax_LSB.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
ax_LSB.set_xlabel("#eta")
ax_LSB.set_ylabel("#font[52]{E}_{T}/ADC [MeV]")
ax_LSB.set_xlim(0, 3.3)
ax_LSB.set_ylim(0, 600)
line = root.TLine(2.5, ax_LSB.get_ylim()[0], 2.5, ax_LSB.get_ylim()[1])
line.SetLineColor(root.kGray+1)
line.SetLineStyle(2)
line.Draw()
ax_LSB.text(0.2, 0.86, "#phi = 1.62", size=22, align=13)
ax_LSB.text(0.2, 0.86, "(#Delta#phi = 0.1)", size=22, align=13)
ax_LSB.text(0.2, 0.86, "#phi = 1.66", size=22, align=13)
ax_LSB.text(0.2, 0.86, "(#Delta#phi = 0.2)", size=22, align=13)
fig_LSB.canvas.SetName("LSB")
fig_LSB.canvas.Write()



