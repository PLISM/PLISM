import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

DAC2MeVTree = CellPropertyGeneratorModule.DAC2MeVTree(
    #'/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArDAC2MeV.root',
    'LArDAC2MeV.root',
    'LArIDtranslator_test.txt'
)

DAC2MeVTree.OutputDACtoMeV('SCDAC2MeV.root')
#DAC2MeVTree.OutputDACuAMeVDistribution('LArIDtranslator_tmp.root', "DACuAMeVDistribution.root", 'DACuAMeVDistribution.pdf')
