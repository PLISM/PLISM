import sys
from ROOT import TFile, TTree
args = sys.argv

inputFileName = args[1]
outputFileName = args[2]
#inputFileName = "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArCaliWave_I03L_sat_merged_revised.root"
#outputFileName = "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArCaliWave_I03L_sat_merged_revised_4SCs.root"
fin = TFile(inputFileName, "read")
tin = fin.Get("CALIWAVE")
ofile = TFile(outputFileName, "RECREATE")

newtree = tin.CopyTree("channelId == 958474240 || channelId == 958490624 || channelId == 958507008 || channelId == 958523392")

ofile.cd()
newtree.Write()
ofile.Close()
