import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

#from ROOT import *
#ROOT.gROOT.LoadMacro(os.path.join(fn.MAINDIR, 'source/atlasrootstyle/AtlasStyle.C'))
#ROOT.gROOT.LoadMacro(os.path.join(fn.MAINDIR, 'source/atlasrootstyle/AtlasUtils.C'))
#SetAtlasStyle()


AveragedDigitTree = CellPropertyGeneratorModule.AveragedDigitTree(
    'LArDigits_382579_converted.root'
)
AveragedDigitTree.PulseShape()
#AveragedDigitTree.PulseTree()
#AveragedDigitTree.LSB_delayPeak()
