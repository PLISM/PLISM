void LArIDtranslatorTreeMaker() {
  TString filename_bare("LArIDtranslator_test");
  TTree* rawtree = new TTree("LIT", "Tree from LArIDtranslator");
  rawtree->ReadFile(Form("%s.txt", filename_bare.Data()), "ONL_ID/I:DET/I:SAM/I:ETA/F:PHI/F:SC_ONL_ID/I:SCETA/F:SCPHI/F");
  TTree* tree = rawtree->CopyTree("DET>=0 && SC_ONL_ID!=0");
  TFile* fout = new TFile(Form("%s.root", filename_bare.Data()), "recreate");
  tree->Write();
  fout->Close();
}
