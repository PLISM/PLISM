import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn

#target = ["ONL_ID", "SAM", "ETA", "PHI", "IETA", "IPHI", "CL", "SC_ONL_ID", "SCETA", "SCPHI", "SCIETA", "SCIPHI"]
#target = ["ONL_ID", "DET", "SAM", "ETA", "PHI", "SC_ONL_ID", "SCETA", "SCPHI"]
condition = "AC=1 or AC=-1"
#target = ["ONL_ID", "SC_ONL_ID", "CL"]
target = ["ONL_ID"]
#condition = "FTNAME like 'I12L' or FTNAME like 'I13R'"
#condition = "FTNAME like 'I01L' or FTNAME like 'I05R' or FTNAME like 'I02L' or FTNAME like 'I02R' or FTNAME like 'I03L' or FTNAME like 'I03R' or FTNAME like 'I04L' or FTNAME like 'I04R'"
#condition = "FTNAME like 'I05L' or FTNAME like 'I06L' or FTNAME like 'I06R' or FTNAME like 'I07L' or FTNAME like 'I07R' or FTNAME like 'I08L' or FTNAME like 'I08R' or FTNAME like 'I09R'"
#condition = "FTNAME like 'I01L' or FTNAME like 'I05R' or FTNAME like 'I02L' or FTNAME like 'I02R' or FTNAME like 'I03L' or FTNAME like 'I03R' or FTNAME like 'I04L' or FTNAME like 'I04R' or FTNAME like 'I05L' or FTNAME like 'I06L' or FTNAME like 'I06R' or FTNAME like 'I07L' or FTNAME like 'I07R' or FTNAME like 'I08L' or FTNAME like 'I08R' or FTNAME like 'I09R'"
#output_file_name = "LArIDtranslator_test.txt"
output_file_name = "LIT_onlid.txt"
overlap_canceller = True

fn.MakeListFromLArIDtranslator(target, condition, output_file_name, overlap_canceller)
