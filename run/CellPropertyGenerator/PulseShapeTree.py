import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'LArCaliWave_pulse.root'
)
PulseShapeTree.PedAlreadySubtracted = True
PulseShapeTree.GenerateCellProperty(
    'LArCaliWave_LSBDelayPeak.root',
    os.path.join(fn.CELLPROPDIR, 'CellProperty_test.root'),
    0
)
