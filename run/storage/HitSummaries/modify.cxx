

void modify()
{
  gInterpreter->GenerateDictionary("std::vector<std::vector<float> >",
  "vector");
  // Get old file, old tree and set top branch address
  TFile *oldfile = new TFile("HitSummaryLowPt.root");
  TTree *oldtree = (TTree *)oldfile->Get("HitSummary");
  Long64_t nentries = oldtree->GetEntries();
  Int_t channelId;
  oldtree->SetBranchAddress("channelId", &channelId);
  // Create a new file + a clone of old tree in new file
  TFile *newfile =
    new TFile("HitSummary_LowPt_968435712.root", "recreate");
  TTree *newtree = oldtree->CloneTree(0);
  for (Long64_t i = 0; i < nentries; i++) {
    oldtree->GetEntry(i);

    if (channelId == 968435712) newtree->Fill();  // filtering unwanted entries.
  }
  newtree->AutoSave();
}

