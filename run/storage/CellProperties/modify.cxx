

void modify()
{
  gInterpreter->GenerateDictionary("std::vector<std::deque<double> >",
				   "vector;deque");
  // Get old file, old tree and set top branch address
  TFile *oldfile = new TFile("CellProperty_968435712.root");
  TTree *oldtree = (TTree *)oldfile->Get("CellProperty");
  Long64_t nentries = oldtree->GetEntries();
  Float_t eta;
  Float_t phi;
  Int_t layer;
  oldtree->SetBranchAddress("eta", &eta);
  oldtree->SetBranchAddress("phi", &phi);
  oldtree->SetBranchAddress("layer", &layer);
  // Create a new file + a clone of old tree in new file
  TFile *newfile =
    new TFile("CellProperty_968435712_modified.root", "recreate");
  TTree *newtree = oldtree->CloneTree(0);
  for (Long64_t i = 0; i < nentries; i++) {
    oldtree->GetEntry(i);
    
    eta = 0.0125;
    phi = -1.7211;
    layer = 2;
    
    newtree->Fill();  // filtering unwanted entries.
  }
  newtree->AutoSave();
}

