void update() {
  TFile *f = new TFile("CellProperty_968435712.root", "update");
  TTree *T = (TTree*)f->Get("CellProperty");

  Float_t eta;
  Float_t phi;
  Int_t layer;
  TBranch *beta = T->Branch("eta",&eta);
  TBranch *bphi = T->Branch("phi",&phi);
  TBranch *blayer = T->Branch("layer",&layer);
  T->SetBranchAddress("eta", &eta);
  T->SetBranchAddress("phi", &phi);
  T->SetBranchAddress("layer", &layer);
  
  Long64_t nentries = T->GetEntries();
  for (Long64_t i = 0; i < nentries; i++) {
    T->GetEntry(i);
    
    eta = 0.0125;
    phi = -1.7211;
    layer = 2;
    beta->Fill();
    bphi->Fill();
    blayer->Fill();
  }
  T->Print();
  T->Write();
  delete f;
}
