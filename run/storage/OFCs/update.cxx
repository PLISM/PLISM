void update() {
  TFile *f = new TFile("OFC_968435712_athena_mu80.root", "update");
  TTree *T = (TTree*)f->Get("OFC");

  Int_t OFBCshift;
  TBranch *bOFBCshift = T->Branch("OFBCshift",&OFBCshift);
  T->SetBranchAddress("OFBCshift", &OFBCshift);
  
  Long64_t nentries = T->GetEntries();
  for (Long64_t i = 0; i < nentries; i++) {
    T->GetEntry(i);

    OFBCshift = 0;
    bOFBCshift->Fill();
  }
  T->Print();
  T->Write();
  delete f;
}
