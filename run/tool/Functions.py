import os
import subprocess

MAINDIR       = os.environ['LAR_SIMLATOR_MAIN_DIR']
RUNDIR        = os.path.join(MAINDIR,    'run')
STORAGEDIR    = os.path.join(RUNDIR,     'storage')
HITSDIR       = os.path.join(STORAGEDIR, 'HitFiles')
HITDIR        = os.path.join(STORAGEDIR, 'HitSummaries')
OFCDIR        = os.path.join(STORAGEDIR, 'OFCs')
CELLPROPDIR   = os.path.join(STORAGEDIR, 'CellProperties')
DIGITDIR      = os.path.join(STORAGEDIR, 'DigitSequences')
RECDIR        = os.path.join(STORAGEDIR, 'ReconstructedSequences')
PULSESHAPEDIR = os.path.join(STORAGEDIR, 'PulseShapes')
NOISEDIR      = os.path.join(STORAGEDIR, 'Noise')

def execute(cmd):
    print(cmd)
    subprocess.call(cmd, shell = True)

import sqlite3
def MakeListFromLArIDtranslator(target, condition, output_file_name, overlap_canceller, LIDTpath = os.environ['LITDB']):
    #connection = sqlite3.connect('/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArId.db')
    connection = sqlite3.connect(LIDTpath)
    cursor = connection.cursor()
    line_list = []
    NbadLines = 0
    for row in cursor.execute("select " + ",".join(target) + " from LARID where " + condition):
        line = []
        for column in row: line.append(str(column))
        bad = False
        for i in line:
            if not i:
                #print('Bad line')
                bad = True
                NbadLines += 1
        if bad: continue
        line_list.append(" ".join(line))
    if overlap_canceller: line_list = sorted(set(line_list), key=line_list.index)
    with open(output_file_name, mode='w') as f:
        for line in line_list: f.write(line + "\n")
    connection.close()
    print('There are ' + str(NbadLines) + ' records with empty attributes in the range of LAr ID translator. These usually correspond to channels that are unconnected by default.')

