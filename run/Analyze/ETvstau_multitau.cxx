#include "VecExtractor.h"
#include "./AtlasStyle/AtlasStyle.C"
#include "./AtlasStyle/AtlasUtils.C"
#include "./AtlasStyle/AtlasLabels.C"

#define LEFT -25
#define RIGHT 25
#define TOP 55
#define BOTTOM -15

void ETvstau_multitau() {
  std::vector<double> ET0 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau0.root", "ET_of");
  std::vector<double> ET6 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau6.root", "ET_of");
  std::vector<double> ET12 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau12.root", "ET_of");
  std::vector<double> tau0 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau0.root", "tau_of");
  std::vector<double> tau6 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau6.root", "tau_of");
  std::vector<double> tau12 = getVec("reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau12.root", "tau_of");

  TFile *f;
  f = new TFile("haveAlook.root", "recreate");
  f->cd();

  // Set ATLAS Style
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  TCanvas *c = new TCanvas("c", "c",1200, 800);
  c->Clear();
  c->Update();
  SetAtlasStyle();
  c->SetFrameFillColor(kWhite);

  TGraph *g0 = new TGraph();
  g0->SetTitle(";calculated #tau [ns];calculated E_{T} [GeV]");
  g0->SetLineWidth(2);
  g0->SetMarkerSize(0.2);
  g0->SetMarkerColor(kBlue);
  g0->SetMarkerStyle(20);
  g0->SetLineColor(kBlue);
  g0->SetLineWidth(2);

  TGraph *g6 = new TGraph();
  g6->SetTitle(";calculated #tau [ns];calculated E_{T} [GeV]");
  g6->SetLineWidth(2);
  g6->SetMarkerSize(0.2);
  g6->SetMarkerColor(kGreen+2);
  g6->SetMarkerStyle(20);
  g6->SetLineColor(kGreen+2);
  g6->SetLineWidth(2);

  TGraph *g12 = new TGraph();
  g12->SetTitle(";calculated #tau [ns];calculated E_{T} [GeV]");
  g12->SetLineWidth(2);
  g12->SetMarkerSize(0.2);
  g12->SetMarkerColor(kRed);
  g12->SetMarkerStyle(20);
  g12->SetLineColor(kRed);
  g12->SetLineWidth(2);

  const int size = ET0.size();
  for (int i = 0; i < size; ++i) {
    g0->SetPoint(i, tau0[i], ET0[i]/1000.);
    g6->SetPoint(i, tau6[i], ET6[i]/1000.);
    g12->SetPoint(i, tau12[i], ET12[i]/1000.);
  }

  c->cd();
  g0->GetXaxis()->SetLimits(LEFT, RIGHT);
  g0->SetMaximum(TOP);
  g0->SetMinimum(BOTTOM);
  g0->GetXaxis()->SetTitleOffset(0.9);
  g0->GetYaxis()->SetTitleOffset(0.9);
  //g0->GetXaxis()->SetNdivisions(505);
  g0->Draw("ap");
  g6->Draw("p");
  g12->Draw("p");

  TLatex *latex1 = new TLatex();
  latex1->SetTextSize(0.04);
  latex1->DrawLatexNDC(0.2, 0.55, "Middle layer");
  latex1->DrawLatexNDC(0.2, 0.5, "#eta = 0.0125, #phi = -1.7211");
  latex1->DrawLatexNDC(0.2, 0.45, "#mu = 80, true E_{T} = 50 GeV");

  TLegend *leg = new TLegend(0.2, 0.33, 0.4, 0.43, "");
  leg->SetName("legend");
  leg->SetBorderSize(0);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->SetTextSize(0.035);
  leg->AddEntry(g0, "true #tau = 0 ns", "pl");
  leg->AddEntry(g6, "true #tau = 6 ns", "pl");
  leg->AddEntry(g12, "true #tau = 12 ns", "pl");
  leg->Draw();
  
  c->SaveAs("ETvstau_multitau.pdf");
  c->Write();
  c->Close();

  f->Close();
}
