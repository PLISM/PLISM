#include "digitSequence.C"

#define PERIOD 128
#define LEFT 0
#define RIGHT (PERIOD*25)
#define TOP 340
#define BOTTOM 180

double getRMS(std::deque<int>& digits, int i, int numMean) {
  double mean = 0.;
  for (int j = 0; j < numMean; ++j) mean += digits[PERIOD * j + i];
  mean /= numMean;
  double rms = 0.;
  for (int j = 0; j < numMean; ++j) {
    const double d = digits[PERIOD * j + i] - mean;
    rms += (d * d);
  }
  rms = TMath::Sqrt(rms / numMean);
  
  return rms;
}

void BaselineShiftRMS() {

  const double LSB = 273.859 / 1.05225;
  const double NOISE = 0.6357581 * 273.859;
  
  digitSequence obj20("digitSequence_968435712_mu20_50000000BC_8b4e_pileup.root");
  std::deque<int> digits20 = obj20.getDigits();
  digitSequence obj50("digitSequence_968435712_mu50_50000000BC_8b4e_pileup.root");
  std::deque<int> digits50 = obj50.getDigits();
  digitSequence obj80("digitSequence_968435712_mu80_50000000BC_8b4e_pileup.root");
  std::deque<int> digits80 = obj80.getDigits();
  
  TFile *f;
  f = new TFile("haveAlook.root", "recreate");
  f->cd();
  
  // Set ATLAS Style
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  TCanvas *c = new TCanvas("c", "c",1200, 800);
  c->Clear();
  c->Update();
  SetAtlasStyle();
  c->SetFrameFillColor(kWhite);

  TGraphErrors *g20 = new TGraphErrors();
  g20->SetTitle(";time [ns];RMS of baseline [MeV]");
  g20->SetLineWidth(2);
  g20->SetMarkerSize(1);
  g20->SetMarkerColor(kBlue);
  g20->SetMarkerStyle(20);
  g20->SetLineColor(kBlue);
  g20->SetLineWidth(2);

  TGraphErrors *g50 = new TGraphErrors();
  g50->SetTitle(";time [ns];RMS of baseline [MeV]");
  g50->SetLineWidth(2);
  g50->SetMarkerSize(1);
  g50->SetMarkerColor(kGreen+2);
  g50->SetMarkerStyle(20);
  g50->SetLineColor(kGreen+2);
  g50->SetLineWidth(2);

  TGraphErrors *g80 = new TGraphErrors();
  g80->SetTitle(";time [ns];RMS of baseline [MeV]");
  g80->SetLineWidth(2);
  g80->SetMarkerSize(1);
  g80->SetMarkerColor(kRed);
  g80->SetMarkerStyle(20);
  g80->SetLineColor(kRed);
  g80->SetLineWidth(2);

  const int numMean = digits20.size() / PERIOD;
  for (int i = 0; i < PERIOD; ++i) {
    g20->SetPoint(i, i * 25., getRMS(digits20, i, numMean) * LSB);
    g50->SetPoint(i, i * 25., getRMS(digits50, i, numMean) * LSB);
    g80->SetPoint(i, i * 25., getRMS(digits80, i, numMean) * LSB);
    g20->SetPointError(i, 0, 0);
    g50->SetPointError(i, 0, 0);
    g80->SetPointError(i, 0, 0);
  }
  /*
  for (int i = 0; i < PERIOD; ++i) {
    double mean = 0.;
    for (int j = 0; j < numMean; ++j) mean += (digits50[PERIOD * j + i] + 0.5);
    mean /= numMean;
    double rms = 0.;
    for (int j = 0; j < numMean; ++j) rms += (digits50[PERIOD * j + i] + 0.5) * (digits50[PERIOD * j + i] + 0.5);
    rms = TMath::Sqrt(rms / numMean - mean * mean);
    g50->SetPoint(i, i * 25., mean);
    g50->SetPointError(i, 0, 0);
  }
  */
  c->cd();
  g80->GetXaxis()->SetLimits(LEFT, RIGHT);
  g80->SetMaximum(TOP);
  g80->SetMinimum(BOTTOM);
  g80->GetXaxis()->SetTitleOffset(0.9);
  g80->GetYaxis()->SetTitleOffset(1);
  g80->GetXaxis()->SetNdivisions(505);
  g80->Draw("al");
  g50->Draw("l");
  g20->Draw("l");

  TLatex *latex1 = new TLatex();
  latex1->SetTextSize(0.04);
  latex1->DrawLatexNDC(0.17, 0.75, "Middle layer, #eta = 0.0125, #phi = -1.7211");
  latex1->DrawLatexNDC(0.17, 0.8, Form("%d digits are used to take mean at each BC", numMean));

  TLegend *leg = new TLegend(0.17, 0.63, 0.37, 0.73, "");
  leg->SetName("legend");
  leg->SetBorderSize(0);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->SetTextSize(0.035);
  leg->AddEntry(g80, "#mu = 80", "l");
  leg->AddEntry(g50, "#mu = 50", "l");
  leg->AddEntry(g20, "#mu = 20", "l");
  leg->Draw();

  TLine *line = new TLine(LEFT, NOISE, RIGHT, NOISE);
  line->SetLineColor(12);
  line->SetLineWidth(1);
  line->SetLineStyle(2);
  //line->Draw();

  c->SaveAs("baselineRMS_8b4e.pdf");
  c->Write();
  c->Close();

  f->Close();
}

