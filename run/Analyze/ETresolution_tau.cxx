#define ETresolution_tau_cxx
#include "ETresolution_tau.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "./AtlasStyle/AtlasStyle.C"
#include "./AtlasStyle/AtlasUtils.C"
#include "./AtlasStyle/AtlasLabels.C"

//#define LEFT 0
//#define RIGHT 10000
#define BCINTERVAL 50
#define NBCS 50000000

void ETresolution_tau::Loop()
{
  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<1;jentry++) {
    //for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    TFile *f;
    f = new TFile("haveAlook.root", "recreate");
    f->cd();
    
    // Set ATLAS Style
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    //TCanvas *c = new TCanvas("c", "",1200, 800);
    TCanvas *c = new TCanvas("c", "");
    c->Clear();
    c->Update();
    SetAtlasStyle();
    c->SetFrameFillColor(kWhite);

    // 2D hist gradation
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t Red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t Green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t Blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
    gStyle->SetNumberContours(NCont);

    TH2D* h = new TH2D("hist", "hist", 200, -24., 24., 100, -0.3, 0.1);
    h->SetXTitle("true #tau [ns]");
    h->SetYTitle("(E_{T}^{out} - true E_{T}) / true E_{T}");
    h->SetZTitle("Entries");

    for (int i = 0; i < NBCS; i += BCINTERVAL) {
      if (!(i % 1000000)) std::cout << i/1000000 << "M BCs processed\n";
      const double truetautmp = tau_true->at(i);
      const double trueETtmp = ET_true->at(i);
      const double ofETtmp = ET_of->at(i);
      h->Fill(truetautmp, (ofETtmp - trueETtmp) / trueETtmp);
    }
    h->SetStats(0);
    h->Draw("colz");
    
    gPad->SetTopMargin(0.2);
    gPad->SetRightMargin(0.2);
    gPad->SetLeftMargin(0.2);
    gPad->SetBottomMargin(0.2);
    h->SetTitleOffset(1);
    h->GetYaxis()->SetTitleOffset(1.);
    h->GetZaxis()->SetTitleOffset(1.2);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetNdivisions(505);

    TLatex *latex1 = new TLatex();
    latex1->SetTextSize(0.05);
    latex1->DrawLatexNDC(0.45, 0.25, "true E_{T} = 25 GeV");

    c->SaveAs("ETresolution_tau.pdf");
    c->Write();
    c->Close();
    
    f->Close();
  }
  
}

