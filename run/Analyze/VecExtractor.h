#ifndef VecExtractor_h
#define VecExtractor_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "deque"


std::unordered_map<unsigned, std::vector<double>> getVec(const char* ifn, const char* branchName) {
  auto file = new TFile(ifn);
  TTreeReader reader("reconstructed", file);
  TTreeReaderValue<unsigned> channelId(reader, "channelId");
  TTreeReaderValue<std::vector<double>> branch(reader, branchName);
  
  std::unordered_map<unsigned, std::vector<double>> m;

  while (reader.Next()) {
    m[*channelId] = std::move(*branch);
  }
  return m;
}
#endif // #ifdef VecExtractor_cxx
