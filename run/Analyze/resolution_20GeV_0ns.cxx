#define resolution_20GeV_0ns_cxx
#include "resolution_20GeV_0ns.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

#define BCINTERVAL 50
#define NBCS 500000

void resolution_20GeV_0ns::Loop()
{
  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();

  const int NEVENTS = 10000;
  
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<1;jentry++) {
    //for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    double ETresmean = 0.;
    double taudiffmean = 0.;
    for (int i = 0; i < NBCS; i += BCINTERVAL) {
      if (!(i % 1000000)) std::cout << i/1000000 << "M BCs processed\n";
      const double trueETtmp = ET_true->at(i);
      const double ofETtmp = ET_of->at(i);
      const double truetautmp = tau_true->at(i);
      const double oftautmp = tau_of->at(i);
      ETresmean += (ofETtmp - trueETtmp) / trueETtmp;
      taudiffmean += oftautmp - truetautmp;
    }
    ETresmean /= NEVENTS;
    taudiffmean /= NEVENTS;

    double ETresrms = 0.;
    double taudiffrms = 0.;
    for (int i = 0; i < NBCS; i += BCINTERVAL) {
      const double trueETtmp = ET_true->at(i);
      const double ofETtmp = ET_of->at(i);
      const double truetautmp = tau_true->at(i);
      const double oftautmp = tau_of->at(i);
      const double dET = (ofETtmp - trueETtmp) / trueETtmp - ETresmean;
      const double dtau = oftautmp - truetautmp - taudiffmean;
      ETresrms += (dET * dET);
      taudiffrms += (dtau * dtau);
    }
    ETresrms = TMath::Sqrt(ETresrms / NEVENTS);
    taudiffrms = TMath::Sqrt(taudiffrms / NEVENTS);

    std::cout << "ET resolution: mean = " << ETresmean << " , RMS = " << ETresrms << "\n";
    std::cout << "tau resolution: mean = " << taudiffmean << " , RMS = " << taudiffrms << "\n";
  }
  
}

