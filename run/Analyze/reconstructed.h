#ifndef reconstructed_h
#define reconstructed_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "deque"

class reconstructed {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  UInt_t          channelId;
  Float_t          LSB;
  
  vector<double>   *SignalTrueEt;
  vector<double>   *SignalTrueTau;
  deque<int>      *Digit;
  vector<double>   *RecEt;
  vector<double>   *RecTau;

  // List of branches
  TBranch        *b_channelId;   //!
  TBranch        *b_LSB;   //!
  TBranch        *b_SignalTrueEt;   //!
  TBranch        *b_SignalTrueTau;   //!
  TBranch        *b_Digit;   //!
  TBranch        *b_RecEt;   //!
  TBranch        *b_RecTau;   //!

  reconstructed(TTree *tree=0);
  virtual ~reconstructed();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef reconstructed_cxx
reconstructed::reconstructed(TTree *tree) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../storage/ReconstructedSequences/reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau-12.root");
    if (!f || !f->IsOpen()) {
      f = new TFile("../storage/ReconstructedSequences/reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau-12.root");
    }
    f->GetObject("reconstructed",tree);

  }
  Init(tree);
}

reconstructed::~reconstructed()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t reconstructed::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t reconstructed::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void reconstructed::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  SignalTrueEt = 0;
  SignalTrueTau = 0;
  Digit = 0;
  RecEt = 0;
  RecTau = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
  fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
  fChain->SetBranchAddress("SignalTrueEt", &SignalTrueEt, &b_SignalTrueEt);
  fChain->SetBranchAddress("SignalTrueTau", &SignalTrueTau, &b_SignalTrueTau);
  fChain->SetBranchAddress("Digit", &Digit, &b_Digit);
  fChain->SetBranchAddress("RecEt", &RecEt, &b_RecEt);
  fChain->SetBranchAddress("RecTau", &RecTau, &b_RecTau);
  Notify();
}

Bool_t reconstructed::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void reconstructed::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t reconstructed::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif // #ifdef reconstructed_cxx
