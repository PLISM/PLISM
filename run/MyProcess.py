import sys
import os
sys.path[0] = os.path.abspath("tool")

import HitSummarizerModule
import DigitizerModule
import ReconstructorModule
import Functions as fn

#########################
EtMin = 50000  # MeV
EtMax = 50000  # MeV
TauMin = 0  # ns
TauMax = 0  # ns
NBC = 100000
mu = 80
phase = 0
TrainPattern = [0, 1]
SignalPattern = [1, 49]
#########################

SimulationConfiguration = 'mu' + str(mu) + '_' + str(NBC) + 'BC' + '_' + 'Train' + str(TrainPattern) + '_' + 'Signal' + \
    str(SignalPattern) + '_' + 'Et' + str(EtMin) + '-' + \
    str(EtMax) + '_' + 'Tau' + str(TauMin) + '-' + str(TauMax)

SignalHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummaryTest.root')
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.NEvent = 100000000
# Uniform distribution
HitSummarizer.EtMin = EtMin  # MeV
HitSummarizer.EtMax = EtMax  # MeV
HitSummarizer.TauMin = TauMin  # ns
HitSummarizer.TauMax = TauMax  # ns
HitSummarizer.makeSignal()

CellPropertyFileName = os.path.join(
    fn.CELLPROPDIR, 'CellProperty_968435712.root')
DigitSequenceFileName = os.path.join(
    fn.DIGITDIR, 'digitSequence' + '_' + SimulationConfiguration + '.root')
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = NBC
Digitizer.mu = mu
Digitizer.phase = phase
Digitizer.TrainPattern = TrainPattern
Digitizer.SignalPattern = SignalPattern
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = os.path.join(
    fn.HITDIR, 'HitSummary_iguchiLow_968435712.root')
Digitizer.HighPtPileupHitSummaryFile = os.path.join(
    fn.HITDIR, 'HitSummary_iguchiHigh_968435712.root')
Digitizer.sequence()
print(fn.DIGITDIR + '/digitSequence' + '_' + SimulationConfiguration + '.root')

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = os.path.join(
    fn.RECDIR, 'reconstructed' + '_' 'athena' + '_' + SimulationConfiguration + '.root')
Reconstructor.OFCFileName = os.path.join(
    fn.OFCDIR, 'OFC_968435712_athena_mu80.root')
Reconstructor.reconstruct()
print(fn.RECDIR + '/reconstructed' + '_' 'athena' +
      '_' + SimulationConfiguration + '.root')
