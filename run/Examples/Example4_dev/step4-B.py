import ROOT as root
import atlasplots as aplt
import math
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

fin0 = root.TFile('files/gFunctionOFC_cali.root', "read")
fin1 = root.TFile('files/gFunctionOFC_phys.root', "read")
tin0 = fin0.Get("OFC")
tin1 = fin1.Get("OFC")
fout = root.TFile('canvas.root', "recreate")

gCali = root.TGraph()
gPhys = root.TGraph()

nEntries = tin0.GetEntries()
for j in range(nEntries):
    tin0.GetEntry(j)
    tin1.GetEntry(j)

    if tin0.channelId != tin1.channelId:
        print("ERROR: Inconsistent order of channelId between cali and phys trees")
        sys.exit(1)

    for i in range(len(tin0.NgFunctionOFCa[0])):
        signalIndex = gCali.GetN()
        gCali.SetPoint(signalIndex, 25.*i/tin0.sampperbc, tin0.NgFunctionOFCa[0][i])
        gPhys.SetPoint(signalIndex, 25.*i/tin1.sampperbc, tin1.NgFunctionOFCa[0][i])

    fig, ax = aplt.subplots(1, 1)
    ax.plot(gCali, options="pl", linewidth = 3, linecolor=root.kGreen, markersize=0.5, markercolor=root.kGreen, label="CaliWave", labelfmt="pl")
    ax.plot(gPhys, options="pl", linewidth = 2, linecolor=root.kBlue-3, markersize=0.3, markercolor=root.kBlue-3, label="PhysWave", labelfmt="pl")

    ax.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0,textcolor=root.kBlack, textsize=20)
    ax.set_xlabel("Time [ns]")
    ax.set_ylabel("Amplitude")
    ax.add_margins(bottom=0.1, top=0.2)

    ax.text(0.6, 0.80, "channelId = " + str(tin0.channelId),size=20, align=13)
    ax.text(0.6, 0.77, "#eta="+str(round(tin0.eta, 4))+ " #phi= " +str(round(tin0.phi, 4)) ,size=20, align=13)
    ax.text(0.6, 0.71, "amp:cali=" + str(round(tin0.peakEt[0], 5)) + " phys=" + str(round(tin1.peakEt[0], 5)),size=20, align=13)
    ax.text(0.6, 0.68, "Tau:cali="+str(round(tin0.peakTau[0], 5)) + " phys=" + str(round(tin1.peakTau[0], 5)), size=20, align=13)
    if tin0.layer == 0:
        L = "Presampler"
    elif tin0.layer == 1:
        L = "Front layer"
    elif tin0.layer == 2:
        L = "Middle layer"
    elif tin0.layer == 3:
        L = "Back layer"

    ax.text(0.6, 0.74, L, size=20, align=13)

    fig.canvas.SetName(str(tin0.channelId))
    fig.canvas.Write()
