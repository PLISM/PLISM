import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import CellPropertyGeneratorModule
import OFCCalibratorModule

OFCCaliTree = OFCCalibratorModule.OFCCaliTree('files/OFCCali_test.root')
OFCCaliTree.NthPhase = 0
OFCCaliTree.OFBCshift = 0

#make gFunction by CaliWave times OFC by cali
OFCCaliTree.OutputFileName = 'files/gFunctionOFC_cali.root'
OFCCaliTree.CellPropertyFileName = 'files/CellProperty_test.root'
OFCCaliTree.MakegFuncOFCTree()

#make gFunction by PhysWave times OFC by cali
OFCCaliTree.OutputFileName = 'files/gFunctionOFC_phys.root'
OFCCaliTree.CellPropertyFileName = 'files/cellprop_PhysWave_test.root'
OFCCaliTree.MakegFuncOFCTree()

