import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import atlasplots as aplt
import math
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

fin = root.TFile('files/reconstructed.root', "read")
tin = fin.Get("reconstructed")
fout = root.TFile('canvas.root', "recreate")

h = root.TH1D("", "", 25, -0.02, 0.02)

nEntries = tin.GetEntries()
for j in range(nEntries):
    tin.GetEntry(j)
    for i in range(len(tin.RecEt)):
        if tin.SignalTrueEt[i]:
            h.Fill((tin.RecEt[i] - tin.SignalTrueEt[i]) / tin.SignalTrueEt[i])

    fig, ax = aplt.subplots(1, 1)
    fig.canvas.SetCanvasSize(800, 600)
    ax.plot(h, linecolor=root.kBlue)
    #ax.plot(gSel, options="p", markercolor=root.kRed, label="Selected BC", labelfmt="p")
    ax.set_xlabel("(E_{T}^{reco} - E_{T}^{true}) / E_{T}^{true}")
    ax.set_ylabel("Events")
    ax.add_margins(top=0.05)

    fig.canvas.SetName(str(tin.channelId))
    fig.canvas.Write()
    
