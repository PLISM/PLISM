import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import atlasplots as aplt
import math
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

fin = root.TFile('files/reconstructed.root', "read")
tin = fin.Get("reconstructed")
fout = root.TFile('canvas.root', "recreate")

gTruth = root.TGraph()
gADC = root.TGraph()
gRec = root.TGraph()
gSel = root.TGraph()

nEntries = tin.GetEntries()
for j in range(nEntries):
    tin.GetEntry(j)
    for i in range(len(tin.RecEt)):
        gTruth.SetPoint(i, i * 25., tin.SignalTrueEt[i])
        gADC.SetPoint(i, i * 25., tin.Digit[i] * tin.LSB)
        gRec.SetPoint(i, i * 25., tin.RecEt[i])
        selected = False
        if tin.RecEt[i] < 10000.:
            selected = abs(tin.RecTau[i]) < 8.
        else:
            selected = (tin.RecTau[i] > -8. and tin.RecTau[i] < 16.)
        if selected:
            gSel.SetPoint(gSel.GetN(), i * 25, tin.RecEt[i])

    fig, ax = aplt.subplots(1, 1)
    fig.canvas.SetCanvasSize(1000, 500)
    ax.plot(gTruth, options="pl", linecolor=root.kBlack, markercolor=root.kBlack, label="True signal", labelfmt="pl")
    ax.plot(gADC, options="pl", linecolor=root.kBlue, markercolor=root.kBlue, label="ADC #times F_{ADC#rightarrowE_{T}}", labelfmt="pl")
    ax.plot(gRec, options="pl", linecolor=root.kGreen, markercolor=root.kGreen, label="Reconstructed E_{T}", labelfmt="pl")
    ax.plot(gSel, options="p", markercolor=root.kRed, label="Selected BC", labelfmt="p")
    ax.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
    ax.set_xlabel("Time [ns]")
    ax.set_ylabel("Amplitude [MeV_{T}]")
    ax.set_xlim(25. * 90, 25. * 240)
    ax.add_margins(top=0.1, bottom=0.05)

    fig.canvas.SetName(str(tin.channelId))
    fig.canvas.Write()
    
