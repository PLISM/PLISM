import sys,os
import ROOT
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
ROOT.gROOT.SetBatch(True)
import HitSummarizerModule

HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = 'files/HitSummaryTest.root'
HitSummarizer.CellPropertyFileName = 'files/CellProperty_968435712.root'
HitSummarizer.NEvent = 20000
# Uniform distribution
HitSummarizer.EtMin = 30000 #MeV
HitSummarizer.EtMax = 50000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

