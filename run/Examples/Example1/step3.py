import sys,os
import ROOT
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
ROOT.gROOT.SetBatch(True)
import DigitizerModule

Digitizer = DigitizerModule.Digitizer()
Digitizer.CellPropertyFileName = 'files/CellProperty_968435712.root'
Digitizer.NBC = 10000
Digitizer.mu = 80
Digitizer.phase = 0
Digitizer.TrainPattern = [1, 0]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = 'files/digitSequence.root'
Digitizer.SignalHitSummaryFile = 'files/HitSummaryTest.root'
Digitizer.LowPtPileupHitSummaryFile = 'files/HitSummary_LowPt_968435712.root'
Digitizer.HighPtPileupHitSummaryFile = 'files/HitSummary_HighPt_968435712.root'
Digitizer.OutputBranch_PileupTrueEt = False
Digitizer.OutputBranch_PileupTrueTau = False
Digitizer.OutputBranch_PileupTrueEtSum = False
Digitizer.sequence()
