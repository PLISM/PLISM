import ROOT as root
import atlasplots as aplt
import math, sys
import numpy as np
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

fin = root.TFile('files/cellprop_LArCaliWave_I03L_sat_merged_revised_4SCs.root', "read")
tin = fin.Get("CellProperty")
fout = root.TFile('canvas.root', "recreate")

g = {}
colors = [root.kOrange+7, root.kTeal-6, root.kAzure-6, root.kOrange-3, root.kAzure+8, root.kRed+3, root.kRed-9, root.kMagenta+2]
eta = {}
phi = {}
layer = {}
inputET = {}
pedADC = {}

nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)

    if tin.layer < 0 or tin.layer > 4:
        print("ERROR: Unknown layer")
        sys.exit()

    if not (tin.channelId in g.keys()):
        eta[tin.channelId] = tin.eta
        phi[tin.channelId] = tin.phi
        layer[tin.channelId] = tin.layer
        inputET[tin.channelId] = {}
    inputET[tin.channelId][tin.iCLset] = [et for et in tin.inputET]
    
    size = len(tin.inputET)
    if tin.channelId in g.keys():
        if tin.iCLset in g[tin.channelId].keys():
            g[tin.channelId][tin.iCLset] = [root.TGraph() for i in range(size)]
        else:
            print("WARNING: There are entries which have the same pair of channelId and iCLset.")
            continue
    else:
        g[tin.channelId] = {tin.iCLset:[root.TGraph() for i in range(size)]}

    gtmp = g[tin.channelId][tin.iCLset]
    for i in range(size):
        for j in range(len(tin.gFunction[i])):
            gtmp[i].SetPoint(j, j * 25./tin.sampperbc, tin.gFunction[i][j] + tin.pedADC)

labels = ["Presampler", "Front layer", "Middle layer", "Back layer"]

for ch, dict1 in g.items():
    for cl, graphs in dict1.items():
        fig, ax = aplt.subplots(1, 1)
        for i in range(len(graphs)):
            ax.plot(graphs[i], options="p", markercolor=colors[i % len(colors)], markersize=0.2, markerstyle=20)

        ax.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
        ax.set_xlabel("Time [ns]")
        ax.set_ylabel("Amplitude [ADC]")
        ax.set_xlim(0, 800)
        ax.add_margins(top=0.05, bottom=0.05)
        ax.text(0.2, 0.86, labels[layer[ch]], size=22, align=13)
        ax.text(0.2, 0.86, "#eta = " + '{:.4f}'.format(eta[ch]) + ", #phi = " + '{:.4f}'.format(phi[ch]), size=22, align=13)
        inputETs = inputET[ch][cl]
        nhalf = int(len(inputETs)/2)
        for i in range(nhalf):
            etstr = '{:.0f}'.format(inputETs[i] * 1e-3)
            ax.text(0.6, 0.88 - 0.08*(i/2), etstr + " GeV", size=22, align=13, color=colors[i % len(colors)])
        for i in range(nhalf, len(inputETs)):
            etstr = '{:.0f}'.format(inputETs[i] * 1e-3)
            ax.text(0.8, 0.88 - 0.08*((i-nhalf)/2), etstr + " GeV", size=22, align=13, color=colors[i % len(colors)])
        fig.canvas.SetName("_".join([str(ch), str(cl)]))
        fig.canvas.Write()
