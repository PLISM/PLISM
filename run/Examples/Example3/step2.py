import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import HitSummarizerModule

HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = 'files/HitSummaryTest.root'
HitSummarizer.CellPropertyFileName = 'files/cellprop_LArCaliWave_I03L_sat_merged_revised_4SCs.root'
HitSummarizer.NEvent = 20000
# Uniform distribution
HitSummarizer.EtMin = 0 #MeV
HitSummarizer.EtMax = 3000000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()
