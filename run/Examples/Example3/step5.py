import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import ReconstructorModule

Reconstructor = ReconstructorModule.Reconstructor('files/digitSequence_OFCtest.root')
Reconstructor.OutputFileName = 'files/reconstructed_Sequence_AllEt_Tau0_phase0.root'
Reconstructor.OFCFileName = 'files/OFC_test_cali_phase0.root'
Reconstructor.reconstruct()
