import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import DigitizerModule

Digitizer = DigitizerModule.Digitizer()
Digitizer.CellPropertyFileName = 'files/cellprop_LArCaliWave_I03L_sat_merged_revised_4SCs.root'
Digitizer.Interpolator = "barycentric_rational"
Digitizer.NBC = 10000
Digitizer.mu = 0
Digitizer.phase = 0
Digitizer.TrainPattern = [0, 1]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = 'files/digitSequence_OFCtest.root'
Digitizer.SignalHitSummaryFile = 'files/HitSummaryTest.root'
Digitizer.sequence()
