import ROOT as root
import atlasplots as aplt
import math
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

ofcfin = root.TFile('files/OFC_test_cali_phase0.root', "read")
ofctin = ofcfin.Get("OFC")
nEntries = ofctin.GetEntries()
OFCs = {}
for j in range(nEntries):
    ofctin.GetEntry(j)
    a = [i for i in ofctin.a]
    b = [i for i in ofctin.b]
    OFCs[ofctin.channelId] = (a, b)

fin = root.TFile('files/cellprop_LArCaliWave_I03L_sat_merged_revised_4SCs.root', "read")
tin = fin.Get("CellProperty")
fout = root.TFile('canvas.root', "recreate")

nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    
    if not (tin.channelId in OFCs.keys()):
        continue

    a = OFCs[tin.channelId][0]
    b = OFCs[tin.channelId][1]

    g0 = root.TGraph()
    gm1 = root.TGraph()
    gp1 = root.TGraph()
    gm2 = root.TGraph()
    rec0 = []
    recm1 = []
    recp1 = []
    recm2 = []
    for pulse in tin.gFunction:
        Et = sum([a[k]*pulse[24+24*k] for k in [0, 1, 2, 3]])
        Ettau = sum([b[k]*pulse[24+24*k] for k in [0, 1, 2, 3]])
        g0.SetPoint(g0.GetN(), Et*1e-3, Ettau*1e-3)
        rec0.append([Et*1e-3, Ettau*1e-3])

        Et = sum([a[k]*pulse[24*k] for k in [0, 1, 2, 3]])
        Ettau = sum([b[k]*pulse[24*k] for k in [0, 1, 2, 3]])
        gm1.SetPoint(gm1.GetN(), Et*1e-3, Ettau*1e-3)
        recm1.append([Et*1e-3, Ettau*1e-3])

        Et = sum([a[k]*pulse[-24+24*k] for k in [1, 2, 3]])
        Ettau = sum([b[k]*pulse[-24+24*k] for k in [1, 2, 3]])
        gm2.SetPoint(gm2.GetN(), Et*1e-3, Ettau*1e-3)
        recm2.append([Et*1e-3, Ettau*1e-3])

        Et = sum([a[k]*pulse[48+24*k] for k in [0, 1, 2, 3]])
        Ettau = sum([b[k]*pulse[48+24*k] for k in [0, 1, 2, 3]])
        gp1.SetPoint(gp1.GetN(), Et*1e-3, Ettau*1e-3)
        recp1.append([Et*1e-3, Ettau*1e-3])

    
    fig, ax = aplt.subplots(1, 1)
    ax.plot(gm2, options="p", linecolor=root.kAzure-6, markercolor=root.kAzure-6, markerstyle=23, label="Signal BC - 2", labelfmt="p")
    ax.plot(gm1, options="p", linecolor=root.kOrange+10, markercolor=root.kOrange+10, markerstyle=21, label="Signal BC - 1", labelfmt="p")
    ax.plot(g0, options="p", linecolor=root.kBlack, markercolor=root.kBlack, markerstyle=20, label="Signal BC", labelfmt="p")
    ax.plot(gp1, options="p", linecolor=root.kTeal-6, markercolor=root.kTeal-6, markerstyle=22, label="Signal BC + 1", labelfmt="p")
    
    gl = {}
    for j in [2, 5, 8, 10, 11, 19]:
        gl[j] = root.TGraph()
        gl[j].SetPoint(0, recm2[j][0], recm2[j][1])
        gl[j].SetPoint(1, recm1[j][0], recm1[j][1])
        gl[j].SetPoint(2, rec0[j][0], rec0[j][1])
        gl[j].SetPoint(3, recp1[j][0], recp1[j][1])
        if j == 2:
            ax.plot(gl[j], options="l", linecolor=root.kGray+1, markercolor=root.kGray+1, label="Injected E_{T}", labelfmt="l")
        else:
            ax.plot(gl[j], options="l", linecolor=root.kGray+1, markercolor=root.kGray+1)

    ax.plot(gm2, options="p", linecolor=root.kAzure-6, markercolor=root.kAzure-6, markerstyle=23)
    ax.plot(gm1, options="p", linecolor=root.kOrange+10, markercolor=root.kOrange+10, markerstyle=21)
    ax.plot(g0, options="p", linecolor=root.kBlack, markercolor=root.kBlack, markerstyle=20)
    ax.plot(gp1, options="p", linecolor=root.kTeal-6, markercolor=root.kTeal-6, markerstyle=22)

    ax.legend(loc=(0.62, 0.8, 0.88, 0.92), fillstyle=0, textcolor=root.kBlack)
    ax.set_xlabel("E_{T} [GeV]")
    ax.set_ylabel("E_{T} #upoint t [GeV #upoint ns]")
    ax.add_margins(top=0.05, bottom=0.05, right=0.05)
    ax.add_margins(bottom=0.05)
    #ax.text(0.2, 0.86, "channelId = " + str(tin.channelId), size=22, align=13)
    if tin.layer==0:
        layerStr = "Presampler"
    elif tin.layer==1:
        layerStr = "Front layer"
    elif tin.layer==2:
        layerStr = "Middle layer"
    elif tin.layer==3:
        layerStr = "Back layer"
    ax.text(0.2, 0.86, layerStr, size=22, align=13)
    ax.text(0.2, 0.82, "#eta =  " + "{:.4f}".format(tin.eta) + ", #phi = " + "{:.4f}".format(tin.phi), size=22, align=13)
    fig.canvas.SetName(str(tin.channelId))
    fig.canvas.Write()
