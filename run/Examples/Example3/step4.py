import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import OFCCalibratorModule

OFCCaliTree = OFCCalibratorModule.OFCCaliTree('files/LArOFCCali_00389323.root')
OFCCaliTree.NthPhase = 0
OFCCaliTree.OFBCshift = 1
OFCCaliTree.OutputFileName = 'files/OFC_test_cali_phase0.root'
OFCCaliTree.CellPropertyFileName = 'files/cellprop_LArCaliWave_I03L_sat_merged_revised_4SCs.root'
OFCCaliTree.MakeTree()
