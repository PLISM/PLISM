import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import OFCCalibratorModule

OFCPhysTree = OFCCalibratorModule.OFCPhysTree('files/OFCphys_80mu.root')
OFCPhysTree.NthPhase = 23
OFCPhysTree.OFBCshift = 0
OFCPhysTree.OutputFileName = 'files/OFCphys_test.root'
OFCPhysTree.CellPropertyFileName = 'files/cellprop_PhysWave_test.root'
OFCPhysTree.MakeTree()
