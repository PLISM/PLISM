import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

PulsePattern = CellPropertyGeneratorModule.PulsePattern(
    'files/parameters_SCDelay_FMhighEtaBack_389323_210304-195651.dat',
    'files/LArIDtranslator_test.txt'
)
PulsePattern.OutputPattern()

DigitTree = CellPropertyGeneratorModule.DigitTree(
    'files/LArDigits_00389323.root',
    PulsePattern,
    'files/LArIDtranslator_test.txt',
    'files/LArPedAutoCorr_00389320.root',
    'files/LArDAC2MeV.root',
    100,
    32
)
DigitTree.MakeTree()
del PulsePattern
del DigitTree

AveragedDigitTree = CellPropertyGeneratorModule.AveragedDigitTree(
    'files/converted_LArDigits_00389323.root'
)
AveragedDigitTree.PulseShape()
AveragedDigitTree.PulseTree()
AveragedDigitTree.LSB_delayPeak()
del AveragedDigitTree

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'files/pulse_LArDigits_00389323.root'
)
PulseShapeTree.PedAlreadySubtracted = False
PulseShapeTree.LSBFileName = 'files/lsb_LArDigits_00389323.root'
PulseShapeTree.BCShift = 3
PulseShapeTree.GenerateCellProperty()
