#!/bin/sh

#root -l -b -q 'nonreal.cxx(1, 0, 0, 0, 0)'
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu20_50000000BC_48b8e_pileup.root 50000000 20 48b8e noBC
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu50_50000000BC_48b8e_pileup.root 50000000 50 48b8e noBC
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_48b8e_pileup.root 50000000 80 48b8e noBC
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu20_50000000BC_8b4e_pileup.root 50000000 20 8b4e noBC
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu50_50000000BC_8b4e_pileup.root 50000000 50 8b4e noBC
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_8b4e_pileup.root 50000000 80 8b4e noBC

#root -l -b -q 'nonreal.cxx(1000000, 0, 500000, 0, 0)'
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-500_tau0.root 50000000 80 allBC 1b49e
#root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-500_tau0.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et0-500_tau0.root")'

#root -l -b -q 'nonreal.cxx(1000000, 0, 50000, 0, 0)'
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-50_tau0.root 50000000 80 allBC 1b49e
#root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-50_tau0.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et0-50_tau0.root")'

#root -l -b -q 'nonreal.cxx(1000000, 0, 50000, 4, 4)'
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-50_tau4.root 50000000 80 allBC 1b49e
#root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et0-50_tau4.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et0-50_tau4.root")'

#root -l -b -q 'nonreal.cxx(1000000, 25000, 25000, -24, 24)'
#./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et25_tau-24-24.root 50000000 80 allBC 1b49e
#root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_50000000BC_allBC_each50BC_et25_tau-24-24.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et25_tau-24-24.root")'

root -l -b -q 'nonreal.cxx(1000000, 10000, 10000, 0, 0)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et10_tau0.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et10_tau0.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et10_tau0.root")'

root -l -b -q 'nonreal.cxx(1000000, 30000, 30000, 0, 0)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et30_tau0.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et30_tau0.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et30_tau0.root")'

root -l -b -q 'nonreal.cxx(1000000, 50000, 50000, 0, 0)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau0.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau0.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau0.root")'

root -l -b -q 'nonreal.cxx(1000000, 50000, 50000, 6, 6)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau6.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau6.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau6.root")'

root -l -b -q 'nonreal.cxx(1000000, 50000, 50000, -6, -6)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau-6.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau-6.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau-6.root")'

root -l -b -q 'nonreal.cxx(1000000, 50000, 50000, 12, 12)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau12.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau12.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau12.root")'

root -l -b -q 'nonreal.cxx(1000000, 50000, 50000, -12, -12)'
./Digitizer_ex EMBI13R/CellProperty_968435712.root digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau-12.root 100000 80 allBC 1b49e
root -l -b -q 'CellHitReconstructorMain.cxx("digitSequence_968435712_mu80_100000BC_allBC_each50BC_et50_tau-12.root", "EMBI13R/OFC_968435712_athena_mu80.root", "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_tau-12.root")'
