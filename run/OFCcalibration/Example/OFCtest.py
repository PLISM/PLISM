import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
sys.argv.append('-b')
NthPhase = int(sys.argv[1])
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule
from ROOT import TFile, TTree, TH1D, TCanvas

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_test.root')

SignalHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummaryTest.root')
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 1
# Uniform distribution
HitSummarizer.EtMin = 50000 #MeV
HitSummarizer.EtMax = 50000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

DigitSequenceFileName = os.path.join(fn.DIGITDIR, 'digitSequence_OFCtest.root')
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = 10000
Digitizer.mu = 0
Digitizer.phase = NthPhase * 25./24.
Digitizer.TrainPattern = [0, 1]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
#Digitizer.LowPtPileupHitSummaryFile =  "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/data4PLISM/HitSummary_iguchiLow_968435712_channelId.root"
#Digitizer.HighPtPileupHitSummaryFile = "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/data4PLISM/HitSummary_iguchiHigh_968435712_channelId.root"
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = 'reconstructed_OFCtest_phase' + str(NthPhase) + '.root'
Reconstructor.OFCFileName = os.path.join(fn.OFCDIR, 'OFC_test_cali_phase' + str(NthPhase) + '.root')
Reconstructor.reconstruct()

hEt = TH1D("", "(RecEt-SignalTrueEt)/SignalTrueEt;;", 100, -0.012, 0.012)
hTau = TH1D("", "RecTau [ns];;", 100, -0.2, 0.2)
fFile = TFile(Reconstructor.OutputFileName)
fTree = fFile.Get("reconstructed")
for cell in fTree:
    for i in range(fTree.NBC):
        if fTree.SignalTrueEt[i]:
            hEt.Fill((fTree.RecEt[i] - fTree.SignalTrueEt[i]) / fTree.SignalTrueEt[i])
            hTau.Fill(fTree.RecTau[i])
c = TCanvas()
c.Divide(1, 2)
c.cd(1)
hEt.Draw()
c.cd(2)
hTau.Draw()
c.SaveAs('plot' + str(NthPhase) + '.png')
