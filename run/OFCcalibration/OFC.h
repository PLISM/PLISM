//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec  3 15:07:06 2020 by ROOT version 6.20/06
// from TTree OFC/OFC
// found on file: ../storage/OFCs/OFC_test_cali_phase0.root
//////////////////////////////////////////////////////////

#ifndef OFC_h
#define OFC_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class OFC {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          channelId;
   vector<double>  *a;
   vector<double>  *b;
   Double_t        pedADC;
   Float_t        LSB;

   // List of branches
   TBranch        *b_channelId;   //!
   TBranch        *b_a;   //!
   TBranch        *b_b;   //!
   TBranch        *b_pedADC;   //!
   TBranch        *b_LSB;   //!

   OFC(TTree *tree=0);
   virtual ~OFC();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef OFC_cxx
OFC::OFC(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../storage/OFCs/OFC_test_cali_phase0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../storage/OFCs/OFC_test_cali_phase0.root");
      }
      f->GetObject("OFC",tree);

   }
   Init(tree);
}

OFC::~OFC()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t OFC::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t OFC::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void OFC::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   a = 0;
   b = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
   fChain->SetBranchAddress("a", &a, &b_a);
   fChain->SetBranchAddress("b", &b, &b_b);
   fChain->SetBranchAddress("pedADC", &pedADC, &b_pedADC);
   fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
   Notify();
}

Bool_t OFC::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void OFC::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t OFC::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef OFC_cxx
