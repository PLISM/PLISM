#pragma once

class PulseConverter {
 public:
  std::vector<double> g_phys;
  std::vector<double> g_cali;
  double tau_cali{}, f_step{}, tau_0{}, tau_r{};
  int size{}, tMaxAmp{}, tMinAmp{}, tZeroCross{};
  static const double delta;
  int layer {2};
  PulseConverter(const char* CalibrationPulseName);
  void ComputePhysicsPulse();
  void DrawGraph();
  void ComputeParameters();
  
 private:
  void TauCaliExtraction();
  void StepResponseCalculation();
  void CosineResponseCalculation();
  void InjectPointCalculation();
  std::function<double(const double)> InjectPointCorrectionFunc(const double tau_0_tmp, const double tau_r_tmp);
  std::vector<double> convolution(const std::vector<double> g1, const std::vector<double> g2, const int tail = 0);
  std::vector<double> w_out(const std::vector<double> ftran, const int tail);
  double Q2(const std::vector<double> ftran, const int tail);
  double logQ2(const std::vector<double> ftran, const int tail);
  void Plot1DFunctionGraph(std::function<double(const double)> func, const double a, const double b, const char* name);
  void PlotGWaves(const char* name);
  double FMINBRsolution(std::function<double(const double)> func, double a, double b);
};
