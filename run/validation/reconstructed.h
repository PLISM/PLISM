
#ifndef reconstructed_h
#define reconstructed_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "struct.h"
// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "deque"
#include "deque"


class reconstructed {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          channelId;
   Float_t         LSB;
   Float_t         phase;
   Double_t        mu;
   vector<int>     *TrainPattern;
   vector<int>     *SignalPattern;
   vector<double>  *SignalTrueEt;
   vector<double>  *SignalTrueTau;
   vector<double>  *SignalTrueEtTau;
   deque<double>   *BgAnalogEt;
   deque<int>      *Digit;
   vector<double>  *RecEt;
   vector<double>  *RecTau;
   vector<double>  *RecEtTau;

   // List of branches
   TBranch        *b_channelId;   //!
   TBranch        *b_LSB;   //!
   TBranch        *b_phase;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_TrainPattern;   //!
   TBranch        *b_SignalPattern;   //!
   TBranch        *b_SignalTrueEt;   //!
   TBranch        *b_SignalTrueTau;   //!
   TBranch        *b_SignalTrueEtTau;   //!
   TBranch        *b_BgAnalogEt;   //!
   TBranch        *b_Digit;   //!
   TBranch        *b_RecEt;   //!
   TBranch        *b_RecTau;   //!
   TBranch        *b_RecEtTau;   //!

   reconstructed(TTree *tree=0);
   virtual ~reconstructed();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
  variables ExportVariables(int ient);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef reconstructed_cxx
reconstructed::reconstructed(TTree *tree) : fChain(0) 
{

   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../storage/ReconstructedSequences/reconstructedRef.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../storage/ReconstructedSequences/reconstructedRef.root");
      }
      f->GetObject("reconstructed",tree);

   }
   Init(tree);
}

reconstructed::~reconstructed()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t reconstructed::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t reconstructed::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void reconstructed::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TrainPattern = 0;
   SignalPattern = 0;
   SignalTrueEt = 0;
   SignalTrueTau = 0;
   SignalTrueEtTau = 0;
   BgAnalogEt = 0;
   Digit = 0;
   RecEt = 0;
   RecTau = 0;
   RecEtTau = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
   fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
   fChain->SetBranchAddress("phase", &phase, &b_phase);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("TrainPattern", &TrainPattern, &b_TrainPattern);
   fChain->SetBranchAddress("SignalPattern", &SignalPattern, &b_SignalPattern);
   fChain->SetBranchAddress("SignalTrueEt", &SignalTrueEt, &b_SignalTrueEt);
   fChain->SetBranchAddress("SignalTrueTau", &SignalTrueTau, &b_SignalTrueTau);
   fChain->SetBranchAddress("SignalTrueEtTau", &SignalTrueEtTau, &b_SignalTrueEtTau);
   fChain->SetBranchAddress("BgAnalogEt", &BgAnalogEt, &b_BgAnalogEt);
   fChain->SetBranchAddress("Digit", &Digit, &b_Digit);
   fChain->SetBranchAddress("RecEt", &RecEt, &b_RecEt);
   fChain->SetBranchAddress("RecTau", &RecTau, &b_RecTau);
   fChain->SetBranchAddress("RecEtTau", &RecEtTau, &b_RecEtTau);
   Notify();
}

Bool_t reconstructed::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void reconstructed::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t reconstructed::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef reconstructed_cxx
