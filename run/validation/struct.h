#pragma once

struct variables {
  UInt_t          channelId;
  Float_t         LSB;
  Float_t         phase;
  Double_t        mu;
  vector<int>     TrainPattern;
  vector<int>     SignalPattern;
  vector<double>  SignalTrueEt;
  vector<double>  SignalTrueTau;
  vector<double>  SignalTrueEtTau;
  deque<double>   BgAnalogEt;
  deque<int>      Digit;
  vector<double>  RecEt;
  vector<double>  RecTau; // sometimes nan -> don't compare
  vector<double>  RecEtTau;

  bool operator==(const variables& rhs) const
    {
     return (channelId == rhs.channelId &&
	     LSB == rhs.LSB &&
	     phase == rhs.phase &&
	     mu == rhs.mu &&
	     TrainPattern == rhs.TrainPattern &&
	     SignalPattern == rhs.SignalPattern &&
	     SignalTrueEt == rhs.SignalTrueEt &&
	     SignalTrueTau == rhs.SignalTrueTau &&
	     SignalTrueEtTau == rhs.SignalTrueEtTau &&
	     BgAnalogEt == rhs.BgAnalogEt &&
	     Digit == rhs.Digit &&
	     RecEt == rhs.RecEt &&
	     RecEtTau == rhs.RecEtTau);
    }
  bool operator!=(const variables& rhs) const
  {
   return !(*this == rhs);
  }
};
