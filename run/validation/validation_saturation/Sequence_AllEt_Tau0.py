import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
sys.argv.append('-b')
NthPhase = int(sys.argv[1])
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule
from ROOT import TFile, TTree, TH1D, TCanvas

CellPropertyFileName = 'CellProperty_958474240.root'

SignalHitSummaryFile = 'HitSummaryTest.root'
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 10000000
# Uniform distribution
HitSummarizer.EtMin = 5000 #MeV
HitSummarizer.EtMax = 5000000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

DigitSequenceFileName = 'digitSequence_OFCtest.root'
Digitizer = DigitizerModule.Digitizer()
Digitizer.CellPropertyFileName = CellPropertyFileName
Digitizer.NBC = 2000000
Digitizer.mu = 0
Digitizer.phase = NthPhase * 25./24.
Digitizer.TrainPattern = [0, 1]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = 'HitSummaryLowPt.root'
Digitizer.HighPtPileupHitSummaryFile = 'HitSummaryHighPt.root'
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = 'reconstructed_Sequence_AllEt_Tau0_phase' + str(NthPhase) + '.root'
Reconstructor.OFCFileName = 'OFC_test_cali_phase' + str(NthPhase) + '.root'
Reconstructor.reconstruct()
