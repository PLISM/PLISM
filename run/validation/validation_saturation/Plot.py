import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import PlotterModule

#InputFilenames = [
#    os.path.join(fn.RECDIR, 'reconstructed_968435712_athena_mu80_5000000BC_allBC_each50BC_et0-500_tau0.root'),
#]

#InputFilenames = ["reconstructed_OFCtest_phase" + str(i) + ".root" for i in range(24)]
InputFilenames = ["reconstructed_OFCtest_phase23.root"]
Plotter1 = PlotterModule.Plotter(InputFilenames)
Plotter1.SequencePlotter(14700, 19700, "drts", "sequence.pdf", True)

InputFilenames = ["reconstructed_Sequence_AllEt_Tau0_phase0.root"]
Plotter2 = PlotterModule.Plotter(InputFilenames)
Plotter2.ScatterPlotter('RecTau', 'RecEt', -80., 50., -150., 900., False, "TauVsEt.pdf")
Plotter2.ScatterPlotter('RecEt', 'RecEtTau', 0., 900., -30000., 20000., False, "EtVsEtTau.pdf")
