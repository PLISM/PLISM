from ROOT import TFile, TTree


channelId = 958474240
inputFileName = 'cellprop_CaliWave_test.root'
outputFileName = 'CellProperty_' + str(channelId) + '.root'
fin = TFile(inputFileName, "read")
tin = fin.Get("CellProperty")
ofile = TFile(outputFileName, "RECREATE")

newtree = tin.CopyTree("channelId == " + str(channelId))

ofile.cd()
newtree.Write()
ofile.Close()
