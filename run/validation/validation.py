import sys,os
from ROOT import gROOT
sys.path += [os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')]
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_968435712.root')

SignalHitSummaryFile = os.path.join(fn.HITDIR, 'validation.root')
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 10000
# Uniform distribution
HitSummarizer.EtMin = 0 #MeV
HitSummarizer.EtMax = 50000 #MeV
HitSummarizer.TauMin = -12 #ns
HitSummarizer.TauMax = 12 #ns
HitSummarizer.seed = 24231
HitSummarizer.makeSignal()

DigitSequenceFileName = os.path.join(fn.DIGITDIR, 'validation.root')
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = 10000
Digitizer.mu = 80
Digitizer.phase = 0
Digitizer.TrainPattern = [48, 8, 48, 8, 48, 32]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = "../storage/HitSummaries/HitSummary_iguchiLow_968435712_channelId.root"
Digitizer.HighPtPileupHitSummaryFile = "../storage/HitSummaries/HitSummary_iguchiHigh_968435712_channelId.root"
Digitizer.seed = 188
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = os.path.join(fn.RECDIR, 'validation.root')
Reconstructor.OFCFileName = os.path.join(fn.OFCDIR, 'OFC_968435712_athena_mu80.root')
Reconstructor.reconstruct()

gROOT.ProcessLine(".x compare.cxx")
