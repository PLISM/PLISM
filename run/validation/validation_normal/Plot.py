import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import PlotterModule
import Functions as fn

# InputFilenames = [
#    os.path.join(fn.RECDIR, 'reconstructed_968435712_athena_mu80_5000000BC_allBC_each50BC_et0-500_tau0.root'),
# ]
print(1)
InputFilenames = ["reconstructed_OFCtest_phase" + str(i) + ".root" for i in range(24)]
InputFilenames = ["reconstructed_for_sequencePlot.root"]
Plotter1 = PlotterModule.Plotter(InputFilenames)
Plotter1.SequencePlotter(0, 5000, "drts", "sequence.pdf", True)
print(2)
InputFilenames = ["reconstructed_Sequence_Et0-500_Tau0.root"]
Plotter2 = PlotterModule.Plotter(InputFilenames)
Plotter2.TH2DPlotter('SignalTrueEt', 'EtResolution', 50, 0., 500., 100, -0.006, 0.006, True, "EtVsEtRes.pdf")
print(3)
InputFilenames = ["reconstructed_Sequence_Et0-50_Tau0.root"]
Plotter3 = PlotterModule.Plotter(InputFilenames)
Plotter3.TH2DPlotter('SignalTrueEt', 'TauResolution', 50, 0., 50., 100, -3., 3., True, "EtVsTauRes.pdf")
print(4)
InputFilenames = ["reconstructed_Sequence_Et50_Tau0.root"]
Plotter4 = PlotterModule.Plotter(InputFilenames)
Plotter4.ScatterPlotter('RecTau', 'RecEt', -25., 25., -15., 55., False, "EtVsTau.pdf")
print(5)
InputFilenames = ["reconstructed_Sequence_Et25_Tau-24-24.root"]
Plotter4 = PlotterModule.Plotter(InputFilenames)
Plotter4.TH2DPlotter('SignalTrueTau', 'EtResolution', 100, -24., 24., 100, -0.3, 0.1, True, "TauVsEtRes.pdf")
Plotter4.TH2DPlotter('SignalTrueTau', 'TauResolution', 100, -24., 24., 100, -7., 5., True, "TauVsTauRes.pdf")
print(6)
InputFilenames = ["digitSequence_baseline.root"]
Plotter5 = PlotterModule.Plotter(InputFilenames)
Plotter5.TrainPeriodicAveragePlotter(0, 'Digit', "baseline_mean.pdf", False)
Plotter5.TrainPeriodicRMSPlotter(0, 'Digit', "baseline_rms.pdf", False)

