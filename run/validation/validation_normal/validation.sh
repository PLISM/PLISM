#cp /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HitSummary_LowPt_968435712.root .
#cp /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HitSummary_HighPt_968435712.root .

function exe() {
    echo "----------------------------------------------------------------------------"
    echo "Executing ${1}.py ..."
    python3 ${1}.py
    echo "Finished ${1}.py"
    echo "----------------------------------------------------------------------------"
}

exe Process
exe Sequence_Et0-500_Tau0
exe Sequence_Et0-50_Tau0
exe Sequence_Et50_Tau0
exe Sequence_Et25_Tau-24-24
exe Sequence_baseline
exe Plot
