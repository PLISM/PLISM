import sys
from ROOT import TFile, TTree
args = sys.argv

#inputFileName = args[1]
#outputFileName = args[2]
inputFileName = 'storage/CellProperties/CellProperty_test.root'
outputFileName = 'storage/CellProperties/CellProperty_958474240.root'
fin = TFile(inputFileName, "read")
tin = fin.Get("CellProperty")
ofile = TFile(outputFileName, "RECREATE")

newtree = tin.CopyTree("channelId == 958474240")

ofile.cd()
newtree.Write()
ofile.Close()
