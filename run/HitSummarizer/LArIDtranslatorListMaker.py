import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn

target = ["ONL_ID", "DET", "SAM", "ETA", "PHI", "SC_ONL_ID", "SCETA", "SCPHI"]
#condition = "AC=1 or AC=-1"
condition = "FTNAME like 'I13R'"
output_file_name = "LArIDtranslator_test.txt"
overlap_canceller = True

fn.MakeListFromLArIDtranslator(target, condition, output_file_name, overlap_canceller)
