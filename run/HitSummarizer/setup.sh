
myhost=`hostname`
if [[ $myhost == lxplus*.cern.ch || $myhost == pcatutt28 ]] ; then
    ln -s /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608147._000001.root HITSforAREUS.05608147._000001.root
    ln -s /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608152._000001.root HITSforAREUS.05608152._000001.root
else
    if [ ! -e HITSforAREUS.05608147._000001.root ]; then
	scp lxplus:/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608147._000001.root . || \
	    echo "Failed in scp. Please copy /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608147._000001.root from lxplus to this directory."
    fi
    if [ ! -e HITSforAREUS.05608152._000001.root ]; then
	scp lxplus:/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608152._000001.root . || \
	    echo "Failed in scp. Please copy /eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HITSforAREUS.05608152._000001.root from lxplus to this directory."
    fi
fi
