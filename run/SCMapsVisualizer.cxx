
enum AtlasID {LArEM, LArHEC, LArFCal, LArTile};
enum EMSubsystem {EMB, InnerEMEC, OuterEMEC};
enum EMLayer {presampler, front, middle, back};
enum TileSubsystem {barrel, extbarrel, gap, gapscin};
enum TileSample {A = 0, B = 1, BC = 1, C = 1, D = 2, E = 3, X = 4};
static unsigned int getID(AtlasID AtlasID, EMSubsystem subsystem, EMLayer layer);
static unsigned int getID(AtlasID AtlasID, int region);
static unsigned int getID(AtlasID AtlasID, TileSubsystem subsystem, TileSample sample);

unsigned int getID(AtlasID AtlasID, EMSubsystem subsystem, EMLayer layer) {
  if (AtlasID == LArEM) return (0b0001 | (1 << (6 + subsystem)) | layer << 4);
  std::cout << "Unknown region.\n";
  return 0;
}

unsigned int getID(AtlasID AtlasID, int region) {
  if (AtlasID == LArHEC && region >= 0 && region < 4) return (0b0010 | (region << 9));
  if (AtlasID == LArFCal && region > 0 && region < 4) return (0b0100 | (region << 11));
  std::cout << "Unknown region.\n";
  return 0;
}

unsigned int getID(AtlasID AtlasID, TileSubsystem subsystem, TileSample sampling) {
  if (AtlasID == LArTile) return (0b1000 | (1 << (16 + subsystem)) | (sampling << 13));
  std::cout << "Unknown region.\n";
  return 0;
}

void SCMapsVisualizer() {
  unsigned int channelId = 0;
  Float_t eta;
  Float_t phi;
  unsigned int ID;
  Float_t phiWidth;
  auto add = [&] {
	       ++channelId;
	       if (ID == getID(LArEM, EMB, middle) && eta> -0.1 && eta < 0.1 && phi > -1.9 && phi < -1.7) std::cout << channelId << ": ID=" << ID << ", phi=" << phi << ", eta=" << eta << "\n";
	       //std::cout << "channelId" << channelId << ": ID=" << ID << ", phi=" << phi << ", eta=" << eta << "\n";
	     };
  auto iterate = [&](Float_t etaBegin, Float_t etaWidth, int etaNum) {
		   Int_t ieta = 0;
		   for (ieta = 0, eta = etaBegin; ieta < etaNum; ++ieta, eta += etaWidth) add();
		   //std::cout << ", etaWidth=" << etaWidth << ", phiWidth=" << phiWidth << "\n";
		   Float_t etaTmp = eta;
		   for (ieta = 0, eta = -eta; ieta < etaNum; ++ieta, eta += etaWidth) add();
		   //std::cout << ", etaWidth=" << etaWidth << ", phiWidth=" << phiWidth << "\n";
		   eta = std::move(etaTmp);
		 };
  Int_t iphi;

  for (iphi = 0, phi = -3.2, phiWidth = .1; iphi <= 63; ++iphi, phi += phiWidth) {
    ID = getID(LArEM, EMB, presampler);
    iterate(0., .1, 14); iterate(eta, .12, 1);
    ID = getID(LArEM, EMB, front);
    iterate(0., .025, 56); iterate(eta, .025, 3);
    ID = getID(LArEM, EMB, middle);
    iterate(0., .025, 56); iterate(eta, .075, 1);
    ID = getID(LArEM, EMB, back);
    iterate(0., .1, 13); iterate(eta, .05, 1);
    ID = getID(LArEM, OuterEMEC, presampler);
    iterate(1.5, .1, 3);
    ID = getID(LArEM, OuterEMEC, front);
    iterate(1.375, .125, 1); iterate(eta, .025, 11); iterate(eta, .2/6., 6); iterate(eta, .025, 16); iterate(eta, .1, 1);
    ID = getID(LArEM, OuterEMEC, middle);
    iterate(1.375, .05, 1); iterate(eta, .025, 3); iterate(eta, .025, 12); iterate(eta, .025, 28);
    ID = getID(LArEM, OuterEMEC, back);
    iterate(1.5, .1, 3); iterate(eta, .1, 7);
    ID = getID(LArHEC, 0);
    iterate(1.5, .1, 10);
    ID = getID(LArHEC, 1);
    iterate(1.5, .1, 10);
    ID = getID(LArHEC, 2);
    iterate(1.5, .1, 10);
    ID = getID(LArHEC, 3);
    iterate(1.5, .1, 10);
  }  
  for (iphi = 0, phi = -3.2, phiWidth = 0.2; iphi <= 31; ++iphi, phi += phiWidth) {
    ID = getID(LArEM, InnerEMEC, front);
    iterate(2.5, .2, 39); iterate(eta, .1, 1);
    ID = getID(LArEM, InnerEMEC, middle);
    iterate(2.5, .2, 47); iterate(eta, .1, 1);
    ID = getID(LArHEC, 0);
    iterate(2.5, 0.2, 14);
    ID = getID(LArHEC, 1);
    iterate(2.5, 0.2, 14);
    ID = getID(LArHEC, 2);
    iterate(2.5, 0.2, 14);
    ID = getID(LArHEC, 3);
    iterate(2.5, 0.2, 14);
  }
}
