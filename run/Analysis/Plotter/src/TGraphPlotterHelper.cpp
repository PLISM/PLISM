#include <TCanvas.h>
#include <TColor.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TLegend.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::TGraphPlotterHelper(TGraph*& g, const std::string& Option,
                                      const double MarkerSize,
                                      const std::string& OutputFilename)
    {
        // canvas
        TCanvas c{"c", "c", 800, 600};
        c.Clear();
        c.Update();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        SetAtlasStyle();
        c.SetFrameFillColor(kWhite);
        c.cd();

        // Draw
        g->SetMarkerStyle(20);
        g->SetMarkerColor(kBlue);
        g->SetLineColor(kBlue);
        g->SetLineWidth(2);
        g->SetMarkerSize(MarkerSize);
        g->GetXaxis()->SetNdivisions(505);
        g->GetYaxis()->SetNdivisions(505);
        g->Draw(Option.c_str());

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);
        // latex

        // Save
        if (OutputFilename.empty()) {
            c.SaveAs("figure/TGraph.pdf");
            std::cout << "Outputfile is saved as TGraph.pdf" << std::endl;
        } else {
            c.SaveAs(OutputFilename.c_str());
            std::cout << "Outputfile is saved as " << OutputFilename.c_str()
                      << std::endl;
        }
        std::cout << OutputFilename << std::endl;
        c.Close();
    }
}  // namespace PLISMAnalysis