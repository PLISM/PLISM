#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TTreeReader.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    // DataName should be BranchName , "EtResolution", "TauResoolution" or
    // "DigitLSB". DefaultAxisLabel and DefaultDataScaler are applied.
    void Plotter::TH2DPlotter(const int InputFileLabel,
                              const std::string& xDataName,
                              const std::string& yDataName, const int nbinsx,
                              const double xlow, const double xup,
                              const int nbinsy, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(InputFileLabel, xDataName, yDataName,
                                           nbinsx, xlow, xup, nbinsy, ylow, yup,
                                           isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" +
                                     std::to_string(InputFileLabel) + ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& InputFilename,
                              const std::string& xDataName,
                              const std::string& yDataName, const int nbinsx,
                              const double xlow, const double xup,
                              const int nbinsy, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(InputFilename, xDataName, yDataName,
                                           nbinsx, xlow, xup, nbinsy, ylow, yup,
                                           isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(
                h, "./figure/TH2D_" + xDataName + "_vs_" + yDataName + "_" +
                       std::to_string(
                           database.InputFilenameToLabel[InputFilename]) +
                       ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& xDataName,
                              const std::string& yDataName, const int nbinsx,
                              const double xlow, const double xup,
                              const int nbinsy, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h =
            generator.TH2DGenerator(xDataName, yDataName, nbinsx, xlow, xup,
                                    nbinsy, ylow, yup, isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" + std::to_string(0) +
                                     ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }

    // DataName should be BranchName , "EtResolution", "TauResoolution" or
    // "DigitLSB". DefaultAxisLabel, DefaultDataScaler and Default nbins
    // (=200) are applied.
    void Plotter::TH2DPlotter(const int InputFileLabel,
                              const std::string& xDataName,
                              const std::string& yDataName, const double xlow,
                              const double xup, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h =
            generator.TH2DGenerator(InputFileLabel, xDataName, yDataName, xlow,
                                    xup, ylow, yup, isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" +
                                     std::to_string(InputFileLabel) + ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& InputFilename,
                              const std::string& xDataName,
                              const std::string& yDataName, const double xlow,
                              const double xup, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h =
            generator.TH2DGenerator(InputFilename, xDataName, yDataName, xlow,
                                    xup, ylow, yup, isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(
                h, "./figure/TH2D_" + xDataName + "_vs_" + yDataName + "_" +
                       std::to_string(
                           database.InputFilenameToLabel[InputFilename]) +
                       ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& xDataName,
                              const std::string& yDataName, const double xlow,
                              const double xup, const double ylow,
                              const double yup, bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(xDataName, yDataName, xlow, xup,
                                           ylow, yup, isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" + std::to_string(0) +
                                     ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }

    // DataName should be BranchName , "EtResolution", "TauResoolution"
    // or "DigitLSB". DefaultAxisLabel , DefaultDataScaler,  Default nbins
    // (=200), DefaultAxisLow/Up are applied.
    void Plotter::TH2DPlotter(const int InputFileLabel,
                              const std::string& xDataName,
                              const std::string& yDataName,
                              bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(InputFileLabel, xDataName, yDataName,
                                           isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" +
                                     std::to_string(InputFileLabel) + ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& InputFileName,
                              const std::string& xDataName,
                              const std::string& yDataName,
                              bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(InputFileName, xDataName, yDataName,
                                           isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(
                h, "./figure/TH2D_" + xDataName + "_vs_" + yDataName + "_" +
                       std::to_string(
                           database.InputFilenameToLabel[InputFileName]) +
                       ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }
    void Plotter::TH2DPlotter(const std::string& xDataName,
                              const std::string& yDataName,
                              bool isOnlyEventBCDataUsed,
                              const std::string& OutputFileName)
    {
        auto&& h = generator.TH2DGenerator(xDataName, yDataName,
                                           isOnlyEventBCDataUsed);
        if (OutputFileName.empty()) {
            TH2DPlotterHelper(h, "./figure/TH2D_" + xDataName + "_vs_" +
                                     yDataName + "_" + std::to_string(0) +
                                     ".pdf");
        } else {
            TH2DPlotterHelper(h, OutputFileName);
        }
    }

    void Plotter::TH2DPlotterALL(const int InputFileLabel,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFileName)
    {
        // color map
        const Int_t NRGBs = 5;
        const Int_t NCont = 255;
        Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
        Double_t Red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
        Double_t Green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
        Double_t Blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
        TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
        gStyle->SetNumberContours(NCont);

        // draw
        std::vector<std::string> DataNames{
            "SignalTrueEt", "SignalTrueTau", "RecEt",        "RecTau",
            "RecEtTau",     "EtResolution",  "TauResolution"};
        for (unsigned int i = 0; i < DataNames.size(); i++) {
            for (unsigned int j = i; j < DataNames.size(); j++) {
                // canvas
                TCanvas c{"c", "c", 1200, 800};
                c.Clear();
                c.Update();
                gStyle->SetPadTickX(1);
                gStyle->SetPadTickY(1);
                SetAtlasStyle();
                c.SetFrameFillColor(kWhite);
                c.cd();

                // color map
                const Int_t NRGBs = 5;
                const Int_t NCont = 255;
                Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
                Double_t Red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
                Double_t Green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
                Double_t Blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
                TColor tc{};
                TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue,
                                                 NCont);
                gStyle->SetNumberContours(NCont);
                std::cout << "\nDrawing \"" << DataNames[i] << " vs "
                          << DataNames[j] << "\"\n";
                auto&& h = generator.TH2DGenerator(InputFileLabel, DataNames[i],
                                                   DataNames[j],
                                                   isOnlyEventBCDataUsed);
                h->Draw("colz");

                // gPad
                gPad->SetTopMargin(0.2);
                gPad->SetRightMargin(0.2);
                gPad->SetLeftMargin(0.2);
                gPad->SetBottomMargin(0.2);

                // Save
                if (OutputFileName.empty()) {
                    if (i == 0 && j == 0) {
                        c.Print(std::string{"./figure/TH2DALL_" +
                                            std::to_string(InputFileLabel) +
                                            ".pdf("}
                                    .c_str());
                    } else if (i == DataNames.size() - 1 &&
                               j == DataNames.size() - 1) {
                        c.Print(std::string{"./figure/TH2DALL_" +
                                            std::to_string(InputFileLabel) +
                                            ".pdf)"}
                                    .c_str());
                    } else {
                        c.Print(std::string{"./figure/TH2DALL_" +
                                            std::to_string(InputFileLabel) +
                                            ".pdf"}
                                    .c_str());
                    }

                } else {
                    if (i == 0 && j == 0) {
                        c.Print((OutputFileName + "(").c_str());
                    } else if (i == DataNames.size() - 1 &&
                               j == DataNames.size() - 1) {
                        c.Print((OutputFileName + ")").c_str());
                    } else {
                        c.Print(OutputFileName.c_str());
                    }
                }
                c.Close();
            }
        }
    }
}  // namespace PLISMAnalysis