#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>
#include <TStyle.h>
#include <TTreeReader.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::SequencePlotter(const int InputFileLabel,
                                  const std::string& Option, int MinTime_ns,
                                  int MaxTime_ns,
                                  const std::string& OutputFileName,
                                  bool isMeVtoGeV)
    {
        // canvas
        TCanvas c{"c", "c", 1200, 800};
        c.Clear();
        c.Update();
        SetAtlasStyle();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        c.SetFrameFillColor(kWhite);
        c.cd();

        // legend
        TLegend legend{0.64, 0.70, 0.76, 0.79};
        legend.SetTextSize(0.02);
        legend.SetLineColor(0);
        legend.SetBorderSize(0);
        legend.SetFillColor(0);

        // Draw Graph
        std::vector<TGraph*> vg;
        double Etlow{__DBL_MAX__}, Etup{__DBL_MIN__};

        if (std::regex_search(Option, std::regex{"a"})) {
            auto&& g_AnalogEt = generator.SequenceGenerator(
                InputFileLabel, std::string{"AnalogEt"}, isMeVtoGeV);
            g_AnalogEt->SetMarkerColor(kPink + 1);
            g_AnalogEt->SetLineColor(kPink + 1);
            Etlow = std::min(Etlow, g_AnalogEt->GetMinimum());
            Etup = std::max(Etup, g_AnalogEt->GetMaximum());
            vg.push_back(g_AnalogEt);
            legend.AddEntry(g_AnalogEt, "Analog E_{T}", "lp");
        }
        if (std::regex_search(Option, std::regex{"b"})) {
            auto&& g_BgAnalogEt = generator.SequenceGenerator(
                InputFileLabel, std::string{"BgAnalogEt"}, isMeVtoGeV);
            g_BgAnalogEt->SetMarkerColor(kCyan + 1);
            g_BgAnalogEt->SetLineColor(kCyan + 1);
            Etlow = std::min(Etlow, g_BgAnalogEt->GetMinimum());
            Etup = std::max(Etup, g_BgAnalogEt->GetMaximum());
            vg.push_back(g_BgAnalogEt);
            legend.AddEntry(g_BgAnalogEt, "BgAnalog E_{T}", "lp");
        }
        if (std::regex_search(Option, std::regex{"t"})) {
            auto&& g_TrueEt = generator.SequenceGenerator(
                InputFileLabel, std::string{"SignalTrueEt"}, isMeVtoGeV);
            g_TrueEt->SetMarkerColor(kBlack);
            g_TrueEt->SetLineColor(kBlack);
            Etlow = std::min(Etlow, g_TrueEt->GetMinimum());
            Etup = std::max(Etup, g_TrueEt->GetMaximum());
            vg.push_back(g_TrueEt);
            legend.AddEntry(g_TrueEt, "True E_{T}", "lp");
        }
        if (std::regex_search(Option, std::regex{"d"})) {
            auto&& g_DigitLSB = generator.SequenceGenerator(
                InputFileLabel, std::string{"DigitLSB"}, isMeVtoGeV);
            g_DigitLSB->SetMarkerColor(kBlue);
            g_DigitLSB->SetLineColor(kBlue);
            g_DigitLSB->Draw("pl");
            Etlow = std::min(Etlow, g_DigitLSB->GetMinimum());
            Etup = std::max(Etup, g_DigitLSB->GetMaximum());
            vg.push_back(g_DigitLSB);
            legend.AddEntry(g_DigitLSB, "Digit #times LSB", "lp");
        }
        if (std::regex_search(Option, std::regex{"r"})) {
            auto&& g_RecEt = generator.SequenceGenerator(
                InputFileLabel, std::string{"RecEt"}, isMeVtoGeV);
            g_RecEt->SetMarkerColor(kGreen + 2);
            g_RecEt->SetLineColor(kGreen + 2);
            Etlow = std::min(Etlow, g_RecEt->GetMinimum());
            Etup = std::max(Etup, g_RecEt->GetMaximum());
            vg.push_back(g_RecEt);
            legend.AddEntry(g_RecEt, "Reconstructed E_{T}", "lp");
        }
        if (vg.size()) {
            for (auto it = vg.begin(); it != vg.end(); it++) {
                if (it == vg.begin()) {
                    (*it)->SetMaximum(Etup);
                    (*it)->SetMinimum(Etlow);
                    std::string title{";time [ns]; E_{T}"};
                    if (isMeVtoGeV) {
                        title += std::string{" [GeV]"};
                    } else {
                        title += std::string{" [MeV]"};
                    }
                    (*it)->SetTitle(title.c_str());
                    (*it)->GetXaxis()->SetLimits(MinTime_ns, MaxTime_ns);
                    (*it)->GetXaxis()->SetNdivisions(505);
                    (*it)->Draw("apl");
                } else {
                    (*it)->Draw("pl");
                }
            };
        }

        if (std::regex_search(Option, std::regex{"s"})) {
            auto&& g_SelectedEt =
                generator.SelectedEtGenerator(InputFileLabel, isMeVtoGeV);
            g_SelectedEt->SetMarkerColor(kRed + 1);
            g_SelectedEt->SetLineColor(kRed + 1);
            if (vg.size()) {
                g_SelectedEt->Draw("p");
                legend.AddEntry(g_SelectedEt, "Passed #tau criteria", "p");
            } else {
                std::string title{";time [ns]; E_{T}"};
                if (isMeVtoGeV) {
                    title += std::string{" [GeV]"};
                } else {
                    title += std::string{" [MeV]"};
                }
                g_SelectedEt->SetTitle(title.c_str());
                g_SelectedEt->Draw("ap");
                legend.AddEntry(g_SelectedEt, "Passed #tau criteria", "p");
            }
        }
        legend.Draw();

        // line
        TLine line{static_cast<double>(MinTime_ns), 0.0,
                   static_cast<double>(MaxTime_ns), 0.0};
        line.SetLineColor(12);
        line.SetLineWidth(1);
        line.SetLineStyle(2);
        line.Draw();

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        // Save
        if (OutputFileName.empty()) {
            const std::string of{"Sequence_" + Option + "_" +
                                 std::to_string(InputFileLabel) + ".pdf"};
            c.SaveAs(("./figure/" + of).c_str());
            std::cout << "Outputfile is saved as " << of << "." << std::endl;
        } else {
            c.SaveAs(OutputFileName.c_str());
            std::cout << "Outputfile is saved as " << OutputFileName
                      << std::endl;
        }

        c.Close();
    }
}  // namespace PLISMAnalysis