#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    double Generator::DefaultAxisUpForGraph(int InputFileLabel,
                                            const std::string& DataName,
                                            bool MeVtoGeV)
    {
        if (std::regex_search(DataName, std::regex("EtResolution"))) {
            double etmax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            if (etmax < 1e-8) { return 0.0; }
            return 0.03 * 500000. / etmax;
        } else if (std::regex_search(DataName, std::regex("TauResolution"))) {
            double etmax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            if (etmax < 1e-8) { return 0.0; }
            return 3.0 * 50000. / etmax;
        } else if (std::regex_search(DataName, std::regex("EtTau"))) {
            double e{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second)};
            e = (e + 1000.) * 1.2;
            return std::round(e * 25. / (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("RecEt"))) {
            double e{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second)};
            e = (e + 1000.) * 1.2;
            return std::round(e / (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("Et"))) {
            double e{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second)};
            return std::round(e / (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("Tau"))) {
            return std::round(25.);
        } else {
            std::cerr << WarningrMessage << "::DefaultAxisUpForGraph: "
                      << "Unenable to determine AxisLUP.\n";
            exit(EXIT_FAILURE);
        }
    }

    double Generator::DefaultAxisLowForGraph(int InputFileLabel,
                                             const std::string& DataName,
                                             bool MeVtoGeV)
    {
        if (std::regex_search(DataName, std::regex("EtResolution"))) {
            double etmax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            if (etmax < 1e-8) { return 0.0; }
            return -0.03 * 500000. / etmax;
        } else if (std::regex_search(DataName, std::regex("TauResolution"))) {
            double etmax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            if (etmax < 1e-8) { return 0.0; }
            return -3.0 * 50000. / etmax;
        } else if (std::regex_search(DataName, std::regex("EtTau"))) {
            double e{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second)};
            e = -(e + 1000.) * 0.5;
            return std::round(e * 25. / (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("RecEt"))) {
            double e{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second)};
            e = -(e + 1000.) * 0.5;
            return std::round(e / (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("Et"))) {
            return std::round(0.);
        } else if (std::regex_search(DataName, std::regex("Tau"))) {
            return std::round(-25.);
        } else {
            std::cerr << WarningrMessage << "::DefaultAxisLowForGraph: "
                      << "Unenable to determine AxisLow.\n";
            exit(EXIT_FAILURE);
        }
    }
}  // namespace PLISMAnalysis