#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    TGraph* Generator::SequenceGenerator(const int InputFileLabel,
                                         const std::string& DataName,
                                         bool MeVtoGeV)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        auto&& fin = AnalysisData.InputFiles[InputFileLabel];
        if (DataName == std::string{"AnalogEt"} ||
            DataName == std::string{"BgAnalogEt"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::deque<double>> Data{reader, DataName.c_str()};
            reader.Next();
            std::string title{";time [ns]; " +
                              DefaultAxisLabel(DataName, MeVtoGeV)};
            return SequenceGenerator<std::deque<double>>(
                InputFileLabel, *Data, MeVtoGeV ? 0.001 : 1., title);
        } else if (DataName == std::string{"DigitLSB"}) {
            auto&& Data = DigitLSBGenerator(InputFileLabel);
            std::string title{";time [ns]; " +
                              DefaultAxisLabel(DataName, MeVtoGeV)};
            return SequenceGenerator<std::vector<double>>(
                InputFileLabel, Data, MeVtoGeV ? 0.001 : 1., title);
        } else if (DataName == std::string{"RecEt"} ||
                   DataName == std::string{"SignalTrueEt"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> Data{reader,
                                                       DataName.c_str()};
            reader.Next();
            std::string title{";time [ns]; " +
                              DefaultAxisLabel(DataName, MeVtoGeV)};
            return SequenceGenerator<std::vector<double>>(
                InputFileLabel, *Data, MeVtoGeV ? 0.001 : 1., title);
        } else {
            std::cerr << ErrorMessage << "::SequenceGenerator: "
                      << "UnknownDataName.\n";
            exit(EXIT_FAILURE);
        }
    }
}  // namespace PLISMAnalysis