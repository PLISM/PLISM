#pragma once

#include <TFile.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>

#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

namespace PLISMAnalysis
{
    class DataBase
    {
    private:
        static inline std::ostream& ErrorMessage(std::ostream& os)
        {
            os << "Error in PLISMAnalysis::Base";
            return os;
        }
        static inline std::ostream& WarningMessage(std::ostream& os)
        {
            os << "Warning in PLISMAnalysis::Base";
            return os;
        }

    public:
        std::vector<TFile*> InputFiles;

        std::vector<std::string> InputFilenames;
        std::unordered_map<std::string, int> InputFilenameToLabel;
        std::vector<std::string>
            InputFileTypes;  // "reconstructed" or "digitSequence"
        size_t NumInputFiles;

        const int BCInterval_ns;
        std::vector<int> Periods_BC;
        std::vector<int> EventIntervals_BC;
        std::vector<long long> TotalBCs;
        std::vector<std::vector<int>> TrainPatterns;
        std::vector<std::vector<int>> SignalPatterns;
        std::vector<double> Mus;
        std::vector<std::pair<double, double>> MinMaxEts_MeV;
        std::vector<std::pair<double, double>> MinMaxTaus_ns;

        // exception handling
        void InputFileLabelHandler(int InputFileLabel);
        void InputFilenameHandler(const std::string& InputFilename);

        // constructor
        DataBase(const std::vector<std::string>& InputFilenames_);

        // getter
        inline size_t getNumInputFiles() { return NumInputFiles; }
        inline std::unordered_map<std::string, int> getInputFilenameToLabel()

        {
            return InputFilenameToLabel;
        }
        void PrintLabels();
    };
}  // namespace PLISMAnalysis

