# Setup

```Bash
source build.sh
cd run/Analysis/test
```

Four static libraries, AtlasStyle_lib DataBase_lib Generator_lib and Plotter_lib are generated in `run/Analysis/test`. They help us generate some plots based on digit sequence Ntuples and reconstructed Ntuples.

# How to use

First, you have to write source codes in `run/Analysis/sample.cpp` in order to generate the plots with the four libraries . The references of the libraries are shown on the follwing sections.
And then, you can execute the cpp source code in the following way;

```Bash
source test.sh sample
```
  
## PLISMAnalysis::Plotter

PLISMAnalysis::Plotter help us generate 2D histogram, scatter plots and sequence plots based on a digit/reconstructed Ntuple.

#### Constructor

```cpp
PLISM::Analysis Plotter(std::string PathToInputFile);
```

#### Memberfunctions

```cpp
void TH2DPlotter(const std::string& xDataName, const std::string& yDataName, 
                 const int nbinsx, const double xlow, const double xup, 
                 const int nbinsy, const double ylow, const double yup,
                 bool isOnlyEventBCDataUsed = true, const std::string& OutputFileName = "");
```

`std::string& DataName` should be RecEt, "RecTau" , "RecEtTau", "SignalTrueEt" , "SignalTrueTau", "SignalTrueEtTau", "EtResolution" or "TauResolution". `isOnlyEventBCDataUsed` is the flag to specify wherther or not to plot only BCs ingected `TrueEt`.   

```cpp
void ScatterPlotter(const std::string& xDataName, const std::string& yDataName, 
                    const double xlow, const double xup, 
                    const double ylow, const double yup,
                    bool isOnlyEventBCDataUsed = false, const std::string& OutputFilename = "");
```
`std::string& DataName` should be "RecEt", "RecTau" , "RecEtTau", "SignalTrueEt" "SignalTrueTau", "SignalTrueEtTau", EtResolution" or "TauResolution". `bool isOnlyEventBCDataUsed` is the flag to specify wherther or not to plot only BCs ingected `TrueEt`. 

```cpp
void SequencePlotter(const std::string& Option = "drts",
                     int MinTime_ns = 1000, int MaxTime_ns = 2000,
                     const std::string& OutputFileName = "", bool isMeVtoGeV = true)
```

`const std::string& Option` should be "d" (`Digit`*`LSB`) , "r" (`RecEt`), "s"(`SelectedEt`),"t" (`TrueEt`) or a combination of them such as "dtr".

#### Example

```cpp
#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
int main()
{
    PLISMAnalysis::Plotter p{
        "../../storage/ReconstructedSequences/"
        "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_"
        "tau-"
        "12.root",
    };

    p.TH2DPlotter(std::string{"EtResolution"}, std::strin{"TauResolution"},
                  100, -0.15, 0.0,  
                  100, -0.5, 0.5,  
                  false, std::string{"./figure/sample_TH2D.pdf"});

    p.ScatterPlotter(std::string{"RecTau"}, std::string{"RecEt"},  
                     -30, 30,  
                     -20, 100,  
                     false, std::string{"./figure/sample_Scatter.pdf"});

    p.SequencePlotter(std::string{"drts"}, 
                      1000, 2000,
                      std::string{"./figure/sample_Sequence.pdf"}, true);
    return 0;
}
```

- sample_TH2D.png
![image](./test/figure/sample_TH2D.png)
- sample_Scatter.png
![image](./test/figure/sample_Scatter.png)
- sample_Sequnece.png
![image](./test/figure/sample_Sequence.png)
