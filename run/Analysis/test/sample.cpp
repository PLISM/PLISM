#include <string>

#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
int main()
{
    const std::vector<std::string> InputFilenames{
        "../../storage/ReconstructedSequences/"
        "reconstructed_968435712_athena_mu80_100000BC_allBC_each50BC_et50_"
        "tau-"
        "12.root"};
    PLISMAnalysis::Plotter p{InputFilenames};

    p.TH2DPlotter(0, std::string{"EtResolution"}, std::string{"TauResolution"},
                  100, -0.15, 0.0, 100, -0.5, 0.5, false,
                  std::string{"./figure/sample_TH2D.pdf"});
    p.TH2DPlotterALL(0, true,
                     std::string{"./figure/sample_TH2DPlotterALL.pdf"});
    p.ScatterPlotter(std::string{"RecTau"}, std::string{"RecEt"}, -30, 30, -20,
                     100, false, std::string{"./figure/sample_Scatter.pdf"});
    p.SequencePlotter(std::string{"drts"}, 1000, 5000,
                      std::string{"./figure/sample_Sequence.pdf"}, true);

    return 0;
}
