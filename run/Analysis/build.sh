#!/bin/sh

function build () {
    if [ ! -e $1 ] ; then
        mkdir $1
    fi

    cd $1

    if [ ! -e build/Makefile ]; then
        cmake ..
    fi

    make
}

for Dir in AtlasStyle DataBase Generator Plotter
do
    cd $LAR_SIMLATOR_MAIN_DIR/run/Analysis/${Dir}/
    build $LAR_SIMLATOR_MAIN_DIR/run/Analysis/${Dir}/build
done

cd $LAR_SIMLATOR_MAIN_DIR/run/Analysis/

