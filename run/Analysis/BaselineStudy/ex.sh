#!/bin/bash

clang++ -std=c++11 $1.cpp `root-config --cflags --libs` -I. -L. -lBaselineCorrectedDataBase_lib -lAtlasStyle_lib -lDataBase_lib -lGenerator_lib -lPlotter_lib -o $1_ex && ./$1_ex $2