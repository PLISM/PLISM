#include <TCanvas.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TLine.h>

#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
#include "BaselineCorrectedDataBase/inc/BaselineCorrectedDataBase.hpp"
#include "Function.hpp"

int main(int argc, char const* argv[])
{
    PLISMAnalysis::BaselineCorrectedDataBase d{argv[1]};

    std::vector<TH1I*> vh;
    double ylow{0}, yup{50000};

    const auto Period_BC{d.Periods_BC[0]};
    const auto TotalBC{d.TotalBCs[0]};
    std::string of{"SelectedEt.pdf"};

    // TCanvas
    TCanvas c{"c", ""};
    // Legend
    TLegend leg{0.81, 0.85, 0.87, 0.89};
    leg.SetLineColor(0);
    leg.SetFillColor(0);
    leg.SetTextSize(0.02);
    int count{0};
    for (int InputFileLabel = 0; InputFileLabel < d.getNumInputFiles();
         InputFileLabel++) {
        std::string title{"TrainStructure = " +
                          PLISMAnalysis::toStringTrainPattern(
                              d.TrainPatterns[InputFileLabel]) +
                          ";time [th BC];# of Selected BC / " +
                          std::to_string(static_cast<int>(
                              d.getDigits()[InputFileLabel].size() /
                              d.Periods_BC[InputFileLabel]))};
        auto h = new TH1I{"h1", title.c_str(), Period_BC, 0.0,
                          static_cast<double>(Period_BC)};

        // get Data
        auto&& CorrectedRecEt = d.getBaselineCorrectedRecEts()[InputFileLabel];
        auto&& CorrectedRecEtTau =
            d.getBaselineCorrectedRecEtTaus()[InputFileLabel];
        auto&& CorrectedRecTau =
            d.getBaselineCorrectedRecTaus()[InputFileLabel];

        for (long long BC = 0; BC < Period_BC; BC++) {
            for (long long _BC = BC; _BC < CorrectedRecEt.size();
                 _BC += Period_BC) {
                if (PLISMAnalysis::TauCriteria(CorrectedRecTau[_BC],
                                               CorrectedRecEt[_BC])) {
                    h->Fill(BC);
                    count++;
                }
            }
        }
      
        yup = std::max(yup, h->GetMaximum());
        yup += 0.05 * abs(yup);
        vh.push_back(h);
        leg.AddEntry(
            h,
            ("#mu = " + std::to_string(static_cast<int>(d.Mus[InputFileLabel])))
                .c_str(),
            "l");
    }

    for (int i = 0; i < vh.size(); i++) {
        vh[i]->SetMaximum(yup);
        vh[i]->SetMinimum(ylow);
        vh[i]->SetStats(0);
        vh[i]->SetFillColor(i + 2);
        vh[i]->SetLineColor(i + 2);
        if (i == 0) {
            vh[i]->Draw("");
        } else {
            vh[i]->Draw("same");
        }
    }
    // Tline
    int t{0};
    std::vector<TLine> vtl;
    for (int i = 0; i < d.TrainPatterns[0].size(); i++) {
        t += d.TrainPatterns[0][i];
        vtl.emplace_back(t, ylow, t, yup);
    }
    for (auto& tl : vtl) {
        tl.SetLineStyle(3);
        tl.Draw();
    }
    gPad->SetLeftMargin(0.2);
    leg.Draw();
    c.RedrawAxis();
    c.Print(of.c_str());
    c.Close();
}
