#include <TCanvas.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TLine.h>

#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
#include "BaselineCorrectedDataBase/inc/BaselineCorrectedDataBase.hpp"
#include "Function.hpp"

int main(int argc, char const* argv[])
{
    PLISMAnalysis::BaselineCorrectedDataBase d{argv[1]};

    for (unsigned int InputFileLabel = 0; InputFileLabel < d.getNumInputFiles();
         InputFileLabel++) {
        std::string title{
            "#mu = " + std::to_string(static_cast<int>(d.Mus[InputFileLabel])) +
            ", Train = " +
            PLISMAnalysis::toStringTrainPattern(
                d.TrainPatterns[InputFileLabel]) +
            "; time [BC];Average of E_{T} [MeV]"};
        std::string of{"AverageEt.pdf"};

        const auto Period_BC{d.Periods_BC[InputFileLabel]};
        const auto TotalBC{d.TotalBCs[InputFileLabel]};

        // for read file
        auto&& fin = d.InputFiles[InputFileLabel];
        TTreeReader reader{d.InputFileTypes[InputFileLabel].c_str(), fin};
        TTreeReaderValue<std::deque<double>> AnalogEt{reader, "AnalogEt"};
        TTreeReaderValue<Float_t> LSB{reader, "LSB"};
        reader.Next();

        // get Data
        auto&& DigitAve = PLISMAnalysis::PeriodicAverage(
            d.getDigits()[InputFileLabel], Period_BC);
        auto&& CorrectedDigitAve = PLISMAnalysis::PeriodicAverage(
            d.getBaselineCorrectedDigits()[InputFileLabel], Period_BC);
        auto&& CorrectedRecEtAve = PLISMAnalysis::PeriodicAverage(
            d.getBaselineCorrectedRecEts()[InputFileLabel], Period_BC);
        auto&& CorrectedSelectedEtAve =
            PLISMAnalysis::SelectedEtPeriodicAverage(
                d.getBaselineCorrectedRecTaus()[InputFileLabel],
                d.getBaselineCorrectedRecEts()[InputFileLabel], Period_BC);

        ///// Plot //////////////////////////////////////////////////////
        TCanvas c{"c", "c"};
        TGraph* g1 = new TGraph{};
        TGraph* g2 = new TGraph{};
        TGraph* g3 = new TGraph{};
        TGraph* g4 = new TGraph{};
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};
        double xlow{0.0};
        double xup{static_cast<double>(Period_BC) - 1.};
        for (long long BC = 0; BC < Period_BC; BC++) {
            g1->SetPoint(g1->GetN(), BC, DigitAve[BC] * *LSB);
            g2->SetPoint(g2->GetN(), BC, CorrectedDigitAve[BC] * *LSB);
            g3->SetPoint(g3->GetN(), BC, CorrectedRecEtAve[BC]);
            g4->SetPoint(g4->GetN(), BC, CorrectedSelectedEtAve[BC]);

            yup = std::max(
                yup, std::max(DigitAve[BC] * *LSB,
                              std::max(CorrectedDigitAve[BC] * *LSB,
                                       std::max(CorrectedRecEtAve[BC],
                                                CorrectedSelectedEtAve[BC]))));
            ylow = std::min(
                ylow, std::min(DigitAve[BC] * *LSB,
                               std::min(CorrectedDigitAve[BC] * *LSB,
                                        std::min(CorrectedRecEtAve[BC],
                                                 CorrectedSelectedEtAve[BC]))));
        }
        yup += 0.35 * std::abs(yup);
        ylow -= 0.2 * std::abs(ylow);
        g1->GetXaxis()->SetLimits(xlow, xup);
        g1->SetMaximum(yup);
        g1->SetMinimum(ylow);
        g1->SetTitle(title.c_str());
        g1->SetLineColor(kGreen);
        g2->SetLineColor(kRed);
        g3->SetLineColor(kBlue);
        g4->SetLineColor(kOrange);
        g1->SetLineWidth(2.);
        g2->SetLineWidth(2.);
        g3->SetLineWidth(2.);
        g4->SetLineWidth(2.);
        g1->Draw("al");
        g2->Draw("l");
        g3->Draw("l");
        g4->Draw("l");

        // Legend
        TLegend leg{0.20, 0.75, 0.57, 0.85};
        leg.SetLineColor(0);
        leg.SetFillColor(0);
        leg.SetTextSize(0.03);
        leg.AddEntry(g1, "ADC * LSB (Baseline is uncorrected)", "l");
        leg.AddEntry(g2, "ADC * LSB (Baseline is corrected)", "l");

        leg.AddEntry(g3, "Reconstructed E_{T} (Baseline is corrected)", "l");

        leg.AddEntry(g4, "Selected E_{T} (Baseline is corrected)", " l ");

        leg.Draw();

        // Tline
        int t{0};
        std::vector<TLine> vtl;
        for (int i = 0; i < d.TrainPatterns[InputFileLabel].size(); i++) {
            t += d.TrainPatterns[InputFileLabel][i];
            vtl.emplace_back(t - 1, ylow, t - 1, yup);
        }
        for (auto& tl : vtl) {
            tl.SetLineStyle(3);
            tl.Draw();
        }
        c.Print(of.c_str());
        c.Close();
        ///// Plot //////////////////////////////////////////////////////
    }
}
