#pragma once
#include <vector>

namespace PLISMAnalysis
{
    bool TauCriteria(const double Tau_ns, const double Et_MeV)
    {
        if (Et_MeV >= 10000. && (Tau_ns > -8. && Tau_ns < 16.)) return true;
        if (Et_MeV < 10000. && (Tau_ns > -8. && Tau_ns < 8.)) return true;
        return false;
    }
    /*
     bool TauCriteria(const double EtTau_MeVns, const double Et_MeV)
     {
         if (Et_MeV > 10000. &&
             (EtTau_MeVns > -8. * Et_MeV && EtTau_MeVns < 16. * Et_MeV)) {
             return true;
         }
         if (0. <= Et_MeV < 10000. &&
             (EtTau_MeVns > -8. * Et_MeV && EtTau_MeVns < 8. * Et_MeV)) {
             return true;
         }
         if (Et_MeV < 0. &&
             (EtTau_MeVns < -8. * Et_MeV && EtTau_MeVns > 8. * Et_MeV)) {
             return true;
         }
         return false;
     }
     */
    std::vector<double> SelectedEtPeriodicAverage(
        const std::vector<double>& RecTau, const std::vector<double>& RecEt,
        const unsigned int Period_BC)
    {
        if (RecTau.size() != RecEt.size()) {
            std::cerr
                << "Error in SelectedEtPeriodicAverage : RecTau.size() != "
                   "RecEt.size()\n";
        }
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            double sum{0.0};
            int ctr{0};
            for (unsigned int _BC = BC; _BC < RecEt.size(); _BC += Period_BC) {
                if (std::isnan(RecEt[_BC]) || std::isnan(RecTau[_BC])) {
                    continue;
                }
                if (TauCriteria(RecTau[_BC], RecEt[_BC])) {
                    ctr++;
                    sum += RecEt[_BC];
                }
            }
            res[BC] = sum / static_cast<double>(ctr);
        }

        /*
        for (int i = 0; i < 128 / 4; i++) {
            double tmp{0.};
            for (int j = 0; j < 4; j++) { tmp += res[i * 4 + j]; }
            for (int j = 0; j < 4; j++) { res[i * 4 + j] = tmp / 4.; }
        }
        */

        return res;
    }

    std::vector<double> SelectedEtPeriodicRMS(const std::vector<double>& RecTau,
                                              const std::vector<double>& RecEt,
                                              const unsigned int Period_BC)
    {
        if (RecTau.size() != RecEt.size()) {
            std::cerr
                << "Error in SelectedEtPeriodicAverage : RecTau.size() != "
                   "RecEt.size()\n";
        }
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            double sum{0.0}, sum2{0.0};
            int ctr{0};
            for (unsigned int _BC = BC; _BC < RecEt.size(); _BC += Period_BC) {
                if (std::isnan(RecEt[_BC]) || std::isnan(RecTau[_BC])) {
                    continue;
                }
                if (TauCriteria(RecTau[_BC], RecEt[_BC])) {
                    ctr++;
                    sum += RecEt[_BC];
                    sum2 += RecEt[_BC] * RecEt[_BC];
                }
            }
            sum /= static_cast<double>(ctr);
            sum2 /= static_cast<double>(ctr);
            res[BC] = std::sqrt(sum2 - sum * sum);
        }
        return res;
    }

    std::vector<double> SelectedTauPeriodicAverage(
        const std::vector<double>& RecTau, const std::vector<double>& RecEt,
        const unsigned int Period_BC)
    {
        if (RecTau.size() != RecEt.size()) {
            std::cerr
                << "Error in SelectedEtPeriodicAverage : RecTau.size() != "
                   "RecEt.size()\n";
        }
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            double sum{0.0};
            int ctr{0};
            for (unsigned int _BC = BC; _BC < RecEt.size(); _BC += Period_BC) {
                if (std::isnan(RecEt[_BC]) || std::isnan(RecTau[_BC])) {
                    continue;
                }
                if (TauCriteria(RecTau[_BC], RecEt[_BC])) {
                    ctr++;
                    sum += RecTau[_BC];
                }
            }
            res[BC] = sum / static_cast<double>(ctr);
        }
        return res;
    }

    std::vector<double> SelectedTauPeriodicRMS(
        const std::vector<double>& RecTau, const std::vector<double>& RecEt,
        const unsigned int Period_BC)
    {
        if (RecTau.size() != RecEt.size()) {
            std::cerr
                << "Error in SelectedEtPeriodicAverage : RecTau.size() != "
                   "RecEt.size()\n";
        }
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            double sum{0.0}, sum2{0.0};
            int ctr{0};
            for (unsigned int _BC = BC; _BC < RecEt.size(); _BC += Period_BC) {
                if (std::isnan(RecEt[_BC]) || std::isnan(RecTau[_BC])) {
                    continue;
                }
                if (TauCriteria(RecTau[_BC], RecEt[_BC])) {
                    ctr++;
                    sum += RecTau[_BC];
                    sum2 += RecTau[_BC] * RecTau[_BC];
                }
            }
            sum /= static_cast<double>(ctr);
            sum2 /= static_cast<double>(ctr);
            res[BC] = std::sqrt(sum2 - sum * sum);
        }
        return res;
    }

    template <typename T>
    std::vector<double> PeriodicAverage(const T& data,
                                        const unsigned int Period_BC)
    {
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            double sum{0.0};
            int ctr{0};
            for (unsigned int _BC = BC; _BC < data.size(); _BC += Period_BC) {
                if (std::isnan(data[_BC])) {
                    // std::cout << BC << std::endl;
                    continue;
                }
                sum += static_cast<double>(data[_BC]);
                ctr++;
            }
            res[BC] = sum / static_cast<double>(ctr);
        }
        /*
        for (int i = 0; i < 128 / 4; i++) {
            double tmp{0.};
            for (int j = 0; j < 4; j++) { tmp += res[i * 4 + j]; }
            for (int j = 0; j < 4; j++) { res[i * 4 + j] = tmp / 4.; }
        }
        */

        return res;
    }

    template <typename T>
    std::vector<double> PeriodicRMS(const T& data, const unsigned int Period_BC)
    {
        std::vector<double> res(Period_BC);
        for (unsigned int BC = 0; BC < Period_BC; BC++) {
            int ctr{0};
            double sum{0.0}, sum2{0.0};
            for (unsigned int _BC = BC; _BC < data.size();
                 _BC += Period_BC, ctr++) {
                if (std::isnan(data[_BC])) {
                    ctr--;
                    continue;
                }
                sum += static_cast<double>(data[_BC]);
                sum2 += static_cast<double>(data[_BC]) *
                        static_cast<double>(data[_BC]);
            }
            sum /= static_cast<double>(ctr);
            sum2 /= static_cast<double>(ctr);
            res[BC] = std::sqrt(sum2 - sum * sum);
        }
        return res;
    }

    std::string toStringTrainPattern(const std::vector<int> TrainPattern)
    {
        std::string tp{""};
        int ctr{0};
        for (auto it = TrainPattern.begin(); it != TrainPattern.end();
             it++, ctr++) {
            tp += std::to_string(*it);
            if (ctr % 2 == 0) {
                tp += "b";
            } else {
                tp += "e";
            }
        }
        return tp;
    }

}  // namespace PLISMAnalysis