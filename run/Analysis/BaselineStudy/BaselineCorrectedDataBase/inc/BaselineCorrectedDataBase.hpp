#pragma once
#include <vector>

#include "../../../DataBase/inc/DataBase.hpp"

namespace PLISMAnalysis
{
    struct OFC {
        std::vector<double> a{-56.26673, -25.01753, 167.05583, 127.86447};
        std::vector<double> b{2940.9382, -6401.169, -2146.891, 4846.0407};
       /*
        std::vector<double> a{-29.64470, -87.58797, 89.547881, 221.28834};
        std::vector<double> b{2956.3294, -2800.171, -9704.812, 6879.0387};
        */
    };

    class BaselineCorrectedDataBase : public DataBase
    {
      private:
        static inline std::ostream& ProcessMessage(std::ostream& os)
        {
            os << "Process in PLISMAnalysis::BaselineCorrectedDataBase";
            return os;
        }
        static inline std::ostream& ErrorMessage(std::ostream& os)
        {
            os << "Error in PLISMAnalysis::Base";
            return os;
        }
        static inline std::ostream& WarningMessage(std::ostream& os)
        {
            os << "Warning in PLISMAnalysis::Base";
            return os;
        }

        OFC ofc;
        const int NSample;
        std::vector<std::deque<int>> Digits;
        std::vector<std::vector<double>> DigitBaselines;
        std::vector<std::vector<double>> BaselineCorrectedDigits;
        std::vector<std::vector<double>> BaselineCorrectedRecEts;
        std::vector<std::vector<double>> BaselineCorrectedRecTaus;
        std::vector<std::vector<double>> BaselineCorrectedRecEtTaus;

        void SetDigits();
        void SetDigitBaselines();
        void SetBaselineCorrectedDigits();
        void SetBaselineCorrectedRecs();

      public:
        // constructor
        template <typename... Args>
        BaselineCorrectedDataBase(Args... args);

        // getter
        const std::vector<std::deque<int>>& getDigits() const&
        {
            return Digits;
        };
        const std::vector<std::vector<double>>& getDigitBaseline() const&{
            return DigitBaselines;
        };
        const std::vector<std::vector<double>>& getBaselineCorrectedDigits() const&
        {
            return BaselineCorrectedDigits;
        };
        const std::vector<std::vector<double>>& getBaselineCorrectedRecEts() const&
        {
            return BaselineCorrectedRecEts;
        };
        const std::vector<std::vector<double>>& getBaselineCorrectedRecTaus()
            const&
        {
            return BaselineCorrectedRecTaus;
        };
        const std::vector<std::vector<double>>& getBaselineCorrectedRecEtTaus()
            const&
        {
            return BaselineCorrectedRecEtTaus;
        };
    };
}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    template <typename... Args>
    BaselineCorrectedDataBase::BaselineCorrectedDataBase(Args... args)
        : DataBase(args...), NSample(4)
    {
        std::cout << ProcessMessage << ": Setting Digit.\n";
        SetDigits();
        std::cout << ProcessMessage << ": Setting DigitBaselines.\n";
        SetDigitBaselines();
        std::cout << ProcessMessage << ": Setting BaselineCorrectedDigits.\n";
        SetBaselineCorrectedDigits();
        std::cout << ProcessMessage << ": Setting BaselineCorrectedRecs.\n";
        SetBaselineCorrectedRecs();
    }
}  // namespace PLISMAnalysis