import ROOT as root
import gc
root.gROOT.SetBatch(True)
#make input file by PLISM. It chould be divided in area below. Then, specify input file line 62. Input file name should be including to area name below.
area = ['EMB0', 'EMB1', 'EMB2', 'EMB3', 'EMEC0', 'EMEC1', 'EMEC2', 'EMEC3', 'HEC', 'FCal']

wave = 'Phys' #Phys or Cali
sigma = 4 #threshold of amplitude criteira
sigma_lsb = 6 #threshold of LSB criteira
data = 'BarrelEndcapWeekly_240416-004831' #Whatever you can recognise the data. This will be printed at the first line of output .txt files.
inputlist = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/BadChannelList_total.txt' #output of OFCqualitycheck.py
fout_txt = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/test/BadDetail_{wave}.txt'
bad_amp_hist_pdf = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/test/Bad_AmpHist_{wave}.pdf'
bad_lsb_hist_pdf = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/test/Bad_LSBHist_{wave}.pdf'
bad_ofc_hist_pdf = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/test/Bad_OFCHist_{wave}.pdf'

#make bad SC list.
knownBads = []
with open(inputlist) as fin:
    lines = fin.readlines()
    for i in range(3,len(lines)):
        line = lines[i]
        value = line.split()
        knownBads.append(int(value[1]))
fin.close()
print(f"{len(knownBads)} SCs are scanned.")


#Bad Flug on LAr ID translator dictionary
dFlug = {}
fin = root.TFile('./rootfiles/SCdict_DBtest.root','read')
tin = fin.Get("treeSCcounts")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("channelId",1)
tin.SetBranchStatus("sc_badtext_UPD1",1)
tin.SetBranchStatus("sc_badtext_UPD1_cells",1)
nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    dFlug[tin.channelId] = [str(tin.sc_badtext_UPD1).replace(' ',''), str(tin.sc_badtext_UPD1_cells).replace(' ','')]
fin.Close()

#ready for output .txt files
f = open(fout_txt, 'w')
f.write('input data : '+ data + '\n')
#f.write(f'Threshold {sigma}\u03b4 for Amplitude, {sigma}\u03b4 for LSB\n')
f.write(f'Threshold {sigma}sigma for Amplitude, {sigma}sigma for LSB\n')
f.write('layer channelId    eta     phi   MeanRecTau  RMSRecTau  MeanReldifEt  RMSReldifEt   LSB   Pedestal reason\n')

c = root.TCanvas("hamp","hamp")
c.Print(bad_amp_hist_pdf+"[")
c.Print(bad_lsb_hist_pdf+"[")
c.Print(bad_ofc_hist_pdf+"[")

#main function
for m in area:
    print(f'area= {m}')

    fin = root.TFile(f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/RecoAccu{wave}_{m}.root')
    tin = fin.Get('RecoAccuracy')
    nEntries = tin.GetEntries()
    
    #make eta dictionary
    etadict = {}
    etalist = []
    for i in range(0, nEntries):
        tin.GetEntry(i)
        eta = tin.eta
        if eta not in etalist:
            etalist.append(eta)
            etadict[eta] = etalist.index(eta)

    #Hists to calculate criteria thresholds 
    hslope = root.TH1D(f'{m}_slope', f'{m}_slope', 50, 0, 0)
    hamp = [root.TH1D(f'{m}_amplitude_{j}', f'{m}_amplitude_{j}', 1000, 0, 3000) for j in range(len(etalist))]
    hLSB = [root.TH1D(f'{m}_LSB_{j}', f'{m}_LSB_{j}', 700, 0, 700) for j in range(len(etalist))]
    
    hOFCa0 = [root.TH1D(f'a0_{j}', f'a0_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa1 = [root.TH1D(f'a1_{j}', f'a1_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa2 = [root.TH1D(f'a2_{j}', f'a2_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa3 = [root.TH1D(f'a3_{j}', f'a3_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb0 = [root.TH1D(f'b0_{j}', f'b0_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb1 = [root.TH1D(f'b1_{j}', f'b1_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb2 = [root.TH1D(f'b2_{j}', f'b2_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb3 = [root.TH1D(f'b3_{j}', f'b3_{j}', 100, 0, 0) for j in range(len(etalist))]

    for i in range(0, nEntries):
        tin.GetEntry(i)
        #if tin.channelId in knownBads:continue
        pulse = tin.gFunction[0]
        eta = tin.eta

        #OFC criteria threshold
        hOFCa0[etadict[eta]].Fill(tin.OFCa[0])
        hOFCa1[etadict[eta]].Fill(tin.OFCa[1])
        hOFCa2[etadict[eta]].Fill(tin.OFCa[2])
        hOFCa3[etadict[eta]].Fill(tin.OFCa[3])
        hOFCb0[etadict[eta]].Fill(tin.OFCb[0])
        hOFCb1[etadict[eta]].Fill(tin.OFCb[1])
        hOFCb2[etadict[eta]].Fill(tin.OFCb[2])
        hOFCb3[etadict[eta]].Fill(tin.OFCb[3])

        #calculate pulse slope threshold
        N = len(pulse)-2
        difflist = []
        for n in range(N):
            center = pulse[n+1]
            diff1 = center - pulse[n]
            diff2 = pulse[n+2] - center
            diff3 = diff2-diff1
            difflist.append(abs(diff3))
       
        hslope.Fill(max(difflist))
        hamp[etadict[eta]].Fill(max(pulse))
        hLSB[etadict[eta]].Fill(tin.LSB)

    #THRESHOLD
    #pulse slope threshold
    th_slope = 2*hslope.GetStdDev() + hslope.GetMean()

    #OFC threshold
    th_OFCa_dict = {}
    th_OFCb_dict = {}

    #amplitude threshold
    thMax_ampdict = {}
    thMin_ampdict = {}

    #LSB threshold
    thMax_LSBdict = {}
    thMin_LSBdict = {}

    for i in range(len(etadict)):
        keys = [key for key, value in etadict.items() if value == i]
        key = keys[0]

        # OFC threshold
        th_OFCa_dict[key] = [hOFCa0[i].GetMean(), hOFCa0[i].GetStdDev(),  hOFCa1[i].GetMean(), hOFCa1[i].GetStdDev(), hOFCa2[i].GetMean(), hOFCa2[i].GetStdDev(), hOFCa3[i].GetMean(), hOFCa3[i].GetStdDev()]
        th_OFCb_dict[key] = [hOFCb0[i].GetMean(), hOFCb0[i].GetStdDev(), hOFCb1[i].GetMean(), hOFCb1[i].GetStdDev(), hOFCb2[i].GetMean(), hOFCb2[i].GetStdDev(), hOFCb3[i].GetMean(), hOFCb3[i].GetStdDev()]
        #Amplitde threshold
        h_amp = hamp[i]
        hmean = h_amp.GetMean()
        hstd = h_amp.GetStdDev()
        f1 = root.TF1("function", "gaus", hmean - 2*hstd, hmean + 2*hstd)
        h_amp.Fit(f1, "RL")
        Std = f1.GetParameter(2)
        Mean = f1.GetParameter(1)
        maxth = Mean + sigma*Std
        minth = Mean - sigma*Std
        thMax_ampdict[key] = maxth
        thMin_ampdict[key] = minth

        #LSB threshold
        h_lsb = hLSB[i]
        hlmean = h_lsb.GetMean()
        hlstd = h_lsb.GetStdDev()
        f2 = root.TF1("function", "gaus", hlmean - 2*hlstd, hlmean + 2*hlstd)
        h_lsb.Fit(f2, "RL")
        Std = f2.GetParameter(2)
        Mean = f2.GetParameter(1)
        maxth = Mean + sigma_lsb*Std
        minth = Mean - sigma_lsb*Std
        thMax_LSBdict[key] = maxth
        thMin_LSBdict[key] = minth

    outhistlist = []
    outhistlsblist = []
    for i in range(0, nEntries):
        tin.GetEntry(i)
        if tin.channelId not in knownBads:continue
        pulse = tin.gFunction[0]
        time = tin.time[0]
        eta = tin.eta
        #pickup OFC threshold
        th_OFCa = th_OFCa_dict[eta]
        th_OFCb = th_OFCb_dict[eta]
        th_OFCa0_5max = th_OFCa[0]+5*th_OFCa[1]
        th_OFCa0_5min = th_OFCa[0]-5*th_OFCa[1]
        th_OFCb0_5max = th_OFCb[0]+5*th_OFCb[1]
        th_OFCb0_5min = th_OFCb[0]-5*th_OFCb[1]
        th_OFCa0_6max = th_OFCa[0]+6*th_OFCa[1]
        th_OFCa0_6min = th_OFCa[0]-6*th_OFCa[1]
        th_OFCb0_6max = th_OFCb[0]+6*th_OFCb[1]
        th_OFCb0_6min = th_OFCb[0]-6*th_OFCb[1]
        th_OFCa0_7max = th_OFCa[0]+7*th_OFCa[1]     
        th_OFCa0_7min = th_OFCa[0]-7*th_OFCa[1]
        th_OFCb0_7max = th_OFCb[0]+7*th_OFCb[1]
        th_OFCb0_7min = th_OFCb[0]-7*th_OFCb[1]
        th_OFCa1_5max = th_OFCa[2]+5*th_OFCa[3]
        th_OFCa1_5min = th_OFCa[2]-5*th_OFCa[3]
        th_OFCb1_5max = th_OFCb[2]+5*th_OFCb[3]
        th_OFCb1_5min = th_OFCb[2]-5*th_OFCb[3]
        th_OFCa1_6max = th_OFCa[2]+6*th_OFCa[3]
        th_OFCa1_6min = th_OFCa[2]-6*th_OFCa[3]
        th_OFCb1_6max = th_OFCb[2]+6*th_OFCb[3]
        th_OFCb1_6min = th_OFCb[2]-6*th_OFCb[3]
        th_OFCa1_7max = th_OFCa[2]+7*th_OFCa[3]     
        th_OFCa1_7min = th_OFCa[2]-7*th_OFCa[3]
        th_OFCb1_7max = th_OFCb[2]+7*th_OFCb[3]
        th_OFCb1_7min = th_OFCb[2]-7*th_OFCb[3]
        th_OFCa2_5max = th_OFCa[4]+5*th_OFCa[5]
        th_OFCa2_5min = th_OFCa[4]-5*th_OFCa[5]
        th_OFCb2_5max = th_OFCb[4]+5*th_OFCb[5]
        th_OFCb2_5min = th_OFCb[4]-5*th_OFCb[5]
        th_OFCa2_6max = th_OFCa[4]+6*th_OFCa[5]
        th_OFCa2_6min = th_OFCa[4]-6*th_OFCa[5]
        th_OFCb2_6max = th_OFCb[4]+6*th_OFCb[5]
        th_OFCb2_6min = th_OFCb[4]-6*th_OFCb[5]
        th_OFCa2_7max = th_OFCa[4]+7*th_OFCa[5]     
        th_OFCa2_7min = th_OFCa[4]-7*th_OFCa[5]
        th_OFCb2_7max = th_OFCb[4]+7*th_OFCb[5]
        th_OFCb2_7min = th_OFCb[4]-7*th_OFCb[5]
        th_OFCa3_5max = th_OFCa[6]+5*th_OFCa[7]
        th_OFCa3_5min = th_OFCa[6]-5*th_OFCa[7]
        th_OFCb3_5max = th_OFCb[6]+5*th_OFCb[7]
        th_OFCb3_5min = th_OFCb[6]-5*th_OFCb[7]
        th_OFCa3_6max = th_OFCa[6]+6*th_OFCa[7]
        th_OFCa3_6min = th_OFCa[6]-6*th_OFCa[7]
        th_OFCb3_6max = th_OFCb[6]+6*th_OFCb[7]
        th_OFCb3_6min = th_OFCb[6]-6*th_OFCb[7]
        th_OFCa3_7max = th_OFCa[6]+7*th_OFCa[7]     
        th_OFCa3_7min = th_OFCa[6]-7*th_OFCa[7]
        th_OFCb3_7max = th_OFCb[6]+7*th_OFCb[7]
        th_OFCb3_7min = th_OFCb[6]-7*th_OFCb[7]
        
        Bad = "no"
        maxpulse = max(pulse)
        LSB = tin.LSB
        reason1 = ""
        reason2 = ""
        reason3 = ""
        reason4 = ""
        reason5 = ""
        reason6 = ""
        OK = False
        #pulse slope
        N = len(pulse)-2
        for n in range(N):
            center = pulse[n+1]
            diff1 = center - pulse[n]
            diff2 = pulse[n+2] - center
            diff3 = abs(diff2-diff1)

            if diff3 > 90 and n <= 100:
                Bad = "yes"
                reason4 = f'BadSlope({round(25/24*(n+1),2)}ns)_'
            elif diff3 > th_slope and n > 100:
                Bad = "yes"
                reason4 = f'BadSlope({round(25/24*(n+1),2)}ns)_'
        
        #Undershoot criteria
        under = min(pulse)
        if maxpulse < abs(under) or under > 0:
            Bad = "undershoot"
            reason6 = f"BadUnder({round((abs(under)/maxpulse),2)*100}%)"
        elif m != 'FCal' and (maxpulse/2 < abs(under) or under > 0):
            Bad = "undershoot"
            reason6 = f"BadUnder({round((abs(under)/maxpulse),2)*100}%)"

        # Peak Amplitude criteria
        gap = (thMax_ampdict[eta]-thMin_ampdict[eta])*0.2
        if m == 'EMEC0' and abs(eta) == 1.5499999523162842:
            if not (400 < maxpulse < 600 or 700 < maxpulse < 900):
                Bad = "amp"
                outhistlist.append(eta)
                reason2 = f'BadAmp({round(maxpulse,2)})_'
                if 360 < maxpulse < 640 or 660 < maxpulse < 940:
                    OK = True
                    reason2 = f'OKAmp({round(maxpulse,2)})_'
        elif m == 'EMEC3' and abs(eta) == 1.5499999523162842:
            if not (700 < maxpulse < 900 or 1500 <  maxpulse < 1700):
                Bad = "amp"
                outhistlist.append(eta)
                reason2 = f'BadAmp({round(maxpulse,2)})_'
                if 660 < maxpulse < 940 or 1460 < maxpulse < 1740:
                    OK = True
                    reason2 = f'OKAmp({round(maxpulse,2)})_'
                
        elif maxpulse < thMin_ampdict[eta] and m != "FCal":
                Bad = "amp"
                reason2 = f'LowAmp({round(maxpulse,2)})_'
                outhistlist.append(eta)
                if thMin_ampdict[eta]-gap < maxpulse:
                    OK = True
                    reason2 = f'OKLowAmp({round(maxpulse,2)})_'
        
        elif maxpulse > thMax_ampdict[eta] and m != "FCal":
                Bad = "amp"
                reason2 = f'HighAmp({round(maxpulse,2)})_'
                outhistlist.append(eta)
                if maxpulse < thMax_ampdict[eta]+gap:
                    OK = True
                    reason2 = f'OKHighAmp({round(maxpulse,2)})_'

        # LSB criteira
        if m == 'EMEC0' and abs(eta) == 1.5499999523162842:
            if not (280 < LSB < 370 or 450 < LSB < 570):
                Bad = "LSB"
                reason5 = f'BadLSB_'
                outhistlsblist.append(eta)
        elif m == 'EMEC3' and abs(eta) == 1.5499999523162842:
            if not (110 < LSB < 170 or 250 <  LSB < 320):
                Bad = "LSB"
                reason5 = f'BadLSB_'
                outhistlsblist.append(eta)

        elif LSB < thMin_LSBdict[eta] and m != "FCal":
                Bad = "LSB"
                reason5 = 'LowLSB_'
                outhistlsblist.append(eta)

        elif LSB > thMax_LSBdict[eta] and m != "FCal":
                Bad = "LSB"
                reason5 = 'HighLSB_'
                outhistlsblist.append(eta)

        # OFC criteria       
        if not (th_OFCa0_5min < tin.OFCa[0] < th_OFCa0_5max and th_OFCa1_5min < tin.OFCa[1] < th_OFCa1_5max and th_OFCa2_5min < tin.OFCa[2] < th_OFCa2_5max and th_OFCa3_5min < tin.OFCa[3] < th_OFCa3_5max and th_OFCb0_5min < tin.OFCb[0] < th_OFCb0_5max and th_OFCb1_5min < tin.OFCb[1] < th_OFCb1_5max and th_OFCb2_5min < tin.OFCb[2] < th_OFCb2_5max and th_OFCb3_5min < tin.OFCb[3] < th_OFCb3_5max) and (m != "FCal"):
            if not (th_OFCa0_7min < tin.OFCa[0] < th_OFCa0_7max and th_OFCa1_7min < tin.OFCa[1] < th_OFCa1_7max and th_OFCa2_7min < tin.OFCa[2] < th_OFCa2_7max and th_OFCa3_7min < tin.OFCa[3] < th_OFCa3_7max and th_OFCb0_7min < tin.OFCb[0] < th_OFCb0_7max and th_OFCb1_7min < tin.OFCb[1] < th_OFCb1_7max and th_OFCb2_7min < tin.OFCb[2] < th_OFCb2_7max and th_OFCb3_7min < tin.OFCb[3] < th_OFCb3_7max):
                Bad = "OFC"
                reason3 = 'BadOFC(7sigma)_'
                OK = False
                ''' under developping
                label7 = []
                n7 = 0
                if not (th_OFCa0_7min < tin.OFCa[0] < th_OFCa0_7max):
                '''
            else:
                label5 = []
                label6 = []
                n5 = 0
                n6 = 0
                if not (th_OFCa0_6min < tin.OFCa[0] < th_OFCa0_6max):
                    label6.append('a0')
                    n6 += 1
                if not (th_OFCa1_6min < tin.OFCa[1] < th_OFCa1_6max):
                    label6.append('a1')
                    n6 += 1
                if not (th_OFCa2_6min < tin.OFCa[2] < th_OFCa2_6max):
                    label6.append('a1')
                    n6 += 1
                if not (th_OFCa3_6min < tin.OFCa[3] < th_OFCa3_6max):
                    label6.append('a3')
                    n6 += 1
                if not (th_OFCb0_6min < tin.OFCb[0] <th_OFCb0_6max):
                    label6.append('b0')
                    n6 += 1
                if not (th_OFCb1_6min < tin.OFCb[1] < th_OFCb1_6max):
                    label6.append('b1')
                    n6 += 1
                if not (th_OFCb2_6min < tin.OFCb[2] < th_OFCb2_6max):
                    label6.append('b2')
                    n6 += 1
                if not (th_OFCb3_6min < tin.OFCb[3] < th_OFCb3_6max):
                    label6.append('b3')
                    n6 += 1
                if not (th_OFCa0_5min < tin.OFCa[0] < th_OFCa0_5max):
                    label5.append('a0')
                    n5 += 1
                if not (th_OFCa1_5min < tin.OFCa[1] < th_OFCa1_5max):
                    label5.append('a1')
                    n5 += 1
                if not (th_OFCa2_5min < tin.OFCa[2] < th_OFCa2_5max):
                    label5.append('a2')
                    n5 += 1
                if not (th_OFCa3_5min < tin.OFCa[3] < th_OFCa3_5max):
                    label5.append('a3')
                    n5 += 1
                if not (th_OFCb0_5min < tin.OFCb[0] <th_OFCb0_5max):
                    label5.append('b0')
                    n5 += 1
                if not (th_OFCb1_5min < tin.OFCb[1] < th_OFCb1_5max):
                    label5.append('b1')
                    n5 += 1
                if not (th_OFCb2_5min < tin.OFCb[2] < th_OFCb2_5max):
                    label5.append('b2')
                    n5 += 1
                if not (th_OFCb3_5min < tin.OFCb[3] < th_OFCb3_5max):
                    label5.append('b3')
                    n5 += 1
                if n6 > 3:
                    Bad = "OFC"
                    OK = False
                    reason3 = "BadOFC(6sigma)*"+str(n6) + ','.join(label6)
                elif n5 > 4:
                    Bad = "OFC"
                    OK = False
                    reason3 = "BadOFC(5sigma)*"+str(n5) + ','.join(label5)

        # Mean, RMS, pesADC criteria 
        if abs(tin.MeanRecTau) >=0.1 or abs(tin.RMSRecTau) >= 0.2 or abs(tin.MeanReldiffRecTrueEt) >= 0.01 or abs(tin.RMSReldiffRecTrueEt) >= 0.01 or tin.pedADC == 0:
                Bad = "yes"
                reason1 = '_Mean&RMS'
                OK = False
        
        #check LAr ID translator
        UPD = dFlug[tin.channelId]

        #output bad channel's pulse
        if Bad != "no":
            if UPD[0] != "" or UPD[1] != "":
                if OK == False:
                    f.write('{:^6s}{:<11d}{:< 8.4f}{:< 8.4f}{:< 12.6f}{:< 11.6f}{:< 14.6f}{:< 13.6f}{:<7.2f}{:<9.2f}'.format(m, tin.channelId, tin.eta, tin.phi,tin.MeanRecTau,tin.RMSRecTau,tin.MeanReldiffRecTrueEt,tin.RMSReldiffRecTrueEt,tin.LSB,tin.pedADC)+reason4+reason2+reason5+reason3+reason6+reason1+"_"+UPD[0]+':'+UPD[1]+'\n')
                else:
                    f.write('{:^6s}{:<11d}{:< 8.4f}{:< 8.4f}{:< 12.6f}{:< 11.6f}{:< 14.6f}{:< 13.6f}{:<7.2f}{:<9.2f}'.format(m, tin.channelId, tin.eta, tin.phi,tin.MeanRecTau,tin.RMSRecTau,tin.MeanReldiffRecTrueEt,tin.RMSReldiffRecTrueEt,tin.LSB,tin.pedADC)+reason4+reason2+reason5+reason3+reason6+reason1+"_"+UPD[0]+':'+UPD[1]+'\n')
            elif OK == False:
                f.write('{:^6s}{:<11d}{:< 8.4f}{:< 8.4f}{:< 12.6f}{:< 11.6f}{:< 14.6f}{:< 13.6f}{:<7.2f}{:<9.2f}'.format(m, tin.channelId, tin.eta, tin.phi,tin.MeanRecTau,tin.RMSRecTau,tin.MeanReldiffRecTrueEt,tin.RMSReldiffRecTrueEt,tin.LSB,tin.pedADC)+reason4+reason2+reason5+reason3+reason6+reason1+'\n')
            else:
                f.write('{:^6s}{:<11d}{:< 8.4f}{:< 8.4f}{:< 12.6f}{:< 11.6f}{:< 14.6f}{:< 13.6f}{:<7.2f}{:<9.2f}'.format(m, tin.channelId, tin.eta, tin.phi,tin.MeanRecTau,tin.RMSRecTau,tin.MeanReldiffRecTrueEt,tin.RMSReldiffRecTrueEt,tin.LSB,tin.pedADC)+reason4+reason2+reason5+reason3+reason6+reason1+'_REMOVAL\n')
    
    if m == 'EMB0':title = 'EMB Presampler'
    elif m == 'EMB1':title = 'EMB Front'
    elif m == 'EMB2':title = 'EMB Middle'
    elif m == 'EMB3':title = 'EMB Back'
    elif m == 'EMEC0':title = 'EMEC Presampler'
    elif m == 'EMEC1':title = 'EMEC Front'
    elif m == 'EMEC2':title = 'EMEC Middle'
    elif m == 'EMEC3':title = 'EMEC Back'
    elif m == 'HEC':title = 'HEC'
    elif m == 'FCal':title = 'FCal'
    else:
        print('!!!CAUTION!!! this area is not in the list')
        title = 'Not exist'
    Outhistlist = set(outhistlist)
    c = root.TCanvas('hamp','hamp')
    for eta in Outhistlist:
        h_amp = hamp[etadict[eta]]
        amp_mean = h_amp.GetMean() 
        amp_StdDev = h_amp.GetStdDev() 
        h_amp.SetTitle('Amplitude '+title + ' #eta=' + str(round(eta,4))+';Amplitude [ADC counts] (pedestal subtracted);Number of SCs')
        root.gStyle.SetOptStat(1000111101)
        h_amp.Draw()
        c.Print(bad_amp_hist_pdf)
    Outhistlist = set(outhistlsblist)
    c = root.TCanvas('hLSB','hLSB')
    for eta in Outhistlist:
        h_lsb = hLSB[etadict[eta]]
        h_lsb.SetTitle('LSB distribution '+title + ' #eta=' + str(round(eta,4))+';LSB;Number of SCs')
        root.gStyle.SetOptStat(1000111101)
        h_lsb.Draw()
        c.Print(bad_lsb_hist_pdf)
    fin.Close()

c.Print(bad_amp_hist_pdf+"]")
c.Print(bad_lsb_hist_pdf+"]")
f.close()
