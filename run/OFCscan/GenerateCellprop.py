import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule
args = sys.argv
#execute this file like "python3 GenerateCellprop.py EMB0 Phys"
#command line arguments should be EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal
area = args[1]

#set input file names.
if area == 'EMB0' or area == 'EMB1' or area == 'EMB2' or area == 'EMB3':
    LArIDtranslatorList = "./rootfiles/LArIDtranslator_Barrel.txt"
else:
    LArIDtranslatorList = "./rootfiles/LArIDtranslator_Endcap.txt"
DAC2MeVFileName = './rootfiles/LArDAC2MeV.root'
PedFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/LArPedAutoCorr_00473386.root'
inputCaliWavefilename = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/CaliWave_{area}.root' #input CALIWAVE file
inputPhysWavefilename = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/PhysWave_{area}.root' #input PHYSWAVE&MPMC file
LSBcalifileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/lsb_CaliWave_{area}.root'
outputLSBphysName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/lsb_PhysWave_{area}.root' #output LSBphys file

CaliWaveTree = CellPropertyGeneratorModule.CaliWaveTree(inputCaliWavefilename)
CaliWaveTree.ID_SCID_CL_fileName = LArIDtranslatorList
CaliWaveTree.DAC2MeVFileName = DAC2MeVFileName
CaliWaveTree.LSB_delayPeak() 
CaliWaveTree.PedFileName = PedFileName
CaliWaveTree.PulseTree() 

LSBTree = CellPropertyGeneratorModule.LSBTree(LSBcalifileName)
LSBTree.OutputLSBphysName = outputLSBphysName
LSBTree.LSBphys(inputPhysWavefilename)  

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/pulse_CaliWave_{area}.root'
)
PulseShapeTree.PedAlreadySubtracted = True

PulseShapeTree.LSBFileName = LSBcalifileName
PulseShapeTree.BCShift = 0 
PulseShapeTree.GenerateCellProperty()

PhysWaveTree = CellPropertyGeneratorModule.PhysWaveTree(inputPhysWavefilename)
PhysWaveTree.ID_SCID_CL_fileName = LArIDtranslatorList
PhysWaveTree.DAC2MeVFileName = DAC2MeVFileName
PhysWaveTree.PedFileName = PedFileName
PhysWaveTree.InputMPMCFileName = inputPhysWavefilename
PhysWaveTree.PulseTree(LSBcalifileName)

PulsephysShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/pulse_PhysWave_{area}.root'
)
PulsephysShapeTree.PedAlreadySubtracted = True
PulsephysShapeTree.LSBFileName = outputLSBphysName
PulsephysShapeTree.BCShift = 0 
PulsephysShapeTree.GenerateCellProperty()
