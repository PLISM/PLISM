#include <TStyle.h>
#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <map>
#include <sstream>
#include <vector>
//execute this file like; root 'DvidePhysTree.cxx("EMEC3")'
//command line argument should be EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal
using namespace std;


struct SC{
    Short_t det, lay;
};

void DvidePhysTree(TString area){
    Int_t detector;
    Int_t layer;
    if (area.Contains("EMB")){
      detector = 0;
      TSubString layer_tsub = area(area.Length()-1,1);
      layer = TString(layer_tsub).Atoi();
    }
    else if (area.Contains("EMEC")){
      detector = 1;
      TSubString layer_tsub = area(area.Length()-1,1);
      layer = TString(layer_tsub).Atoi();
    }
    else if (area.Contains("HEC")){
      detector = 2;
      layer = 0;
    }
    else{detector = 3;}

    //read dictionary Tree 
    TFile *dictfile = TFile::Open("./rootfiles/SCdict_DBtest.root");
    TTree *dicttree = (TTree *)dictfile->Get("treeSCcounts");
    Int_t channelId;
    Short_t sc_det, sc_layer;

    dicttree->SetBranchAddress("channelId", &channelId);
    dicttree->SetBranchAddress("sc_det", &sc_det);
    dicttree->SetBranchAddress("sc_layer", &sc_layer);
    int entries = dicttree->GetEntries();
    //make map
    map<int, SC> detmap;
    for (unsigned int i = 0; i < entries; i++){
        dicttree->GetEntry(i);
        SC sc_data = {sc_det, sc_layer};
        detmap[channelId] = sc_data;
    }
    dictfile->Close();

    //read PHYSWAVE Tree
    TFile *oldfile = TFile::Open("/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/PhysWave.root");
    TTree *oldphys = (TTree *)oldfile->Get("PHYSWAVE");
    Int_t channelId_in;
    Double_t Time[768], Amplitude[768];

    oldphys->SetBranchAddress("channelId",&channelId_in);
    oldphys->SetBranchAddress("Amplitude",&Amplitude);
    oldphys->SetBranchAddress("Time",&Time);

    // read MPMC Tree
    TTree *oldmpmc = (TTree *)oldfile->Get("MPMC");
    Int_t channelId_min;
    Float_t mphysovermcal_in;
    oldmpmc->SetBranchAddress("channelId",&channelId_min);
    oldmpmc->SetBranchAddress("mphysovermcal",&mphysovermcal_in);

    //make new PHYSWAVE Tree
    TFile *newfile = new TFile("/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/PhysWave_"+area+".root", "recreate");
    newfile->cd();
    TTree *newphys = new TTree("PHYSWAVE", "Physics wave");
    Int_t channelId_out, det_out, layer_out;
    std::vector<Double_t> time_out, amplitude_out;

    newphys->Branch("channelId", &channelId_out);
    newphys->Branch("detector", &det_out);
    newphys->Branch("layer", &layer_out);
    newphys->Branch("Amplitude", &amplitude_out);
    newphys->Branch("Time", &time_out);

    Int_t nEntries = oldphys->GetEntries();
    for (unsigned int i = 0; i < nEntries; i++) {
        oldphys->GetEntry(i);
        const auto& map = detmap[channelId_in];
        int DET = map.det;
        int LAYER = map.lay;
        switch (detector){
          case 0:
          case 1:
          case 2:
              if (DET == detector && LAYER == layer){
                  amplitude_out.clear();
                  amplitude_out.shrink_to_fit();
                  amplitude_out.reserve(768);
                  time_out.clear();
                  time_out.shrink_to_fit();
                  time_out.reserve(768);
                  channelId_out = channelId_in;
                  det_out = DET;
                  layer_out = LAYER;
                  for (const auto& amp : Amplitude) amplitude_out.push_back(amp);
                  for (const auto& time : Time) time_out.push_back(time);
                  newphys->Fill();
                }
                break;
          case 3:
              if (DET == detector){ 
                  amplitude_out.clear();
                  amplitude_out.shrink_to_fit();
                  amplitude_out.reserve(768);
                  time_out.clear();
                  time_out.shrink_to_fit();
                  time_out.reserve(768);
                  channelId_out = channelId_in;
                  det_out = DET;
                  layer_out = LAYER;
                  for (const auto& amp : Amplitude) amplitude_out.push_back(amp);
                  for (const auto& time : Time) time_out.push_back(time);
                  newphys->Fill();
              }
              break;
          default:
              std::cout << "detector " << detector << "is not exist" << std::endl;
              break;
        }
    }

    //make new MPMC Tree
    TTree *newmpmc = new TTree("MPMC", "Mphys over Mcali");
    Int_t channelId_mout;
    Float_t mphysovermcal_out;

    newmpmc->Branch("channelId", &channelId_mout);
    newmpmc->Branch("mphysovermcal", &mphysovermcal_out);
    
    Int_t mEntries = oldmpmc->GetEntries();
    switch (detector){
      case 0:
      case 1:
      case 2:
          for (unsigned int i = 0; i < mEntries; i++){
            oldmpmc->GetEntry(i);
            const auto& map = detmap[channelId_min];
            if (map.det == detector && map.lay == layer && mphysovermcal_in != -999){
                channelId_mout = channelId_min;
                mphysovermcal_out = mphysovermcal_in;
                newmpmc->Fill();
            }
          }
          break;
      case 3:
          for (unsigned int i = 0; i < nEntries; i++){
            oldphys->GetEntry(i);
            const auto& map = detmap[channelId_in];
            if (map.det == detector){
                channelId_mout = channelId_in;
                mphysovermcal_out = 1;
                newmpmc->Fill();
             }
           }
           break;
      default:
          std::cout << "detector " << detector << "is not exist" << std::endl;
          break;
    }
    newfile->cd();
    newphys->Write();
    newmpmc->Write();
    oldfile->Close();
    newfile->Close();
}
