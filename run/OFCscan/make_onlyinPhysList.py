inputCali = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/BadChannelList_Cali.txt' #set output of OFCqualitycheck.py
inputPhys = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/BadChannelList_Phys.txt' #set output of OFCqualitycheck.py
outfile = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/BadChannelList_onlyInPhys.txt'

Cali = []
with open(inputCali) as fin:
    lines = fin.readlines()
    for i in range(3,len(lines)):
        tmp = lines[i].split()
        Cali.append(tmp[1])
fin.close()

fout = open(outfile, 'w')
with open(inputPhys) as fin:
    lines = fin.readlines()
    for i in range(len(lines)):
        if i < 3:
            fout.write(lines[i])
            continue
        tmp = lines[i].split()
        channel = tmp[1]
        if channel not in Cali:fout.write(lines[i])
fin.close()
