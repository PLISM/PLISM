import ROOT as root
import gc
root.gROOT.SetBatch(True)
#make input file by PLISM. It chould be divided in area below. Then, specify input file line 62. Input file name should be including to area name below.
area = ['EMB0', 'EMB1', 'EMB2', 'EMB3', 'EMEC0', 'EMEC1', 'EMEC2', 'EMEC3', 'HEC', 'FCal'] #specify the area you want to check.

wave = 'Phys' #Phys or Cali
sigma = 4 #threshold of amplitude criteira
data = 'BarrelEndcapWeekly_240416-004831' #Whatever you can recognise the data. This will be printed at the first line of output .txt files.
fout = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/MeanRMS{wave}.pdf'
fout_p = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/Pulse_{wave}.pdf'
fout_dp = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/DeformedPulse_{wave}.pdf'
fout_png = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/BadChannels_{wave}.png'
fout_txt = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/BadChannelList_{wave}.txt'
threshold_txt = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/ThresholdsList_{wave}.txt'
bad_amp_hist_pdf = f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/BadAmpHist_{wave}.pdf'

#read LAr ID translator Flag
dFlag = {}
fin = root.TFile('./rootfiles/SCdict_DBtest.root','read')
tin = fin.Get("treeSCcounts")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("channelId",1)
tin.SetBranchStatus("sc_badtext_UPD1",1)
tin.SetBranchStatus("sc_badtext_UPD1_cells",1)
nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    dFlag[tin.channelId] = [str(tin.sc_badtext_UPD1).replace(' ',''), str(tin.sc_badtext_UPD1_cells).replace(' ','')]
fin.Close()

#text obj for printing bad pusles
text = root.TText()
text.SetTextSize(0.045)
text.SetTextFont(42)

#ready for output .txt files
fth = open(threshold_txt, 'w')
fth.write('input data : '+ data + '\n')
fth.write('|MeanRecTau| >= 0.1 |RMSRecTau| >= 0.2 or |Mean (recEt-trueEt)/trueEt| >= 0.01 or |RMS (recEt-trueEt)/trueEt| >= 0.01\n')
fth.write('\n')
f = open(fout_txt, 'w')
f.write('input data : '+ data + '\n')
f.write('\n')
f.write('layer channelId    eta     phi   MeanRecTau  RMSRecTau  MeanReldifEt  RMSReldifEt   LSB   Pedestal reason\n')

c = root.TCanvas("hamp","hamp")
c.Print(bad_amp_hist_pdf+"[")
c = root.TCanvas('pulse','pulse')
c.Print(fout_p + "[")
c.Print(fout_dp + "[")

#main function
for m in area:
    print(f'area= {m}')

    fin = root.TFile(f'/afs/cern.ch/user/m/mfurukaw/storage/{data}/RecoAccu{wave}_{m}.root','read')
    fin.cd()
    tin = fin.Get('RecoAccuracy')
    nEntries = tin.GetEntries()
    
    #make eta dictionary
    etadict = {}
    etalist = []
    for i in range(0, nEntries):
        tin.GetEntry(i)
        eta = tin.eta
        if eta not in etalist:
            etalist.append(eta)
            etadict[eta] = etalist.index(eta)

    #Hists to calculate criteria thresholds 
    hslope = root.TH1D(f'{m}_slope', f'{m}_slope', 50, 0, 0)
    hamp = [root.TH1D(f'{m}_amplitude_{j}', f'{m}_amplitude_{j}', 1000, 0, 3000) for j in range(len(etalist))]
    
    hOFCa0 = [root.TH1D(f'a0_{j}', f'a0_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa1 = [root.TH1D(f'a1_{j}', f'a1_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa2 = [root.TH1D(f'a2_{j}', f'a2_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCa3 = [root.TH1D(f'a3_{j}', f'a3_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb0 = [root.TH1D(f'b0_{j}', f'b0_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb1 = [root.TH1D(f'b1_{j}', f'b1_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb2 = [root.TH1D(f'b2_{j}', f'b2_{j}', 100, 0, 0) for j in range(len(etalist))]
    hOFCb3 = [root.TH1D(f'b3_{j}', f'b3_{j}', 100, 0, 0) for j in range(len(etalist))]

    for i in range(0, nEntries):
        tin.GetEntry(i)
        pulse = tin.gFunction[0]
        eta = tin.eta

        #OFC criteria threshold
        hOFCa0[etadict[eta]].Fill(tin.OFCa[0])
        hOFCa1[etadict[eta]].Fill(tin.OFCa[1])
        hOFCa2[etadict[eta]].Fill(tin.OFCa[2])
        hOFCa3[etadict[eta]].Fill(tin.OFCa[3])
        hOFCb0[etadict[eta]].Fill(tin.OFCb[0])
        hOFCb1[etadict[eta]].Fill(tin.OFCb[1])
        hOFCb2[etadict[eta]].Fill(tin.OFCb[2])
        hOFCb3[etadict[eta]].Fill(tin.OFCb[3])

        #calculate pulse slope threshold
        N = len(pulse)-2
        difflist = []
        for n in range(N):
            center = pulse[n+1]
            diff1 = center - pulse[n]
            diff2 = pulse[n+2] - center
            diff3 = diff2-diff1
            difflist.append(abs(diff3))
       
        hslope.Fill(max(difflist))
        hamp[etadict[eta]].Fill(max(pulse))

    #THRESHOLD
    #pulse slope threshold
    th_slope = 2*hslope.GetStdDev() + hslope.GetMean()

    #OFC threshold
    th_OFCa_dict = {}
    th_OFCb_dict = {}

    #amplitude threshold
    thMax_ampdict = {}
    thMin_ampdict = {}

    for i in range(len(etadict)):
        keys = [key for key, value in etadict.items() if value == i]
        key = keys[0]

        # OFC threshold
        th_OFCa_dict[key] = [hOFCa0[i].GetMean(), hOFCa0[i].GetStdDev(),  hOFCa1[i].GetMean(), hOFCa1[i].GetStdDev(), hOFCa2[i].GetMean(), hOFCa2[i].GetStdDev(), hOFCa3[i].GetMean(), hOFCa3[i].GetStdDev()]
        th_OFCb_dict[key] = [hOFCb0[i].GetMean(), hOFCb0[i].GetStdDev(), hOFCb1[i].GetMean(), hOFCb1[i].GetStdDev(), hOFCb2[i].GetMean(), hOFCb2[i].GetStdDev(), hOFCb3[i].GetMean(), hOFCb3[i].GetStdDev()]

        h_amp = hamp[i]
        hmean = h_amp.GetMean()
        hstd = h_amp.GetStdDev()
        f1 = root.TF1("function", "gaus", hmean - 2*hstd, hmean + 2*hstd)
        h_amp.Fit(f1, "RL")
        Std = f1.GetParameter(2)
        Mean = f1.GetParameter(1)
        maxth = Mean + sigma*Std
        minth = Mean - sigma*Std
        thMax_ampdict[key] = maxth
        thMin_ampdict[key] = minth

    #output thresholds list    
    fth.write(f'Amplitude criteria for {m}\n')
    fth.write('  eta    :  good amplitude range  (min ~ max)\n')
    for Eta, minamp in thMin_ampdict.items():
        fth.write('{:< 8.4f}{:^8.2f}~{:^8.2f}\n'.format(Eta,thMin_ampdict[Eta],thMax_ampdict[Eta]))
    fth.write('\n')
    fth.write(f'OFC criteria for {m}\n')
    fth.write('   eta         0th OFCa               1th OFCa               2th OFCa               3th OFCa                0th OFCb               1th OFCb               2th OFCb                3th OFCb\n')
    fth.write('          Mean       StdDev      Mean       StdDev      Mean       StdDev      Mean       StdDev      Mean       StdDev      Mean       StdDev      Mean       StdDev      Mean       StdDev   \n')
    for Eta,ofca in th_OFCa_dict.items():
        ofcb = th_OFCb_dict[Eta]
        fth.write('{:< 7.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}{:^11.4f}{:^12.4f}\n'.format(Eta,ofca[0],ofca[1],ofca[2],ofca[3],ofca[4],ofca[5],ofca[6],ofca[7],ofcb[0],ofcb[1],ofcb[2],ofcb[3],ofcb[4],ofcb[5],ofcb[6],ofcb[7]))
    fth.write('\n')
    gMT = root.TGraph2D()
    gRT = root.TGraph2D()
    gME = root.TGraph2D()
    gRE = root.TGraph2D()
    outhistlist = []
    outhistlsblist = []
    for i in range(0, nEntries):
        tin.GetEntry(i)
        pulse = tin.gFunction[0]
        time = tin.time[0]
        eta = tin.eta
        #pickup OFC threshold
        th_OFCa = th_OFCa_dict[eta]
        th_OFCb = th_OFCb_dict[eta]
        th_OFCa0_5max = th_OFCa[0]+5*th_OFCa[1]
        th_OFCa0_5min = th_OFCa[0]-5*th_OFCa[1]
        th_OFCb0_5max = th_OFCb[0]+5*th_OFCb[1]
        th_OFCb0_5min = th_OFCb[0]-5*th_OFCb[1]
        th_OFCa0_6max = th_OFCa[0]+6*th_OFCa[1]
        th_OFCa0_6min = th_OFCa[0]-6*th_OFCa[1]
        th_OFCb0_6max = th_OFCb[0]+6*th_OFCb[1]
        th_OFCb0_6min = th_OFCb[0]-6*th_OFCb[1]
        th_OFCa0_7max = th_OFCa[0]+7*th_OFCa[1]     
        th_OFCa0_7min = th_OFCa[0]-7*th_OFCa[1]
        th_OFCb0_7max = th_OFCb[0]+7*th_OFCb[1]
        th_OFCb0_7min = th_OFCb[0]-7*th_OFCb[1]
        th_OFCa1_5max = th_OFCa[2]+5*th_OFCa[3]
        th_OFCa1_5min = th_OFCa[2]-5*th_OFCa[3]
        th_OFCb1_5max = th_OFCb[2]+5*th_OFCb[3]
        th_OFCb1_5min = th_OFCb[2]-5*th_OFCb[3]
        th_OFCa1_6max = th_OFCa[2]+6*th_OFCa[3]
        th_OFCa1_6min = th_OFCa[2]-6*th_OFCa[3]
        th_OFCb1_6max = th_OFCb[2]+6*th_OFCb[3]
        th_OFCb1_6min = th_OFCb[2]-6*th_OFCb[3]
        th_OFCa1_7max = th_OFCa[2]+7*th_OFCa[3]     
        th_OFCa1_7min = th_OFCa[2]-7*th_OFCa[3]
        th_OFCb1_7max = th_OFCb[2]+7*th_OFCb[3]
        th_OFCb1_7min = th_OFCb[2]-7*th_OFCb[3]
        th_OFCa2_5max = th_OFCa[4]+5*th_OFCa[5]
        th_OFCa2_5min = th_OFCa[4]-5*th_OFCa[5]
        th_OFCb2_5max = th_OFCb[4]+5*th_OFCb[5]
        th_OFCb2_5min = th_OFCb[4]-5*th_OFCb[5]
        th_OFCa2_6max = th_OFCa[4]+6*th_OFCa[5]
        th_OFCa2_6min = th_OFCa[4]-6*th_OFCa[5]
        th_OFCb2_6max = th_OFCb[4]+6*th_OFCb[5]
        th_OFCb2_6min = th_OFCb[4]-6*th_OFCb[5]
        th_OFCa2_7max = th_OFCa[4]+7*th_OFCa[5]     
        th_OFCa2_7min = th_OFCa[4]-7*th_OFCa[5]
        th_OFCb2_7max = th_OFCb[4]+7*th_OFCb[5]
        th_OFCb2_7min = th_OFCb[4]-7*th_OFCb[5]
        th_OFCa3_5max = th_OFCa[6]+5*th_OFCa[7]
        th_OFCa3_5min = th_OFCa[6]-5*th_OFCa[7]
        th_OFCb3_5max = th_OFCb[6]+5*th_OFCb[7]
        th_OFCb3_5min = th_OFCb[6]-5*th_OFCb[7]
        th_OFCa3_6max = th_OFCa[6]+6*th_OFCa[7]
        th_OFCa3_6min = th_OFCa[6]-6*th_OFCa[7]
        th_OFCb3_6max = th_OFCb[6]+6*th_OFCb[7]
        th_OFCb3_6min = th_OFCb[6]-6*th_OFCb[7]
        th_OFCa3_7max = th_OFCa[6]+7*th_OFCa[7]     
        th_OFCa3_7min = th_OFCa[6]-7*th_OFCa[7]
        th_OFCb3_7max = th_OFCb[6]+7*th_OFCb[7]
        th_OFCb3_7min = th_OFCb[6]-7*th_OFCb[7]
        signalIndex = gMT.GetN()
        gMT.SetPoint(signalIndex, eta, tin.phi, tin.MeanRecTau)
        gRT.SetPoint(signalIndex, eta, tin.phi, tin.RMSRecTau)
        gME.SetPoint(signalIndex, eta, tin.phi, tin.MeanReldiffRecTrueEt)
        gRE.SetPoint(signalIndex, eta, tin.phi, tin.RMSReldiffRecTrueEt)
         
        c = root.TCanvas('pulse','pulse')
        Bad = "no"
        maxpulse = max(pulse)

        #pulse slope criteria
        N = len(pulse)-2
        for n in range(N):
            center = pulse[n+1]
            diff1 = center - pulse[n]
            diff2 = pulse[n+2] - center
            diff3 = abs(diff2-diff1)
            if n <= 100 and diff3 > 90:
                Bad = "slope"
                reason = f'BadSlope({round((25/24*(n+1)),2)}ns)'
            elif diff3 > th_slope and n > 100:
                Bad = "slope"
                reason = f'BadSlope({round((25/24*(n+1)),2)}ns)'

        #Undershoot criteria
        if Bad == "no":
            under = min(pulse)
            if maxpulse < abs(under) or under > 0:
                Bad = "undershoot"
                reason = f"BadUnder({round(abs(under)/maxpulse,2)*100}%)"
            elif m != "FCal" and maxpulse/2 < abs(under):
                Bad = "undershoot"
                reason = f"BadUnder({round(abs(under)/maxpulse,2)*100}%)"

            # Peak Amplitude criteria
            elif m == 'EMEC0' and abs(eta) == 1.5499999523162842:
                if not (400 < maxpulse < 600 or 700 < maxpulse < 900):
                    Bad = "amp"
                    reason = f'BadAmp({round(maxpulse,2)})'
                    outhistlist.append(eta)
            elif m == 'EMEC3' and abs(eta) == 1.5499999523162842:
                if not (700 < maxpulse < 900 or 1500 <  maxpulse < 1700):
                    Bad = "amp"
                    reason = f'BadAmp({round(maxpulse,2)})'
                    outhistlist.append(eta)
                    
            elif maxpulse < thMin_ampdict[eta] and m != "FCal":
                    Bad = "amp"
                    reason = f'LowAmp({round(maxpulse,2)})'
                    outhistlist.append(eta)
            
            elif maxpulse > thMax_ampdict[eta] and m != "FCal":
                    Bad = "amp"
                    reason = f'HighAmp({round(maxpulse,2)})'
                    outhistlist.append(eta)

            # OFC criteria       
            elif not (th_OFCa0_5min < tin.OFCa[0] < th_OFCa0_5max and th_OFCa1_5min < tin.OFCa[1] < th_OFCa1_5max and th_OFCa2_5min < tin.OFCa[2] < th_OFCa2_5max and th_OFCa3_5min < tin.OFCa[3] < th_OFCa3_5max and th_OFCb0_5min < tin.OFCb[0] < th_OFCb0_5max and th_OFCb1_5min < tin.OFCb[1] < th_OFCb1_5max and th_OFCb2_5min < tin.OFCb[2] < th_OFCb2_5max and th_OFCb3_5min < tin.OFCb[3] < th_OFCb3_5max) and (m != "FCal"):
                if not (th_OFCa0_7min < tin.OFCa[0] < th_OFCa0_7max and th_OFCa1_7min < tin.OFCa[1] < th_OFCa1_7max and th_OFCa2_7min < tin.OFCa[2] < th_OFCa2_7max and th_OFCa3_7min < tin.OFCa[3] < th_OFCa3_7max and th_OFCb0_7min < tin.OFCb[0] < th_OFCb0_7max and th_OFCb1_7min < tin.OFCb[1] < th_OFCb1_7max and th_OFCb2_7min < tin.OFCb[2] < th_OFCb2_7max and th_OFCb3_7min < tin.OFCb[3] < th_OFCb3_7max):
                    Bad = "OFC"
                    reason = 'BadOFC(7sigma)'
                else:
                    label5 = []
                    label6 = []
                    n5 = 0
                    n6 = 0
                    if not (th_OFCa0_6min < tin.OFCa[0] < th_OFCa0_6max):
                        label6.append('a0')
                        n6 += 1
                    if not (th_OFCa1_6min < tin.OFCa[1] < th_OFCa1_6max):
                        label6.append('a1')
                        n6 += 1
                    if not (th_OFCa2_6min < tin.OFCa[2] < th_OFCa2_6max):
                        label6.append('a2')
                        n6 += 1
                    if not (th_OFCa3_6min < tin.OFCa[3] < th_OFCa3_6max):
                        label6.append('a3')
                        n6 += 1
                    if not (th_OFCb0_6min < tin.OFCb[0] <th_OFCb0_6max):
                        label6.append('b0')
                        n6 += 1
                    if not (th_OFCb1_6min < tin.OFCb[1] < th_OFCb1_6max):
                        label6.append('b1')
                        n6 += 1
                    if not (th_OFCb2_6min < tin.OFCb[2] < th_OFCb2_6max):
                        label6.append('b2')
                        n6 += 1
                    if not (th_OFCb3_6min < tin.OFCb[3] < th_OFCb3_6max):
                        label6.append('b3')
                        n6 += 1
                    if not (th_OFCa0_5min < tin.OFCa[0] < th_OFCa0_5max):
                        label5.append('a0')
                        n5 += 1
                    if not (th_OFCa1_5min < tin.OFCa[1] < th_OFCa1_5max):
                        label5.append('a1')
                        n5 += 1
                    if not (th_OFCa2_5min < tin.OFCa[2] < th_OFCa2_5max):
                        label5.append('a2')
                        n5 += 1
                    if not (th_OFCa3_5min < tin.OFCa[3] < th_OFCa3_5max):
                        label5.append('a3')
                        n5 += 1
                    if not (th_OFCb0_5min < tin.OFCb[0] <th_OFCb0_5max):
                        label5.append('b0')
                        n5 += 1
                    if not (th_OFCb1_5min < tin.OFCb[1] < th_OFCb1_5max):
                        label5.append('b1')
                        n5 += 1
                    if not (th_OFCb2_5min < tin.OFCb[2] < th_OFCb2_5max):
                        label5.append('b2')
                        n5 += 1
                    if not (th_OFCb3_5min < tin.OFCb[3] < th_OFCb3_5max):
                        label5.append('b3')
                        n5 += 1
                    if n6 > 3:
                        Bad = "OFC"
                        reason = "BadOFC(6sigma)*"+str(n6) + ','.join(label6)
                    elif n5 > 4:
                        Bad = "OFC"
                        reason = "BadOFC(5sigma)*"+str(n5) + ','.join(label5)
     
            # Mean, RMS, pesADC criteria
            elif abs(tin.MeanRecTau) >=0.1 or abs(tin.RMSRecTau) >= 0.2 or abs(tin.MeanReldiffRecTrueEt) >= 0.01 or abs(tin.RMSReldiffRecTrueEt) >= 0.01 or tin.pedADC == 0:
                    Bad = "yes"
                    reason = 'Mean&RMS'

        #output bad channel's pulse
        if Bad != "no":
            UPD = dFlag[tin.channelId]
            f.write('{:^6s}{:<11d}{:< 8.4f}{:< 8.4f}{:< 12.6f}{:< 11.6f}{:< 14.6f}{:< 13.6f}{:<7.2f}{:<9.2f}'.format(m, tin.channelId, tin.eta, tin.phi,tin.MeanRecTau,tin.RMSRecTau,tin.MeanReldiffRecTrueEt,tin.RMSReldiffRecTrueEt,tin.LSB,tin.pedADC)+reason+":"+UPD[0]+":"+UPD[1]+'\n')

            gbad = root.TGraph()
            for j in range(len(pulse)):
                gbad.SetPoint(gbad.GetN(), time[j], pulse[j])
            gbad.SetTitle('Pulse Shape (pedestal is subtracted);time[ns];ADC')
            gbad.Draw("AC")
            text.DrawTextNDC(0.55, 0.83, f"channelId={tin.channelId}")
            c.Print(fout_p)
            if Bad == "slope":c.Print(fout_dp)
        
    if m == 'EMB0':title = 'EMB Presampler'
    elif m == 'EMB1':title = 'EMB Front'
    elif m == 'EMB2':title = 'EMB Middle'
    elif m == 'EMB3':title = 'EMB Back'
    elif m == 'EMEC0':title = 'EMEC Presampler'
    elif m == 'EMEC1':title = 'EMEC Front'
    elif m == 'EMEC2':title = 'EMEC Middle'
    elif m == 'EMEC3':title = 'EMEC Back'
    elif m == 'HEC':title = 'HEC'
    elif m == 'FCal':title = 'FCal'
    else:
        print('!!!CAUTION!!! this area is not in the list')
        title = 'Not exist'
    Outhistlist = set(outhistlist)
    c = root.TCanvas('hamp','hamp')
    for eta in Outhistlist:
        h_amp = hamp[etadict[eta]]
        h_amp.SetTitle('Amplitude '+title+' #eta='+str(round(eta,4))+';Amplitude [ADC counts] (Pedestal subtracted);Number of SCs per 3')
        root.gStyle.SetOptStat(1000111101)
        h_amp.Draw()
        c.Print(bad_amp_hist_pdf)
    #Fllowing lines criate 2D map of simulation result. These are good visualization but off because of spending a lot of time ...
    ''' 
    c1 = root.TCanvas('canvas','canvas')
    root.gPad.SetRightMargin(0.20)
    root.gPad.SetLeftMargin(0.12)
    root.gPad.SetBottomMargin(0.16)
    gMT.SetMarkerStyle(20)
    gMT.SetMarkerSize(0.6)
    gMT.SetTitle(';'+title+'       #eta;#phi;Mean #tau')
    gMT.Draw("PCOL, Z")
    gMT.SetMargin(0.05)
    gMT.GetZaxis().SetTitleOffset(1.8)
    #gMT.GetYaxis().SetTitleOffset(1.3)
    gMT.GetXaxis().SetTitleOffset(1.8)
    root.gPad.Modified()
    root.gPad.Update()
    root.gPad.GetView().TopView()
    if m == area[0]:
        c1.Print(fout + "[")
        c1.Print(fout)
    else:c1.Print(fout)

    c1 = root.TCanvas('canvas','canvas')
    root.gPad.SetRightMargin(0.20)
    root.gPad.SetLeftMargin(0.12)
    root.gPad.SetBottomMargin(0.16)
    gME.SetMarkerStyle(20)
    gME.SetMarkerSize(0.6)
    gME.SetTitle(';'+title+'       #eta;#phi;Mean (E_{T}^{reco} - E_{T}^{true})/E_{T}^{true}')
    gME.Draw("PCOL, Z")
    gME.SetMargin(0.05)
    gME.GetZaxis().SetTitleOffset(1.8)
    #gME.GetYaxis().SetTitleOffset(1.3)
    gME.GetXaxis().SetTitleOffset(1.8)
    root.gPad.Modified()
    root.gPad.Update()
    root.gPad.GetView().TopView()
    c1.Print(fout)

    c1 = root.TCanvas('canvas','canvas')
    root.gPad.SetRightMargin(0.20)
    root.gPad.SetLeftMargin(0.12)
    root.gPad.SetBottomMargin(0.16)
    gRT.SetMarkerStyle(20)
    gRT.SetMarkerSize(0.6)
    gRT.SetTitle(';'+title+'       #eta;#phi;RMS #tau')
    gRT.Draw("PCOL, Z")
    gRT.SetMargin(0.05)
    gRT.GetZaxis().SetTitleOffset(1.8)
    #gRT.GetYaxis().SetTitleOffset(1.3)
    gRT.GetXaxis().SetTitleOffset(1.8)
    root.gPad.Modified()
    root.gPad.Update()
    root.gPad.GetView().TopView()
    c1.Print(fout)

    c1 = root.TCanvas('canvas','canvas')
    root.gPad.SetRightMargin(0.20)
    root.gPad.SetLeftMargin(0.12)
    root.gPad.SetBottomMargin(0.16)
    gRE.SetMarkerStyle(20)
    gRE.SetMarkerSize(0.6)
    gRE.SetTitle(';'+title+'       #eta;#phi;RMS (E_{T}^{reco} - E_{T}^{true})/E_{T}^{true}')
    gRE.Draw("PCOL, Z")
    gRE.SetMargin(0.05)
    gRE.GetZaxis().SetTitleOffset(1.8)
    #gRE.GetYaxis().SetTitleOffset(1.3)
    gRE.GetXaxis().SetTitleOffset(1.8)
    root.gPad.Modified()
    root.gPad.Update()
    root.gPad.GetView().TopView()
    if m == area[-1]:
        c1.Print(fout)
        c1.Print(fout + "]")
    else:c1.Print(fout)
    '''
    fin.Close()
    
f.close()
fth.close()
c.Print(bad_amp_hist_pdf+"]")
c.Print(fout_p+"]")
c.Print(fout_dp+"]")
