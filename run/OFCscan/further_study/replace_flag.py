my_list = "BadChannelList_public.txt"
patch_list = "usable_channels.txt"
outfile = "BadChannelList_public_v2.txt"

#SC list needed to patch the label
dPatch = {}
with open(patch_list) as fin:
    lines = fin.readlines()
    for line in lines:
        value = line.split()
        dPatch[value[0]] = "Usable" #set the new lable
fin.close()
PatchedSCs = dPatch.keys()
print(f"{len(Patches)} SCs will be patched.")

#patch labels
count = 0
fout = open(outfile, "w")
with open(my_list) as fin:
    lines = fin.readlines()
    for line in lines:
        value = line.split()
        channel = value[0]
        if channel in PatchedSCs:
            fout.write(channel+" "+dPatch[channel])
            count = count+1
        else: fout.write(line)
fin.close()
print(f"{count} SCs are patched.")
