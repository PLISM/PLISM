import ROOT as root
import math
import re
root.gROOT.SetBatch(True)

run = '473617'
cut = 5
lim = 500
nbin = 250
Badlist = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/BadChannelList_public.txt.txt'
Coldata_list = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/Need_to_check_wCollisionData.txt'
PAdata_list = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/Need_to_check_wPulseAll.txt'
fout = f"/afs/cern.ch/user/m/mfurukaw/storage/LadiesInvestigation/{run}/DTETvsMainET_summary.png"

fin = root.TFile(f'/afs/cern.ch/user/m/mfurukaw/storage/condor_output/{run}/DB25Apr/treeSC_{run}.root','read')
tin = fin.Get("treeSC")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("sum_main",1)
tin.SetBranchStatus("ev_et",1)
tin.SetBranchStatus("sc_eta",1)
tin.SetBranchStatus("channelId",1)
#make target list
BAD = []
COL = []
PA = []
badlist = open(Badlist, 'r')
lines = badlist.readlines()
for line in lines:
        value = line.split()
        BAD.append(int(value[0]))
collist = open(Coldata_list, 'r')
lines = collist.readlines()
for line in lines:
        value = line.split()
        COL.append(int(value[0]))
palist = open(PAdata_list, 'r')
lines = palist.readlines()
for line in lines:
        value = line.split()
        PA.append(int(value[0]))

hCOL = root.TH2D(f"collision_check",f"collision_check, run"+run+", E_{T}^{main} > 5 GeV;Sum of cells E_{T} [GeV];Super Cells E_{T} [GeV]", nbin, 0, lim, nbin, 0, lim)
hbad = root.TH2D(f"bad",f"bad, run"+run+", E_{T}^{main} > 5 GeV;Sum of cells E_{T} [GeV];Super Cells E_{T} [GeV]", nbin, 0, lim, nbin, 0, lim)
hPA = root.TH2D(f"PA_check",f"PulseAll_check_needed run"+run+", E_{T}^{main} > 5 GeV;Sum of cells E_{T} [GeV];Super Cells E_{T} [GeV]", nbin, 0, lim, nbin, 0, lim)
nEntries = tin.GetEntries()
for i in range(0, nEntries):
    tin.GetEntry(i)
    Et_main = (tin.sum_main/math.cosh(tin.sc_eta))/1000
    if Et_main > cut:
        evet = tin.ev_et*12.5/1000
        if tin.channelId in COL:hCOL.Fill(Et_main,evet)
        elif tin.channelId in BAD:hbad.Fill(Et_main,evet)
        elif tin.channelId in PA:hPA.Fill(Et_main,evet)

#line
line = root.TLine(0, 0, lim, lim)
line.SetLineColor(2)
line.SetLineWidth(1)

c = root.TCanvas()
root.gStyle.SetOptStat(0)
hbad.SetMarkerStyle(20)
hbad.SetMarkerSize(0.5)
hbad.SetMarkerColor(root.kBlue)
hbad.Draw()
hCOL.SetMarkerStyle(20)
hCOL.SetMarkerSize(0.5)
hCOL.SetMarkerColor(root.kRed)
hCOL.Draw("same")
hPA.SetMarkerStyle(20)
hPA.SetMarkerSize(0.5)
hPA.SetMarkerColor(root.kGreen)
hPA.Draw("same")
line.Draw()
c.Print(fout)

