import ROOT as root
root.gROOT.SetBatch(True)

#area = ['EMEC0', 'FCal']
area = ['EMB0', 'EMB1', 'EMB2', 'EMB3', 'EMEC0', 'EMEC1', 'EMEC2', 'EMEC3', 'HEC', 'FCal'] #area you want to process.
wave = 'Cali' 

directory = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831' #input and putput directory. Input file name is set in line 20.
#output file names
fout = f'{directory}/OFC_{wave}_distribution.pdf' 
fout_hist = f'{directory}/OFChist_{wave}.pdf'
Nout_root = f'{directory}/OFC_{wave}.root'
Nout_hist_root = f'{directory}/OFChist_{wave}.root'

hAll_a = [root.TH1D(f'all_a{j}', f'all_a{j}', 100, 0, 0) for j in range(4)]
hAll_b = [root.TH1D(f'all_b{j}', f'all_b{j}', 100, 0, 0) for j in range(4)]

fout_hist_root = root.TFile(Nout_hist_root, 'recreate')
fout_root = root.TFile(Nout_root, 'recreate')
for m in area:
    fin = root.TFile(f'{directory}/RecoAccu{wave}_{m}.root') 
    tin = fin.Get('RecoAccuracy')

    if m == 'EMB0':title = 'EMB Presampler'
    elif m == 'EMB1':title = 'EMB Front'
    elif m == 'EMB2':title = 'EMB Middle'      
    elif m == 'EMB3':title = 'EMB Back'
    elif m == 'EMEC0':title = 'EMEC Presampler'
    elif m == 'EMEC1':title = 'EMEC Front'
    elif m == 'EMEC2':title = 'EMEC Middle'
    elif m == 'EMEC3':title = 'EMEC Back'   
    elif m == 'HEC':title = 'HEC'
    elif m == 'FCal':title = 'FCal'

    h_a = [root.TH1D(f'{m}_OFCa{j}', f'{m}_OFCa{j}', 100, 0, 0) for j in range(4)]
    h_b = [root.TH1D(f'{m}_OFCb{j}', f'{m}_OFCb{j}', 100, 0, 0) for j in range(4)]
    if m == 'EMB0' or m == 'EMB1' or m == 'EMB2' or m == 'EMB3':
        hOFCa = [root.TH2D(f'{m}_OFCa{j}', f'{m}_OFCa{j}', 120, -1.5, 1.5, 68, -3.4, 3.4) for j in range(4)]
        hOFCb = [root.TH2D(f'{m}_OFCb{j}', f'{m}_OFCb{j}', 120, -1.5, 1.5, 68, -3.4, 3.4) for j in range(4)]
    elif m == 'EMEC0' or m == 'EMEC1' or m == 'EMEC2' or m == 'EMEC3' or m == 'HEC':
        hOFCa = [root.TH2D(f'{m}_OFCa{j}', f'{m}_OFCa{j}', 272, -3.4, 3.4, 68, -3.4, 3.4) for j in range(4)]
        hOFCb = [root.TH2D(f'{m}_OFCb{j}', f'{m}_OFCb{j}', 272, -3.4, 3.4, 68, -3.4, 3.4) for j in range(4)]
    gOFCa = [root.TGraph2D() for j in range(4)]
    gOFCb = [root.TGraph2D() for j in range(4)]
    
    nEntries = tin.GetEntries()
    for i in range(0, nEntries):
        tin.GetEntry(i)
        eta = tin.eta
        phi = tin.phi
        signalIndex = gOFCa[0].GetN()
        for j in range(4):
            a = tin.OFCa[j]
            b = tin.OFCb[j]
            h_a[j].Fill(a)
            h_b[j].Fill(b)
            hAll_a[j].Fill(a)
            hAll_b[j].Fill(b)
            gOFCa[j].SetPoint(signalIndex, eta, phi, a)
            gOFCb[j].SetPoint(signalIndex, eta, phi, b)
            if m == 'FCal':continue
            hOFCa[j].Fill(eta, phi, a)
            hOFCb[j].Fill(eta, phi, b)
    
    for j in range(4):
        c1 = root.TCanvas('ha_area','ha_area')
        h_a[j].SetTitle(f'{title} {j} th OFCa')
        h_a[j].Draw()
        if m == area[0] and j == 0:
            c1.Print(fout_hist+"[")
            c1.Print(fout_hist)
        else:c1.Print(fout_hist)           

    for j in range(4):
        c1 = root.TCanvas('hb_area','hb_area')
        h_b[j].SetTitle(f'{title} {j} th OFCb')
        h_b[j].Draw()
        c1.Print(fout_hist)

    for j in range(4):
        c2 = root.TCanvas('plotA_area', 'plotA_area')
        root.gPad.SetRightMargin(0.20)
        root.gPad.SetLeftMargin(0.12)
        root.gPad.SetBottomMargin(0.16)
        gOFCa[j].SetMarkerStyle(20)
        gOFCa[j].SetMarkerSize(0.6)
        gOFCa[j].SetTitle(f'{title} {j} th OFCa;#eta;#phi;OFCa')   
        gOFCa[j].Draw("PCOL, Z")
        gOFCa[j].SetMargin(0.05)
        gOFCa[j].GetYaxis().SetTitleOffset(1.3)
        gOFCa[j].GetXaxis().SetTitleOffset(1.8)
        root.gPad.Modified()
        root.gPad.Update()
        root.gPad.GetView().TopView()
        if m == area[0] and j == 0:
            c2.Print(fout + "[")
            c2.Print(fout)
        else:c2.Print(fout)

    for j in range(4):
        c = root.TCanvas('plotB_area', 'plotB_area')
        root.gPad.SetRightMargin(0.20)
        root.gPad.SetLeftMargin(0.12)
        root.gPad.SetBottomMargin(0.16)
        gOFCb[j].SetMarkerStyle(20)
        gOFCb[j].SetMarkerSize(0.6)
        gOFCb[j].SetTitle(f'{title} {j} th OFCb;#eta;#phi;OFCb')   
        gOFCb[j].Draw("PCOL, Z")
        gOFCb[j].SetMargin(0.05)
        gOFCb[j].GetYaxis().SetTitleOffset(1.3)
        gOFCb[j].GetXaxis().SetTitleOffset(1.8)
        root.gPad.Modified()
        root.gPad.Update()
        root.gPad.GetView().TopView()
        if m == area[-1] and j == 3:
            c.Print(fout)
            c.Print(fout + "]")
        else:c.Print(fout)
        
    if m == 'FCal':continue 
    fout_root.cd()
    for j in range(4):
        hOFCa[j].Write()
        hOFCb[j].Write()

    fout_hist_root.cd()
    for j in range(4):
        h_a[j].Write()
        h_b[j].Write()

fout_root.Close()
fout_hist_root.Close()

for j in range(4):
    c1 = root.TCanvas('ha_All','ha_All')
    hAll_a[j].SetTitle(f'All area {j} th OFCa;#eta;#phi;OFCa')
    hAll_a[j].Draw()
    c1.Print(fout_hist)
 
for j in range(4):
    c1 = root.TCanvas('hb_All','hb_All')
    hAll_b[j].SetTitle(f'All area {j} th OFCb;#eta;#phi;OFCb')
    hAll_b[j].Draw()
    if j == 3:
        c1.Print(fout_hist)
        c1.Print(fout_hist+"]")
    else:c1.Print(fout_hist)

