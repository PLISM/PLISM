import ROOT as root
import math
root.gROOT.SetBatch(True)

run = '456714'
fout = f"/afs/cern.ch/user/m/mfurukaw/storage/LadiesInvestigation/{run}/PublicPlots/Comparison" #put directory name
date = "15 Jul 2023" #date to print on the plot
cut = 5
xlim = 500.
ylim = 500.
nXbin = int(xlim/5)
nYbin = int(ylim/5)

badlist = f"/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/BadChannelList_public.txt" #bad SC to plot

#make bad list
BadSC = []
f = open(badlist, "r")
lines = f.readlines()
for i in range(len(lines)):
    value = lines[i].split()
    BadSC.append(int(value[0]))
print(f'Bad {len(BadSC)} SCs')

#detector dictonary
dDet = {}
fin = root.TFile("/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_230425-153106/CaliWave.root","read")
tin = fin.Get("CALIWAVE")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("channelId",1)
tin.SetBranchStatus("detector",1)
nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    dDet[tin.channelId] = tin.detector
fin.Close()

fin = root.TFile(f'/afs/cern.ch/user/m/mfurukaw/storage/condor_output/{run}/treeSC_{run}.root',"read")

tin = fin.Get("treeSC")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("sum_main",1)
tin.SetBranchStatus("ev_et",1)
tin.SetBranchStatus("channelId",1)
tin.SetBranchStatus("sc_det",1)
tin.SetBranchStatus("sc_eta",1)
tin.SetBranchStatus("sc_layer",1)
tin.SetBranchStatus("trigger",1)
tin.SetBranchStatus("LArEventBits",1)
tin.SetBranchStatus("sc_badword_UPD1_cells",1)
tin.SetBranchStatus("sc_badword_UPD4_cells",1)

hEt = root.TH2D('DTEt_vs_mainEt_ALL', 'hist', nXbin, 0, xlim, nYbin, 0, ylim)
hEt0 =  root.TH2D('DTEt_vs_mainEt_EMB','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt00 = root.TH2D('DTEt_vs_mainEt_EMB0','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt01 = root.TH2D('DTEt_vs_mainEt_EMB1','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt02 = root.TH2D('DTEt_vs_mainEt_EMB2','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt03 = root.TH2D('DTEt_vs_mainEt_EMB3','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt1 =  root.TH2D('DTEt_vs_mainEt_EMEC','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt_outer =  root.TH2D('DTEt_vs_mainEt_EMECouter','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt_inner =  root.TH2D('DTEt_vs_mainEt_EMECinner','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtI10 = root.TH2D('DTEt_vs_mainEt_EMEC0_Inner','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtI11 = root.TH2D('DTEt_vs_mainEt_EMEC1_Inner','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtI12 = root.TH2D('DTEt_vs_mainEt_EMEC2_Inner','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtI13 = root.TH2D('DTEt_vs_mainEt_EMEC3_Inner','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtO10 = root.TH2D('DTEt_vs_mainEt_EMEC0_Outer','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtO11 = root.TH2D('DTEt_vs_mainEt_EMEC1_Outer','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtO12 = root.TH2D('DTEt_vs_mainEt_EMEC2_Outer','hist',nXbin,0,xlim,nYbin,0,ylim)
hEtO13 = root.TH2D('DTEt_vs_mainEt_EMEC3_Outer','hist',nXbin,0,xlim,nYbin,0,ylim)
hEt3 = root.TH2D('DTEt_vs_mainEt_HEC','hist', nXbin,0,xlim,nYbin,0,ylim)
hEt4 = root.TH2D('DTEt_vs_mainEt_FCal','hist', 125, 0, 250, 125, 0, 250)

nEntries = tin.GetEntries()
for i in range(0, nEntries):
    tin.GetEntry(i)
    if tin.channelId in BadSC and tin.LArEventBits == 0:
        Et_main = (tin.sum_main/math.cosh(tin.sc_eta))/1000
        EtLATOME = tin.ev_et*12.5/1000
        hEt.Fill(Et_main, EtLATOME)
        det = dDet[tin.channelId]
        if det == 3:
            hEt3.Fill(Et_main, EtLATOME)
        elif det == 4:
            hEt4.Fill(Et_main, EtLATOME)
        elif det == 0:
            hEt0.Fill(Et_main, EtLATOME)    
            if tin.sc_layer == 0:hEt00.Fill(Et_main, EtLATOME)
            elif tin.sc_layer == 1:hEt01.Fill(Et_main, EtLATOME)
            elif tin.sc_layer == 2:hEt02.Fill(Et_main, EtLATOME)
            elif tin.sc_layer == 3:hEt03.Fill(Et_main, EtLATOME)
        elif det == 1 or det == 2:
            hEt1.Fill(Et_main, EtLATOME)
            if det == 1:
                hEt_outer.Fill(Et_main, EtLATOME)
                if tin.sc_layer == 0:hEtO10.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 1:hEtO11.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 2:hEtO12.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 3:hEtO13.Fill(Et_main, EtLATOME)
            else:
                hEt_inner.Fill(Et_main, EtLATOME)
                if tin.sc_layer == 0:hEtI10.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 1:hEtI11.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 2:hEtI12.Fill(Et_main, EtLATOME)
                elif tin.sc_layer == 3:hEtI13.Fill(Et_main, EtLATOME)

#text
text = root.TText()
text.SetTextSize(0.045)
text.SetTextFont(42)
text.SetNDC()
l = root.TLatex()
l.SetNDC()
l.SetTextAlign(22)
l.SetTextSize(0.05)
l.SetTextFont(72)
l.SetTextColor(root.kBlack)
l2 = root.TLatex()
l2.SetNDC()
l2.SetTextSize(0.045)
l2.SetTextFont(42)

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.cd()
p1.SetTickx(1)
p1.SetTicky(1)
hEt.Draw("COLZ")
root.gStyle.SetOptStat(0)
hEt.GetZaxis().SetTitleOffset(1.2)
hEt.GetZaxis().SetMaxDigits(3)
hEt.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.79, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
c.Print(fout+"_All.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt0.Draw("COLZ")
hEt0.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt0.GetZaxis().SetTitleOffset(1.2)
hEt0.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMB")
c.Print(fout+"_EMB.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt00.Draw("COLZ")
hEt00.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt00.GetZaxis().SetTitleOffset(1.2)
hEt00.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMB Presampler")
c.Print(fout+"_EMB0.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt01.Draw("COLZ")
hEt01.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt01.GetZaxis().SetTitleOffset(1.2)
hEt01.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMB Front")
c.Print(fout+"_EMB1.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt02.Draw("COLZ")
hEt02.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt02.GetZaxis().SetTitleOffset(1.2)
hEt02.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMB Middle")
c.Print(fout+"_EMB2.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt03.Draw("COLZ")
hEt03.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt03.GetZaxis().SetTitleOffset(1.2)
hEt03.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMB Back")
c.Print(fout+"_EMB3.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt1.Draw("COLZ")
hEt1.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt1.GetZaxis().SetTitleOffset(1.2)
hEt1.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC")
c.Print(fout+"_EMEC.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt_inner.Draw("COLZ")
hEt_inner.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt_inner.GetZaxis().SetTitleOffset(1.2)
hEt_inner.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Inner wheel")
c.Print(fout+"_Inner.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt_outer.Draw("COLZ")
hEt_outer.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt_outer.GetZaxis().SetTitleOffset(1.2)
hEt_outer.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Outer wheel")
c.Print(fout+"_Outer.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtI10.Draw("COLZ")
hEtI10.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtI10.GetZaxis().SetTitleOffset(1.2)
hEtI10.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Presampler (Inner)")
c.Print(fout+"_EMEC0Inner.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtI11.Draw("COLZ")
hEtI11.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtI11.GetZaxis().SetTitleOffset(1.2)
hEtI11.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Front (Inner)")
c.Print(fout+"_EMEC1Inner.png")


c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtI12.Draw("COLZ")
hEtI12.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtI12.GetZaxis().SetTitleOffset(1.2)
hEtI12.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Middle (Inner)")
c.Print(fout+"_EMEC2Inner.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtI13.Draw("COLZ")
hEtI13.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtI13.GetZaxis().SetTitleOffset(1.2)
hEtI13.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Back (Inner)")
c.Print(fout+"_EMEC3Inner.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtO10.Draw("COLZ")
hEtO10.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtO10.GetZaxis().SetTitleOffset(1.2)
hEtO10.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Presampler (Outer)")
c.Print(fout+"_EMEC0Outer.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtO11.Draw("COLZ")
hEtO11.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtO11.GetZaxis().SetTitleOffset(1.2)
hEtO11.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Front (Outer)")
c.Print(fout+"_EMEC1Outer.png")


c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtO12.Draw("COLZ")
hEtO12.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtO12.GetZaxis().SetTitleOffset(1.2)
hEtO12.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Middle (Outer)")
c.Print(fout+"_EMEC2Outer.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEtO13.Draw("COLZ")
hEtO13.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEtO13.GetZaxis().SetTitleOffset(1.2)
hEtO13.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "EMEC Back (Outer)")
c.Print(fout+"_EMEC3Outer.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt3.Draw("COLZ")
hEt3.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 5#times5 GeV^{2}')
p1.SetLogz()
hEt3.GetZaxis().SetTitleOffset(1.2)
hEt3.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "HEC")
c.Print(fout+"_HEC.png")

c = root.TCanvas('canvas','canvas',800, 600)
p1 = root.TPad("p1","p1",0,0,1,1,0,0,0)
p1.SetMargin(0.1,0.15,0.1,0.1)
p1.Draw()
p1.SetTickx(1)
p1.SetTicky(1)
p1.cd()
hEt4.Draw("COLZ")
hEt4.SetTitle(';Sum of Cells E_{T} [GeV];Super Cells E_{T} [GeV];Entries per 2#times2 GeV^{2}')
p1.SetLogz()
hEt4.GetZaxis().SetTitleOffset(1.2)
hEt4.GetZaxis().SetMaxDigits(3)
l.DrawLatex(0.2, 0.86, "ATLAS")
text.DrawTextNDC(0.26, 0.84, "Internal")
text.DrawTextNDC(0.14, 0.70, "Run "+run)
l2.DrawLatex(0.14, 0.74,date)
text.DrawTextNDC(0.14, 0.79, "FCal")
c.Print(fout+"_FCal.png")

