import ROOT as root
root.gROOT.SetBatch(True)

area = ['EMB0', 'EMB1', 'EMB2', 'EMB3', 'EMEC0', 'EMEC1', 'EMEC2', 'EMEC3', 'HEC', 'FCal']

directory = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831' #input and output directory. Input file name is set in the line 11.
fout = f'{directory}/PedDistribution.pdf' #output file name
fout_hist = f'{directory}/PedDistribution_hist.pdf' #output file name

for m in area:
    fin = root.TFile(f'{directory}/cellprop_CaliWave_{m}.root')
    tin = fin.Get('CellProperty')
    nEntries = tin.GetEntries()

    gPed = root.TGraph2D()
    h_area = root.TH1D(m, 'Pedestal distribution', 100, -500, 1500)
    for i in range(0, nEntries):
        tin.GetEntry(i)
        gPed.SetPoint(gPed.GetN(), tin.eta, tin.phi, tin.pedADC)
        h_area.Fill(tin.pedADC)

    #to make plots
    if m == 'EMB0':title = 'EMB Presampler'
    elif m == 'EMB1':title = 'EMB Front'
    elif m == 'EMB2':title = 'EMB Middle'
    elif m == 'EMB3':title = 'EMB Back'
    elif m == 'EMEC0':title = 'EMEC Presampler'
    elif m == 'EMEC1':title = 'EMEC Front'
    elif m == 'EMEC2':title = 'EMEC Middle'
    elif m == 'EMEC3':title = 'EMEC Back'
    elif m == 'HEC':title = 'HEC'
    elif m == 'FCal':title = 'FCal'
    else:
        print('!!!CAUTION!!! this area is not in the list')
        title = 'Not exist'

    c1 = root.TCanvas()
    h_area.SetTitle(f'Pedestal distribution {title}')
    h_area.Draw()
    if m == area[0]:
        c1.Print(fout_hist+"[")
        c1.Print(fout_hist)
    elif m == area[-1]:
        c1.Print(fout_hist)
        c1.Print(fout_hist+"]")
    else:c1.Print(fout_hist)           

    c2 = root.TCanvas()
    root.gPad.SetRightMargin(0.20)
    root.gPad.SetLeftMargin(0.12)
    root.gPad.SetBottomMargin(0.16)
    gPed.SetMarkerStyle(20)
    gPed.SetMarkerSize(0.6)
    gPed.Draw("PCOL, Z")
    gPed.SetMargin(0.05)
    gPed.SetTitle("Pedestal level "+title+";#eta;#phi;ADC counts")
    gPed.GetYaxis().SetTitleOffset(1.3)
    gPed.GetXaxis().SetTitleOffset(1.8)
    gPed.GetZaxis().SetTitleOffset(1.8)
    root.gPad.Modified()
    root.gPad.Update()
    root.gPad.GetView().TopView()
    if m == area[0]:
        c2.Print(fout + "[")
        c2.Print(fout)
    elif m == area[-1]:
        c2.Print(fout)
        c2.Print(fout + "]")
    else:c2.Print(fout)

    fin.Close()


