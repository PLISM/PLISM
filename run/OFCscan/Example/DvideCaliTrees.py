from ROOT import TFile, TTree
import sys,os
args = sys.argv
#execute this file like "python3 DvideCaliTrees.py EMB0 Phys"

area = args[1] #EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal

if area == 'EMB0':
    det = 0
    layer = 0
elif area == 'EMB1':
    det = 0
    layer = 1
elif area == 'EMB2':
    det = 0
    layer = 2
elif area == 'EMB3':
    det = 0
    layer = 3
elif area == 'EMEC0':layer = 0
elif area == 'EMEC1':layer = 1
elif area == 'EMEC2':layer = 2
elif area == 'EMEC3':layer = 3
elif area == 'HEC':det = 3
elif area == 'FCal':det = 4

inputFileName = f'./rootfiles/CaliWave.root'
fin = TFile(inputFileName, "read")
tin = fin.Get("CALIWAVE")

outputFileName = f'./rootfiles/CaliWave_{area}.root'
ofile = TFile(outputFileName, "RECREATE")

if (area == 'FCal') or (area == 'HEC'):newtree = tin.CopyTree(f"detector == {det}")
elif ('EMEC' in area):newtree = tin.CopyTree(f"(detector == 1||detector == 2) && layer == {layer}")
else:newtree = tin.CopyTree(f"detector == {det} && layer == {layer}") 

ofile.cd()
newtree.Write()
ofile.Close()

