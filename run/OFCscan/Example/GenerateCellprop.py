import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule
args = sys.argv

area = args[1]

#set input file names.
if area == 'EMB0' or area == 'EMB1' or area == 'EMB2' or area == 'EMB3':
    LArIDtranslatorList = "./rootfiles/LArIDtranslator_Barrel.txt"
else:
    LArIDtranslatorList = "./rootfiles/LArIDtranslator_Endcap.txt"
DAC2MeVFileName = './rootfiles/LArDAC2MeV.root'
PedFileName = f'./rootfiles/LArPedAutoCorr_00450640.root'
inputCaliWavefilename = f'./rootfiles/CaliWave_{area}.root' #input CALIWAVE file
inputPhysWavefilename = f'./rootfiles/PhysWave_{area}.root' #input PHYSWAVE&MPMC file
LSBcalifileName = f'./rootfiles/lsb_CaliWave_{area}.root'
outputLSBphysName = f'./rootfiles/lsb_PhysWave_{area}.root' #output LSBphys file

CaliWaveTree = CellPropertyGeneratorModule.CaliWaveTree(inputCaliWavefilename)
CaliWaveTree.ID_SCID_CL_fileName = LArIDtranslatorList
CaliWaveTree.DAC2MeVFileName = DAC2MeVFileName
CaliWaveTree.LSB_delayPeak() 
CaliWaveTree.PedFileName = PedFileName
CaliWaveTree.PulseTree() 

LSBTree = CellPropertyGeneratorModule.LSBTree(LSBcalifileName)
LSBTree.OutputLSBphysName = outputLSBphysName
LSBTree.LSBphys(inputPhysWavefilename)  

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    f'./rootfiles/pulse_CaliWave_{area}.root'
)
PulseShapeTree.PedAlreadySubtracted = True

PulseShapeTree.LSBFileName = LSBcalifileName
PulseShapeTree.BCShift = 0 
PulseShapeTree.GenerateCellProperty()

PhysWaveTree = CellPropertyGeneratorModule.PhysWaveTree(inputPhysWavefilename)
PhysWaveTree.ID_SCID_CL_fileName = LArIDtranslatorList
PhysWaveTree.DAC2MeVFileName = DAC2MeVFileName
PhysWaveTree.PedFileName = PedFileName
PhysWaveTree.InputMPMCFileName = inputPhysWavefilename
PhysWaveTree.PulseTree(LSBcalifileName)

PulsephysShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    f'./rootfiles/pulse_PhysWave_{area}.root'
)
PulsephysShapeTree.PedAlreadySubtracted = True
PulsephysShapeTree.LSBFileName = outputLSBphysName
PulsephysShapeTree.BCShift = 0 
PulsephysShapeTree.GenerateCellProperty()
