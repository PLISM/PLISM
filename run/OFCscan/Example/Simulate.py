import sys,os
import time
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import HitSummarizerModule
import DigitizerModule
import OFCCalibratorModule
import ReconstructorModule
args = sys.argv

#execute this file like "python3 Simulate.py EMB0 Phys"

#command line arguments
area = args[1] #EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal
wave = args[2] #Cali or Phys

#HitSummarizer
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = f'./rootfiles/HitSummary{wave}_{area}.root'
HitSummarizer.CellPropertyFileName = f'./rootfiles/cellprop_{wave}Wave_{area}.root'
HitSummarizer.NEvent = 2000
HitSummarizer.seed = int(time.time())
# Uniform distribution
HitSummarizer.EtMin = 150*1000 #MeV
HitSummarizer.EtMax = 150*1000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

#Digitizer
Digitizer = DigitizerModule.Digitizer()
Digitizer.CellPropertyFileName = f'./rootfiles/cellprop_{wave}Wave_{area}.root'
Digitizer.Interpolator = "barycentric_rational"
Digitizer.NBC = 500
Digitizer.mu = 60
Digitizer.phase = 23*(25/24)
Digitizer.TrainPattern = [0, 1]
Digitizer.SignalPattern = [1, 49]
Digitizer.seed = int(time.time())
Digitizer.OutputBranch_PileupAnalogEt = False
Digitizer.OutputBranch_PileupTrueEt = False
Digitizer.OutputBranch_PileupTrueEtSum = False
Digitizer.OutputBranch_PileupTrueTau = False
Digitizer.OutputFileName = f'./rootfiles/digitSequence{wave}_{area}.root'
Digitizer.SignalHitSummaryFile = f'./rootfiles/HitSummary{wave}_{area}.root'
Digitizer.sequence()

#Reconstructor
Reconstructor = ReconstructorModule.Reconstructor(f'./rootfiles/digitSequence{wave}_{area}.root')
Reconstructor.OutputFileName = f'./rootfiles/reconstructed{wave}_{area}.root'
if wave == "Cali":Reconstructor.OFCFileName = f'./rootfiles/OFC_Cali_{area}_phase23.root'
elif wave == "Phys":Reconstructor.OFCFileName = f'./rootfiles/OFC1nsmu_Phys_{area}_phase23.root'
Reconstructor.reconstruct()

