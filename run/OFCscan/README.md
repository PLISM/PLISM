# OFC scan
OFC is calculated using calibration data. Every time before OFC updates, it should be scanned if all OFCs are OK to use.
You can see LAr calibration results and its Ntuple location [here](https://larp1validation.web.cern.ch/LArP1Validation/calibration-PHASE1/index.php?dirName=LATOMERun_BarrelEndcapWeekly_221119-102907/output/). OFC is computed using the calibration wave first. It's called "OFCcali" in this document. After the caliration wave is transformed to physics wave, OFC is computed again using physics wave. It's called "OFCphys" in this document.OFCphys is used for the collision. But If calibration wave has an issue, corresponding physics wave and OFCs can be problematic. So both of OFCcali and OFCphys should be scanned.

# Example
Here are the scripts to scan both of OFCcali and OFCphys in the same time. These are designed to scan per layers. 
0. Get input files executing `scp.sh`.
1. CALIWAVE tree and PHYSWAVE tree should be divided in layers. execute `DvideCaliTrees.py` and `DvidePhysTree.cxx`. Layer name have to be given as a command line argument. For example,
```
python3 DvideCaliTrees.py EMB0
root 'DvidePhysTree.cxx("EMB0")'
```
`DvidePhysTree.cxx` not only extracts the data for the specified layer, but also saves only the required entries in OFCscan.
Layer names are defined in the table below. `DvideCaliTrees.sh` is useful when you scan whole of the LAr Calorimeter.

|Layer name | sub-detector and layer |
|:---------:|:----------------------:|
|   EMB0    |  EMB Presampler layer  |
|   EMB1    |  EMB Front layer       |
|   EMB2    |  EMB Middle layer      |
|   EMB3    |  EMB Back layer        |
|   EMEC0   |  EMEC Presampler layer |
|   EMEC1   |  EMEC Front layer      |
|   EMEC2   |  EMEC Middle layer     |
|   EMEC3   |  EMEC Back layer       |
|    HEC    |  HEC                   |
|   FCal    |  FCal layer 1 2, and 3 |

3. Generate cellproperty tree. `GenerateCellprop.py` can do it for both of Calibration wave and Physics wave by one click.
4. Generate OFC tree executing `makeOFCTree.py`. This should be done before you run `Simulate.py` in the next step. It's good to use phase 23 to scan.
5. Run PLISM simulation. Run `Simulate.py`. The higher injected Et is better. It's defined in line 27 and 28.
```
HitSummarizer.EtMin = 150*1000 #MeV
HitSummarizer.EtMax = 150*1000 #MeV
```
Ten times injection is enough. For example, this script definds these as
```
Digitizer.NBC = 500
Digitizer.SignalPattern = [1, 49]
```
This means one signal is injected every 50 BCs, and 500 BCs are simulated. Therefore, 500/50 = 10 signals are simulated.

6. Run `RecoAccu.py`. Average of reconstructed Et and tau simulated in the previous step is calculated and all values required for OFC scan are stored in a single TTree. If you see errors, do `rm AutoDict_std__*` and try again.

7. Run `OFCqualityCheck.py`. This scan pulse shape and OFCs of each SC. There are five criteria: Et and tau reconstruction, pedestal, amplitude, OFC, pulse shape.If any one of them is not saticefied, the SC is found bad and the following criteria are skiped. (Detail of these criteria are described in back up page of this [slide](https://indico.cern.ch/event/1196671/contributions/5127754/attachments/2541639/4376121/DQ20221104.pdf).)If you want to check the results of all criteria without skipping, use `OFCqualityCheck_allCriteria.py`. Additional criteria about LSB is also implemented. 

# Tips
- You can do quick check of a calibration data set by [LAr Phase1 Validation Plot Viewer](https://larp1validation.web.cern.ch/LArP1Validation/calibration-PHASE1/index.php?dirName=LATOMERun_BarrelEndcapWeekly_221119-102907/output/). You can also find its Ntuple Location on the top. Instead of the files copied in scp.sh in Example, you can use a file with the following name to scan the calibration set of your choice.

|  Example file name  |&rarr;|File name of calibration data set|
|:-------------------:|:----:|:-------------------------------:|
|    CaliWave.root    |&rarr;|    LArCaliWave_00XXXXXX.root    |
|    PhysWave.root    |&rarr;| LArPhysWave_RTM_00XXXXXX_.root  |
|    OFCCali.root     |&rarr;|    LArOFCCali_00XXXXXX.root     |
|    OFCPhys.root     |&rarr;|LArOFCPhys_00XXXXXX_4samples.root|
|   PedAutoCorr.root  |&rarr;|   LArPedAutoCorr_00XXXXXX.root  |

`SCdict_DBtest.root`,`LArIDtranslator_Barrel.txt`, `LArIDtranslator_Endcap.txt` are the dictionary of SCs basic information (layers, eta, phi ...) so not needed to be replaced. `LArDAC2MeV.root` is TTrees of DAC to MeV factors. This isn't needed to replaced untill the factor is updated.
- Calibration run output is devided in four files. You need to merge these before processing the OFCscan steps. `hadd` can do that. Try
```
hadd ~/Your/output/direcrotry/CaliWave.root Ntuple/Location/found/on/LAr_Phase1_Validation_Plot_Viewer/LArCaliWave_*.root
```
- Calibration output sometimes lacks some SC data. This should be reported. You can get missing SC list by `NoCalibDatacheck.py`.
- `make_onlyinPhysList.py` can extract SCs which is in `BadChannelList_Phys.txt` but not in `BadChannelList_Cali.txt`. This should be run after you scaned both of OFCphys and OFCcali.
- `make_simple_list.py` make a simple list comtaining SC online ID and its issue. This is useful when you share the result or use in [LArPromptAnalysis](https://indico.cern.ch/event/1155051/).
- Condor script to process all of sub-detectors is not developped. You can use `*.sh` scripts for now.

# Routin in 2022 ~ 2023.
I, [Marin FURUKAWA](marin.furukawa@cern.ch), did OFC scan in 2022 and 2023. After I scanned OFCphys and cali as described in [Example](#example),
1. Run `make_onlyinPhysList.py`. Then get `BadChannelList_onlyInPhys.txt`.
2. Execute 
```
cat BadChannelList_Cali.txt BadChannelList_onlyInPhys.txt > BadChannelList_total.txt
```
and delete the first line of the second input file in `BadChannelList_total.txt`. This will be the base of the bad SC list.

3. Run `NoCalibDatacheck.py` and check if there are missing SCs.

4. Check all of bad SCs in BadChannelList_total.txt and find a cuase. The bad SCs are sorted into the following three types.
- Bad SC or OFC: The SC has cleary an issue and its OFC isn't usanle. The SC should be masked.
- Usable SC: The SC has issues but not serious. OFC is usable.
- OK: The SC is detected by OFC scan algo because the value is slightly higher than the criteria. Its pulse shape and simulation result looks OK and its OFC is usable. The SC should be removed from the bad list.

If you don't know which type is applicable, you can discuss with the DQ experts. Collision data is also good to know usability of OFCs. ([LArPromptAnalysis](https://indico.cern.ch/event/1155051/) can apply the OFC to the pulse generated by the collision.) It's worth to reporting how many and which SCs are recovered or went bad comparing the last scan. `compaire_2results.py` help you to do that.

5. run `make_simple_list.py`, remove OK SCs, and share it.

6. Usable SCs, Bad SCs and the result of `NoCalibDatacheck.py` should be reported in [the DQ meeting](https://indico.cern.ch/category/6430/) and need to investigate to fix it. You can ask an expert to mask bad SCs.

# Useful scripts
These are usefull when you investigate the cause of detected issues. These are in `./further_study/`
- `EtComparison.py`, `EtComparison_perLayer.py`:These plot Et by DT readout vs Et by main readout. It's useful to validate OFC with pp collision data. Input TTree is `TreeSC` generated by [LArPromptAnalysis](https://indico.cern.ch/event/1155051/).
- `OFCdistribution.py`: This visualise each OFC valiue per Layer.
- `Pedestal_distribution.py`: This visualise each pedestal per layer.
- `replace_flag.py`: This help you when you need to patch some flag of bad SCs. The output file of `make_simple_list.py` can be an input file.
