inputlist = 'BadChannelList_total.txt'
#skiplist = 'skiplist.txt' #if you want to remove some SCs from the inputlist above.
outfile = 'BadChannelList_public.txt'

#skip = []
#with open(skiplist) as fin:
#    lines = fin.readlines()
#    for line in lines:
#        tmp = line.split()
#        skip.append(tmp[1])

fout = open(outfile, 'w')
with open(inputlist) as fin:
    lines = fin.readlines()
    for i in range(4,len(lines)):
        tmp = lines[i].split()
        reason = tmp[-1]
        if tmp[1] in skip:continue
        elif "Amp" in reason:fout.write(tmp[1]+" Bad_Amplitude\n")
        elif "OFC" in reason:fout.write(tmp[1]+" Bad_OFC\n")
        elif "Slope" in reason:fout.write(tmp[1]+" Deformed_Pulse\n")
        elif "Under" in reason:fout.write(tmp[1]+" Deformed_Pulse\n")
        elif "Mean" in reason:fout.write(tmp[1]+" Bad_reconstruction\n")
        else: print(f"WARNING: {reason} don't match any flag!")
fin.close()
fout.close()
