import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import OFCCalibratorModule
args = sys.argv

#execute this file like "python3 makeOFCTree.py EMB0 Phys"

#command line auguments
area = args[1] #EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal
wave = args[2] #Cali or Phys

#OFCCalibrator
if wave == "Cali":
    OFCCaliTree = OFCCalibratorModule.OFCCaliTree(f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/OFCCali.root')
    OFCCaliTree.NthPhase = 23
    OFCCaliTree.OFBCshift = 0
    OFCCaliTree.OutputFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/OFC_Cali_{area}_phase23.root'
    OFCCaliTree.CellPropertyFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/cellprop_CaliWave_{area}.root'
    OFCCaliTree.MakeTree()
elif wave == "Phys":
    OFCPhysTree = OFCCalibratorModule.OFCPhysTree(f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/OFCPhys.root')
    OFCPhysTree.NthPhase = 23
    OFCPhysTree.OFBCshift = 0
    OFCPhysTree.OutputFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/OFC1nsmu_Phys_{area}_phase23.root'
    OFCPhysTree.CellPropertyFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/cellprop_PhysWave_{area}.root'
    OFCPhysTree.MakeTree()
