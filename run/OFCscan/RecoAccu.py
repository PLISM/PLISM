import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import SCconditionSimulatorModule
args = sys.argv
#execute this file like "python3 RecoAccu.py EMB0 Phys"

#command line arguments
area = args[1] #EMB0, EMB1, EMB2, EMB3, EMEC0, EMEC1, EMEC2, EMEC3, HEC or FCal
wave = args[2] #Cali or Phys

SCconditionSimulator = SCconditionSimulatorModule.ReconstructedTree(f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/reconstructed{wave}_{area}.root')
SCconditionSimulator.OutputFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/RecoAccu{wave}_{area}.root'
SCconditionSimulator.OutputBranch_PileupTrueEt = False
SCconditionSimulator.OutputBranch_PileupTrueTau = False
SCconditionSimulator.OutputBranch_SignalTrueEtTau = False
SCconditionSimulator.OutputBranch_RecEtTau = False
SCconditionSimulator.OutputBranch_MeanRecEtTau = False
SCconditionSimulator.OutputBranch_RMSRecEtTau = False
SCconditionSimulator.inputCellpropFileName = f'/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/cellprop_{wave}Wave_{area}.root'
SCconditionSimulator.RecoAccuracyTree()
