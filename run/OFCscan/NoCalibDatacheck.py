import ROOT as root
root.gROOT.SetBatch(True)


inputdata = '/afs/cern.ch/user/m/mfurukaw/storage/BarrelEndcapWeekly_240416-004831/CaliWave.root'
SCdict = '/afs/cern.ch/user/m/mfurukaw/storage/public/data4PLISM/SCfiber.root'
channel = []

fin = root.TFile(inputdata)
tin = fin.Get("CALIWAVE")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("channelId",1)
nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    channel.append(tin.channelId)
fin.Close()


fin = root.TFile(SCdict)
tin = fin.Get("SCfiber")
tin.SetBranchStatus("*",0)
tin.SetBranchStatus("channelId",1)
tin.SetBranchStatus("latome_fiber",1)
nEntries = tin.GetEntries()
for i in range(nEntries):
    tin.GetEntry(i)
    if tin.channelId not in channel:
        print(f"{tin.channelId} {tin.latome_fiber}")
fin.Close()
