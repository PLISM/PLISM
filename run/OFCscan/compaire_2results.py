pre_list = "/afs/cern.ch/user/m/mfurukaw/storage/public/list/current_BadSC_list"
new_list = "/afs/cern.ch/user/m/mfurukaw/public/OFCscan_result.txt"
outfile = "/afs/cern.ch/user/m/mfurukaw/public/Compair_to_LastScan.txt"

#make previous bad list
dPre = {}
with open(pre_list) as fin:
    lines = fin.readlines()
    for line in lines:
        value = line.split()
        dPre[value[0]] = value[-1]
fin.close()
preSCs = dPre.keys()
print(f"{len(preSCs)} SCs in previous bad list.")

#Search newly added bad SCs.
new_result = []
fout = open(outfile, "w")
fout.write('compared two file;\n')
fout.write(f'{pre_list} as previous bad list\n')
fout.write(f'{new_list} as new bad list\n')
fout.write('\nBad SCs in the new list but not in the previous (Newly Added).\n')
with open(new_list) as fin:
    lines = fin.readlines()
    for line in lines:
        value = line.split()
        channel = value[0]
        new_result.append(channel)
        if (channel not in preSCs):fout.write(line)
fin.close()
print(f"{len(new_result)} SCs in new bad list.")

#search SCs looks recovered.
fout.write('\nBad SCs in the previous bad list but not in the new (recovered).\n')
for preSC in preSCs:
    if preSC not in new_result:fout.write('{:<11s}{:<15s}\n'.format(preSC,dPre[preSC]))
fout.close()

