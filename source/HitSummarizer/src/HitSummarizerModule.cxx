#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "HitSummaryGenerator.h"

using namespace HitSummarizer;

PYBIND11_MODULE(HitSummarizerModule, m)
{
    pybind11::class_<HitSummaryGenerator>(m, "HitSummarizer")
      .def_readwrite("InputFileName", &HitSummaryGenerator::InputFileName)
      .def_readwrite("OutputFileName", &HitSummaryGenerator::OutputFileName)
      .def_readwrite("CellPropertyFileName", &HitSummaryGenerator::CellPropertyFileName)
      .def_readwrite("LITFileName", &HitSummaryGenerator::LITFileName)
      .def_readwrite("NEvent", &HitSummaryGenerator::NEvent)
      .def_readwrite("EtMin", &HitSummaryGenerator::EtMin)
      .def_readwrite("EtMax", &HitSummaryGenerator::EtMax)
      .def_readwrite("TauMin", &HitSummaryGenerator::TauMin)
      .def_readwrite("TauMax", &HitSummaryGenerator::TauMax)
      .def_readwrite("seed", &HitSummaryGenerator::seed)
      .def(pybind11::init<const std::string&>())
      .def(pybind11::init<>())
      .def("summarize", &HitSummaryGenerator::summarize)
      .def("makeSignal", &HitSummaryGenerator::makeSignal);
}
