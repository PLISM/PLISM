/// @addtogroup HitSummarizer
/// @{

#pragma once
#include <deque>
#include "CellPropertyTreeBase.h"

namespace HitSummarizer
{
    /// Class to extract information from cell property tree
    class CellPropertyTree : public CellPropertyTreeBase
    {
      public:
        /// Structure containing pulse shape information
        struct GData {
            std::vector<float>* ET; ///< Input Et[event] in MeVT following order in cell property tree
                                    /**
                                      * @brief g function g[nthET][nthSample]
                                      *
                                      * nthET and nthSample follow order in cell property tree
                                      */
            std::vector<std::deque<double> >* g;
            int sampperbc; ///< Number of samples per BC
        };

        /// Structure containing Cell properties
        struct CellConstant {
            float LSB;   ///< ET/ADC in MeVT
            float noise; ///< Pedestal RMS in ADC
        };

        typedef std::unordered_map<unsigned int, GData> GDataMap;               ///< channelId vs @ref GData map
        typedef std::unordered_map<unsigned int, CellConstant> CellConstantMap; ///< channelId vs @ref CellConstant map

        /// @param inputFileName Input cell property file name
        CellPropertyTree(const std::string& inputFileName) :
            CellPropertyTreeBase(inputFileName) {}

        std::vector<UInt_t> getChannelIDs(); ///< Output vector of channelId
    };
} // namespace HitSummarizer
/// @}
