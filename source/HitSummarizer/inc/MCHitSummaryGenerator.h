/// @addtogroup HitSummarizer
/// @{

#pragma once
#include <TInterpreter.h>
#include <TMath.h>
#include <TRandom.h>
#include "LArIDtranslator.h"
#include "TreeReader.h"

namespace HitSummarizer
{
    /// Generator class of pileup hit summary trees
    class MCHitSummaryGenerator : public TreeReader
    {
      public:
        /// @param inputFileName Input HITS file name
        MCHitSummaryGenerator(const std::string& inputFileName) :
            TreeReader(inputFileName, "gend3pd"),
            hitemb_n(m_reader, "hitemb_n"),
            hitemb_eta(m_reader, "hitemb_eta"),
            hitemb_phi(m_reader, "hitemb_phi"),
            hitemb_E(m_reader, "hitemb_E"),
            hitemb_Time(m_reader, "hitemb_Time"),
            hitemb_ID(m_reader, "hitemb_ID"),
            hitemec_n(m_reader, "hitemec_n"),
            hitemec_eta(m_reader, "hitemec_eta"),
            hitemec_phi(m_reader, "hitemec_phi"),
            hitemec_E(m_reader, "hitemec_E"),
            hitemec_Time(m_reader, "hitemec_Time"),
            hitemec_ID(m_reader, "hitemec_ID"),
            hitfcal_n(m_reader, "hitfcal_n"),
            hitfcal_eta(m_reader, "hitfcal_eta"),
            hitfcal_phi(m_reader, "hitfcal_phi"),
            hitfcal_E(m_reader, "hitfcal_E"),
            hitfcal_Time(m_reader, "hitfcal_Time"),
            hitfcal_ID(m_reader, "hitfcal_ID"),
            hithec_n(m_reader, "hithec_n"),
            hithec_eta(m_reader, "hithec_eta"),
            hithec_phi(m_reader, "hithec_phi"),
            hithec_E(m_reader, "hithec_E"),
            hithec_Time(m_reader, "hithec_Time"),
            hithec_ID(m_reader, "hithec_ID") {}

        void summarize(); ///< Generate pileup hit summary file

        std::string OutputFileName; ///< Return pileup hit summary file name
        std::string LITFileName;    ///< Input LAr ID translator file name

      private:
        TTreeReaderValue<Int_t> hitemb_n;
        TTreeReaderValue<std::vector<float>> hitemb_eta;
        TTreeReaderValue<std::vector<float>> hitemb_phi;
        TTreeReaderValue<std::vector<float>> hitemb_E;
        TTreeReaderValue<std::vector<float>> hitemb_Time;
        TTreeReaderValue<std::vector<unsigned int>> hitemb_ID;
        TTreeReaderValue<Int_t> hitemec_n;
        TTreeReaderValue<std::vector<float>> hitemec_eta;
        TTreeReaderValue<std::vector<float>> hitemec_phi;
        TTreeReaderValue<std::vector<float>> hitemec_E;
        TTreeReaderValue<std::vector<float>> hitemec_Time;
        TTreeReaderValue<std::vector<unsigned int>> hitemec_ID;
        TTreeReaderValue<Int_t> hitfcal_n;
        TTreeReaderValue<std::vector<float>> hitfcal_eta;
        TTreeReaderValue<std::vector<float>> hitfcal_phi;
        TTreeReaderValue<std::vector<float>> hitfcal_E;
        TTreeReaderValue<std::vector<float>> hitfcal_Time;
        TTreeReaderValue<std::vector<unsigned int>> hitfcal_ID;
        TTreeReaderValue<Int_t> hithec_n;
        TTreeReaderValue<std::vector<float>> hithec_eta;
        TTreeReaderValue<std::vector<float>> hithec_phi;
        TTreeReaderValue<std::vector<float>> hithec_E;
        TTreeReaderValue<std::vector<float>> hithec_Time;
        TTreeReaderValue<std::vector<unsigned int>> hithec_ID;
    };
} // namespace HitSummarizer
/// @}
