#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class TreeReader;
#    pragma link C++ class CellPropertyTreeBase;
#    pragma link C++ class LArIDtranslator;
#    pragma link C++ class HitSummarizer::HitSummaryGenerator;
#    pragma link C++ class HitSummarizer::MCHitSummaryGenerator;
#    pragma link C++ class HitSummarizer::CellPropertyTree;

#endif
