#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TTreeReader.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::TrainPeriodicAverageOverlayPlotter(
        const std::string& DataName, const std::string& legend,
        const std::string& OutputFileName, bool isMeVtoGeV)
    {
        // canvas
        TCanvas c{"c", "c", 1200, 800};
        c.Clear();
        c.Update();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        SetAtlasStyle();
        c.SetFrameFillColor(kWhite);
        c.cd();

        // legend
        TLegend leg{0.46, 0.72, 0.53, 0.79};
        leg.SetBorderSize(0);
        leg.SetLineColor(0);
        leg.SetFillColor(0);
        leg.SetTextSize(0.02);

        for (unsigned int label = 0; label - 1 < database.NumInputFiles;
             label++) {
            if (database.Periods_BC[label] != database.Periods_BC[label + 1]) {
                std::cerr
                    << ErrorMessage << "::TrainPeriodicAverageOverlayPlotter: "
                    << "Period_BC shuld be the same for all InputFiles.\n";
                return exit(EXIT_FAILURE);
            }
        }

        std::vector<TGraph*> vecg;
        double xup{__DBL_MIN__};
        double xlow{__DBL_MAX__};
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};

        for (unsigned int label = 0; label < database.NumInputFiles; label++) {
            vecg.push_back(generator.TrainPeriodicAverageGenerator(
                label, DataName, isMeVtoGeV));
            vecg[label]->SetMarkerColor(label + 2);
            vecg[label]->SetLineColor(label + 2);
            xup = std::max(xup, vecg[label]->GetXaxis()->GetXmax());
            xlow = std::min(xlow, vecg[label]->GetXaxis()->GetXmin());
            yup = std::max(yup, vecg[label]->GetMaximum());
            ylow = std::min(ylow, vecg[label]->GetMinimum());
            leg.AddEntry(vecg[label],
                         DefaultLegendGenerator(label, legend).c_str(), "l");
        }
        for (unsigned int i = 0; i < database.NumInputFiles; i++) {
            if (i == 0) {
                vecg[i]->GetXaxis()->SetLimits(xlow, xup);
                vecg[i]->SetMaximum(yup);
                vecg[i]->SetMinimum(ylow);
                vecg[i]->GetXaxis()->SetNdivisions(505);
                vecg[i]->Draw("al");
            } else {
                vecg[i]->Draw("l");
            }
        }
        leg.Draw();

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        // latex

        // Save
        if (OutputFileName.empty()) {
            c.SaveAs("./figure/TrainPeriodicAverageOvelayPlotter.pdf");
            std::cout << "Outputfile is saved as "
                         "./figure/TrainPeriodicAverageOvelayPlotter.pdf"
                      << std::endl;
        } else {
            c.SaveAs(OutputFileName.c_str());
            std::cout << "Outputfile is saved as " << OutputFileName.c_str()
                      << std::endl;
        }
        c.Close();
    }
}  // namespace PLISMAnalysis