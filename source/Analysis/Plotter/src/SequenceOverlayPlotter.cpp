#include <TCanvas.h>
#include <TColor.h>
#include <TLegend.h>
#include <TLine.h>
#include <TStyle.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::SequenceOverlayPlotter(const std::string& DataName,
                                         const std::string& legend,
                                         int MinTime_ns, int MaxTime_ns,
                                         const std::string& OutputFileName,
                                         bool isMeVtoGeV)
    {
        // canvas
        TCanvas c{"c", "c", 1200, 800};
        c.Clear();
        c.Update();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        SetAtlasStyle();
        c.SetFrameFillColor(kWhite);
        c.cd();

        // legend
        TLegend leg{0.46, 0.72, 0.52, 0.79};
        leg.SetBorderSize(0);
        leg.SetLineColor(0);
        leg.SetFillColor(0);
        leg.SetTextSize(0.02);

        std::vector<TGraph*> vecg;
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};

        for (unsigned int label = 0; label < database.NumInputFiles; label++) {
            vecg.push_back(
                generator.SequenceGenerator(label, DataName, isMeVtoGeV));
            vecg[label]->SetMarkerSize(1.0);
            vecg[label]->SetLineWidth(2);
            vecg[label]->SetMarkerColor(label + 2);
            vecg[label]->SetLineColor(label + 2);
            vecg[label]->GetXaxis()->SetLimits(MinTime_ns, MaxTime_ns);
            yup = std::max(yup, vecg[label]->GetMaximum());
            ylow = std::min(ylow, vecg[label]->GetMinimum());
            leg.AddEntry(vecg[label],
                         DefaultLegendGenerator(label, legend).c_str(), "lp");
        }
        yup += abs(yup) * 0.1;
        ylow -= abs(ylow) * 0.1;

        for (unsigned int i = 0; i < database.NumInputFiles; i++) {
            if (i == 0) {
                vecg[i]->SetMaximum(yup);
                vecg[i]->SetMinimum(ylow);
                vecg[i]->GetXaxis()->SetNdivisions(505);
                vecg[i]->Draw("apl");
            } else {
                vecg[i]->Draw("pl");
            }
        }
        leg.Draw();

        // line
        TLine line{static_cast<double>(MinTime_ns), 0.0,
                   static_cast<double>(MaxTime_ns), 0.0};
        line.SetLineColor(12);
        line.SetLineWidth(1);
        line.SetLineStyle(2);
        line.Draw();

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        // Save
        if (OutputFileName.empty()) {
            c.SaveAs("./figure/SequenceOverlay.pdf");
            std::cout << "Outputfile is saved as ./figure/SequenceOverlay.pdf."
                      << std::endl;
        } else {
            c.SaveAs(OutputFileName.c_str());
            std::cout << "Outputfile is saved as " << OutputFileName
                      << std::endl;
        }

        c.Close();
    }
}  // namespace PLISMAnalysis