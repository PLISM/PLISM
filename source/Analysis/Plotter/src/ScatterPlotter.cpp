#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TTreeReader.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::ScatterPlotter(const int InputFileLabel,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 const double xlow, const double xup,
                                 const double ylow, const double yup,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        auto&& g = generator.ScatterGenerator(InputFileLabel, xDataName,
                                              yDataName, xlow, xup, ylow, yup,
                                              isOnlyEventBCDataUsed);
        if (OutputFilename.empty()) {
            TGraphPlotterHelper(g, "AP", 0.075,
                                "./figure/Scatter_" + xDataName + "_vs_" +
                                    yDataName + "_" +
                                    std::to_string(InputFileLabel) + ".pdf");
        } else {
            TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
        }
    }
    void Plotter::ScatterPlotter(const std::string& InputFilename,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 const double xlow, const double xup,
                                 const double ylow, const double yup,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        auto&& g = generator.ScatterGenerator(InputFilename, xDataName,
                                              yDataName, xlow, xup, ylow, yup,
                                              isOnlyEventBCDataUsed);
        if (OutputFilename.empty()) {
            TGraphPlotterHelper(
                g, "AP", 0.075,
                "./figure/Scatter_" + xDataName + "_vs_" + yDataName + "_" +
                    std::to_string(
                        database.InputFilenameToLabel[InputFilename]) +
                    ".pdf");
        } else {
            TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
        }
    }
    void Plotter::ScatterPlotter(const std::string& xDataName,
                                 const std::string& yDataName,
                                 const double xlow, const double xup,
                                 const double ylow, const double yup,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        for (unsigned int InputFileLabel = 0;
             InputFileLabel < database.NumInputFiles; InputFileLabel++) {

            auto&& g = generator.ScatterGenerator(
                InputFileLabel, xDataName, yDataName, xlow, xup, ylow, yup, isOnlyEventBCDataUsed);

            if (database.NumInputFiles == 1) {
                TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);  
            } else {
                if (InputFileLabel == 0) {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename+ "(");
                } else if (InputFileLabel == database.NumInputFiles - 1) {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename + ")");
                } else {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
                }
            }
        }
    }

    void Plotter::ScatterPlotter(const int InputFileLabel,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        auto&& g = generator.ScatterGenerator(InputFileLabel, xDataName,
                                              yDataName, isOnlyEventBCDataUsed);
        if (OutputFilename.empty()) {
            TGraphPlotterHelper(g, "AP", 0.075,
                                "./figure/Scatter_" + xDataName + "_vs_" +
                                    yDataName + "_" +
                                    std::to_string(InputFileLabel) + ".pdf");
        } else {
            TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
        }
    }
    void Plotter::ScatterPlotter(const std::string& InputFilename,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        auto&& g = generator.ScatterGenerator(InputFilename, xDataName,
                                              yDataName, isOnlyEventBCDataUsed);
        if (OutputFilename.empty()) {
            TGraphPlotterHelper(
                g, "AP", 0.05,
                "./figure/Scatter_" + xDataName + "_vs_" + yDataName + "_" +
                    std::to_string(
                        database.InputFilenameToLabel[InputFilename]) +
                    ".pdf");
        } else {
            TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
        }
    }
    void Plotter::ScatterPlotter(const std::string& xDataName,
                                 const std::string& yDataName,
                                 bool isOnlyEventBCDataUsed,
                                 const std::string& OutputFilename)
    {
        for (unsigned int InputFileLabel = 0;
             InputFileLabel < database.NumInputFiles; InputFileLabel++) {
            auto&& g = generator.ScatterGenerator(InputFileLabel, xDataName,
                                                  yDataName, isOnlyEventBCDataUsed);

            if (database.NumInputFiles == 1) {
                TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
            } else {
                if (InputFileLabel == 0) {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename + "(");
                } else if (InputFileLabel == database.NumInputFiles - 1) {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename + ")");
                } else {
                    TGraphPlotterHelper(g, "AP", 0.075, OutputFilename);
                }
            }
        }
    }

    void Plotter::ScatterPlotterALL(const int InputFileLabel,
                                    bool isOnlyEventBCDataUsed,
                                    const std::string& OutputFolderName)
    {
        std::vector<std::string> DataNames{
            "SignalTrueEt", "SignalTrueTau", "RecEt",        "RecTau",
            "RecEtTau",     "EtResolution",  "TauResolution"};
        for (unsigned int i = 0; i < DataNames.size(); i++) {
            for (unsigned int j = i; j < DataNames.size(); j++) {
                // canvas
                TCanvas c{"c", "c", 300, 200};
                c.Clear();
                c.Update();
                gStyle->SetPadTickX(1);
                gStyle->SetPadTickY(1);
                SetAtlasStyle();
                c.SetFrameFillColor(kWhite);
                c.cd();

                std::cout << "\nDrawing \"" << DataNames[i] << " vs "
                          << DataNames[j] << "\"\n";
                auto&& g = generator.ScatterGenerator(
                    InputFileLabel, DataNames[i], DataNames[j],
                    isOnlyEventBCDataUsed);

                g->SetMarkerColor(kBlue);
                g->SetMarkerSize(0.075);
                g->Draw("ap");

                // gPad
                gPad->SetTopMargin(0.2);
                gPad->SetRightMargin(0.2);
                gPad->SetLeftMargin(0.2);
                gPad->SetBottomMargin(0.2);

                // Save
                c.Print(
                    std::string{"./figure/" + OutputFolderName + "/Scatter_" +
                                DataNames[i] + "_vs_" + DataNames[j] + ".pdf"}
                        .c_str(),
                    "pdf");
                c.Close();
            }
        }
    }
}  // namespace PLISMAnalysis