#include <TCanvas.h>
#include <TLegend.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::ScatterOverlayPlotter(const ::std::string& xDataName,
                                        const std::string& yDataName,
                                        const std::string& legend,
                                        bool isOnlyEventBCDataUsed,
                                        const std::string& OutputFilename)
    {
        // canvas
        TCanvas c{"c", "c", 1200, 800};
        c.Clear();
        c.Update();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        SetAtlasStyle();
        c.SetFrameFillColor(kWhite);
        c.cd();

        // legend
        TLegend leg{0.46, 0.72, 0.53, 0.79};
        leg.SetBorderSize(0);
        leg.SetLineColor(0);
        leg.SetFillColor(0);
        leg.SetTextSize(0.02);

        std::vector<TGraph*> vecg;
        double xup{__DBL_MIN__};
        double xlow{__DBL_MAX__};
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};

        for (unsigned int label = 0; label < database.NumInputFiles; label++) {
            vecg.push_back(generator.ScatterGenerator(
                label, xDataName, yDataName, isOnlyEventBCDataUsed));
            vecg[label]->SetMarkerColor(label + 2);
            vecg[label]->SetLineColor(label + 2);
            vecg[label]->SetMarkerSize(0.20);
            xup = std::max(xup, vecg[label]->GetXaxis()->GetXmax());
            xlow = std::min(xlow, vecg[label]->GetXaxis()->GetXmin());
            yup = std::max(yup, vecg[label]->GetMaximum());
            ylow = std::min(ylow, vecg[label]->GetMinimum());
            leg.AddEntry(vecg[label],
                         DefaultLegendGenerator(label, legend).c_str(), "l");
        }
        yup += abs(yup) * 0.1;
        ylow -= abs(ylow) * 0.1;
        for (unsigned int i = 0; i < database.NumInputFiles; i++) {
            if (i == 0) {
                vecg[i]->GetXaxis()->SetLimits(xlow, xup);
                vecg[i]->SetMaximum(yup);
                vecg[i]->SetMinimum(ylow);
                vecg[i]->Draw("ap");
            } else {
                vecg[i]->Draw("p");
            }
        }
        leg.Draw();

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        // latex

        // Save
        if (OutputFilename.empty()) {
            c.SaveAs("figure/OverlayScatter.pdf");
            std::cout << "Outputfile is saved as OverlayScatter.pdf"
                      << std::endl;
        } else {
            c.SaveAs(OutputFilename.c_str());
            std::cout << "Outputfile is saved as " << OutputFilename.c_str()
                      << std::endl;
        }
        c.Close();
    }
}  // namespace PLISMAnalysis