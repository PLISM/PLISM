#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "Plotter.hpp"

PYBIND11_MODULE(PlotterModule, m)
{
    pybind11::class_<PLISMAnalysis::Plotter>(m, "Plotter")
        .def(pybind11::init<const std::vector<std::string>&>())
        .def(
            "TH2DPlotter",
            pybind11::overload_cast<const int, const std::string&,
                                    const std::string&, const int, const double,
                                    const double, const int, const double,
                                    const double, bool, const std::string&>(
                &PLISMAnalysis::Plotter::TH2DPlotter))
        .def(
            "TH2DPlotter",
            pybind11::overload_cast<const std::string&, const std::string&,
                                    const std::string&, const int, const double,
                                    const double, const int, const double,
                                    const double, bool, const std::string&>(
                &PLISMAnalysis::Plotter::TH2DPlotter))
        .def("TH2DPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const int, const double, const double,
                                     const int, const double, const double,
                                     bool, const std::string&>(
                 &PLISMAnalysis::Plotter::TH2DPlotter))
        .def("TH2DPlotter",
             pybind11::overload_cast<const int, const std::string&,
                                     const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::TH2DPlotter))
        .def("TH2DPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::TH2DPlotter))
        .def("TH2DPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     bool, const std::string&>(
                 &PLISMAnalysis::Plotter::TH2DPlotter))
        .def("TH2DPlotterALL",
             pybind11::overload_cast<const int, bool, const std::string&>(
                 &PLISMAnalysis::Plotter::TH2DPlotterALL))
        .def("ScatterPlotter",
             pybind11::overload_cast<const int, const std::string&,
                                     const std::string&, const double,
                                     const double, const double, const double,
                                     bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, const double,
                                     const double, const double, const double,
                                     bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const double, const double, const double,
                                     const double, bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterPlotter",
             pybind11::overload_cast<const int, const std::string&,
                                     const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotter))
        .def("ScatterOverlayPlotter",
             pybind11::overload_cast<const ::std::string&, const std::string&,
                                     const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterOverlayPlotter))
        .def("ScatterPlotterALL",
             pybind11::overload_cast<const int, bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotterALL))
        .def("ScatterPlotterALL",
             pybind11::overload_cast<const std::string&, bool,
                                     const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotterALL))
        .def("ScatterPlotterALL",
             pybind11::overload_cast<bool, const std::string&>(
                 &PLISMAnalysis::Plotter::ScatterPlotterALL))
        .def("SequencePlotter",
             pybind11::overload_cast<const int, int, int, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::SequencePlotter))
        .def("SequencePlotter",
             pybind11::overload_cast<const std::string&, int, int,
                                     const std::string&, const std::string&,
                                     bool>(
                 &PLISMAnalysis::Plotter::SequencePlotter))
        .def("SequencePlotter",
             pybind11::overload_cast<int, int, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::SequencePlotter))
        .def("SequenceOverlayPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     int, int, const std::string&, bool>(
                 &PLISMAnalysis::Plotter::SequenceOverlayPlotter))
        .def("TrainPeriodicAveragePlotter",
             pybind11::overload_cast<const int, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicAveragePlotter))
        .def("TrainPeriodicAveragePlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicAveragePlotter))
        .def("TrainPeriodicAverageOverlayPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicAverageOverlayPlotter))
        .def("TrainPeriodicRMSPlotter",
             pybind11::overload_cast<const int, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicRMSPlotter))
        .def("TrainPeriodicRMSOverlayPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicRMSOverlayPlotter))
        .def("TrainPeriodicRMSPlotter",
             pybind11::overload_cast<const std::string&, const std::string&,
                                     const std::string&, bool>(
                 &PLISMAnalysis::Plotter::TrainPeriodicRMSPlotter));
}
