#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TTreeReader.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::TrainPeriodicRMSPlotter(const int InputFileLabel,
                                          const std::string& DataName,
                                          const std::string& OutputFileName,
                                          bool isMeVtoGeV)
    {
        auto&& g = generator.TrainPeriodicRMSGenerator(InputFileLabel, DataName,
                                                       isMeVtoGeV);
        if (OutputFileName.empty()) {
            TGraphPlotterHelper(g, "al", 0.5,
                                "./figure/TrainPeriodicRMS_" +
                                    std::to_string(InputFileLabel) + ".pdf");
        } else {
            TGraphPlotterHelper(g, "al", 0.5, OutputFileName);
        }
    }
}  // namespace PLISMAnalysis
