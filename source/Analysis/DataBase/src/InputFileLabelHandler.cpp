#include "../inc/DataBase.hpp"

namespace PLISMAnalysis
{
    void DataBase::InputFileLabelHandler(int InputFileLabel)
    {
        try {
            if (NumInputFiles <= static_cast<unsigned int>(InputFileLabel))
                throw InputFileLabel;
        } catch (int label) {
            std::cerr << ErrorMessage << "::InputFileLabelHandler: "
                      << "InputFileLabel:" << label << " is Invalid.\n";
            exit(EXIT_FAILURE);
        }
    }
}  // namespace PLISMAnalysis
