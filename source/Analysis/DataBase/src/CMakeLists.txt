include_directories(${PROJECT_SOURCE_DIR}/inc ${CMAKE_SOURCE_DIR}/Bases/inc)
file(GLOB INCS "${PROJECT_SOURCE_DIR}/inc/*hpp")
file(GLOB LINKDEF_H "${PROJECT_SOURCE_DIR}/inc/LinkDef.h")
list(REMOVE_ITEM INCS ${LINKDEF_H})
root_generate_dictionary(DataBaseRTARGETDict ${INCS} LINKDEF ${LINKDEF_H})

# create libDataBaseLib.so
add_library(DataBaseLib
  SHARED
    InputFileLabelHandler.cpp
    InputFilenameHandler.cpp
    PrintLabels.cpp
    DataBase.cpp
    DataBaseRTARGETDict.cxx
)

link_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_BINARY_DIR}/Bases/src)

target_link_libraries(DataBaseLib ${ROOT_LIBRARIES} BasesLib)

#pybind11_add_module(DataBaseModule DataBaseModule.cxx)
#target_link_libraries(DataBaseModule PRIVATE DataBaseLib)
