#include "../inc/DataBase.hpp"

namespace PLISMAnalysis
{
    void DataBase::PrintLabels()
    {
        if (NumInputFiles == 0) {
            std::cout << "No Inputfile!!!"
                      << "\n";
            return;
        }
        std::cout << "\n"
                  << std::setw(7) << std::left << "label" << std::left
                  << "filename"
                  << "\n";
        for (unsigned i = 0; i < InputFilenames.size(); i++) {
            std::cout << std::setw(7) << std::left << i << std::setw(7)
                      << std::left << InputFilenames[i] << "\n";
        }
    }
}  // namespace PLISMAnalysis
