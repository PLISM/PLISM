#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "AtlasStyle.hpp"

PYBIND11_MODULE(AtlasStyleModule, m)
{
  m.def("SetAtlasStyle", &SetAtlasStyle);
  m.def("AtlasStyle", &AtlasStyle);
}
