#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    TGraph* Generator::SelectedEtGenerator(const int InputFileLabel, bool isGeV)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        auto&& fin = AnalysisData.InputFiles[InputFileLabel];
        auto g = new TGraph{};
        std::string title{";time [ns]; " +
                          DefaultAxisLabel(std::string{"SelectedEt"}, isGeV)};
        g->SetTitle(title.c_str());
        TTreeReader reader{AnalysisData.InputFileTypes[InputFileLabel].c_str(),
                           fin};
        TTreeReaderValue<std::vector<double>> RecEt{reader, "RecEt"};
        TTreeReaderValue<std::vector<double>> RecTau{reader, "RecTau"};
        double Etlow{__DBL_MAX__}, Etup{__DBL_MIN__};
        while (reader.Next()) {
            for (unsigned long long BC = 0; BC < (*RecEt).size(); BC++) {
                if (TauCriteria((*RecTau)[BC], (*RecEt)[BC])) {
                    double et{(*RecEt)[BC] / (1. + 999. * isGeV)};
                    g->SetPoint(g->GetN(), BC * AnalysisData.BCInterval_ns, et);
                    Etlow = std::min(Etlow, et);
                    Etup = std::max(Etup, et);
                }
            }
        }
        g->SetMaximum(Etup);
        g->SetMinimum(Etlow);
        return g;
    }
}  // namespace PLISMAnalysis