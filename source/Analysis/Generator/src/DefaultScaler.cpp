#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    double Generator::DefaultDataScaler(const std::string& DataName)
    {
        if (std::regex_search(DataName, std::regex{"SignalTrueEt"}) ||
            std::regex_search(DataName, std::regex{"SignalTrueEtTau"}) ||
            std::regex_search(DataName, std::regex{"RecEt"}) ||
            std::regex_search(DataName, std::regex{"RecEtTau"}) ||
            std::regex_search(DataName, std::regex("DigitLSB"))) {
            return 0.001;
        } else {
            return 1.0;
        }
    }
}  // namespace PLISMAnalysis
