#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    TGraph* Generator::ScatterGenerator(const int InputFileLabel,
                                        const std::string& xDataName,
                                        const std::string& yDataName,
                                        const double xlow, const double xup,
                                        const double ylow, const double yup,
                                        bool isOnlyEventBCDataUsed)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);

        if (std::regex_search(xDataName, std::regex{"Resolution"}) &&
            std::regex_search(yDataName, std::regex{"Resolution"})) {
            auto&& x = ResolutionGenerator(InputFileLabel, xDataName);
            auto&& y = ResolutionGenerator(InputFileLabel, yDataName);
            return ScatterGenerator<std::vector<double>, std::vector<double>>(
                InputFileLabel, x, y, DefaultDataScaler(xDataName),
                DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                isOnlyEventBCDataUsed);
        } else if (std::regex_search(xDataName, std::regex{"Resolution"})) {
            auto&& x = ResolutionGenerator(InputFileLabel, xDataName);
            auto&& fin = AnalysisData.InputFiles[InputFileLabel];
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            if (yDataName == std::string{"Digit"}) {
                TTreeReaderValue<std::deque<int>> yrv{reader,
                                                      yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<std::vector<double>, std::deque<int>>(
                    InputFileLabel, x, *yrv, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            } else {
                TTreeReaderValue<std::vector<double>> yrv{reader,
                                                          yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<std::vector<double>,
                                        std::vector<double>>(
                    InputFileLabel, x, *yrv, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            }
        } else if (std::regex_search(yDataName, std::regex{"Resolution"})) {
            auto&& y = ResolutionGenerator(InputFileLabel, yDataName);
            auto&& fin = AnalysisData.InputFiles[InputFileLabel];
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            if (xDataName == std::string{"Digit"}) {
                TTreeReaderValue<std::deque<int>> xrv{reader,
                                                      xDataName.c_str()};
                reader.Next();
                return ScatterGenerator<std::deque<int>, std::vector<double>>(
                    InputFileLabel, *xrv, y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            } else {
                TTreeReaderValue<std::vector<double>> xrv{reader,
                                                          xDataName.c_str()};
                reader.Next();
                return ScatterGenerator<std::vector<double>,
                                        std::vector<double>>(
                    InputFileLabel, *xrv, y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            }
        } else {
            auto&& fin = AnalysisData.InputFiles[InputFileLabel];
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            if (xDataName == std::string{"Digit"} &&
                yDataName == std::string{"Digit"}) {
                using xDataType = std::deque<int>;
                using yDataType = std::deque<int>;
                TTreeReaderValue<xDataType> x{reader, xDataName.c_str()};
                TTreeReaderValue<yDataType> y{reader, yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<xDataType, yDataType>(
                    InputFileLabel, *x, *y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            } else if (xDataName == std::string{"Digit"}) {
                using xDataType = std::deque<int>;
                using yDataType = std::vector<double>;
                TTreeReaderValue<xDataType> x{reader, xDataName.c_str()};
                TTreeReaderValue<yDataType> y{reader, yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<xDataType, yDataType>(
                    InputFileLabel, *x, *y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            } else if (yDataName == std::string{"Digit"}) {
                using xDataType = std::vector<double>;
                using yDataType = std::deque<int>;
                TTreeReaderValue<xDataType> x{reader, xDataName.c_str()};
                TTreeReaderValue<yDataType> y{reader, yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<xDataType, yDataType>(
                    InputFileLabel, *x, *y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            } else {
                using xDataType = std::vector<double>;
                using yDataType = std::vector<double>;
                TTreeReaderValue<xDataType> x{reader, xDataName.c_str()};
                TTreeReaderValue<yDataType> y{reader, yDataName.c_str()};
                reader.Next();
                return ScatterGenerator<xDataType, yDataType>(
                    InputFileLabel, *x, *y, DefaultDataScaler(xDataName),
                    DefaultDataScaler(yDataName), DefaultAxisLabel(xDataName),
                    DefaultAxisLabel(yDataName), xlow, xup, ylow, yup,
                    isOnlyEventBCDataUsed);
            }
        }
    }

    // DataName should be BranchName , "EtResolution", "TauResoolution"
    // or "DigitLSB". DefaultAxisLabel , DefaultDataScaler,  Default nbins
    // (=200), DefaultAxisLow/Up are applied.
    TGraph* Generator::ScatterGenerator(const int InputFileLabel,
                                        const std::string& xDataName,
                                        const std::string& yDataName,
                                        bool isOnlyEventBCDataUsed)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        auto&& xlow = DefaultAxisLowForGraph(InputFileLabel, xDataName);
        auto&& xup = DefaultAxisUpForGraph(InputFileLabel, xDataName);
        auto&& ylow = DefaultAxisLowForGraph(InputFileLabel, yDataName);
        auto&& yup = DefaultAxisUpForGraph(InputFileLabel, yDataName);
        if (std::abs(xlow - xup) < 0.001) xlow -= 1., xup += 1;
        if (std::abs(ylow - yup) < 0.001) ylow -= 1., yup += 1;
        return ScatterGenerator(InputFileLabel, xDataName, yDataName, xlow, xup,
                                ylow, yup, isOnlyEventBCDataUsed);
    }
}  // namespace PLISMAnalysis