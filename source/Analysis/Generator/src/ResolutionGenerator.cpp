#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    std::vector<double> Generator::ResolutionGenerator(const int InputFileLabel,
                                                       const std::string& Type)
    {
        std::smatch m;
        std::vector<double> Resolution;
        if (std::regex_search(Type, m, std::regex{"EtResolution"})) {
            auto&& fin = AnalysisData.InputFiles[InputFileLabel];
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> SignalTrueEt{reader,
                                                               "SignalTrueEt"};
            TTreeReaderValue<std::vector<double>> RecEt{reader, "RecEt"};
            while (reader.Next()) {
                for (unsigned long long i = 0; i < (*RecEt).size(); i++) {
                    Resolution.push_back(((*RecEt)[i] - (*SignalTrueEt)[i]) /
                                         (*SignalTrueEt)[i]);
                }
            }
        } else if (std::regex_search(Type, m, std::regex{"TauResolution"})) {
            auto&& fin = AnalysisData.InputFiles[InputFileLabel];
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> SignalTrueTau{
                reader, "SignalTrueTau"};
            TTreeReaderValue<std::vector<double>> RecTau{reader, "RecTau"};
            while (reader.Next()) {
                for (unsigned long long i = 0; i < (*RecTau).size(); i++) {
                    Resolution.push_back((*RecTau)[i] - (*SignalTrueTau)[i]);
                }
            }
        } else {
            std::cerr << ErrorMessage << "::ResolutionGenerator: "
                      << "Unknown Resolution type. Resolution type should be "
                         "EtResolution or TauResolution.\n";
            exit(EXIT_FAILURE);
        }
        return Resolution;
    }
}  // namespace PLISMAnalysis