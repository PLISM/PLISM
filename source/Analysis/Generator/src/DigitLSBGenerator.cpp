#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    std::vector<double> Generator::DigitLSBGenerator(const int InputFileLabel)
    {
        auto&& fin = AnalysisData.InputFiles[InputFileLabel];
        TTreeReader reader{AnalysisData.InputFileTypes[InputFileLabel].c_str(),
                           fin};
        TTreeReaderValue<std::deque<int>> Digit{reader, "Digit"};
        TTreeReaderValue<float> LSB{reader, "LSB"};
        std::vector<double> DigitLSB;
        while (reader.Next()) {
            for (const auto& d : *Digit) { DigitLSB.push_back(*LSB * d); }
        }
        return DigitLSB;
    }
}  // namespace PLISMAnalysis