#pragma once

#include <TGraph.h>
#include <TH2D.h>

#include "../../DataBase/inc/DataBase.hpp"

namespace PLISMAnalysis
{
    class Generator
    {
    private:
        // AnalysisData
        DataBase AnalysisData;

        // Error / Waring Message
        static inline std::ostream& ErrorMessage(std::ostream& os)
        {
            os << "Error in PLISMAnalysis::Generator";
            return os;
        }
        static inline std::ostream& WarningrMessage(std::ostream& os)
        {
            os << "Warning in PLISMAnalysis::Generator";
            return os;
        }

        // Default Axis configuration
        double DefaultAxisLowForHistogram(int InputFileLabel,
                                          const std::string& DataName,
                                          bool MeVtoGeV = true);
        double DefaultAxisUpForHistogram(int InputFileLabel,
                                         const std::string& DataName,
                                         bool MeVtoGeV = true);
        double DefaultAxisLowForGraph(int InputFileLabel,
                                      const std::string& DataName,
                                      bool MeVtoGeV = true);
        double DefaultAxisUpForGraph(int InputFileLabel,
                                     const std::string& DataName,
                                     bool MeVtoGeV = true);
        std::string DefaultAxisLabel(const std::string& DataName,
                                     bool MeVtoGeV = true);
        double DefaultDataScaler(const std::string& DataName);

        // sign function
        template <typename T>
        int sgn(T val)
        {
            return (T(0) < val) - (val < T(0));
        }

        // Generator of datum other than branches of DigitSequence and
        // ReconstructedSequences
        std::vector<double> ResolutionGenerator(const int InputFileLabel,
                                                const std::string& Type);
        std::vector<double> DigitLSBGenerator(const int InputFileLabel);
        std::vector<double> SelectedEtGenerator(const int InputFileLabel);

        // TauCriteria
        static inline bool TauCriteria(const double Tau_ns, const double Et_MeV)
        {
            if (Et_MeV >= 10000. && (Tau_ns > -8. && Tau_ns < 16.)) return true;
            if (Et_MeV < 10000. && (Tau_ns > -8. && Tau_ns < 8.)) return true;
            return false;
        }

        // TH2DGenerator Base
        template <typename xDataType, typename yDataType>
        TH2D* TH2DGenerator(const int InputFileLabel, const xDataType& xData,
                            const yDataType& yData, const double xScale,
                            const double yScale, const std::string& xLabel,
                            const std::string& yLabel, const int nbinsx,
                            const double xlow, const double xup,
                            const int nbinsy, const double ylow,
                            const double yup, bool isOnlyEventBCDataUsed);

        // ScatterGenerator Base
        template <typename xDataType, typename yDataType>
        TGraph* ScatterGenerator(const int InputFileLabel,
                                 const xDataType& xData, const yDataType& yData,
                                 const double xScale, const double yScale,
                                 const std::string& xLabel,
                                 const std::string& yLabel, const double xlow,
                                 const double xup, const double ylow,
                                 const double yup, bool isOnlyEventBCDataUsed);

        // Sequence Generator Base
        template <typename DataType>
        TGraph* SequenceGenerator(const int InputFileLabel,
                                  const DataType& Data, const double scale,
                                  const std::string& title);

        // TrainPeriodicAverageGenerator Base
        template <typename DataType>
        TGraph* TrainPeriodicAverageGenerator(const int InputFileLabel,
                                              const DataType& Data,
                                              const double Scale,
                                              const std::string& title);

        // TrainPeriodicRMSGenerator Base
        template <typename DataType>
        TGraph* TrainPeriodicRMSGenerator(const int InputFileLabel,
                                          const DataType& Data,
                                          const double Scale,
                                          const std::string& title);

    public:
        // constructor
        Generator(const std::vector<std::string>& InputFilenames)
            : AnalysisData(DataBase{InputFilenames}){};
        explicit Generator(const DataBase& database) : AnalysisData(database){};

        // DataName should be "RecEt", "RecTau", "RecEtTau",
        // "SignalTrueEt","SignalTrueTau", "SignalTrueEtTau”, "EtResolution"
        // or  "TauResolution". DefaultAxisLabel and DefaultDataScaler are
        // applied.
        TH2D* TH2DGenerator(const int InputFileLabel,
                            const std::string& xDataName,
                            const std::string& yDataName, const int nbinsx,
                            const double xlow, const double xup,
                            const int nbinsy, const double ylow,
                            const double yup, bool isOnlyEventBCDataUsed);

        inline TH2D* TH2DGenerator(const std::string& InputFileName,
                                   const std::string& xDataName,
                                   const std::string& yDataName,
                                   const int nbinsx, const double xlow,
                                   const double xup, const int nbinsy,
                                   const double ylow, const double yup,
                                   bool isOnlyEventBCDataUsed = true)
        {
            AnalysisData.InputFilenameHandler(InputFileName);
            return TH2DGenerator(
                AnalysisData.InputFilenameToLabel[InputFileName], xDataName,
                yDataName, nbinsx, xlow, xup, nbinsy, ylow, yup,
                isOnlyEventBCDataUsed);
        }

        // DataName should be "RecEt", "RecTau", "RecEtTau",
        // "SignalTrueEt","SignalTrueTau", "SignalTrueEtTau”, "EtResolution"
        // or  "TauResolution". DefaultAxisLabel , DefaultDataScaler,
        // Default nbins
        // (=200), DefaultAxisLow/Up are applied.
        TH2D* TH2DGenerator(const int InputFileLabel,
                            const std::string& xDataName,
                            const std::string& yDataName,
                            bool isOnlyEventBCDataUsed = true);

        inline TH2D* TH2DGenerator(const std::string& InputFilename,
                                   const std::string& xDataName,
                                   const std::string& yDataName,
                                   bool isOnlyEventBCDataUsed = true)
        {
            AnalysisData.InputFilenameHandler(InputFilename);
            return TH2DGenerator(
                AnalysisData.InputFilenameToLabel[InputFilename], xDataName,
                yDataName, isOnlyEventBCDataUsed);
        };

        // DataName should be "RecEt", "RecTau", "RecEtTau",
        // "SignalTrueEt","SignalTrueTau", "SignalTrueEtTau”, "EtResolution"
        // or  "TauResolution". DefaultAxisLabel and DefaultDataScaler are
        // applied.
        TGraph* ScatterGenerator(const int InputFileLabel,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 const double xlow, const double xup,
                                 const double ylow, const double yup,
                                 bool isOnlyEventBCDataUsed = false);
        inline TGraph* ScatterGenerator(const std::string& InputFileName,
                                        const std::string& xDataName,
                                        const std::string& yDataName,
                                        const double xlow, const double xup,
                                        const double ylow, const double yup,
                                        bool isOnlyEventBCDataUsed = false)
        {
            AnalysisData.InputFilenameHandler(InputFileName);
            return ScatterGenerator(
                AnalysisData.InputFilenameToLabel[InputFileName], xDataName,
                yDataName, xlow, xup, ylow, yup, isOnlyEventBCDataUsed);
        }

        inline TGraph* ScatterGenerator(const std::string& xDataName,
                                        const std::string& yDataName,
                                        const double xlow, const double xup,
                                        const double ylow, const double yup,
                                        bool isOnlyEventBCDataUsed = false)
        {
            return ScatterGenerator(0, xDataName, yDataName, xlow, xup, ylow,
                                    yup, isOnlyEventBCDataUsed);
        }

        // DataName should be "RecEt", "RecTau", "RecEtTau",
        // "SignalTrueEt","SignalTrueTau", "SignalTrueEtTau”, "EtResolution"
        // or  "TauResolution".DefaultAxisLabel , DefaultDataScaler,  Default
        // nbins(=200), DefaultAxisLow/Up are applied.
        TGraph* ScatterGenerator(const int InputFileLabel,
                                 const std::string& xDataName,
                                 const std::string& yDataName,
                                 bool isOnlyEventBCDataUsed = false);
        inline TGraph* ScatterGenerator(const std::string& InputFilename,
                                        const std::string& xDataName,
                                        const std::string& yDataName,
                                        bool isOnlyEventBCDataUsed = false)
        {
            AnalysisData.InputFilenameHandler(InputFilename);
            return ScatterGenerator(
                AnalysisData.InputFilenameToLabel[InputFilename], xDataName,
                yDataName, isOnlyEventBCDataUsed);
        }
        inline TGraph* ScatterGenerator(const std::string& xDataName,
                                        const std::string& yDataName,
                                        bool isOnlyEventBCDataUsed = false)
        {
            return ScatterGenerator(0, xDataName, yDataName,
                                    isOnlyEventBCDataUsed);
        }

        // DataName should be "DigitLSB", "RecEt", "TrueEt", "SelecteEt",
        // "BgAnalogEt" and "AnalogEt".
        TGraph* SequenceGenerator(const int InputFileLabel,
                                  const std::string& DataName,
                                  bool isGeV = true);
        inline TGraph* SequenceGenerator(const std::string& InputFilename,
                                         const std::string& DataName,
                                         bool isGeV = true)
        {
            AnalysisData.InputFilenameHandler(InputFilename);
            return SequenceGenerator(
                AnalysisData.InputFilenameToLabel[InputFilename], DataName,
                isGeV);
        }
        inline TGraph* SequenceGenerator(const std::string& DataName,
                                         bool isGeV = true)
        {
            return SequenceGenerator(0, DataName, isGeV);
        }

        // SelectedEt(=passed tau criteria) Generator
        TGraph* SelectedEtGenerator(const int InputFileLabel,
                                    bool isGeV = true);
        inline TGraph* SelectedEtGenerator(const std::string& InputFilename,
                                           bool isGeV = true)
        {
            AnalysisData.InputFilenameHandler(InputFilename);
            return SelectedEtGenerator(
                AnalysisData.InputFilenameToLabel[InputFilename], isGeV);
        }
        inline TGraph* SelectedEtGenerator(bool isGeV = true)
        {
            return SelectedEtGenerator(0, isGeV);
        }

        // DataName should be "BgAnalogEt", "RecEt", "Digit" or ”DigitLSB”.
        TGraph* TrainPeriodicAverageGenerator(const int InputFileLabel,
                                              const std::string& DataName,
                                              bool isMeVtoGeV = false);
        inline TGraph* TrainPeriodicAverageGenerator(
            const std::string& DataName, bool isMeVtoGeV = false)
        {
            return TrainPeriodicAverageGenerator(0, DataName, isMeVtoGeV);
        }

        // DataName should be "BgAnalogEt", "RecEt", "Digit" or ”DigitLSB”.
        TGraph* TrainPeriodicRMSGenerator(const int InputFileLabel,
                                          const std::string& DataName,
                                          bool isMeVtoGeV = false);
        inline TGraph* TrainPeriodicRMSGenerator(const std::string& DataName,
                                                 bool isMeVtoGeV = false)
        {
            return TrainPeriodicRMSGenerator(0, DataName, isMeVtoGeV);
        }
    };

}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    // ScatterGenerator Base
    template <typename xDataType, typename yDataType>
    TGraph* Generator::ScatterGenerator(
        const int InputFileLabel, const xDataType& xData,
        const yDataType& yData, const double xScale, const double yScale,
        const std::string& xLabel, const std::string& yLabel, const double xlow,
        const double xup, const double ylow, const double yup,
        bool isOnlyEventBCDataUsed)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);

        auto g = new TGraph{};
        std::string title{";" + xLabel + ";" + yLabel};
        g->SetTitle(title.c_str());
        /*
        if (xData.size() != yData.size()) {
            std::cout << "Size of xData : " << xData.size() << " != "
                      << "Size of yData : " << yData.size() << std::endl;
        }
        */
        if (isOnlyEventBCDataUsed) {
            for (unsigned long long BC = 0;
                 BC < std::min(xData.size(), yData.size());) {
                bool isEvent = true;
                for (const auto& v :
                     AnalysisData.SignalPatterns[InputFileLabel]) {
                    if (isEvent) {
                        for (int BC_ = 0;
                             BC_ < v &&
                             BC < std::min(xData.size(), yData.size());
                             BC++, BC_++) {
                            // std::cout << BC << std::endl;
                            g->SetPoint(g->GetN(), xData[BC] * xScale,
                                        yData[BC] * yScale);
                        }
                    } else {
                        BC += v;
                    }
                    isEvent = !isEvent;
                }
            }
        } else {
            for (unsigned long long BC = 0;
                 BC < std::min(xData.size(), yData.size()); BC++) {
                /*
                if (xData[BC] < xlow || xData[BC] > xup || yData[BC] < ylow
                || yData[BC] > yup) { continue;
                }*/
                g->SetPoint(g->GetN(), xData[BC] * xScale, yData[BC] * yScale);
            }
        }
        g->SetMaximum(yup);
        g->SetMinimum(ylow);
        g->GetXaxis()->SetLimits(xlow, xup);
        return g;
    }
}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    template <typename DataType>
    TGraph* Generator::SequenceGenerator(const int InputFileLabel,
                                         const DataType& Data,
                                         const double Scale,
                                         const std::string& title)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        auto g = new TGraph{};
        g->SetTitle(title.c_str());
        double ylow{__DBL_MAX__}, yup{__DBL_MIN__};

        for (long long BC = 0; BC < AnalysisData.TotalBCs[InputFileLabel];
             BC++) {
            g->SetPoint(g->GetN(), BC * AnalysisData.BCInterval_ns,
                        Data[BC] * Scale);
            ylow = std::min(ylow, Data[BC] * Scale);
            yup = std::max(yup, Data[BC] * Scale);
        }
        yup += std::abs(yup) * 0.1;
        ylow -= std::abs(yup) * 0.1;
        g->SetMaximum(yup);
        g->SetMinimum(ylow);
        return g;
    }
}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    // TH2DGenerator Base
    template <typename xDataType, typename yDataType>
    TH2D* Generator::TH2DGenerator(
        const int InputFileLabel, const xDataType& xData,
        const yDataType& yData, const double xScale, const double yScale,
        const std::string& xLabel, const std::string& yLabel, const int nbinsx,
        const double xlow, const double xup, const int nbinsy,
        const double ylow, const double yup, bool isOnlyEventBCDataUsed)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        // hist
        auto h = new TH2D{"hist", "hist", nbinsx, xlow, xup, nbinsy, ylow, yup};
        h->SetXTitle(xLabel.c_str());
        h->SetYTitle(yLabel.c_str());
        h->SetZTitle("Entries");
        h->SetStats(0);
        h->SetTitle(0);
        h->GetYaxis()->SetTitleOffset(1.4);
        h->GetZaxis()->SetTitleOffset(1.4);
        /*
        if (xData.size() != yData.size()) {
            std::cout << "Size of xData : " << xData.size() << " != "
                      << "Size of yData : " << yData.size() << std::endl;
        }
        */
        if (isOnlyEventBCDataUsed) {
            for (unsigned long long BC = 0;
                 BC < std::min(xData.size(), yData.size());) {
                bool isEvent = true;
                for (const auto& v :
                     AnalysisData.SignalPatterns[InputFileLabel]) {
                    if (isEvent) {
                        for (int BC_ = 0;
                             BC_ < v &&
                             BC < std::min(xData.size(), yData.size());
                             BC++, BC_++) {
                            h->Fill(xData[BC] * xScale, yData[BC] * yScale);
                        }
                    } else {
                        BC += v;
                    }
                    isEvent = !isEvent;
                }
            }
        } else {
            for (unsigned long long BC = 0;
                 BC < std::min(xData.size(), yData.size()); BC++) {
                h->Fill(xData[BC] * xScale, yData[BC] * yScale);
            }
        }
        return h;
    }
}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    // TrainPeriodicAverage Generator Base
    template <typename DataType>
    TGraph* Generator::TrainPeriodicAverageGenerator(const int InputFileLabel,
                                                     const DataType& Data,
                                                     const double Scale,
                                                     const std::string& title)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);

        double Period_BC{
            static_cast<double>(AnalysisData.Periods_BC[InputFileLabel])};

        // Graph の軸の up, low
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};
        double xup{static_cast<double>(Period_BC * AnalysisData.BCInterval_ns)};
        double xlow{0.0};

        auto g = new TGraph{};
        g->SetTitle(title.c_str());

        auto getMean = [&](long long BC) {
            double mean{0.0};
            int ctr{0};
            for (long long BC_ = BC; BC_ + Period_BC < Data.size();
                 BC_ += Period_BC, ctr++) {
                mean += Data[BC_];
            }
            mean /= ctr;
            return mean;
        };
        for (long long BC = 0; BC < Period_BC; BC++) {
            auto mean = getMean(BC) * Scale;
            yup = std::max(yup, mean), ylow = std::min(ylow, mean);
            g->SetPoint(BC, BC * AnalysisData.BCInterval_ns, mean);
        };

        // Axis
        yup += std::abs(yup) * 0.2;
        ylow -= std::abs(ylow) * 0.2;
        g->GetXaxis()->SetLimits(xlow, xup);
        g->SetMaximum(yup);
        g->SetMinimum(ylow);

        return g;
    };
}  // namespace PLISMAnalysis

namespace PLISMAnalysis
{
    // TrainPeriodicRMSGenerator Base
    template <typename DataType>
    TGraph* Generator::TrainPeriodicRMSGenerator(const int InputFileLabel,
                                                 const DataType& Data,
                                                 const double Scale,
                                                 const std::string& title)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);

        double Period_BC{
            static_cast<double>(AnalysisData.Periods_BC[InputFileLabel])};

        // Graph の軸の up, low
        double yup{__DBL_MIN__};
        double ylow{__DBL_MAX__};
        double xup{static_cast<double>(Period_BC * AnalysisData.BCInterval_ns)};
        double xlow{0.0};

        auto g = new TGraph{};
        g->SetTitle(title.c_str());

        auto getRMS = [&](long long BC) {
            double mean{0.0}, mean2{0.0};
            int ctr{0};
            for (long long BC_ = BC; BC_ + Period_BC < Data.size();
                 BC_ += Period_BC, ctr++) {
                mean += Data[BC_];
                mean2 += Data[BC_] * Data[BC_];
            }
            mean /= ctr, mean2 /= ctr;
            return std::sqrt(mean2 - mean * mean);
        };
        for (long long BC = 0; BC < Period_BC; BC++) {
            auto rms = getRMS(BC) * Scale;
            yup = std::max(yup, rms), ylow = std::min(ylow, rms);
            g->SetPoint(BC, BC * AnalysisData.BCInterval_ns, rms);
        };

        // Axis
        yup += std::abs(yup) * 0.2;
        ylow -= std::abs(ylow) * 0.2;
        g->GetXaxis()->SetLimits(xlow, xup);
        g->SetMaximum(yup);
        g->SetMinimum(ylow);

        return g;
    }
}  // namespace PLISMAnalysis