#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "DigitSequenceTree.h"

using namespace Reconstructor;

PYBIND11_MODULE(ReconstructorModule, m)
{
    pybind11::class_<DigitSequenceTree>(m, "Reconstructor")
      .def_readwrite("OutputFileName", &DigitSequenceTree::OutputFileName)
      .def_readwrite("OFCFileName", &DigitSequenceTree::OFCFileName)
      .def_readwrite("OutputBranch_SignalTrueEt", &DigitSequenceTree::OutputBranch_SignalTrueEt)
      .def_readwrite("OutputBranch_SignalTrueTau", &DigitSequenceTree::OutputBranch_SignalTrueTau)
      .def_readwrite("OutputBranch_SignalTrueEtTau", &DigitSequenceTree::OutputBranch_SignalTrueEtTau)
      .def_readwrite("OutputBranch_PileupTrueEt", &DigitSequenceTree::OutputBranch_PileupTrueEt)
      .def_readwrite("OutputBranch_PileupTrueEtSum", &DigitSequenceTree::OutputBranch_PileupTrueEtSum)
      .def_readwrite("OutputBranch_PileupTrueTau", &DigitSequenceTree::OutputBranch_PileupTrueTau)
      .def_readwrite("OutputBranch_AnalogEt", &DigitSequenceTree::OutputBranch_AnalogEt)
      .def_readwrite("OutputBranch_SignalAnalogEt", &DigitSequenceTree::OutputBranch_SignalAnalogEt)
      .def_readwrite("OutputBranch_BgAnalogEt", &DigitSequenceTree::OutputBranch_BgAnalogEt)
      .def_readwrite("OutputBranch_PileupAnalogEt", &DigitSequenceTree::OutputBranch_PileupAnalogEt)
      .def_readwrite("OutputBranch_ElecNoiseAnalogEt", &DigitSequenceTree::OutputBranch_ElecNoiseAnalogEt)
      .def_readwrite("OutputBranch_Digit", &DigitSequenceTree::OutputBranch_Digit)
      .def_readwrite("OutputBranch_RecEt", &DigitSequenceTree::OutputBranch_RecEt)
      .def_readwrite("OutputBranch_RecTau", &DigitSequenceTree::OutputBranch_RecTau)
      .def_readwrite("OutputBranch_RecEtTau", &DigitSequenceTree::OutputBranch_RecEtTau)
      .def_readwrite("OutputBranch_RecSignalAnalogEt", &DigitSequenceTree::OutputBranch_RecSignalAnalogEt)
      .def_readwrite("OutputBranch_RecSignalAnalogTau", &DigitSequenceTree::OutputBranch_RecSignalAnalogTau)
      .def_readwrite("OutputBranch_RecSignalAnalogEtTau", &DigitSequenceTree::OutputBranch_RecSignalAnalogEtTau)
      .def(pybind11::init<const std::string&>())
      .def("reconstruct", &DigitSequenceTree::reconstruct);
}
