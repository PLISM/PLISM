#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class Tree;
#    pragma link C++ class Reconstructor::OFCTree;
#    pragma link C++ class Reconstructor::DigitSequenceTree;

#endif
