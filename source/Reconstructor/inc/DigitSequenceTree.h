/// @addtogroup Reconstructor
/// @{

#pragma once
#include "OFCTree.h"
#include "Tree.h"

namespace Reconstructor
{
    /// Class to extract information from digit sequence tree and generate higher level trees
    class DigitSequenceTree : public Tree
    {
      public:
        /**
         * @brief Construct a new DigitSequenceTree object
         *
         * @param DigitSequenceFileName Input digit sequence file name
         */
        DigitSequenceTree(const std::string &DigitSequenceFileName) :
            Tree(DigitSequenceFileName, "digitSequence")
        {
            TrainPattern      = 0;
            SignalPattern     = 0;
            SignalTrueEt      = 0;
            SignalTrueTau     = 0;
            PileupTrueEt      = 0;
            PileupTrueTau     = 0;
            PileupTrueEtSum   = 0;
            AnalogEt          = 0;
            SignalAnalogEt    = 0;
            BgAnalogEt        = 0;
            PileupAnalogEt    = 0;
            ElecNoiseAnalogEt = 0;
            Digit             = 0;
            fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
            fChain->SetBranchAddress("eta", &eta, &b_eta);
            fChain->SetBranchAddress("phi", &phi, &b_phi);
            fChain->SetBranchAddress("layer", &layer, &b_layer);
            fChain->SetBranchAddress("detector", &detector, &b_detector);
            fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
            fChain->SetBranchAddress("pedADC", &pedADC, &b_pedADC);
            fChain->SetBranchAddress("noise", &noise, &b_noise);
            fChain->SetBranchAddress("NBC", &NBC, &b_NBC);
            fChain->SetBranchAddress("phase", &phase, &b_phase);
            fChain->SetBranchAddress("mu", &mu, &b_mu);
            fChain->SetBranchAddress("TrainPattern", &TrainPattern, &b_TrainPattern);
            fChain->SetBranchAddress("SignalPattern", &SignalPattern, &b_SignalPattern);
            fChain->SetBranchAddress("SignalTrueEt", &SignalTrueEt, &b_SignalTrueEt);
            fChain->SetBranchAddress("SignalTrueTau", &SignalTrueTau, &b_SignalTrueTau);
            fChain->SetBranchAddress("PileupTrueEt", &PileupTrueEt, &b_PileupTrueEt);
            fChain->SetBranchAddress("PileupTrueEtSum", &PileupTrueEtSum, &b_PileupTrueEtSum);
            fChain->SetBranchAddress("PileupTrueTau", &PileupTrueTau, &b_PileupTrueTau);
            fChain->SetBranchAddress("AnalogEt", &AnalogEt, &b_AnalogEt);
            fChain->SetBranchAddress("SignalAnalogEt", &SignalAnalogEt, &b_SignalAnalogEt);
            fChain->SetBranchAddress("BgAnalogEt", &BgAnalogEt, &b_BgAnalogEt);
            fChain->SetBranchAddress("PileupAnalogEt", &PileupAnalogEt, &b_PileupAnalogEt);
            fChain->SetBranchAddress("ElecNoiseAnalogEt", &ElecNoiseAnalogEt, &b_ElecNoiseAnalogEt);
            fChain->SetBranchAddress("Digit", &Digit, &b_Digit);
        }

        void reconstruct(); ///< Generate reconstructed sequence file

        std::string OutputFileName{};                 ///< Name of reconstructed sequence file to be output
        std::string OFCFileName{};                    ///< Input PLISM OFC file name
        bool OutputBranch_SignalTrueEt{true};         ///< Whether to output SignalTrueEt branch
        bool OutputBranch_SignalTrueTau{true};        ///< Whether to output SignalTrueTau branch
        bool OutputBranch_SignalTrueEtTau{true};      ///< Whether to output SignalTrueEtTau branch
        bool OutputBranch_PileupTrueEt{true};         ///< Whether to output PileupTrueEt branch
        bool OutputBranch_PileupTrueTau{true};        ///< Whether to output PileupTrueTau branch
        bool OutputBranch_PileupTrueEtSum{true};      ///< Whether to output PileupTrueEtSum branch
        bool OutputBranch_AnalogEt{true};             ///< Whether to output AnalogEt branch
        bool OutputBranch_SignalAnalogEt{true};       ///< Whether to output SignalAnalogEt branch
        bool OutputBranch_BgAnalogEt{true};           ///< Whether to output BgAnalogEt branch
        bool OutputBranch_PileupAnalogEt{true};       ///< Whether to output PileupAnalogEt branch
        bool OutputBranch_ElecNoiseAnalogEt{true};    ///< Whether to output ElecNoiseAnalogEt branch
        bool OutputBranch_Digit{true};                ///< Whether to output Digit branch
        bool OutputBranch_RecEt{true};                ///< Whether to output RecEt branch
        bool OutputBranch_RecTau{true};               ///< Whether to output RecTau branch
        bool OutputBranch_RecEtTau{true};             ///< Whether to output RecEtTau branch
        bool OutputBranch_RecSignalAnalogEt{true};    ///< Whether to output RecSignalAnalogEt branch
        bool OutputBranch_RecSignalAnalogTau{true};   ///< Whether to output RecSignalAnalogTau branch
        bool OutputBranch_RecSignalAnalogEtTau{true}; ///< Whether to output RecSignalAnalogEtTau branch

      private:
        OFCTree::OFCMap m_OFCMap;

        Int_t channelId;
        Float_t eta, phi;
        Int_t layer;
        Int_t detector;
        Float_t LSB;
        Double_t pedADC;
        Float_t noise;
        Int_t NBC;
        Float_t phase;
        Double_t mu;
        std::vector<int> *TrainPattern;
        std::vector<int> *SignalPattern;
        std::vector<double> *SignalTrueEt;
        std::vector<double> *SignalTrueTau;
        std::vector<std::vector<double>> *PileupTrueEt;
        std::vector<double> *PileupTrueEtSum;
        std::vector<std::vector<double>> *PileupTrueTau;
        std::deque<double> *AnalogEt;
        std::deque<double> *SignalAnalogEt;
        std::deque<double> *BgAnalogEt;
        std::deque<double> *PileupAnalogEt;
        std::deque<double> *ElecNoiseAnalogEt;
        std::deque<int> *Digit;

        TBranch *b_channelId;
        TBranch *b_eta;
        TBranch *b_phi;
        TBranch *b_layer;
        TBranch *b_detector;
        TBranch *b_LSB;
        TBranch *b_pedADC;
        TBranch *b_noise;
        TBranch *b_NBC;
        TBranch *b_phase;
        TBranch *b_mu;
        TBranch *b_TrainPattern;
        TBranch *b_SignalPattern;
        TBranch *b_SignalTrueEt;
        TBranch *b_SignalTrueTau;
        TBranch *b_PileupTrueEt;
        TBranch *b_PileupTrueEtSum;
        TBranch *b_PileupTrueTau;
        TBranch *b_AnalogEt;
        TBranch *b_SignalAnalogEt;
        TBranch *b_BgAnalogEt;
        TBranch *b_PileupAnalogEt;
        TBranch *b_ElecNoiseAnalogEt;
        TBranch *b_Digit;
    };
} // namespace Reconstructor
  /// @}
