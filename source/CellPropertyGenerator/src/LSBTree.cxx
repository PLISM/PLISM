#include "LSBTree.h"
#include "MPMCTree.h"

//#define PLISM_ATLAS_STYLE
//#include "../../atlasrootstyle/AtlasStyle.C"
//#include "../../atlasrootstyle/AtlasUtils.C"
//#include "../../atlasrootstyle/AtlasLabels.C"

using namespace CellPropertyGenerator;

void LSBTree::LSBvsEta(const std::string& detector        = "",
                       const std::string& inputLSBRefFile = "LSBreference.txt",
                       const float upper_limit            = 0)
{
    if (fChain == 0) return;

#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif

    int detector_enum{-1};
    if (not detector.empty()) {
        if (detector == "EMB")
            detector_enum = 0;
        else if (detector == "EMEC")
            detector_enum = 1;
        else
            Message::Warning((boost::format("There is no information of the %1% detector in the LSB reference") % detector).str());
    }

    std::array<TGraph*, 4> gref;
    if (not detector.empty()) {
        for (int i = 0; i < 4; ++i) {
            gref[i] = new TGraph();
            gref[i]->SetLineStyle(kDotted);
            gref[i]->SetLineWidth(2);
            switch (i) {
                case 0:
                    gref[i]->SetLineColor(kGreen);
                    break;
                case 1:
                    gref[i]->SetLineColor(kBlue);
                    break;
                case 2:
                    gref[i]->SetLineColor(kRed);
                    break;
                case 3:
                    gref[i]->SetLineColor(kBlack);
                    break;
            }
        }
        std::ifstream ifs(inputLSBRefFile);
        int dettmp, laytmp;
        float etatmp, preampsat, LMsat;
        // std::ofstream ofs("LSBestimation_A03L.txt");
        // ofs << "layer\teta\tsaturation[GeV]\tLSB[MeV]\n";
        if (not detector.empty()) {
            while (ifs >> dettmp >> laytmp >> etatmp >> preampsat >> LMsat) {
                if (dettmp == detector_enum) {
                    gref[laytmp]->SetPoint(
                      gref[laytmp]->GetN(), etatmp, (preampsat < LMsat ? preampsat : LMsat) * 1000 / 3650. / .7);
                    // ofs << laytmp << "\t" << etatmp << "\t" << (preampsat <
                    // LMsat ? preampsat : LMsat) << "\t"
                    //    << (preampsat < LMsat ? preampsat :
                    //    LMsat)*1000/3650./.7 << "\n";
                }
            }
        }
    }

    struct menu {
        std::array<TGraph*, 4> g;
        double min{99999.}, max{-99999.};
        float etamin{99999.}, etamax{-99999.};
    };
    std::map<float, menu> g;
    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;

        phi = std::round(phi * 1e3) * 1e-3;
        if (g.find(phi) == g.end()) {
            menu st;
            for (int i = 0; i < 4; ++i) {
                TGraph* gtmp = new TGraph();
                gtmp->SetMarkerSize(1);
                switch (i) {
                    case 0:
                        gtmp->SetMarkerColor(kGreen);
                        gtmp->SetMarkerStyle(23);
                        break;
                    case 1:
                        gtmp->SetMarkerColor(kBlue);
                        gtmp->SetMarkerStyle(22);
                        break;
                    case 2:
                        gtmp->SetMarkerColor(kRed);
                        gtmp->SetMarkerStyle(21);
                        break;
                    case 3:
                        gtmp->SetMarkerColor(kBlack);
                        gtmp->SetMarkerStyle(20);
                        break;
                }
                st.g[i] = gtmp;
                g[phi]  = st;
            }
        }
        auto& st    = g[phi];
        auto& graph = (st.g)[layer];
        graph->SetPoint(graph->GetN(), fabs(eta), LSB);
        if (LSB < st.min) st.min = LSB;
        if (LSB > st.max) st.max = LSB;
        if (fabs(eta) < st.etamin) st.etamin = fabs(eta);
        if (fabs(eta) > st.etamax) st.etamax = fabs(eta);
    }

    TString pdfname{this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (auto&& ph : g) {
        const float phi = ph.first;
        // auto& st = ph.second;
        auto max     = ph.second.max;
        auto min     = ph.second.min;
        auto etamax  = ph.second.etamax;
        auto etamin  = ph.second.etamin;
        bool b_first = true;
        for (auto&& graph : ph.second.g) {
            if (!graph->GetN()) continue;
            if (b_first) {
                graph->SetMaximum(upper_limit ? upper_limit
                                              : max + (max - min) * 0.05);
                // graph->SetMinimum(min - (max-min)*0.05);
                graph->SetMinimum(0.);
                graph->GetXaxis()->SetLimits(std::floor(etamin * 10) * 0.1,
                                             std::ceil(etamax * 10) * 0.1);
                graph->SetTitle(Form(
                  "#phi=%f;#font[52]{#eta};#font[52]{E}_{T}/ADC [MeV]", phi));
                graph->Draw("ap");
                if (not detector.empty())
                    for (auto&& it : gref)
                        if (it->GetN()) it->Draw("l");
                graph->Draw("p");
                b_first = false;
            } else {
                graph->Draw("p");
            }
        }
#ifdef PLISM_ATLAS_STYLE
        const double xleft{0.2};
        ATLASLabel(xleft, 0.88, "Internal");
        myText(xleft, 0.80, 1, Form("#font[52]{#phi} = %.3f [rad]", phi));
        auto legend = new TLegend(xleft, 0.62, xleft + 0.35, 0.77);
        legend->AddEntry(ph.second.g[0], "Presampler", "p");
        legend->AddEntry(ph.second.g[1], "Front layer", "p");
        legend->AddEntry(ph.second.g[2], "Middle layer", "p");
        legend->AddEntry(ph.second.g[3], "Back layer", "p");
        legend->SetBorderSize(0);
        legend->SetFillStyle(0);
        legend->Draw();
#endif

        c->Print(pdfname, "pdf");
    }
    c->Print(pdfname + "]", "pdf");
}

std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t>>
LSBTree::getLSBMap()
{
    std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t>> map;
    if (fChain == 0) Message::Fatal("fChain == 0");

    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;

        map[channelId][iCLset] = LSB;
    }

    return map;
}

std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t>> LSBTree::getpeakADCMap()
{
    std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t>> map;
    if (fChain == 0) Message::Fatal("fChain == 0");

    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        map[channelId][iCLset] = peakADC;
    }
    return map;
}

std::unordered_map<Int_t, LSBTree::etaphilaydecDAC> LSBTree::getetaphilaydecDACMap()
{
    std::unordered_map<Int_t, LSBTree::etaphilaydecDAC> map;
    if (fChain == 0) Message::Fatal("fChain == 0");

    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        map[channelId] = {eta, phi, layer, detector, DACforSC};
    }

    return map;
}

void LSBTree::LSBphys(std::string InputMPMCFileName)
{
    MPMCTree mpmc_tree(InputMPMCFileName);
    const auto map = mpmc_tree.getMPMCMap();
    const auto fg{FileGenerator(OutputLSBphysName)};
    TFile* outputFile = new TFile(OutputLSBphysName.c_str(), "recreate");
    TTree* outputTree = new TTree("LSBINFO", "LSBINFO");

    int out_channelId, out_layer, out_detector,out_iCLset{-1}, out_DACforSC;
    float out_eta, out_phi, out_peakADC, out_mphysovermcal;
    double out_LSB, out_EtoET, out_DACtoMeV, out_DAC2uA, out_uAMeV;
    bool out_isCali{false};

    outputTree->Branch("channelId", &out_channelId);
    outputTree->Branch("iCLset", &out_iCLset);
    outputTree->Branch("eta", &out_eta);
    outputTree->Branch("phi", &out_phi);
    outputTree->Branch("layer", &out_layer);
    outputTree->Branch("detector", &out_detector);
    outputTree->Branch("EtoET", &out_EtoET);
    outputTree->Branch("DACtoMeV", &out_DACtoMeV);
    outputTree->Branch("DAC2uA", &out_DAC2uA);
    outputTree->Branch("uAMeV", &out_uAMeV);
    outputTree->Branch("DACforSC", &out_DACforSC);
    outputTree->Branch("peakADC", &out_peakADC);
    outputTree->Branch("LSB", &out_LSB);
    outputTree->Branch("mphysovermcal", &out_mphysovermcal);
    outputTree->Branch("isCali", &out_isCali);

    Long64_t nentries = fChain->GetEntriesFast();

    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        auto itr{map.find(channelId)};
        if (itr == map.end()) continue;
        const auto mpmc = map.at(channelId);
        out_channelId     = channelId;
        out_eta           = eta;
        out_phi           = phi;
        out_layer         = layer;
        out_detector      = detector;
        out_EtoET         = EtoET;
        out_DACtoMeV      = DACtoMeV;
        out_DAC2uA        = DAC2uA;
        out_uAMeV         = uAMeV;
        out_DACforSC      = DACforSC;
        out_peakADC       = peakADC * mpmc;
        //out_LSB           = LSB / mpmc;
        out_LSB = LSB; //for normalised physwave 
        out_mphysovermcal = mpmc;
        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();
}
