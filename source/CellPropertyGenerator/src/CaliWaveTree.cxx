#include "CaliWaveTree.h"

using namespace CellPropertyGenerator;

void CaliWaveTree::LSB_delayPeak()
{
    {
        DAC2MeVTree a(DAC2MeVFileName.c_str(), ID_SCID_CL_fileName);
        auto m = a.getDACtoMeV();
        for (const auto& it : m) {
            m_DACtoMeV_map[it.first].DACtoMeV = it.second.DACtoMeV;
            m_DACtoMeV_map[it.first].DAC2uA   = it.second.DAC2uA;
            m_DACtoMeV_map[it.first].uAMeV    = it.second.uAMeV;
        }
    }

    std::unordered_map<int, int> GoodDACMap;
    {
        std::unordered_map<int, std::map<double, int>> MaxAmpDACMap;
        Restart();
        while (Next()) {
            MaxAmpDACMap[*channelId][*MaxAmp] = *DAC;
        }

        for (auto&& it : MaxAmpDACMap) {
            const auto chid{it.first};
            if (it.second.size() > 1) {
                const double halfMaxAmp{it.second.rbegin()->first / 2.};
                auto upper_it{it.second.upper_bound(halfMaxAmp)};
                if (upper_it == it.second.begin()) {
                    GoodDACMap[chid] = it.second.rbegin()->second;
                    continue;
                }
                auto upper{upper_it->first};
                const auto upperDAC{upper_it->second};
                auto lower_it{--upper_it};
                const auto lower{lower_it->first};
                const auto lowerDAC{lower_it->second};
                GoodDACMap[chid] = (upper - halfMaxAmp < halfMaxAmp - lower) ? upperDAC : lowerDAC;
            } else {
                GoodDACMap[chid] = it.second.begin()->second;
            }
        }
    }

    const auto outputFileName{this->AddPrefix("lsb").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("LSBINFO", "LSBINFO");

    int out_channelId, out_layer, out_detector, out_iCLset{-1}, out_DACforSC;
    float out_eta, out_phi, out_peakADC, out_mphysovermcal{1.};
    double out_LSB, out_EtoET, out_DACtoMeV, out_DAC2uA, out_uAMeV;
    bool out_isCali{true};
    outputTree->Branch("channelId", &out_channelId);
    outputTree->Branch("iCLset", &out_iCLset);
    outputTree->Branch("eta", &out_eta);
    outputTree->Branch("phi", &out_phi);
    outputTree->Branch("layer", &out_layer);
    outputTree->Branch("detector", &out_detector);
    outputTree->Branch("EtoET", &out_EtoET);
    outputTree->Branch("DACtoMeV", &out_DACtoMeV);
    outputTree->Branch("DAC2uA", &out_DAC2uA);
    outputTree->Branch("uAMeV", &out_uAMeV);
    outputTree->Branch("DACforSC", &out_DACforSC);
    outputTree->Branch("peakADC", &out_peakADC);
    outputTree->Branch("LSB", &out_LSB);
    outputTree->Branch("mphysovermcal", &out_mphysovermcal);
    outputTree->Branch("isCali", &out_isCali);

    Restart();
    while (Next()) {
        if (*DAC not_eq GoodDACMap[*channelId]) continue;
        out_channelId = *channelId;
        out_eta       = *eta;
        out_phi       = *phi;
        out_layer     = *layer;
        out_detector  = *detector;
        out_EtoET     = TMath::Sin(2 * TMath::ATan(TMath::Exp(-fabs(*eta))));
        out_DACtoMeV  = m_DACtoMeV_map[out_channelId].DACtoMeV;
        out_DAC2uA    = m_DACtoMeV_map[out_channelId].DAC2uA;
        out_uAMeV     = m_DACtoMeV_map[out_channelId].uAMeV;
        out_DACforSC  = *DAC;
        out_peakADC   = *MaxAmp;
        out_LSB       = (out_DACforSC * out_DACtoMeV * out_EtoET / out_peakADC) / out_mphysovermcal;
        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}

void CaliWaveTree::PulseTree()
{
    {
        PedestalTree p(PedFileName);
        if (not p()) Message::Fatal("PedestalTree not configured");
        auto m = p.getPed();
        for (const auto& it : m) {
            m_ped_rms_ac_DACtoMeV_map[it.first].ped = it.second.first;
            m_ped_rms_ac_DACtoMeV_map[it.first].rms = it.second.second;
        }
    }
    {
        DAC2MeVTree a(DAC2MeVFileName.c_str(), ID_SCID_CL_fileName);
        auto m = a.getDACtoMeV();
        for (const auto& it : m) {
            m_ped_rms_ac_DACtoMeV_map[it.first].DACtoMeV = it.second.DACtoMeV;
        }
    }

    Restart();

    const auto outputFileName{this->AddPrefix("pulse").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("PULSE", "PULSE");

    Int_t channelId_out;
    std::vector<Double_t> amplitude_out;
    std::vector<Double_t> time_out;
    Int_t Nsamples_out;
    Float_t maxAmp_out;
    Int_t iMaxAmp_out;
    Float_t minAmp_out;
    std::unordered_set<int> allCL_out;
    std::unordered_set<int> pulsedCL_out;
    Int_t NpulsedCells_out{};
    Int_t DACforSC_out;
    Int_t iCLset_out{-1};
    Int_t iDAC_out{-1};
    Int_t latomeSourceID_out{};
    Int_t layer_out;
    Float_t eta_out;
    Float_t phi_out;
    Int_t detector_out;
    Double_t ped_out;
    Double_t rms_out;
    Double_t DACtoMeV_out;
    Bool_t isCali_out{true};

    outputTree->Branch("channelId", &channelId_out);
    outputTree->Branch("amplitude", &amplitude_out);
    outputTree->Branch("time", &time_out);
    outputTree->Branch("Nsamples", &Nsamples_out);
    outputTree->Branch("maxAmp", &maxAmp_out);
    outputTree->Branch("iMaxAmp", &iMaxAmp_out);
    outputTree->Branch("minAmp", &minAmp_out);
    outputTree->Branch("allCL", &allCL_out);
    outputTree->Branch("pulsedCL", &pulsedCL_out);
    outputTree->Branch("NpulsedCells", &NpulsedCells_out);
    outputTree->Branch("DACforSC", &DACforSC_out);
    outputTree->Branch("iCLset", &iCLset_out);
    outputTree->Branch("iDAC", &iDAC_out);
    outputTree->Branch("latomeSourceID", &latomeSourceID_out);
    outputTree->Branch("layer", &layer_out);
    outputTree->Branch("eta", &eta_out);
    outputTree->Branch("phi", &phi_out);
    outputTree->Branch("detector", &detector_out);
    outputTree->Branch("ped", &ped_out);
    outputTree->Branch("rms", &rms_out);
    outputTree->Branch("DACtoMeV", &DACtoMeV_out);
    outputTree->Branch("isCali", &isCali_out);

    while (Next()) {
        channelId_out = *channelId;
        amplitude_out = std::vector<Double_t>(Amplitude.begin(), Amplitude.end());
        time_out      = std::vector<Double_t>(Time.begin(), Time.end());
        Nsamples_out  = time_out.size() / 24;
        maxAmp_out    = *MaxAmp;

        iMaxAmp_out = std::distance(
          amplitude_out.begin(),
          std::max_element(amplitude_out.begin(), amplitude_out.end()));
        minAmp_out   = *std::min_element(amplitude_out.begin(), amplitude_out.end());
        DACforSC_out = *DAC;
        layer_out    = *layer;
        eta_out      = *eta;
        phi_out      = *phi;
        detector_out = *detector;
        ped_out      = m_ped_rms_ac_DACtoMeV_map[*channelId].ped;
        rms_out      = m_ped_rms_ac_DACtoMeV_map[*channelId].rms;
        DACtoMeV_out = m_ped_rms_ac_DACtoMeV_map[*channelId].DACtoMeV;

        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}
