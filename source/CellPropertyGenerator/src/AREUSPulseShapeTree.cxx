#include "AREUSPulseShapeTree.h"
#include "TInterpreter.h"

using namespace CellPropertyGenerator;

void AREUSPulseShapeTree::Loop()
{
    if (fChain == 0) return;
    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;

    for (Long64_t nthET = 0; nthET < nentries; ++nthET) {
        Long64_t ientry = LoadTree(nthET);
        if (ientry < 0) break;
        nb = fChain->GetEntry(nthET);
        nbytes += nb;
        // if (Cut(ientry) < 0) continue;

        std::deque<double> deq_tmp;
        const double max = *std::max_element(samples->begin(), samples->end());
        if (max <= 0.) Message::Fatal("The max of pulse <= 0");
        bool bStarted = false;
        for (auto&& it : *samples) {
            if (bStarted)
                deq_tmp.push_back(it / max); // normalize pulse height
            else if (it) {
                bStarted = true;
                deq_tmp.push_back(it / max);
            }
        }

        Property theProperty = {eT, sampperbc, std::move(deq_tmp)};
        // sub = 0:EMB, 1:InnerEMEC, 2:OuterEMEC
        Int_t sub       = subsystem ? ((eta < 2.5) ? 2 : 1) : 0;
        Int_t lay       = sub == 1 ? layer - 1 : layer;
        unsigned int ID = (1 | (lay << 4) | (1 << (6 + sub))); // assume layer = 0-3, subsystem = 0-1
        m_propertyMap[std::move(ID)][eta].push_back(std::move(theProperty));
    }

    for (auto&& iID : m_propertyMap) {
        for (auto&& ieta : iID.second)
            std::sort(ieta.second.begin(), ieta.second.end());
    }
}

void AREUSPulseShapeTree::generate(const std::string& noiseFileName, const std::string& outputFileName)
{
    Loop();

    // gInterpreter->GenerateDictionary("vector<vector<float> >","vector");
    gInterpreter->GenerateDictionary("std::vector<std::deque<double> >",
                                     "vector;deque");

    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("CellProperty", "CellProperty");

    Int_t channelId;
    std::vector<Float_t> eTs;
    std::vector<std::deque<double> > gFunctions; // g[nthET][nthSample]
    Float_t LSB;                                 // MeVT
    Float_t noise;

    outputTree->Branch("channelId", &channelId, "channelId/i");
    outputTree->Branch("inputET", &eTs);
    outputTree->Branch("sampperbc", &sampperbc);
    outputTree->Branch("gFunction", &gFunctions);
    outputTree->Branch("LSB", &LSB, "LSB/F");
    outputTree->Branch("noise", &noise, "noise/F");

    ChannelIdVector ChannelIdVector;
    SCMaps SCMap;
    SCMap.fill(ChannelIdVector);
    AREUSNoiseTree AREUSNoiseTree(noiseFileName);
    std::unordered_map<unsigned int, float> noiseMap;
    AREUSNoiseTree.setNoiseMap(noiseMap);

    bool b1stSC = true;
    for (auto&& isc : ChannelIdVector) {
        channelId = isc.channelId;
        if (!(isc.ID & 1)) continue; // select only EM
        auto keyID = m_propertyMap.find(isc.ID);
        if (keyID == m_propertyMap.end()) {
            Message::Warning((boost::format("Pulse shapes for channelId %1% was not found!! (ID=%2%) -> skip") % channelId % isc.ID).str());
            continue;
        }
        if (((isc.ID & 0b110000) >> 4) == 2)
            LSB = 125.;
        else
            LSB = 32.;

        auto keyEta = keyID->second.upper_bound(std::fabs(isc.eta));
        if (keyEta != keyID->second.begin()) --keyEta;
        int size_tmp = keyEta->second.size();
        std::vector<Float_t> eTs_tmp;
        std::vector<std::deque<double> > gFunctions_tmp;
        eTs_tmp.reserve(size_tmp);
        gFunctions_tmp.reserve(size_tmp);
        if (b1stSC) {
            sampperbc = (keyEta->second)[0].sampperbc;
            b1stSC    = false;
        }
        bool bSkip = false;
        for (auto&& pro : keyEta->second) {
            if (pro.sampperbc != sampperbc) {
                Message::Warning((boost::format("Not all sampperbc(s) in SC of channelId %1% are equal -> This SC is skipped") % channelId).str());
                bSkip = true;
                break;
            }
            eTs_tmp.push_back(pro.eT);
            gFunctions_tmp.push_back(pro.gFunction);
        }
        if (bSkip) continue;
        eTs           = std::move(eTs_tmp);
        gFunctions    = std::move(gFunctions_tmp);
        auto keyNoise = noiseMap.find(channelId);
        if (keyNoise == noiseMap.end()) {
            Message::Warning((boost::format("Noise for channelId %1% was not found -> noise is set as 40") % channelId).str());
            noise = 40.;
        } else {
            noise = keyNoise->second;
        }
        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();
}
