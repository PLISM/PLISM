#include "DigitTree.h"

using namespace CellPropertyGenerator;

void DigitTree::MakeTree()
{
    if (fChain == 0) Message::Fatal("fChain == 0");

    auto getSum = [](std::vector<int>& v) -> double { return static_cast<double>(std::accumulate(v.begin(), v.end(), 0.)); };

    Message::Info("ID_SCID_CL file name: " + boost::filesystem::absolute(m_ID_SCID_CL_fileName).string());
    Message::Info("Pedestal file name: " + boost::filesystem::absolute(m_pedFileName).string());
    const auto fg{FileGenerator(m_outputFileName)};
    TFile* outputFile = new TFile(m_outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("AVERAGED_DIGITS", "AVERAGED_DIGITS");
    std::vector<Double_t> digits;
    std::unordered_set<int> allCL;
    std::unordered_set<int> pulsedCL;
    int NpulsedCells, DACforSC, iCLset, iDAC, iDEL, LATOMEsourceID, Layer;
    float Eta, Phi;
    Double_t delay;
    double ped, rms, DACtoMeV, DAC2uA, uAMeV;
    std::vector<Float_t> autocorr;

    outputTree->Branch("channelId", &channelId);
    outputTree->Branch("samples", &digits);
    outputTree->Branch("Nsamples", &m_Nsamples);
    outputTree->Branch("allCL", &allCL);
    outputTree->Branch("pulsedCL", &pulsedCL);
    outputTree->Branch("NpulsedCells", &NpulsedCells);
    outputTree->Branch("DACforSC", &DACforSC);
    outputTree->Branch("delay", &delay);
    outputTree->Branch("iCLset", &iCLset);
    outputTree->Branch("iDAC", &iDAC);
    outputTree->Branch("iDEL", &iDEL);
    outputTree->Branch("latomeSourceID", &LATOMEsourceID);
    outputTree->Branch("layer", &Layer);
    outputTree->Branch("eta", &Eta);
    outputTree->Branch("phi", &Phi);
    if (not m_pedFileName.empty()) {
        outputTree->Branch("ped", &ped);
        outputTree->Branch("rms", &rms);
        outputTree->Branch("autocorr", &autocorr);
    }
    outputTree->Branch("DACtoMeV", &DACtoMeV);
    outputTree->Branch("DAC2uA", &DAC2uA);
    outputTree->Branch("uAMeV", &uAMeV);

    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;

    if (nentries % (m_pattern.NDACs * m_pattern.Ndelays * m_pattern.NCLsets * m_Ntrig)) {
        Message::Error((boost::format("nentries = %1%, NDACs = %2%, Ndelays = %3%, NCLsets = %4%") % nentries % m_pattern.NDACs % m_pattern.Ndelays % m_pattern.NCLsets).str());
        Message::Fatal("nentries % (m_pattern.NDACs * m_pattern.Ndelays * m_pattern.NCLsets * m_Ntrig) != 0");
    }
    m_NCH = nentries / (m_pattern.NDACs * m_pattern.Ndelays * m_pattern.NCLsets * m_Ntrig);
    Message::Info((boost::format("%1% / (%2% * %3% * %4% * %5%)") % nentries % m_pattern.NDACs % m_pattern.Ndelays % m_pattern.NCLsets % m_Ntrig).str());
    Message::Info((boost::format("Number of channels is %1%") % m_NCH).str());

    PatternCounter counter(m_pattern, m_NCH, m_Ntrig, m_ID_SCID_CL_fileName);
    std::vector<std::vector<std::vector<int> > > v(m_NCH);
    bool b_first = true;
    for (Long64_t jentry = 0; jentry < nentries; ++jentry) {
        ++counter;
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        if (!counter.pulsed(channelId)) continue;
        if (b_first) {
            digits.resize(m_Nsamples);
            for (auto&& it : v) {
                it.resize(m_Nsamples);
                for (auto&& it2 : it) it2.resize(m_Ntrig);
            }
            b_first = false;
        }
        for (int i = 0; i < m_Nsamples; ++i) v[counter.iCH][i][counter.iTRIG] = samples[i];

        if (counter.iTRIG == m_Ntrig - 1) {
            LATOMEsourceID = latomeSourceId;
            Eta            = eta;
            Phi            = phi;
            Layer          = layer;
            iCLset         = counter.iCLset;
            iDAC           = counter.iDAC;
            iDEL           = counter.iDEL;
            delay          = m_pattern.delays[iDEL] * .104;
            if (not m_pedFileName.empty()) {
                auto itr = m_ped_rms_ac_DACtoMeV_map.find(channelId);
                if (itr == m_ped_rms_ac_DACtoMeV_map.end()) {
                    Message::Warning((boost::format("pedestal, rms, autocorr, or DACtoMeV information was not found for channnelId %1%") % channelId).str());
                }
                ped      = itr->second.ped;
                rms      = itr->second.rms;
                autocorr = itr->second.autocorr;
                DACtoMeV = itr->second.DACtoMeV;
                DAC2uA   = itr->second.DAC2uA;
                uAMeV    = itr->second.uAMeV;
            }

            for (int i = 0; i < m_Nsamples; ++i) digits[i] = getSum(v[counter.iCH][i]) / m_Ntrig;
            allCL        = counter.getCLs(channelId);
            pulsedCL     = counter.getPulsedCLs(channelId);
            NpulsedCells = counter.getNpulsedCells(channelId);
            DACforSC     = m_pattern.DACs[iDAC] * NpulsedCells;
            outputTree->Fill();
        }
    }

    outputTree->Write();
    outputFile->Close();
}
