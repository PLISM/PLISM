#include "PhysWaveTree.h"
#include "LSBTree.h"
#include "MPMCTree.h"

using namespace CellPropertyGenerator;

void PhysWaveTree::PulseTree(std::string InputLSBFileName) // input LSBTree made by CaliWave
{
    {
        PedestalTree p(PedFileName);

        if (not p()) Message::Fatal("PedestalTree not configured");
        auto m = p.getPed();
        for (const auto& it : m) {
            m_ped_rms_ac_DACtoMeV_map[it.first].ped = it.second.first;
            m_ped_rms_ac_DACtoMeV_map[it.first].rms = it.second.second;
        }
    }
    {
        DAC2MeVTree a(DAC2MeVFileName.c_str(), ID_SCID_CL_fileName);
        auto m = a.getDACtoMeV();
        for (const auto& it : m) {
            m_ped_rms_ac_DACtoMeV_map[it.first].DACtoMeV = it.second.DACtoMeV;
        }
    }

    Restart();
    LSBTree LSB_tree(InputLSBFileName.c_str());
    // MPMCTree MPMC_Tree(InputMPMCFileName.c_str());
    const auto outputFileName{this->AddPrefix("pulse").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("PULSE", "PULSE");
    Int_t channelId_out;
    std::vector<Double_t> amplitude_out;
    std::vector<Double_t> time_out;
    Int_t Nsamples_out;
    Float_t maxAmp_out;
    Int_t iMaxAmp_out;
    Float_t minAmp_out;
    std::unordered_set<int> allCL_out;
    std::unordered_set<int> pulsedCL_out;
    Int_t NpulsedCells_out{};
    Int_t DACforSC_out;
    Int_t iCLset_out{-1};
    Int_t iDAC_out{-1};
    Int_t latomeSourceID_out{};
    Int_t layer_out;
    Float_t eta_out;
    Float_t phi_out;
    Int_t detector_out;
    Double_t ped_out;
    Double_t rms_out;
    Double_t DACtoMeV_out;
    Bool_t isCali_out{false};

    outputTree->Branch("channelId", &channelId_out);
    outputTree->Branch("amplitude", &amplitude_out);
    outputTree->Branch("time", &time_out);
    outputTree->Branch("Nsamples", &Nsamples_out);
    outputTree->Branch("maxAmp", &maxAmp_out);
    outputTree->Branch("iMaxAmp", &iMaxAmp_out);
    outputTree->Branch("minAmp", &minAmp_out);
    outputTree->Branch("allCL", &allCL_out);
    outputTree->Branch("pulsedCL", &pulsedCL_out);
    outputTree->Branch("NpulsedCells", &NpulsedCells_out);
    outputTree->Branch("DACforSC", &DACforSC_out);
    outputTree->Branch("iCLset", &iCLset_out);
    outputTree->Branch("iDAC", &iDAC_out);
    outputTree->Branch("latomeSourceID", &latomeSourceID_out);
    outputTree->Branch("layer", &layer_out);
    outputTree->Branch("eta", &eta_out);
    outputTree->Branch("phi", &phi_out);
    outputTree->Branch("detector", &detector_out);
    outputTree->Branch("ped", &ped_out);
    outputTree->Branch("rms", &rms_out);
    outputTree->Branch("DACtoMeV", &DACtoMeV_out);
    outputTree->Branch("isCali", &isCali_out);

    const auto peakADCmap = LSB_tree.getpeakADCMap();
    const auto epldDmap    = LSB_tree.getetaphilaydecDACMap();
    // const auto map = MPMC_Tree.getMPMCMap();

    while (Next()) {

        amplitude_out.clear();
        amplitude_out.shrink_to_fit();

        const auto peakADC = peakADCmap.at(*channelId).at(-1); // iCLset is initialisd -1 in CaliWaveTree::LSB_delayPeak()
        // const auto mpmc = map.at(*channelId);

        channelId_out = *channelId;
        std::vector<Double_t> amplitudephys;
        const auto ped = Amplitude[0];
        for (const auto& amp : Amplitude) amplitudephys.push_back(amp - ped);
        const auto maxamp = *std::max_element(amplitudephys.begin(), amplitudephys.end());
        // I must multiply exact MPMC to get ADC value of physwave!!
        for (const auto& amp : amplitudephys) amplitude_out.push_back((amp / maxamp) * peakADC);

        // case of PHYSWAVE didn't subtracted
        // if (PedAlreadySubtracted)for (const auto& amp : Amplitude) amplitude_out.push_back((amp/maxamp)* peakADC * mpmc);
        // else for  (const auto& amp : Amplitude) amplitude_out.push_back((amp/maxamp)* peakADC -  * mpmc);
        // }
        time_out     = std::vector<Double_t>(Time.begin(), Time.end());
        Nsamples_out = time_out.size() / 24;
        maxAmp_out   = *std::max_element(amplitude_out.begin(), amplitude_out.end());
        iMaxAmp_out  = std::distance(amplitude_out.begin(), std::max_element(amplitude_out.begin(), amplitude_out.end()));
        minAmp_out   = *std::min_element(amplitude_out.begin(), amplitude_out.end());
        DACforSC_out = epldDmap.at(channelId_out).DACforSC;
        layer_out    = epldDmap.at(channelId_out).layer;
        eta_out      = epldDmap.at(channelId_out).eta;
        phi_out      = epldDmap.at(channelId_out).phi;
        detector_out = epldDmap.at(channelId_out).detector;
        ped_out      = m_ped_rms_ac_DACtoMeV_map[*channelId].ped;
        rms_out      = m_ped_rms_ac_DACtoMeV_map[*channelId].rms;
        DACtoMeV_out = m_ped_rms_ac_DACtoMeV_map[*channelId].DACtoMeV;

        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}
