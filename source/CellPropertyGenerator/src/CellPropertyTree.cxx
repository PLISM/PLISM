#include "CellPropertyTree.h"

//#define PLISM_ATLAS_STYLE
//#include "../../atlasrootstyle/AtlasStyle.C"
//#include "../../atlasrootstyle/AtlasUtils.C"
//#include "../../atlasrootstyle/AtlasLabels.C"

using namespace CellPropertyGenerator;

void CellPropertyTree::PlotPedestalADC(const float upper_limit)
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif
    struct menu {
        std::array<TGraph*, 4> g;
        double min{99999.}, max{-99999.};
        float etamin{99999.}, etamax{-99999.};
    };
    std::map<float, menu> g;

    Restart();
    while (Next()) {
        const double phirnd{std::round(*phi * 1e3) * 1e-3};
        if (g.find(phirnd) == g.end()) {
            menu st;
            for (int i = 0; i < 4; ++i) {
                TGraph* gtmp = new TGraph();
                gtmp->SetMarkerSize(1);
                switch (i) {
                    case 0:
                        gtmp->SetMarkerColor(kGreen);
                        gtmp->SetMarkerStyle(23);
                        break;
                    case 1:
                        gtmp->SetMarkerColor(kBlue);
                        gtmp->SetMarkerStyle(22);
                        break;
                    case 2:
                        gtmp->SetMarkerColor(kRed);
                        gtmp->SetMarkerStyle(21);
                        break;
                    case 3:
                        gtmp->SetMarkerColor(kBlack);
                        gtmp->SetMarkerStyle(20);
                        break;
                }
                st.g[i]   = gtmp;
                g[phirnd] = st;
            }
        }
        auto& st    = g[phirnd];
        auto& graph = (st.g)[*layer];
        graph->SetPoint(graph->GetN(), fabs(*eta), *pedADC);
        if (*pedADC < st.min) st.min = *pedADC;
        if (*pedADC > st.max) st.max = *pedADC;
        if (fabs(*eta) < st.etamin) st.etamin = fabs(*eta);
        if (fabs(*eta) > st.etamax) st.etamax = fabs(*eta);
    }

    TString pdfname{"pedADC_" + this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (auto&& ph : g) {
        const float phi = ph.first;
        // auto& st = ph.second;
        auto max     = ph.second.max;
        auto min     = ph.second.min;
        auto etamax  = ph.second.etamax;
        auto etamin  = ph.second.etamin;
        bool b_first = true;
        for (auto&& graph : ph.second.g) {
            if (!graph->GetN()) continue;
            if (b_first) {
                graph->SetMaximum(upper_limit ? upper_limit : max + (max - min) * 0.05);
                // graph->SetMinimum(min - (max-min)*0.05);
                graph->SetMinimum(0.);
                graph->GetXaxis()->SetLimits(std::floor(etamin * 10) * 0.1, std::ceil(etamax * 10) * 0.1);
                graph->SetTitle(Form("#phi=.3%f;#font[52]{#eta};Pedestal [ADC]", phi));
                graph->Draw("ap");
                b_first = false;
            } else {
                graph->Draw("p");
            }
        }
#ifdef PLISM_ATLAS_STYLE
        ATLASLabel(0.65, 0.65, "Internal");
        myText(0.65, 0.57, 1, Form("#font[52]{#phi} = %.3f [rad]", phi));
        auto legend = new TLegend(0.65, 0.2, 0.9, 0.49);
        legend->AddEntry(ph.second.g[0], "Presampler", "p");
        legend->AddEntry(ph.second.g[1], "Front layer", "p");
        legend->AddEntry(ph.second.g[2], "Middle layer", "p");
        legend->AddEntry(ph.second.g[3], "Back layer", "p");
        legend->SetBorderSize(0);
        legend->SetFillStyle(0);
        legend->Draw();
#endif

        c->Print(pdfname, "pdf");
    }
    c->Print(pdfname + "]", "pdf");
}

void CellPropertyTree::PlotPedestalGeV(const float upper_limit)
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif
    struct menu {
        std::array<TGraph*, 4> g;
        double min{99999.}, max{-99999.};
        float etamin{99999.}, etamax{-99999.};
    };
    std::map<float, menu> g;

    Restart();
    while (Next()) {
        const double phirnd{std::round(*phi * 1e3) * 1e-3};
        if (g.find(phirnd) == g.end()) {
            menu st;
            for (int i = 0; i < 4; ++i) {
                TGraph* gtmp = new TGraph();
                gtmp->SetMarkerSize(1);
                switch (i) {
                    case 0:
                        gtmp->SetMarkerColor(kGreen);
                        gtmp->SetMarkerStyle(23);
                        break;
                    case 1:
                        gtmp->SetMarkerColor(kBlue);
                        gtmp->SetMarkerStyle(22);
                        break;
                    case 2:
                        gtmp->SetMarkerColor(kRed);
                        gtmp->SetMarkerStyle(21);
                        break;
                    case 3:
                        gtmp->SetMarkerColor(kBlack);
                        gtmp->SetMarkerStyle(20);
                        break;
                }
                st.g[i]   = gtmp;
                g[phirnd] = st;
            }
        }
        auto& st    = g[phirnd];
        auto& graph = (st.g)[*layer];
        graph->SetPoint(graph->GetN(), fabs(*eta), *pedADC * *LSB * 1e-3);
        if (*pedADC * *LSB * 1e-3 < st.min) st.min = *pedADC * *LSB * 1e-3;
        if (*pedADC * *LSB * 1e-3 > st.max) st.max = *pedADC * *LSB * 1e-3;
        if (fabs(*eta) < st.etamin) st.etamin = fabs(*eta);
        if (fabs(*eta) > st.etamax) st.etamax = fabs(*eta);
    }

    TString pdfname{"pedGeV_" + this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (auto&& ph : g) {
        const float phi = ph.first;
        // auto& st = ph.second;
        auto max     = ph.second.max;
        auto min     = ph.second.min;
        auto etamax  = ph.second.etamax;
        auto etamin  = ph.second.etamin;
        bool b_first = true;
        for (auto&& graph : ph.second.g) {
            if (!graph->GetN()) continue;
            if (b_first) {
                graph->SetMaximum(upper_limit ? upper_limit : max + (max - min) * 0.05);
                // graph->SetMinimum(min - (max-min)*0.05);
                graph->SetMinimum(0.);
                graph->GetXaxis()->SetLimits(std::floor(etamin * 10) * 0.1, std::ceil(etamax * 10) * 0.1);
                graph->SetTitle(Form("#phi=.3%f;#font[52]{#eta};Pedestal [GeV]", phi));
                graph->Draw("ap");
                b_first = false;
            } else {
                graph->Draw("p");
            }
        }
#ifdef PLISM_ATLAS_STYLE
        const double xleft{0.2};
        ATLASLabel(xleft, 0.88, "Internal");
        myText(xleft, 0.80, 1, Form("#font[52]{#phi} = %.3f [rad]", phi));
        auto legend = new TLegend(xleft, 0.55, xleft + 0.35, 0.77);
        legend->AddEntry(ph.second.g[0], "Presampler", "p");
        legend->AddEntry(ph.second.g[1], "Front layer", "p");
        legend->AddEntry(ph.second.g[2], "Middle layer", "p");
        legend->AddEntry(ph.second.g[3], "Back layer", "p");
        legend->SetBorderSize(0);
        legend->SetFillStyle(0);
        legend->Draw();
#endif

        c->Print(pdfname, "pdf");
    }
    c->Print(pdfname + "]", "pdf");
}

void CellPropertyTree::PlotRMSADC(const float upper_limit)
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif
    struct menu {
        std::array<TGraph*, 4> g;
        double min{99999.}, max{-99999.};
        float etamin{99999.}, etamax{-99999.};
    };
    std::map<float, menu> g;

    Restart();
    while (Next()) {
        const double phirnd{std::round(*phi * 1e3) * 1e-3};
        if (g.find(phirnd) == g.end()) {
            menu st;
            for (int i = 0; i < 4; ++i) {
                TGraph* gtmp = new TGraph();
                gtmp->SetMarkerSize(1);
                switch (i) {
                    case 0:
                        gtmp->SetMarkerColor(kGreen);
                        gtmp->SetMarkerStyle(23);
                        break;
                    case 1:
                        gtmp->SetMarkerColor(kBlue);
                        gtmp->SetMarkerStyle(22);
                        break;
                    case 2:
                        gtmp->SetMarkerColor(kRed);
                        gtmp->SetMarkerStyle(21);
                        break;
                    case 3:
                        gtmp->SetMarkerColor(kBlack);
                        gtmp->SetMarkerStyle(20);
                        break;
                }
                st.g[i]   = gtmp;
                g[phirnd] = st;
            }
        }
        auto& st    = g[phirnd];
        auto& graph = (st.g)[*layer];
        graph->SetPoint(graph->GetN(), fabs(*eta), *noise / *LSB);
        if (*noise / *LSB < st.min) st.min = *noise / *LSB;
        if (*noise / *LSB > st.max) st.max = *noise / *LSB;
        if (fabs(*eta) < st.etamin) st.etamin = fabs(*eta);
        if (fabs(*eta) > st.etamax) st.etamax = fabs(*eta);
    }

    TString pdfname{"RMSADC_" + this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (auto&& ph : g) {
        const float phi = ph.first;
        // auto& st = ph.second;
        auto max     = ph.second.max;
        auto min     = ph.second.min;
        auto etamax  = ph.second.etamax;
        auto etamin  = ph.second.etamin;
        bool b_first = true;
        for (auto&& graph : ph.second.g) {
            if (!graph->GetN()) continue;
            if (b_first) {
                graph->SetMaximum(upper_limit ? upper_limit : max + (max - min) * 0.05);
                // graph->SetMinimum(min - (max-min)*0.05);
                graph->SetMinimum(0.);
                graph->GetXaxis()->SetLimits(std::floor(etamin * 10) * 0.1, std::ceil(etamax * 10) * 0.1);
                graph->SetTitle(Form("#phi=.3%f;#font[52]{#eta};Pedestal RMS [ADC]", phi));
                graph->Draw("ap");
                b_first = false;
            } else {
                graph->Draw("p");
            }
        }
#ifdef PLISM_ATLAS_STYLE
        ATLASLabel(0.65, 0.85, "Internal");
        myText(0.65, 0.78, 1, Form("#font[52]{#phi} = %.3f [rad]", phi));
        auto legend = new TLegend(0.65, 0.2, 0.9, 0.35);
        legend->AddEntry(ph.second.g[0], "Presampler", "p");
        legend->AddEntry(ph.second.g[1], "Front layer", "p");
        legend->AddEntry(ph.second.g[2], "Middle layer", "p");
        legend->AddEntry(ph.second.g[3], "Back layer", "p");
        legend->SetBorderSize(0);
        legend->SetFillStyle(0);
        legend->Draw();
#endif

        c->Print(pdfname, "pdf");
    }
    c->Print(pdfname + "]", "pdf");
}

void CellPropertyTree::PlotRMSMeV(const float upper_limit)
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif
    struct menu {
        std::array<TGraph*, 4> g;
        double min{99999.}, max{-99999.};
        float etamin{99999.}, etamax{-99999.};
    };
    std::map<float, menu> g;

    Restart();
    while (Next()) {
        const double phirnd{std::round(*phi * 1e3) * 1e-3};
        if (g.find(phirnd) == g.end()) {
            menu st;
            for (int i = 0; i < 4; ++i) {
                TGraph* gtmp = new TGraph();
                gtmp->SetMarkerSize(1);
                switch (i) {
                    case 0:
                        gtmp->SetMarkerColor(kGreen);
                        gtmp->SetMarkerStyle(23);
                        break;
                    case 1:
                        gtmp->SetMarkerColor(kBlue);
                        gtmp->SetMarkerStyle(22);
                        break;
                    case 2:
                        gtmp->SetMarkerColor(kRed);
                        gtmp->SetMarkerStyle(21);
                        break;
                    case 3:
                        gtmp->SetMarkerColor(kBlack);
                        gtmp->SetMarkerStyle(20);
                        break;
                }
                st.g[i]   = gtmp;
                g[phirnd] = st;
            }
        }
        auto& st    = g[phirnd];
        auto& graph = (st.g)[*layer];
        graph->SetPoint(graph->GetN(), fabs(*eta), *noise);
        if (*noise < st.min) st.min = *noise;
        if (*noise > st.max) st.max = *noise;
        if (fabs(*eta) < st.etamin) st.etamin = fabs(*eta);
        if (fabs(*eta) > st.etamax) st.etamax = fabs(*eta);
    }

    TString pdfname{"RMSMeV_" + this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (auto&& ph : g) {
        const float phi = ph.first;
        // auto& st = ph.second;
        auto max     = ph.second.max;
        auto min     = ph.second.min;
        auto etamax  = ph.second.etamax;
        auto etamin  = ph.second.etamin;
        bool b_first = true;
        for (auto&& graph : ph.second.g) {
            if (!graph->GetN()) continue;
            if (b_first) {
                graph->SetMaximum(upper_limit ? upper_limit : max + (max - min) * 0.05);
                // graph->SetMinimum(min - (max-min)*0.05);
                graph->SetMinimum(0.);
                graph->GetXaxis()->SetLimits(std::floor(etamin * 10) * 0.1, std::ceil(etamax * 10) * 0.1);
                graph->SetTitle(Form("#phi=.3%f;#font[52]{#eta};Pedestal RMS [MeV]", phi));
                graph->Draw("ap");
                b_first = false;
            } else {
                graph->Draw("p");
            }
        }
#ifdef PLISM_ATLAS_STYLE
        const double xleft{0.2};
        ATLASLabel(xleft, 0.88, "Internal");
        myText(xleft, 0.80, 1, Form("#font[52]{#phi} = %.3f [rad]", phi));
        auto legend = new TLegend(xleft, 0.62, xleft + 0.35, 0.77);
        legend->AddEntry(ph.second.g[0], "Presampler", "p");
        legend->AddEntry(ph.second.g[1], "Front layer", "p");
        legend->AddEntry(ph.second.g[2], "Middle layer", "p");
        legend->AddEntry(ph.second.g[3], "Back layer", "p");
        legend->SetBorderSize(0);
        legend->SetFillStyle(0);
        legend->Draw();
#endif

        c->Print(pdfname, "pdf");
    }
    c->Print(pdfname + "]", "pdf");
}
