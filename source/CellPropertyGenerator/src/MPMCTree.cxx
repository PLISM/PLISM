#include "MPMCTree.h"

using namespace CellPropertyGenerator;

std::unordered_map<Int_t, Float_t> MPMCTree::getMPMCMap()
{
    std::unordered_map<Int_t, Float_t> map;
    if (fChain == 0) Message::Fatal("fChain == 0");
    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        map[channelId] = mphysovermcal;
    }
    return map;
}
