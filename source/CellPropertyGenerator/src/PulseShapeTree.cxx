#include "PulseShapeTree.h"
#include "LSBTree.h"

//#define PLISM_ATLAS_STYLE
//#include "../../atlasrootstyle/AtlasStyle.C"
//#include "../../atlasrootstyle/AtlasUtils.C"
//#include "../../atlasrootstyle/AtlasLabels.C"

using namespace CellPropertyGenerator;

void PulseShapeTree::GenerateCellProperty()
{
    LSBTree LSBTree(LSBFileName);
    const auto LSBMap{LSBTree.getLSBMap()};

    struct str {
        Int_t layer, detector;
        Float_t eta, phi;
        std::map<float, std::deque<double> > inputET_gFunctions;
        Int_t sampperbc;
        Float_t LSB;
        Double_t pedADC;
        Float_t noise;
    };
    std::map<Int_t, std::map<Int_t, str> > strmap;

    Restart();
    while (Next()) {
        if (strmap.find(*channelId) == strmap.end() or strmap[*channelId].find(*iCLset) == strmap[*channelId].end()) {
            strmap[*channelId][*iCLset].sampperbc = 24; // assuming sampperbc doesn't change among DACs

            { // LSB
                auto itch{LSBMap.find(*channelId)};
                if (itch == LSBMap.end()) {
                    Message::Error("ERROR SC = " + *channelId);
                    Message::Fatal("You input LSBTree from run with different region");
                }
                const auto& mappercl{itch->second};
                auto itcl{mappercl.find(*iCLset)};
                if (itcl == mappercl.end()) Message::Fatal("You input LSBTree from run with different calibration line pattern");
                strmap[*channelId][*iCLset].LSB   = itcl->second;
                strmap[*channelId][*iCLset].noise = *rms;
            }

            strmap[*channelId][*iCLset].pedADC   = *ped;
            strmap[*channelId][*iCLset].layer    = *layer;
            strmap[*channelId][*iCLset].eta      = *eta;
            strmap[*channelId][*iCLset].phi      = *phi;
            strmap[*channelId][*iCLset].detector = *detector;
        }

        { // inputET_gFunctions
            std::deque<double> gwave;
            const int size{static_cast<int>(amplitude->size())};
            if (PedAlreadySubtracted)
                for (int i{24 * BCShift}; i < size; ++i) gwave.push_back(amplitude->at(i));
            else
                for (int i{24 * BCShift}; i < size; ++i) gwave.push_back(amplitude->at(i) - *ped);
            (strmap[*channelId][*iCLset].inputET_gFunctions)[*DACforSC * *DACtoMeV * TMath::Sin(2 * TMath::ATan(TMath::Exp(-fabs(*eta))))] = gwave;
        }
    }

    const auto outputFileName{this->HasPrefix("pulse") ? this->ReplacePrefix("cellprop").string() : this->AddPrefix("cellprop").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("CellProperty", "CellProperty");
    std::vector<float> inputET;
    Int_t sampperbc;
    std::vector<std::deque<double> > gFunctions;
    Float_t LSB;
    Float_t noise;
    Int_t channelId_out, iCLset_out, layer_out, detector_out;
    Float_t eta_out, phi_out;
    Double_t ped_out;
    gInterpreter->GenerateDictionary("std::vector<std::deque<double>>", "vector;deque");
    outputTree->Branch("channelId", &channelId_out);
    outputTree->Branch("eta", &eta_out);
    outputTree->Branch("phi", &phi_out);
    outputTree->Branch("layer", &layer_out);
    outputTree->Branch("detector", &detector_out);
    outputTree->Branch("iCLset", &iCLset_out);
    outputTree->Branch("inputET", &inputET);
    outputTree->Branch("sampperbc", &sampperbc);
    outputTree->Branch("gFunction", &gFunctions);
    outputTree->Branch("LSB", &LSB);
    outputTree->Branch("pedADC", &ped_out);
    outputTree->Branch("noise", &noise);
    outputTree->Branch("BCshift", &BCShift);

    for (const auto& itch : strmap) {
        for (const auto& itcl : itch.second) {
            channelId_out = itch.first;
            iCLset_out    = itcl.first;
            const auto& str{itcl.second};
            layer_out = str.layer;
            eta_out   = str.eta;
            phi_out   = str.phi;
            detector_out = str.detector;
            {
                std::vector<float> ve;
                std::vector<std::deque<double> > vg;
                for (const auto& it : str.inputET_gFunctions) {
                    ve.push_back(it.first);
                    vg.push_back(it.second);
                }
                inputET    = std::move(ve);
                gFunctions = std::move(vg);
            }
            sampperbc = str.sampperbc;
            LSB       = str.LSB;
            ped_out   = str.pedADC;
            noise     = str.noise;
            outputTree->Fill();
        }
    }

    outputTree->Write();
    outputFile->Close();
}

void PulseShapeTree::DrawPulseShape()
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif

    Restart();

    struct GraphMenu {
        std::map<double, std::map<float, float> > g; // (inputET, (time, digit))
        float max{-99999.}, min{99999.};
        int layer;
        float eta, phi;
    };
    std::unordered_map<int, std::unordered_map<int, GraphMenu> > map; // (channelId, (iCL, GraphMenu))

    while (Next()) {
        if (map.find(*channelId) == map.end() || map[*channelId].find(*iCLset) == map[*channelId].end()) {
            GraphMenu a;
            map[*channelId][*iCLset] = a;
        }

        auto& st = map[*channelId][*iCLset];
        const double inputET{*DACforSC * *DACtoMeV * TMath::Sin(2 * TMath::ATan(TMath::Exp(-fabs(*eta))))};
        const auto length{time->size()};
        for (std::size_t i{}; i < length; ++i) {
            const auto amp{amplitude->at(i)};
            st.g[inputET][time->at(i)] = amp;
            if (amp > st.max) st.max = amp;
            if (amp < st.min) st.min = amp;
        }
        st.layer = *layer;
        st.eta   = *eta;
        st.phi   = *phi;
    }

    std::vector<std::string> col_str{{"#EB610F", "#09AE7B", "#3069B3", "#F5A102", "#67C8F2", "#893B2C", "#EF908A", "#A53D92"}};
    std::vector<int> col_int;
    for (const auto& it : col_str) col_int.push_back(TColor::GetColor(it.c_str()));

    TString pdfname{this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (const auto& ch : map) {
        const int channelId = ch.first;
        for (const auto& cl : ch.second) {
            const int iCLset = cl.first;
            const auto st    = std::move(cl.second);
            bool b_first     = true;
            int count{};
            for (const auto& dac : st.g) {
                TGraph* graph = new TGraph();
                graph->SetMarkerStyle(8);
                graph->SetMarkerSize(0.2);
                int i = -1;
                for (const auto& t : dac.second) {
                    graph->SetPoint(++i, t.first, t.second);
                }

                graph->SetMarkerColor(col_int[count % col_int.size()]);
                if (b_first) {
                    graph->SetMaximum(st.max + (st.max - st.min) * 0.05);
                    graph->SetMinimum(st.min - (st.max - st.min) * 0.05);
                    graph->GetXaxis()->SetLimits(0, 800);
                    graph->SetTitle(Form("channelId=%d, iCLset=%d;Time [ns];Amplitude [ADC]", channelId, iCLset));
                    graph->Draw("ap");
                    b_first = false;
                } else {
                    graph->Draw("p");
                }
#ifdef PLISM_ATLAS_STYLE
                const auto inputET{dac.first};
                myText(count % 2 ? 0.7 : 0.5, 0.61 - count / 2 * 0.05, col_int[count % col_int.size()], Form("%.lf GeV", round(inputET * 1e-3)));
#endif
                ++count;
            }
#ifdef PLISM_ATLAS_STYLE
            ATLASLabel(0.5, 0.85, "Internal");
            switch (st.layer) {
                case (0):
                    myText(0.5, 0.77, 1, "Presampler");
                    break;
                case (1):
                    myText(0.5, 0.77, 1, "Front layer");
                    break;
                case (2):
                    myText(0.5, 0.77, 1, "Middle layer");
                    break;
                case (3):
                    myText(0.5, 0.77, 1, "Back layer");
                    break;
            }
            myText(0.5, 0.69, 1, Form("#font[52]{#eta} = %5.4f, #font[52]{#phi} = %5.4f", st.eta, st.phi));
#endif
            c->Print(pdfname, "pdf");
        }
    }
    c->Print(pdfname + "]", "pdf");
}
