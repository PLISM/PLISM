#include "AutoCorrTree.h"

using namespace CellPropertyGenerator;

std::unordered_map<Int_t, std::vector<Float_t>> AutoCorrTree::getAutoCorr()
{
    std::unordered_map<Int_t, std::vector<Float_t>> m;

    Restart();
    while (Next()) {
        m[*channelId] = std::vector<Float_t>(std::begin(covr), std::end(covr));
    }
    return m;
}
