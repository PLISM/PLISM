
#include "DAC2MeVTree.h"
#include <TH1D.h>

using namespace CellPropertyGenerator;

std::unordered_map<int, DAC2MeVTree::DAC2uA2MeV> DAC2MeVTree::getDACtoMeV()
{
    std::unordered_map<int, DAC2uA2MeV> SCID_DACtoMeV_map;
    std::unordered_map<int, std::vector<double>> mdu;
    std::unordered_map<int, std::vector<double>> mum;

    Restart();
    while (Next()) {
        auto itr = m_map.find(*channelId);
        if (itr == m_map.end()) continue;
        mdu[itr->second].push_back(static_cast<double>(*DAC2uA));
        mum[itr->second].push_back(static_cast<double>(*uAMeV));
    }
    auto getSum = [](std::vector<double>& v) -> double { return static_cast<double>(std::accumulate(v.begin(), v.end(), 0.)); };
    for (auto&& it : mdu) SCID_DACtoMeV_map[it.first].DAC2uA = getSum(it.second) / it.second.size();
    for (auto&& it : mum) SCID_DACtoMeV_map[it.first].uAMeV = getSum(it.second) / it.second.size();
    for (auto&& it : SCID_DACtoMeV_map) it.second.DACtoMeV = it.second.DAC2uA * it.second.uAMeV;
    return SCID_DACtoMeV_map;
}

void DAC2MeVTree::OutputDACtoMeV(const std::string& outputFileName)
{
    const auto map{getDACtoMeV()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("ADCMEV", "ADCMEV");
    Int_t channelId_out;
    Float_t uAMeV_out;
    Float_t DAC2uA_out;
    Double_t DACtoMeV;
    Double_t DACtoMeVmean;
    outputTree->Branch("channelId", &channelId_out);
    outputTree->Branch("uAMeV", &uAMeV_out);
    outputTree->Branch("DAC2uA", &DAC2uA_out);
    outputTree->Branch("DACtoMeV", &DACtoMeV);
    outputTree->Branch("DACtoMeVmean", &DACtoMeVmean);

    std::unordered_map<Int_t, std::pair<Double_t, Double_t>> SCID_pair_map;
    {
        std::unordered_map<Int_t, std::vector<Float_t>> mtmp_uAMeV;
        std::unordered_map<Int_t, std::vector<Float_t>> mtmp_DAC2uA;
        std::unordered_map<Int_t, Double_t> SCID_uAMeV_map;
        std::unordered_map<Int_t, Double_t> SCID_DAC2uA_map;
        Restart();
        while (Next()) {
            auto itr = m_map.find(*channelId);
            if (itr == m_map.end()) continue;
            const auto scid{itr->second};
            mtmp_uAMeV[scid].push_back(*uAMeV);
            mtmp_DAC2uA[scid].push_back(*DAC2uA);
        }
        auto getSum = [](std::vector<Float_t>& v) -> double { return static_cast<double>(std::accumulate(v.begin(), v.end(), 0.)); };
        for (auto&& it : mtmp_uAMeV) SCID_uAMeV_map[it.first] = getSum(it.second) / it.second.size();
        for (auto&& it : mtmp_DAC2uA) SCID_DAC2uA_map[it.first] = getSum(it.second) / it.second.size();
        for (auto&& it : SCID_uAMeV_map) SCID_pair_map[it.first] = std::make_pair(it.second, SCID_DAC2uA_map[it.first]);
    }

    for (const auto& it : map) {
        channelId_out = it.first;
        DACtoMeV      = it.second.DACtoMeV;
        const auto pair{SCID_pair_map[channelId_out]};
        uAMeV_out    = pair.first;
        DAC2uA_out   = pair.second;
        DACtoMeVmean = uAMeV_out * DAC2uA_out;
        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();
}

void DAC2MeVTree::OutputDACuAMeVDistribution(const std::string& LITFileName, const std::string& outputTreeFileName, const std::string& outputPDFFileName)
{
    LArIDtranslator LIT(LITFileName);

    struct LCInfo {
        Int_t DET;
        Float_t DAC2uA, uAMeV;
    };

    std::map<Int_t, std::map<Int_t, LCInfo>> SCID_ID_str_map;
    {
        auto SCLCPositionMap{LIT.GetSCLCPositionMapSC()};
        for (const auto& it : SCLCPositionMap) {
            const auto SCID{it.first};
            const auto& IDmap{it.second};
            std::map<Int_t, LCInfo> mtmp;
            bool b_first{true};
            Int_t DETforSC;
            bool irregular{};
            for (const auto& id : IDmap) {
                if (b_first) {
                    DETforSC = id.second.DET;
                    b_first  = false;
                } else {
                    if (id.second.DET not_eq DETforSC) {
                        Message::Warning((boost::format("SC %1% has LAr cells which belong to different sub-detectors -> skip") % SCID).str());
                        irregular = true;
                        break;
                    }
                }
                const auto LCID{id.first};
                const auto& info{id.second};
                mtmp[LCID].DET = info.DET;
            }
            if (not irregular) SCID_ID_str_map[SCID] = std::move(mtmp);
        }
    }

    std::set<Int_t> incompleteSC_set;
    { // fill DAC2uA and uAMeV in SCID_ID_str_map
        Restart();
        while (Next()) {
            auto itr = m_map.find(*channelId);
            if (itr == m_map.end()) continue;
            const auto SCID{itr->second};
            auto itr2 = SCID_ID_str_map.find(SCID);
            if (itr2 == SCID_ID_str_map.end()) continue;
            auto itr3 = itr2->second.find(*channelId);
            if (itr3 == itr2->second.end()) {
                incompleteSC_set.insert(SCID);
                continue;
            }
            auto& str{itr3->second};

            str.DAC2uA = *DAC2uA;
            str.uAMeV  = *uAMeV;
        }
    }

    const auto SCPositionMap{LIT.GetSCPositionMap()};
    std::array<std::vector<Double_t>, 4> av_DAC2uA_mean;
    std::array<std::vector<Double_t>, 4> av_DAC2uA_rms;
    std::array<std::vector<Double_t>, 4> av_DAC2uA_r_m;
    std::array<std::vector<Double_t>, 4> av_uAMeV_mean;
    std::array<std::vector<Double_t>, 4> av_uAMeV_rms;
    std::array<std::vector<Double_t>, 4> av_uAMeV_r_m;

    const auto fg{FileGenerator(outputTreeFileName)};
    TFile* outputFile = new TFile(outputTreeFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("DACuAMeV", "DACuAMeV");
    Int_t SC_ONL_ID, DET, SAM;
    Float_t SCETA, SCPHI;
    Int_t Ncells;
    Double_t DAC2uA_ave, DAC2uA_rms, DAC2uA_relrms, DAC2uA_skew;
    std::vector<Float_t> DAC2uA_LC;
    Double_t DAC2uA_range, uAMeV_range;
    Double_t uAMeV_ave, uAMeV_rms, uAMeV_relrms, uAMeV_skew;
    std::vector<Float_t> uAMeV_LC;
    Int_t DAC2uA_Noverlaps, uAMeV_Noverlaps;

    outputTree->Branch("SC_ONL_ID", &SC_ONL_ID);
    outputTree->Branch("DET", &DET);
    outputTree->Branch("SAM", &SAM);
    outputTree->Branch("SCETA", &SCETA);
    outputTree->Branch("SCPHI", &SCPHI);
    outputTree->Branch("Ncells", &Ncells);
    outputTree->Branch("DAC2uA_ave", &DAC2uA_ave);
    outputTree->Branch("DAC2uA_rms", &DAC2uA_rms);
    outputTree->Branch("DAC2uA_relrms", &DAC2uA_relrms);
    outputTree->Branch("DAC2uA_skew", &DAC2uA_skew);
    outputTree->Branch("DAC2uA_LC", &DAC2uA_LC);
    outputTree->Branch("DAC2uA_Noverlaps", &DAC2uA_Noverlaps);
    outputTree->Branch("DAC2uA_range", &DAC2uA_range);
    outputTree->Branch("uAMeV_ave", &uAMeV_ave);
    outputTree->Branch("uAMeV_rms", &uAMeV_rms);
    outputTree->Branch("uAMeV_relrms", &uAMeV_relrms);
    outputTree->Branch("uAMeV_skew", &uAMeV_skew);
    outputTree->Branch("uAMeV_LC", &uAMeV_LC);
    outputTree->Branch("uAMeV_Noverlaps", &uAMeV_Noverlaps);
    outputTree->Branch("uAMeV_range", &uAMeV_range);

    for (const auto& it : SCID_ID_str_map) { // fill the above vectors
        SC_ONL_ID = it.first;
        if (incompleteSC_set.find(SC_ONL_ID) not_eq incompleteSC_set.end()) {
            Message::Warning((boost::format("SC %1% is incomplete -> skip") % SC_ONL_ID).str());
            continue;
        }
        const auto& IDmap{it.second};

        DAC2uA_LC.clear();
        DAC2uA_LC.shrink_to_fit();
        uAMeV_LC.clear();
        uAMeV_LC.shrink_to_fit();
        std::vector<Float_t> v_DAC2uA;
        std::vector<Float_t> v_uAMeV;
        Int_t DETforSC{99999};
        {
            bool b_first{true};
            for (const auto& id : IDmap) {
                if (b_first) {
                    DETforSC = id.second.DET;
                    b_first  = false;
                }
                v_DAC2uA.push_back(id.second.DAC2uA);
                v_uAMeV.push_back(id.second.uAMeV);
            }
            if (DETforSC == 99999) Message::Fatal("There is a bug in DAC2MeVTree.cxx");
        }

        if (v_DAC2uA.size() <= 1) continue;
        Ncells = v_DAC2uA.size();

        auto max  = [](std::vector<Float_t>& v) -> Double_t { return *std::max_element(v.begin(), v.end()); };
        auto min  = [](std::vector<Float_t>& v) -> Double_t { return *std::min_element(v.begin(), v.end()); };
        auto skew = [](std::vector<Float_t>& v, Double_t mean, Double_t rms) -> Double_t {
            auto numerator = std::accumulate(v.begin(), v.end(), 0., [mean](Double_t sum, Float_t val) {
                auto diff{val - mean};
                return sum + diff * diff * diff;
            });
            return numerator / (v.size() * rms * rms * rms);
        };
        auto nov = [](std::vector<Float_t>& v) -> Int_t {
            std::sort(v.begin(), v.end());
            Int_t count{}, count_current{};
            Float_t previous{};
            for (const auto& it : v) {
                if (it not_eq previous) count_current = 0;
                ++count_current;
                if (count_current > count) count = count_current;
            }
            return count;
        };

        DAC2uA_ave                = std::accumulate(v_DAC2uA.begin(), v_DAC2uA.end(), 0.) / v_DAC2uA.size();
        const Double_t DAC2uA_var = std::inner_product(v_DAC2uA.begin(), v_DAC2uA.end(), v_DAC2uA.begin(), 0.) / v_DAC2uA.size() - DAC2uA_ave * DAC2uA_ave;
        DAC2uA_rms                = (DAC2uA_var > 0) ? sqrt(std::inner_product(v_DAC2uA.begin(), v_DAC2uA.end(), v_DAC2uA.begin(), 0.) / v_DAC2uA.size() - DAC2uA_ave * DAC2uA_ave) : 0;
        av_DAC2uA_mean[DETforSC].push_back(DAC2uA_ave);
        av_DAC2uA_rms[DETforSC].push_back(DAC2uA_rms);
        DAC2uA_relrms = DAC2uA_rms / DAC2uA_ave;
        av_DAC2uA_r_m[DETforSC].push_back(DAC2uA_relrms);
        if (DAC2uA_rms) {
            DAC2uA_skew      = Ncells <= 2 ? 0 : skew(v_DAC2uA, DAC2uA_ave, DAC2uA_rms);
            DAC2uA_range     = max(v_DAC2uA) - min(v_DAC2uA);
            DAC2uA_Noverlaps = nov(v_DAC2uA);
        } else {
            DAC2uA_skew      = 0;
            DAC2uA_range     = 0;
            DAC2uA_Noverlaps = Ncells;
        }
        DAC2uA_LC = std::move(v_DAC2uA);

        uAMeV_ave                = std::accumulate(v_uAMeV.begin(), v_uAMeV.end(), 0.) / v_uAMeV.size();
        const Double_t uAMeV_var = std::inner_product(v_uAMeV.begin(), v_uAMeV.end(), v_uAMeV.begin(), 0.) / v_uAMeV.size() - uAMeV_ave * uAMeV_ave;
        uAMeV_rms                = (uAMeV_var > 0) ? sqrt(std::inner_product(v_uAMeV.begin(), v_uAMeV.end(), v_uAMeV.begin(), 0.) / v_uAMeV.size() - uAMeV_ave * uAMeV_ave) : 0;
        av_uAMeV_mean[DETforSC].push_back(uAMeV_ave);
        uAMeV_relrms = 0;
        av_uAMeV_rms[DETforSC].push_back(uAMeV_rms);
        uAMeV_relrms = uAMeV_rms / uAMeV_ave;
        av_uAMeV_r_m[DETforSC].push_back(uAMeV_relrms);
        if (uAMeV_rms) {
            uAMeV_skew      = Ncells <= 2 ? 0 : skew(v_uAMeV, uAMeV_ave, uAMeV_rms);
            uAMeV_range     = max(v_uAMeV) - min(v_uAMeV);
            uAMeV_Noverlaps = nov(v_uAMeV);
        } else {
            uAMeV_skew      = 0;
            uAMeV_range     = 0;
            uAMeV_Noverlaps = Ncells;
        }
        uAMeV_LC = std::move(v_uAMeV);

        DET = DETforSC;
        const auto& postmp{SCPositionMap.at(SC_ONL_ID)};
        SAM   = postmp.SAM;
        SCETA = postmp.ETA;
        SCPHI = postmp.PHI;
        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();

    std::array<TH1D*, 4> h_DAC2uA_mean;
    std::array<TH1D*, 4> h_DAC2uA_rms;
    std::array<TH1D*, 4> h_DAC2uA_r_m;
    std::array<TH1D*, 4> h_uAMeV_mean;
    std::array<TH1D*, 4> h_uAMeV_rms;
    std::array<TH1D*, 4> h_uAMeV_r_m;

    for (int i{}; i < 4; ++i) {
        std::string st_det;
        auto color{kGreen};
        switch (i) {
            case 0:
                color  = kRed;
                st_det = "EMB";
                break;
            case 1:
                color  = kRed;
                st_det = "EMEC";
                break;
            case 2:
                color  = kRed;
                st_det = "HEC";
                break;
            case 3:
                color  = kRed;
                st_det = "FCal";
                break;
        }
        auto high = [i](const std::array<std::vector<Double_t>, 4>& av) -> Double_t {
            const auto maximum{*std::max_element(av[i].begin(), av[i].end())};
            return maximum ? maximum * 1.05 : .001;
        };
        auto size = [i](const std::array<std::vector<Double_t>, 4>& av) {
            return av[i].size();
        };
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_DAC2uA_mean) % high(av_DAC2uA_mean)).str());
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_DAC2uA_rms) % high(av_DAC2uA_rms)).str());
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_DAC2uA_r_m) % high(av_DAC2uA_r_m)).str());
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_uAMeV_mean) % high(av_uAMeV_mean)).str());
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_uAMeV_rms) % high(av_uAMeV_rms)).str());
        Message::Debug((boost::format("%1% %2% %3%") % st_det % size(av_uAMeV_r_m) % high(av_uAMeV_r_m)).str());
        TH1D* Dm  = new TH1D("", Form("%s;<F_{DAC#rightarrow#muA}>;", st_det), 100, 0, high(av_DAC2uA_mean));
        TH1D* Dr  = new TH1D("", Form("%s;#sigma_{F_{DAC#rightarrow#muA}};", st_det), 100, 0, high(av_DAC2uA_rms));
        TH1D* Drm = new TH1D("", Form("%s;#sigma_{F_{DAC#rightarrow#muA}/<F_{DAC#rightarrow#muA}>};", st_det), 100, 0, high(av_DAC2uA_r_m));
        TH1D* um  = new TH1D("", Form("%s;<F_{#muA#rightarrowMeV}>;", st_det), 100, 0, high(av_uAMeV_mean));
        TH1D* ur  = new TH1D("", Form("%s;#sigma_{F_{#muA#rightarrowMeV}};", st_det), 100, 0, high(av_uAMeV_rms));
        TH1D* urm = new TH1D("", Form("%s;#sigma_{F_{#muA#rightarrowMeV}/<F_{#muA#rightarrowMeV}>};", st_det), 100, 0, high(av_uAMeV_r_m));
        {
            auto fill = [i](TH1D* h, const std::array<std::vector<Double_t>, 4>& av) {for (const auto& it : av[i]) h->Fill(it); };
            fill(Dm, av_DAC2uA_mean);
            fill(Dr, av_DAC2uA_rms);
            fill(Drm, av_DAC2uA_r_m);
            fill(um, av_uAMeV_mean);
            fill(ur, av_uAMeV_rms);
            fill(urm, av_uAMeV_r_m);

            std::vector<TH1D*> v = {Dm, Dr, Drm, um, ur, urm};
            for (auto h : v) {
                h->SetLineColor(color);
                h->SetFillColor(color);
                h->SetStats(0);
            }

            h_DAC2uA_mean[i] = Dm;
            h_DAC2uA_rms[i]  = Dr;
            h_DAC2uA_r_m[i]  = Drm;
            h_uAMeV_mean[i]  = um;
            h_uAMeV_rms[i]   = ur;
            h_uAMeV_r_m[i]   = urm;
        }
    }

    {
        TString name(outputPDFFileName);
        TCanvas* c1 = new TCanvas(name, name, 600, 500);
        c1->Print(name + "[", "pdf");

        std::vector<std::array<TH1D*, 4>> v = {h_DAC2uA_mean, h_DAC2uA_rms, h_DAC2uA_r_m, h_uAMeV_mean, h_uAMeV_rms, h_uAMeV_r_m};
        for (int i{}; i < 4; ++i) {
            for (auto&& it : v) {
                it[i]->Draw("hist f");
                c1->Print(name, "pdf");
            }
        }

        c1->Print(name + "]", "pdf");
    }
}
