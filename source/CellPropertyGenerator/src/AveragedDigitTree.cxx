#include "AveragedDigitTree.h"

#define PLISM_ATLAS_STYLE
#include "../../atlasrootstyle/AtlasLabels.C"
#include "../../atlasrootstyle/AtlasStyle.C"
#include "../../atlasrootstyle/AtlasUtils.C"

using namespace CellPropertyGenerator;

void AveragedDigitTree::PulseTree()
{
    m_reader.Restart();

    const auto outputFileName{this->ReplacePrefix("pulse").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("PULSE", "PULSE");

    Int_t channelId_out;
    std::vector<Double_t> amplitude_out;
    std::vector<Double_t> time_out;
    Int_t Nsamples_out;
    Float_t maxAmp_out;
    Int_t iMaxAmp_out;
    Float_t minAmp_out;
    std::unordered_set<int> allCL_out;
    std::unordered_set<int> pulsedCL_out;
    Int_t NpulsedCells_out;
    Int_t DACforSC_out;
    Int_t iCLset_out;
    Int_t iDAC_out;
    Int_t latomeSourceID_out;
    Int_t layer_out;
    Float_t eta_out;
    Float_t phi_out;
	Int_t detector_out{-1};
    Double_t ped_out;
    Double_t rms_out;
    Double_t DACtoMeV_out;
	Bool_t isCali_out{true};

    outputTree->Branch("channelId", &channelId_out);
    outputTree->Branch("amplitude", &amplitude_out);
    outputTree->Branch("time", &time_out);
    outputTree->Branch("Nsamples", &Nsamples_out);
    outputTree->Branch("maxAmp", &maxAmp_out);
    outputTree->Branch("iMaxAmp", &iMaxAmp_out);
    outputTree->Branch("minAmp", &minAmp_out);
    outputTree->Branch("allCL", &allCL_out);
    outputTree->Branch("pulsedCL", &pulsedCL_out);
    outputTree->Branch("NpulsedCells", &NpulsedCells_out);
    outputTree->Branch("DACforSC", &DACforSC_out);
    outputTree->Branch("iCLset", &iCLset_out);
    outputTree->Branch("iDAC", &iDAC_out);
    outputTree->Branch("latomeSourceID", &latomeSourceID_out);
    outputTree->Branch("layer", &layer_out);
    outputTree->Branch("eta", &eta_out);
    outputTree->Branch("phi", &phi_out);
	outputTree->Branch("detector", &detector_out);
    outputTree->Branch("ped", &ped_out);
    outputTree->Branch("rms", &rms_out);
    outputTree->Branch("DACtoMeV", &DACtoMeV_out);

    std::unordered_map<int, std::unordered_map<int, dacMap> >
      map; // (channelId, (iCL, dacMap))

    while (m_reader.Next()) {
        if (map.find(*channelId) == map.end() || map[*channelId].find(*iCLset) == map[*channelId].end()) {
            dacMap a;
            map[*channelId][*iCLset] = a;
        }

        auto& dacMapObj = map[*channelId][*iCLset];
        if (dacMapObj.find(*iDAC) == dacMapObj.end()) {
            waveInfo a;
            a.Nsamples       = *Nsamples;
            a.allCL          = *allCL;
            a.pulsedCL       = *pulsedCL;
            a.NpulsedCells   = *NpulsedCells;
            a.DACforSC       = *DACforSC;
            a.latomeSourceID = *latomeSourceID;
            a.layer          = *layer;
            a.eta            = *eta;
            a.phi            = *phi;
            a.ped            = *ped;
            a.rms            = *rms;
            a.DACtoMeV       = *DACtoMeV;
            dacMapObj[*iDAC] = a;
        }

        auto& waveInfoObj = dacMapObj[*iDAC];
        for (int i{}; i < *Nsamples; ++i)
            waveInfoObj.time_amp[(i + 1) * 25. - (*delay)] = samples->at(i);
    }

    for (const auto& it_map : map) {
        channelId_out = it_map.first;
        for (const auto& it_cl : it_map.second) {
            iCLset_out = it_cl.first;
            for (const auto& it_daq : it_cl.second) {
                iDAC_out = it_daq.first;
                const auto& waveInfoObj{it_daq.second};

                Nsamples_out       = waveInfoObj.Nsamples;
                allCL_out          = waveInfoObj.allCL;
                pulsedCL_out       = waveInfoObj.pulsedCL;
                NpulsedCells_out   = waveInfoObj.NpulsedCells;
                DACforSC_out       = waveInfoObj.DACforSC;
                latomeSourceID_out = waveInfoObj.latomeSourceID;
                layer_out          = waveInfoObj.layer;
                eta_out            = waveInfoObj.eta;
                phi_out            = waveInfoObj.phi;
                ped_out            = waveInfoObj.ped;
                rms_out            = waveInfoObj.rms;
                DACtoMeV_out       = waveInfoObj.DACtoMeV;

                std::vector<Double_t> vtime;
                std::vector<Double_t> vamp;
                const int length{static_cast<int>(waveInfoObj.time_amp.size())};
                if (length != Nsamples_out * 24) Message::Fatal("Inconsistency of length");
                vtime.reserve(length);
                vamp.reserve(length);
                for (const auto& it_ta : waveInfoObj.time_amp) {
                    vtime.push_back(it_ta.first);
                    vamp.push_back(it_ta.second);
                }
                float max{-99999.}, min{99999.};
                int maxi{};
                for (int i{}; i < length; ++i) {
                    if (vamp[i] > max) {
                        max  = vamp[i];
                        maxi = i;
                    } else if (vamp[i] < vamp[i])
                        min = vamp[i];
                }
                maxAmp_out    = max;
                iMaxAmp_out   = maxi;
                minAmp_out    = min;
                amplitude_out = std::move(vamp);
                time_out      = std::move(vtime);
                outputTree->Fill();
            }
        }
    }
    outputTree->Write();
    outputFile->Close();
}

void AveragedDigitTree::PulseShape()
{
#ifdef PLISM_ATLAS_STYLE
    SetAtlasStyle();
#endif

    m_reader.Restart();

    struct GraphMenu {
        std::map<double, std::map<float, float> > g; // (inputET, (time, digit))
        float max{-99999.}, min{99999.};
        int layer;
        float eta, phi;
    };
    std::unordered_map<int, std::unordered_map<int, GraphMenu> >
      map; // (channelId, (iCL, GraphMenu))

    while (m_reader.Next()) {
        if (map.find(*channelId) == map.end() || map[*channelId].find(*iCLset) == map[*channelId].end()) {
            GraphMenu a;
            map[*channelId][*iCLset] = a;
        }

        auto& st = map[*channelId][*iCLset];
        const double inputET{*DACforSC * *DACtoMeV * TMath::Sin(2 * TMath::ATan(TMath::Exp(-fabs(*eta))))};
        for (int i = 0; i < *Nsamples; ++i) {
            st.g[inputET][(i + 1) * 25. - (*delay)] = samples->at(i);
            if (samples->at(i) > st.max) st.max = samples->at(i);
            if (samples->at(i) < st.min) st.min = samples->at(i);
        }
        st.layer = *layer;
        st.eta   = *eta;
        st.phi   = *phi;
    }

    std::vector<std::string> col_str{{"#EB610F", "#09AE7B", "#3069B3", "#F5A102", "#67C8F2", "#893B2C", "#EF908A", "#A53D92"}};
    std::vector<int> col_int;
    for (const auto& it : col_str)
        col_int.push_back(TColor::GetColor(it.c_str()));

    TString pdfname{this->ReplaceExtension("pdf").string()};
    TCanvas* c = new TCanvas(pdfname, pdfname, 800, 600);
    c->Print(pdfname + "[", "pdf");

    for (const auto& ch : map) {
        const int channelId = ch.first;
        for (const auto& cl : ch.second) {
            const int iCLset = cl.first;
            const auto st    = std::move(cl.second);
            bool b_first     = true;
            int count{};
            for (const auto& dac : st.g) {
                TGraph* graph = new TGraph();
                graph->SetMarkerStyle(8);
                graph->SetMarkerSize(0.2);
                int i = -1;
                for (const auto& t : dac.second) {
                    graph->SetPoint(++i, t.first, t.second);
                }

                graph->SetMarkerColor(col_int[count % col_int.size()]);
                if (b_first) {
                    graph->SetMaximum(st.max + (st.max - st.min) * 0.05);
                    graph->SetMinimum(st.min - (st.max - st.min) * 0.05);
                    graph->GetXaxis()->SetLimits(0, 800);
                    graph->SetTitle(
                      Form("channelId=%d, iCLset=%d;Time [ns];Amplitude [ADC]",
                           channelId,
                           iCLset));
                    graph->Draw("ap");
                    b_first = false;
                } else {
                    graph->Draw("p");
                }
#ifdef PLISM_ATLAS_STYLE
                const auto inputET{dac.first};
                myText(count % 2 ? 0.7 : 0.5, 0.61 - count / 2 * 0.05, col_int[count % col_int.size()], Form("%.lf GeV", round(inputET * 1e-3)));
#endif
                ++count;
            }
#ifdef PLISM_ATLAS_STYLE
            // ATLASLabel(0.5, 0.85, "Internal");
            switch (st.layer) {
                case (0):
                    myText(0.5, 0.77, 1, "Presampler");
                    break;
                case (1):
                    myText(0.5, 0.77, 1, "Front layer");
                    break;
                case (2):
                    myText(0.5, 0.77, 1, "Middle layer");
                    break;
                case (3):
                    myText(0.5, 0.77, 1, "Back layer");
                    break;
            }
            myText(0.5, 0.69, 1, Form("#font[52]{#eta} = %5.4f, #font[52]{#phi} = %5.4f", st.eta, st.phi));
#endif
            c->Print(pdfname, "pdf");
        }
    }
    c->Print(pdfname + "]", "pdf");
}

void AveragedDigitTree::LSB_delayPeak()
{
    struct GraphMenu {
        double DACtoMeV{0.}, EtoET{0.};
        double DAC2uA{}, uAMeV{};
        float peak{-99999.};
        float eta, phi;
        int layer, DACforSC;
    };
    std::unordered_map<int, std::unordered_map<int, GraphMenu> > map;

    m_reader.Restart();
    while (m_reader.Next()) {
        if (*iDAC) continue;
        if (map.find(*channelId) == map.end() || map[*channelId].find(*iCLset) == map[*channelId].end()) {
            GraphMenu a;
            a.eta                    = *eta;
            a.phi                    = *phi;
            a.layer                  = *layer;
            a.DACforSC               = *DACforSC;
            a.DACtoMeV               = *DACtoMeV;
            a.DAC2uA                 = *DAC2uA;
            a.uAMeV                  = *uAMeV;
            a.EtoET                  = TMath::Sin(2 * TMath::ATan(TMath::Exp(-fabs(*eta))));
            map[*channelId][*iCLset] = a;
        }
        auto& st            = map[*channelId][*iCLset];
        const float tmppeak = getMax(*samples) - *ped;
        if (tmppeak > st.peak) st.peak = tmppeak;
    }

    const auto outputFileName{this->ReplacePrefix("lsb").string()};
    const auto fg{FileGenerator(outputFileName)};
    TFile* outputFile = new TFile(outputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("LSBINFO", "LSBINFO");

    int out_channelId, out_layer, out_detector{-1}, out_iCLset, out_DACforSC;
    float out_eta, out_phi, out_peakADC, out_mphysovermcal{1.};
    double out_LSB, out_EtoET, out_DACtoMeV, out_DAC2uA, out_uAMeV;
	bool out_isCali{true};
    outputTree->Branch("channelId", &out_channelId);
    outputTree->Branch("iCLset", &out_iCLset);
    outputTree->Branch("eta", &out_eta);
    outputTree->Branch("phi", &out_phi);
    outputTree->Branch("layer", &out_layer);
	outputTree->Branch("detector", &out_detector);
    outputTree->Branch("EtoET", &out_EtoET);
    outputTree->Branch("DACtoMeV", &out_DACtoMeV);
    outputTree->Branch("DAC2uA", &out_DAC2uA);
    outputTree->Branch("uAMeV", &out_uAMeV);
    outputTree->Branch("DACforSC", &out_DACforSC);
    outputTree->Branch("peakADC", &out_peakADC);
    outputTree->Branch("LSB", &out_LSB);
	outputTree->Branch("mphysovermcal", &out_mphysovermcal);
    outputTree->Branch("isCali", &out_isCali);

    for (const auto& ch : map) {
        out_channelId = ch.first;
        for (const auto& cl : ch.second) {
            out_iCLset    = cl.first;
            const auto st = std::move(cl.second);
            out_EtoET     = st.EtoET;
            out_DACtoMeV  = st.DACtoMeV;
            out_DAC2uA    = st.DAC2uA;
            out_uAMeV     = st.uAMeV;
            out_DACforSC  = st.DACforSC;
            out_peakADC   = st.peak;
            out_LSB       = st.EtoET * st.DACtoMeV * st.DACforSC / st.peak;
            out_eta       = st.eta;
            out_phi       = st.phi;
            out_layer     = st.layer;
            outputTree->Fill();
        }
    }
    outputTree->Write();
    outputFile->Close();
}
