#include "AREUSNoiseTree.h"

using namespace CellPropertyGenerator;

void AREUSNoiseTree::setNoiseMap(
  std::unordered_map<unsigned int, float>& noiseMap)
{
    if (fChain == 0) return;

    Long64_t nentries = fChain->GetEntriesFast();

    ChannelIdMap ChannelIdMap;
    SCMaps SCMap;
    SCMap.fill(ChannelIdMap);

    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;

        auto iterate = [&noiseMap, &ChannelIdMap](
                         Int_t cc_n, const std::vector<float>& cc_eta, const std::vector<float>& cc_phi, const std::vector<int>& cc_DetCells, const std::vector<float>& cc_Sigma) {
            auto eta   = std::begin(cc_eta);
            auto phi   = std::begin(cc_phi);
            auto ID    = std::begin(cc_DetCells);
            auto sigma = std::begin(cc_Sigma);

            if (static_cast<int>(cc_eta.size()) != cc_n || static_cast<int>(cc_phi.size()) != cc_n || static_cast<int>(cc_DetCells.size()) != cc_n || static_cast<int>(cc_Sigma.size()) != cc_n) {
                Message::Fatal("Size of cc_* is not consistent");
            }

            for (; eta != std::end(cc_eta); ++eta, ++phi, ++ID, ++sigma) {
                auto keyID = ChannelIdMap.find(*ID);
                if (keyID == ChannelIdMap.end()) Message::Fatal((boost::format("ID %1% was not found") % *ID).str());
                auto keyPhi = keyID->second.upper_bound(*phi);
                if (keyPhi != keyID->second.begin()) --keyPhi;
                auto keyEta = keyPhi->second.upper_bound(*eta);
                if (keyEta != keyPhi->second.begin()) --keyEta;
                noiseMap[keyEta->second] = *sigma;
            }
        };

        iterate(cc_em_n, *cc_em_eta, *cc_em_phi, *cc_em_DetCells, *cc_em_Sigma);
        iterate(cc_hec_n, *cc_hec_eta, *cc_hec_phi, *cc_hec_DetCells, *cc_hec_Sigma);
        iterate(cc_fcal_n, *cc_fcal_eta, *cc_fcal_phi, *cc_fcal_DetCells, *cc_fcal_Sigma);
    }
}
