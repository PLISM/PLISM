#include <AveragedDigitTree.h>
#include <CaliWaveTree.h>
#include <CellPropertyTree.h>
#include <DAC2MeVTree.h>
#include <DigitTree.h>
#include <DirectGenerator.h>
#include <LSBTree.h>
#include <MPMCTree.h>
#include <PhysWaveTree.h>
#include <PulsePattern.h>
#include <PulseShapeTree.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

using namespace CellPropertyGenerator;

PYBIND11_MODULE(CellPropertyGeneratorModule, m)
{
    pybind11::class_<DirectGenerator>(m, "DirectGenerator")
      .def_readwrite("PatternFileName", &DirectGenerator::PatternFileName)
      .def_readwrite("ID_SCID_CL_FileName", &DirectGenerator::ID_SCID_CL_FileName)
      .def_readwrite("DigitFileName", &DirectGenerator::DigitFileName)
      .def_readwrite("PedestalFileName", &DirectGenerator::PedestalFileName)
      .def_readwrite("DAC2MeVFileName", &DirectGenerator::DAC2MeVFileName)
      .def_readwrite("NTriggers", &DirectGenerator::NTriggers)
      .def_readwrite("NSamples", &DirectGenerator::NSamples)
      .def_readwrite("Detector", &DirectGenerator::Detector)
      .def_readwrite("LSBReferenceFileName", &DirectGenerator::LSBReferenceFileName)
      .def_readwrite("LSBPlotUpperLimit", &DirectGenerator::LSBPlotUpperLimit)
      .def_readwrite("OutputCellPropertyFileName", &DirectGenerator::OutputCellPropertyFileName)
      .def_readwrite("BCShift", &DirectGenerator::BCShift)
      .def(pybind11::init<>())
      .def("GenerateFromAREUSPulseShapeTree", &DirectGenerator::GenerateFromAREUSPulseShapeTree)
      .def("GenerateFromCalibFrameWork", &DirectGenerator::GenerateFromCalibFrameWork);

    pybind11::class_<PulsePattern>(m, "PulsePattern")
      .def(pybind11::init<const std::string&>())
      .def(pybind11::init<const std::string&, const std::string&>())
      .def("OutputPattern", &PulsePattern::OutputPattern);

    pybind11::class_<DAC2MeVTree>(m, "DAC2MeVTree")
      .def(pybind11::init<const std::string&, const std::string&>())
      .def("OutputDACtoMeV", &DAC2MeVTree::OutputDACtoMeV)
      .def("OutputDACuAMeVDistribution", &DAC2MeVTree::OutputDACuAMeVDistribution);

    pybind11::class_<DigitTree>(m, "DigitTree")
      .def(pybind11::init<const std::string&, PulsePattern&, const std::string&, const std::string&, const std::string&, int, int>())
      .def("MakeTree", &DigitTree::MakeTree);

    pybind11::class_<AveragedDigitTree>(m, "AveragedDigitTree")
      .def(pybind11::init<const std::string&>())
      .def("PulseTree", &AveragedDigitTree::PulseTree)
      .def("PulseShape", &AveragedDigitTree::PulseShape)
      .def("LSB_delayPeak", &AveragedDigitTree::LSB_delayPeak);

    pybind11::class_<LSBTree>(m, "LSBTree")
      .def_readwrite("OutputLSBphysName", &LSBTree::OutputLSBphysName)
      .def(pybind11::init<const std::string&>())
      .def("LSBvsEta", &LSBTree::LSBvsEta)
      .def("LSBphys", &LSBTree::LSBphys);

    pybind11::class_<PulseShapeTree>(m, "PulseShapeTree")
      .def_readwrite("PedAlreadySubtracted", &PulseShapeTree::PedAlreadySubtracted)
      .def_readwrite("LSBFileName", &PulseShapeTree::LSBFileName)
      .def_readwrite("BCShift", &PulseShapeTree::BCShift)
      .def(pybind11::init<const std::string&>())
      .def("GenerateCellProperty", &PulseShapeTree::GenerateCellProperty)
      .def("DrawPulseShape", &PulseShapeTree::DrawPulseShape);

    pybind11::class_<CaliWaveTree>(m, "CaliWaveTree")
      .def_readwrite("ID_SCID_CL_fileName", &CaliWaveTree::ID_SCID_CL_fileName)
      .def_readwrite("DAC2MeVFileName", &CaliWaveTree::DAC2MeVFileName)
      .def_readwrite("PedFileName", &CaliWaveTree::PedFileName)
      .def(pybind11::init<const std::string&>())
      .def("LSB_delayPeak", &CaliWaveTree::LSB_delayPeak)
      .def("PulseTree", &CaliWaveTree::PulseTree);

    pybind11::class_<PhysWaveTree>(m, "PhysWaveTree")
      .def_readwrite("ID_SCID_CL_fileName", &PhysWaveTree::ID_SCID_CL_fileName)
      .def_readwrite("DAC2MeVFileName", &PhysWaveTree::DAC2MeVFileName)
      .def_readwrite("PedFileName", &PhysWaveTree::PedFileName)
      .def_readwrite("InputMPMCFileName", &PhysWaveTree::InputMPMCFileName)
      .def(pybind11::init<const std::string&>())
      .def("PulseTree", &PhysWaveTree::PulseTree);

    pybind11::class_<MPMCTree>(m, "MPMCTree")
      .def(pybind11::init<const std::string&>());

    pybind11::class_<CellPropertyTree>(m, "CellPropertyTree")
      .def(pybind11::init<const std::string&>())
      .def("PlotPedestalADC", &CellPropertyTree::PlotPedestalADC)
      .def("PlotPedestalGeV", &CellPropertyTree::PlotPedestalGeV)
      .def("PlotRMSADC", &CellPropertyTree::PlotRMSADC)
      .def("PlotRMSMeV", &CellPropertyTree::PlotRMSMeV);
}
