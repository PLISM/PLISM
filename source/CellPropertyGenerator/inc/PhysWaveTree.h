/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "DAC2MeVTree.h"
#include "LSBTree.h"
#include "MPMCTree.h"
#include "PedestalTree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from P1CalibrationProcessing PHYSWAVE tree and output higher level trees
    class PhysWaveTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new PhysWaveTree object
         * @param InputFileName Input PHYWAVE file name
         */
        PhysWaveTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "PHYSWAVE"),
            channelId(m_reader, "channelId"),
            Time(m_reader, "Time"),
            Amplitude(m_reader, "Amplitude")
        {
        }

        /**
         * @brief Generate pulse shape tree
         * @param InputLSBFileName Name of input calibration run LSB file
         */
        void PulseTree(std::string InputLSBFileName);

        std::string ID_SCID_CL_fileName; ///< Input (LArcellID, SCID, CL) list txt file name
        std::string DAC2MeVFileName;     ///< Input DAC2MeV file name
        std::string PedFileName;         ///< Input P1CalibrationProcessing pedestal file name
        std::string InputMPMCFileName;   ///< Input P1CalibrationProcessing MPMC file name
      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderArray<Double_t> Time;
        TTreeReaderArray<Double_t> Amplitude;

        struct EToverADC {
            double LSB{0.}, mphysovermcal{0.};
        };
        std::unordered_map<int, EToverADC> m_EToverADC_map;
        struct st_DACtoMeV {
            double DACtoMeV{0.}, DAC2uA{0.}, uAMeV{0.};
        };
        std::unordered_map<int, st_DACtoMeV> m_DACtoMeV_map;
        struct ped_rms_DACtoMeV {
            double ped{0.}, rms{0.}, DACtoMeV{0.};
        };
        std::unordered_map<int, ped_rms_DACtoMeV> m_ped_rms_ac_DACtoMeV_map;
    };
} // namespace CellPropertyGenerator
  /// @}