/// @addtogroup CellPropertyGenerator
/// @{

#pragma once

#include <TAxis.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TMath.h>
#include <TString.h>
#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <unordered_set>

/// @}