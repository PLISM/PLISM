/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "Tools.h"
#include "Tree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from averaged digit tree and output higher level trees
    class AveragedDigitTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new AveragedDigitTree object
         * 
         * @param InputFileName Input digit file name
         */
        AveragedDigitTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "AVERAGED_DIGITS"),
            channelId(m_reader, "channelId"),
            samples(m_reader, "samples"),
            Nsamples(m_reader, "Nsamples"),
            allCL(m_reader, "allCL"),
            pulsedCL(m_reader, "pulsedCL"),
            NpulsedCells(m_reader, "NpulsedCells"),
            DACforSC(m_reader, "DACforSC"),
            delay(m_reader, "delay"),
            iCLset(m_reader, "iCLset"),
            iDAC(m_reader, "iDAC"),
            iDEL(m_reader, "iDEL"),
            latomeSourceID(m_reader, "latomeSourceID"),
            layer(m_reader, "layer"),
            eta(m_reader, "eta"),
            phi(m_reader, "phi"),
            ped(m_reader, "ped"),
            rms(m_reader, "rms"),
            DACtoMeV(m_reader, "DACtoMeV"),
            DAC2uA(m_reader, "DAC2uA"),
            uAMeV(m_reader, "uAMeV") {}
        void PulseTree();  ///< Output pulse shape file
        void PulseShape(); ///< Output pulse shape plots in PDF

        ///
        /// @brief Output LSB file.
        ///
        /// LSB is computed from delay run result.
        /// Pulse peak height is used for the calculation.
        ///
        void LSB_delayPeak();

      private:
        struct waveInfo {
            std::map<float, float> time_amp;
            Int_t Nsamples;
            std::unordered_set<int> allCL;
            std::unordered_set<int> pulsedCL;
            Int_t NpulsedCells;
            Int_t DACforSC;
            Int_t latomeSourceID;
            Int_t layer;
            Float_t eta;
            Float_t phi;
            Double_t ped;
            Double_t rms;
            Double_t DACtoMeV;
        };

        using dacMap = std::map<int, waveInfo>; // (iDAC, waveInfo)

        inline float getMax(std::vector<Double_t>& v) const { return *std::max_element(v.begin(), v.end()); }

        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<std::vector<Double_t>> samples;
        TTreeReaderValue<Int_t> Nsamples;
        TTreeReaderValue<std::unordered_set<int>> allCL;
        TTreeReaderValue<std::unordered_set<int>> pulsedCL;
        TTreeReaderValue<Int_t> NpulsedCells;
        TTreeReaderValue<Int_t> DACforSC;
        TTreeReaderValue<Double_t> delay;
        TTreeReaderValue<Int_t> iCLset;
        TTreeReaderValue<Int_t> iDAC;
        TTreeReaderValue<Int_t> iDEL;
        TTreeReaderValue<Int_t> latomeSourceID;
        TTreeReaderValue<Int_t> layer;
        TTreeReaderValue<Float_t> eta;
        TTreeReaderValue<Float_t> phi;
        TTreeReaderValue<Double_t> ped;
        TTreeReaderValue<Double_t> rms;
        TTreeReaderValue<Double_t> DACtoMeV;
        TTreeReaderValue<Double_t> DAC2uA;
        TTreeReaderValue<Double_t> uAMeV;
    };
} // namespace CellPropertyGenerator
/// @}
