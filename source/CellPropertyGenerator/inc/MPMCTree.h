/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "Tools.h"
#include "Tree.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from P1CalibrationProcessing MPMC tree containing pulse height ratio between physics and calibration pulses
    class MPMCTree : public Tree
    {
      public:
        /**
         * @brief Construct a new MPMCTree object
         * 
         * @param InputFileName Input MPMC file name
         */
        MPMCTree(const std::string& InputFileName) :
            Tree(InputFileName, "MPMC")
        {
            fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
            fChain->SetBranchAddress("mphysovermcal", &mphysovermcal, &b_mphysovermcal);
        }
        // void EToverADCvsEta(const std::string& detector, const std::string& inputLSBRefFile, const float upper_limit, const std::string& InputLSBFileName);
        std::unordered_map<Int_t, Float_t> getMPMCMap(); ///< Return channelId vs MPMC map
        std::string InputMPMCFileName;                   ///< Input MPMC file name

      private:
        Int_t channelId;
        Float_t mphysovermcal;

        TBranch* b_channelId;
        TBranch* b_mphysovermcal;
    };
} // namespace CellPropertyGenerator
/// @}
