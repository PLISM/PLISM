/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "DAC2MeVTree.h"
#include "PedestalTree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from P1CalibrationProcessing CALIWAVE tree and output higher level trees
    class CaliWaveTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new CaliWaveTree object
         * 
         * @param InputFileName Input CALIWAVE file name
         */
        CaliWaveTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "CALIWAVE"),
            channelId(m_reader, "channelId"),
            isConnected(m_reader, "isConnected"),
            detector(m_reader, "detector"),
            layer(m_reader, "layer"),
            eta(m_reader, "eta"),
            phi(m_reader, "phi"),
            timeIndex(m_reader, "timeIndex"),
            Time(m_reader, "Time"),
            Amplitude(m_reader, "Amplitude"),
            Error(m_reader, "Error"),
            Triggers(m_reader, "Triggers"),
            TmaxAmp(m_reader, "TmaxAmp"),
            MaxAmp(m_reader, "MaxAmp"),
            DAC(m_reader, "DAC"),
            nPulsedCalibLines(m_reader, "nPulsedCalibLines"),
            pulsedCalibLines(m_reader, "pulsedCalibLines"),
            Jitter(m_reader, "Jitter")
        {
        }

        ///
        /// @brief Output LSB file.
        ///
        /// LSB is computed from delay run result.
        /// Pulse peak height is used for the calculation.
        ///
        void LSB_delayPeak();

        void PulseTree(); ///< Output pulse shape file

        std::string ID_SCID_CL_fileName; ///< Input (LArcellID, SCID, CL) list txt file name
        std::string DAC2MeVFileName;     ///< Input DAC2MeV file name
        std::string PedFileName;         ///< Input P1CalibrationProcessing pedestal file name

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<Int_t> isConnected;
        TTreeReaderValue<Int_t> detector;
        TTreeReaderValue<Int_t> layer;
        TTreeReaderValue<Float_t> eta;
        TTreeReaderValue<Float_t> phi;
        TTreeReaderValue<Int_t> timeIndex;
        TTreeReaderArray<Double_t> Time;
        TTreeReaderArray<Double_t> Amplitude;
        TTreeReaderArray<Double_t> Error;
        TTreeReaderArray<Int_t> Triggers;
        TTreeReaderValue<Double_t> TmaxAmp;
        TTreeReaderValue<Double_t> MaxAmp;
        TTreeReaderValue<Int_t> DAC;
        TTreeReaderValue<Int_t> nPulsedCalibLines;
        TTreeReaderArray<Int_t> pulsedCalibLines;
        TTreeReaderValue<Double_t> Jitter;

        struct st_DACtoMeV {
            double DACtoMeV{0.}, DAC2uA{0.}, uAMeV{0.};
        };
        std::unordered_map<int, st_DACtoMeV> m_DACtoMeV_map;
        struct ped_rms_DACtoMeV {
            double ped{0.}, rms{0.}, DACtoMeV{0.};
        };
        std::unordered_map<int, ped_rms_DACtoMeV> m_ped_rms_ac_DACtoMeV_map;
    };
} // namespace CellPropertyGenerator
/// @}
