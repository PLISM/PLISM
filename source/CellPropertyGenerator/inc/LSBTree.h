/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "MPMCTree.h"
#include "Tools.h"
#include "Tree.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from LSB tree
    class LSBTree : public Tree
    {
      public:
        /**
         * @brief Construct a new LSBTree object
         * 
         * @param InputFileName Input LSB file name
         */
        LSBTree(const std::string &InputFileName) :
            Tree(InputFileName, "LSBINFO")
        {
            fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
            fChain->SetBranchAddress("iCLset", &iCLset, &b_iCLset);
            fChain->SetBranchAddress("eta", &eta, &b_eta);
            fChain->SetBranchAddress("phi", &phi, &b_phi);
            fChain->SetBranchAddress("layer", &layer, &b_layer);
            fChain->SetBranchAddress("detector", &detector, &b_detector);
            fChain->SetBranchAddress("EtoET", &EtoET, &b_EtoET);
            fChain->SetBranchAddress("DACtoMeV", &DACtoMeV, &b_DACtoMeV);
            fChain->SetBranchAddress("DAC2uA", &DAC2uA, &b_DAC2uA);
            fChain->SetBranchAddress("uAMeV", &uAMeV, &b_uAMeV);
            fChain->SetBranchAddress("DACforSC", &DACforSC, &b_DACforSC);
            fChain->SetBranchAddress("peakADC", &peakADC, &b_peakADC);
            fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
        }

        ///
        /// @brief Output LSB vs eta plot in PDF
        /// @param detector Subdetector ('EMB' or 'EMEC'. Combination of multiple subdetectors is not considered.)
        /// @param inputLSBRefFile LSB reference file which is already prepared in this repository
        /// @param upper_limit Upper limit of LSB in output graph (max value is set as the upper limit if you set 0)
        ///
        void LSBvsEta(const std::string &detector, const std::string &inputLSBRefFile, const float upper_limit);

        ///
        /// @brief Output LSB file for physics pulse
        /// @param InputMPMCFileName Input P1CalibrationProcessing MPMC file name
        ///
        void LSBphys(std::string InputMPMCFileName);

        std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t> > getLSBMap();     ///< Return <channelId, <iCLset, LSB>> map
        std::unordered_map<Int_t, std::unordered_map<Int_t, Double_t> > getpeakADCMap(); ///< Return <channelId, <iCLset, peakADC>> map
        struct etaphilaydecDAC {
            float eta{0.}, phi{0.};
            int layer{0}, detector{0}, DACforSC{0};
        };
        std::unordered_map<Int_t, etaphilaydecDAC> getetaphilaydecDACMap(); ///< Return channelId vs @ref etaphilaydecDAC map
        // std::unordered_map<Int_t, Int_t>  getDACMap();
        std::string OutputLSBphysName; ///< Name of LSB file for physics pulse to be output

      private:
        Int_t channelId;
        Int_t iCLset;
        Float_t eta;
        Float_t phi;
        Int_t layer;
        Int_t detector;
        Double_t EtoET;
        Double_t DACtoMeV;
        Double_t DAC2uA;
        Double_t uAMeV;
        Int_t DACforSC;
        Float_t peakADC;
        Double_t LSB;

        TBranch *b_channelId;
        TBranch *b_iCLset;
        TBranch *b_eta;
        TBranch *b_phi;
        TBranch *b_layer;
        TBranch *b_detector;
        TBranch *b_EtoET;
        TBranch *b_DACtoMeV;
        TBranch *b_DAC2uA;
        TBranch *b_uAMeV;
        TBranch *b_DACforSC;
        TBranch *b_peakADC;
        TBranch *b_LSB;
    };
} // namespace CellPropertyGenerator
/// @}
