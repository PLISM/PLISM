/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "PulsePattern.h"

namespace CellPropertyGenerator
{
    /// Counter class of DAC, delay, calibration line set, trigger, and channel followed by digit tree
    class PatternCounter
    {
      public:
        int iDAC;   ///< Couner of DAC
        int iDEL;   ///< Couner of delay
        int iCLset; ///< Couner of calibration line set
        int iTRIG;  ///< Couner of trigger
        int iCH;    ///< Couner of channel

        /**
         * @brief Construct a new Pattern Counter object
         * 
         * @param pat @ref PulsePattern object
         * @param Nchannels Number of channels to be counted
         * @param Ntrigs Number of triggers to be counted
         * @param ID_SCID_CL_fileName (LArcellID, SCID, CL) list txt file name
         */
        PatternCounter(PulsePattern& pat, const int Nchannels, const int Ntrigs, const std::string& ID_SCID_CL_fileName) :
            iDAC(0),
            iDEL(0),
            iCLset(0),
            iTRIG(0),
            iCH(-1),
            m_Ndelays(pat.Ndelays),
            m_NCLsets(pat.NCLsets),
            m_Ntrigs(Ntrigs),
            m_Nchannels(Nchannels),
            m_CLsets(pat.CLsets)
        {
            int cellID, SCID, CL;
            std::ifstream ifs(ID_SCID_CL_fileName);
            std::string line;
            while (std::getline(ifs, line)) {
                std::istringstream iss(line);
                if (not(iss >> cellID >> SCID))
                    Message::Fatal("Unexpected format of ID_SCID_CL_fileName");
                while (iss >> CL) {
                    if (SCID) {
                        m_SCID_CLset_map[SCID].insert(CL);
                        m_SCID_CLmultiset_map[SCID].insert(CL);
                    }
                }
            }
        }

        /// Increment the counters
        PatternCounter& operator++()
        {
            if (++iCH == m_Nchannels) {
                iCH = 0;
                if (++iTRIG == m_Ntrigs) {
                    iTRIG = 0;
                    if (++iCLset == m_NCLsets) {
                        iCLset = 0;
                        if (++iDEL == m_Ndelays) {
                            iDEL = 0;
                            ++iDAC;
                        }
                    }
                }
            }
            return *this;
        }

        /**
         * @brief Return true if there is an injected cell in a cell pointed to by the counter at the event
         * @param channelId channel online ID
         */
        inline bool pulsed(const int channelId)
        {
            for (const auto& cl : m_SCID_CLset_map[channelId])
                if (m_CLsets[iCLset].find(cl) != m_CLsets[iCLset].end()) return true;
            return false;
        }

        /**
         * @brief Return calibration line set from channelId vs CLset map
         * @param channelId channel online ID
         */
        inline std::unordered_set<int> getCLs(const int channelId)
        {
            return m_SCID_CLset_map[channelId];
        }

        /**
         * @brief Return set of injected calibration lines in in a cell pointed to by the counter at the event
         * @param channelId channel online ID
         */
        inline std::unordered_set<int> getPulsedCLs(const int channelId)
        {
            std::unordered_set<int> s;
            for (const auto& cl : m_SCID_CLset_map[channelId])
                if (m_CLsets[iCLset].find(cl) != m_CLsets[iCLset].end()) s.insert(cl);
            return s;
        }

        /**
         * @brief Return number of injected calibration lines in in a cell pointed to by the counter at the event
         * @param channelId channel online ID
         */
        inline int getNpulsedCells(const int channelId)
        {
            int i = 0;
            for (const auto& cl : m_SCID_CLmultiset_map[channelId])
                if (m_CLsets[iCLset].find(cl) != m_CLsets[iCLset].end()) ++i;
            return i;
        }

      private:
        const int m_Ndelays;
        const int m_NCLsets;
        const int m_Ntrigs;
        const int m_Nchannels;
        std::vector<std::unordered_set<int> >& m_CLsets;
        std::unordered_map<int, std::unordered_set<int> > m_SCID_CLset_map;
        std::unordered_map<int, std::unordered_multiset<int> > m_SCID_CLmultiset_map;
    };
} // namespace CellPropertyGenerator
  /// @}