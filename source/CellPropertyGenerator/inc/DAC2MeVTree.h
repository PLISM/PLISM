/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include <set>
#include "LArIDtranslator.h"
#include "Tools.h"
#include "Tree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from DAC2MeV tree
    class DAC2MeVTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new DAC2MeVTree object
         * 
         * @param InputFileName Input DAC2MeV file name
         * @param ID_SCID_CL_fileName (LArcellID, SCID, CL) list txt file name
         */
        DAC2MeVTree(const std::string& InputFileName, const std::string& ID_SCID_CL_fileName) :
            TreeReader(InputFileName, "ADCMEV"),
            channelId(m_reader, "channelId"),
            uAMeV(m_reader, "uAMeV"),
            DAC2uA(m_reader, "DAC2uA")
        {
            FileObject ID_SCID_CL_file(ID_SCID_CL_fileName);
            std::ifstream ifs(ID_SCID_CL_file.path().string());
            int chid, scid;
            std::string line;
            while (std::getline(ifs, line)) {
                std::istringstream iss(line);
                if (not(iss >> chid >> scid)) Message::Fatal("Unexpected format of ID_SCID_CL_fileName");
                if (scid) m_map[chid] = scid;
            }
        }

        struct DAC2uA2MeV {
            double DACtoMeV{0.}, DAC2uA{0.}, uAMeV{0.};
        };

        std::unordered_map<int, DAC2uA2MeV> getDACtoMeV(); ///< Return channelId vs @ref DAC2uA2MeV map

        ///
        /// @brief Output ADCMEV tree containing variables concerning DAC->MeVT factor
        /// @param outputFileName Output ADCMEV file name
        ///
        void OutputDACtoMeV(const std::string& outputFileName);

        ///
        /// @brief Output plots and tree containing information averages of DAC->MeVT factors over cells
        /// @param ID_SCID_DET_FileName Input (LArcellID, SCID, DET) list txt file name
        /// @param outputTreeFileName Output tree file name
        /// @param outputPDFFileName Output plot file name
        ///
        void OutputDACuAMeVDistribution(const std::string& ID_SCID_DET_FileName, const std::string& outputTreeFileName, const std::string& outputPDFFileName);

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<Float_t> uAMeV;
        TTreeReaderValue<Float_t> DAC2uA;

        std::unordered_map<int, int> m_map;
    };
} // namespace CellPropertyGenerator
/// @}
