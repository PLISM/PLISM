/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "Tools.h"
#include "Tree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from P1CalibrationProcessing pedestal tree
    class PedestalTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new PedestalTree object
         * @param InputFileName Input PEDESTALS file name
         */
        PedestalTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "PEDESTALS"),
            channelId(m_reader, "channelId"),
            ped(m_reader, "ped"),
            rms(m_reader, "rms") {}

        /// Return <channelId, (pedestal, pedestal RMS)> map
        std::unordered_map<Int_t, std::pair<Double_t, Double_t>> getPed();

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<Double_t> ped;
        TTreeReaderValue<Double_t> rms;
    };
} // namespace CellPropertyGenerator
  /// @}