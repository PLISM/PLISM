/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "AutoCorrTree.h"
#include "DAC2MeVTree.h"
#include "PatternCounter.h"
#include "PedestalTree.h"
#include "PulsePattern.h"
#include "Tree.h"

namespace CellPropertyGenerator
{
    /// Class to convert P1CalibrationProcessing digit tree to averaged digit tree
    class DigitTree : public Tree
    {
      public:
        /**
         * @brief Construct a new Digit Tree object
         * 
         * @param InputFileName Input digit file name
         * @param pattern Input calibration run configuration file (parameters.dat)
         * @param ID_SCID_CL_fileName Input (LArcellID, SCID, CL) list txt file name
         * @param pedFileName Input P1CalibrationProcessing pedestal file name
         * @param DAC2MeVFileName Input DAC2MeV file name
         * @param Ntrig Number of triggers. Typically, pulses with the same (DAC, delay, calibration set) are injected 100   times for each to get averaged ADC values to reduce statistical error. This argument is 100 in this   case. You need to refer to the configuration of the run to know this number.
         * @param Nsamps Number of samples in each event. Typically, this is 32 unless some samples are cut in Athena.
         */
        DigitTree(const std::string &InputFileName, PulsePattern &pattern, const std::string &ID_SCID_CL_fileName, const std::string &pedFileName = "", const std::string &DAC2MeVFileName = "", const int Ntrig = 0, const int Nsamps = 0) :
            Tree(InputFileName, "LARDIGITS"),
            m_outputFileName(this->AddPrefix("converted").string()),
            m_pattern(pattern),
            m_ID_SCID_CL_fileName(ID_SCID_CL_fileName),
            m_pedFileName(pedFileName),
            m_Ntrig(Ntrig),
            m_Nsamples(Nsamps)
        {
            fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
            fChain->SetBranchAddress("barrel_ec", &barrel_ec, &b_barrel_ec);
            fChain->SetBranchAddress("pos_neg", &pos_neg, &b_pos_neg);
            fChain->SetBranchAddress("FT", &FT, &b_FT);
            fChain->SetBranchAddress("slot", &slot, &b_slot);
            fChain->SetBranchAddress("channel", &channel, &b_channel);
            fChain->SetBranchAddress("calibLine", &calibLine, &b_calibLine);
            fChain->SetBranchAddress("isConnected", &isConnected, &b_isConnected);
            fChain->SetBranchAddress("channelHash", &channelHash, &b_channelHash);
            fChain->SetBranchAddress("febHash", &febHash, &b_febHash);
            fChain->SetBranchAddress("oflHash", &oflHash, &b_oflHash);
            fChain->SetBranchAddress("offlineId", &offlineId, &b_offlineId);
            fChain->SetBranchAddress("layer", &layer, &b_layer);
            fChain->SetBranchAddress("eta", &eta, &b_eta);
            fChain->SetBranchAddress("phi", &phi, &b_phi);
            fChain->SetBranchAddress("IEvent", &IEvent, &b_IEvent);
            fChain->SetBranchAddress("Nsamples", &Nsamples, &b_Nsamples);
            fChain->SetBranchAddress("samples", samples, &b_samples);
            fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
            fChain->SetBranchAddress("latomeSourceId", &latomeSourceId, &b_latomeSourceId);

            FileObject ID_SCID_CL_file(ID_SCID_CL_fileName);

            if (not m_pedFileName.empty()) {
                {
                    PedestalTree p(m_pedFileName);
                    if (not p()) Message::Fatal("PedestalTree not configured");
                    auto m = p.getPed();
                    for (const auto &it : m) {
                        m_ped_rms_ac_DACtoMeV_map[it.first].ped = it.second.first;
                        m_ped_rms_ac_DACtoMeV_map[it.first].rms = it.second.second;
                    }
                }
                {
                    AutoCorrTree a(m_pedFileName);
                    if (not a()) Message::Fatal("AutoCorrTree not configured");
                    auto m = a.getAutoCorr();
                    for (const auto &it : m) m_ped_rms_ac_DACtoMeV_map[it.first].autocorr = std::move(it.second);
                }
            }
            if (not m_ID_SCID_CL_fileName.empty()) {
                DAC2MeVTree a(DAC2MeVFileName, m_ID_SCID_CL_fileName);
                auto m = a.getDACtoMeV();
                for (const auto &it : m) {
                    m_ped_rms_ac_DACtoMeV_map[it.first].DACtoMeV = it.second.DACtoMeV;
                    m_ped_rms_ac_DACtoMeV_map[it.first].DAC2uA   = it.second.DAC2uA;
                    m_ped_rms_ac_DACtoMeV_map[it.first].uAMeV    = it.second.uAMeV;
                }
            }
        }
        void MakeTree(); ///< Generate averaged digit tree

      private:
        Int_t channelId;
        Int_t barrel_ec;
        Int_t pos_neg;
        Int_t FT;
        Int_t slot;
        Int_t channel;
        Int_t calibLine;
        Int_t isConnected;
        Int_t channelHash;
        Int_t febHash;
        Int_t oflHash;
        Int_t offlineId;
        Int_t layer;
        Float_t eta;
        Float_t phi;
        ULong64_t IEvent;
        Int_t Nsamples;
        Short_t samples[32];
        Short_t BCID;
        Short_t latomeSourceId;

        TBranch *b_channelId;
        TBranch *b_barrel_ec;
        TBranch *b_pos_neg;
        TBranch *b_FT;
        TBranch *b_slot;
        TBranch *b_channel;
        TBranch *b_calibLine;
        TBranch *b_isConnected;
        TBranch *b_channelHash;
        TBranch *b_febHash;
        TBranch *b_oflHash;
        TBranch *b_offlineId;
        TBranch *b_layer;
        TBranch *b_eta;
        TBranch *b_phi;
        TBranch *b_IEvent;
        TBranch *b_Nsamples;
        TBranch *b_samples;
        TBranch *b_BCID;
        TBranch *b_latomeSourceId;

        struct ped_rms_DACtoMeV {
            double ped{0.}, rms{0.}, DACtoMeV{0.}, DAC2uA{0.}, uAMeV{0.};
            std::vector<Float_t> autocorr;
        };
        const std::string m_outputFileName;
        PulsePattern &m_pattern;
        int m_NCH;
        const std::string m_ID_SCID_CL_fileName;
        const std::string m_pedFileName;
        const int m_Ntrig;
        int m_Nsamples;
        std::unordered_map<int, ped_rms_DACtoMeV> m_ped_rms_ac_DACtoMeV_map;
    };
} // namespace CellPropertyGenerator
/// @}
