/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include <vector>
#include "SCMaps.h"
#include "Structures.h"
#include "Tree.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from AREUS noise file
    class AREUSNoiseTree : public Tree
    {
      public:
        /// @param inputFileName Input AREUS noise file name
        AREUSNoiseTree(const std::string &inputFileName) :
            Tree(inputFileName, "caloD3PD")
        {
            cc_em_eta        = 0;
            cc_em_phi        = 0;
            cc_em_DetCells   = 0;
            cc_em_IdCells    = 0;
            cc_em_Sigma      = 0;
            cc_hec_eta       = 0;
            cc_hec_phi       = 0;
            cc_hec_DetCells  = 0;
            cc_hec_IdCells   = 0;
            cc_hec_Sigma     = 0;
            cc_fcal_eta      = 0;
            cc_fcal_phi      = 0;
            cc_fcal_DetCells = 0;
            cc_fcal_IdCells  = 0;
            cc_fcal_Sigma    = 0;
            fChain->SetBranchAddress("cc_em_n", &cc_em_n, &b_cc_em_n);
            fChain->SetBranchAddress("cc_em_eta", &cc_em_eta, &b_cc_em_eta);
            fChain->SetBranchAddress("cc_em_phi", &cc_em_phi, &b_cc_em_phi);
            fChain->SetBranchAddress("cc_em_DetCells", &cc_em_DetCells, &b_cc_em_DetCells);
            fChain->SetBranchAddress("cc_em_IdCells", &cc_em_IdCells, &b_cc_em_IdCells);
            fChain->SetBranchAddress("cc_em_Sigma", &cc_em_Sigma, &b_cc_em_Sigma);
            fChain->SetBranchAddress("cc_hec_n", &cc_hec_n, &b_cc_hec_n);
            fChain->SetBranchAddress("cc_hec_eta", &cc_hec_eta, &b_cc_hec_eta);
            fChain->SetBranchAddress("cc_hec_phi", &cc_hec_phi, &b_cc_hec_phi);
            fChain->SetBranchAddress("cc_hec_DetCells", &cc_hec_DetCells, &b_cc_hec_DetCells);
            fChain->SetBranchAddress("cc_hec_IdCells", &cc_hec_IdCells, &b_cc_hec_IdCells);
            fChain->SetBranchAddress("cc_hec_Sigma", &cc_hec_Sigma, &b_cc_hec_Sigma);
            fChain->SetBranchAddress("cc_fcal_n", &cc_fcal_n, &b_cc_fcal_n);
            fChain->SetBranchAddress("cc_fcal_eta", &cc_fcal_eta, &b_cc_fcal_eta);
            fChain->SetBranchAddress("cc_fcal_phi", &cc_fcal_phi, &b_cc_fcal_phi);
            fChain->SetBranchAddress("cc_fcal_DetCells", &cc_fcal_DetCells, &b_cc_fcal_DetCells);
            fChain->SetBranchAddress("cc_fcal_IdCells", &cc_fcal_IdCells, &b_cc_fcal_IdCells);
            fChain->SetBranchAddress("cc_fcal_Sigma", &cc_fcal_Sigma, &b_cc_fcal_Sigma);
        }

        ///
        /// Return channelId vs noise map
        /// @param noiseMap channelId vs noise map to be filled
        ///
        void setNoiseMap(std::unordered_map<unsigned int, float> &noiseMap);

      private:
        Int_t cc_em_n;
        std::vector<float> *cc_em_eta;
        std::vector<float> *cc_em_phi;
        std::vector<int> *cc_em_DetCells;
        std::vector<unsigned int> *cc_em_IdCells;
        std::vector<float> *cc_em_Sigma;
        Int_t cc_hec_n;
        std::vector<float> *cc_hec_eta;
        std::vector<float> *cc_hec_phi;
        std::vector<int> *cc_hec_DetCells;
        std::vector<unsigned int> *cc_hec_IdCells;
        std::vector<float> *cc_hec_Sigma;
        Int_t cc_fcal_n;
        std::vector<float> *cc_fcal_eta;
        std::vector<float> *cc_fcal_phi;
        std::vector<int> *cc_fcal_DetCells;
        std::vector<unsigned int> *cc_fcal_IdCells;
        std::vector<float> *cc_fcal_Sigma;

        TBranch *b_cc_em_n;
        TBranch *b_cc_em_eta;
        TBranch *b_cc_em_phi;
        TBranch *b_cc_em_DetCells;
        TBranch *b_cc_em_IdCells;
        TBranch *b_cc_em_Sigma;
        TBranch *b_cc_hec_n;
        TBranch *b_cc_hec_eta;
        TBranch *b_cc_hec_phi;
        TBranch *b_cc_hec_DetCells;
        TBranch *b_cc_hec_IdCells;
        TBranch *b_cc_hec_Sigma;
        TBranch *b_cc_fcal_n;
        TBranch *b_cc_fcal_eta;
        TBranch *b_cc_fcal_phi;
        TBranch *b_cc_fcal_DetCells;
        TBranch *b_cc_fcal_IdCells;
        TBranch *b_cc_fcal_Sigma;
    };
} // namespace CellPropertyGenerator
/// @}
