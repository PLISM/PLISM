/// @addtogroup CellPropertyGenerator
/// @{

#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>
#include "Message.h"

namespace CellPropertyGenerator
{
    /// Class of pulse pattern configuration for a calibration run
    class PulsePattern
    {
      public:
        int NDACs;                                         ///< Number of DAC patterns
        std::vector<int> DACs;                             ///< Vector of input DAC values
        int Ndelays;                                       ///< Number of delay patterns
        std::vector<int> delays;                           ///< Vector of input delays
        int NCLsets;                                       ///< Number of calibration line patterns
        std::vector<std::unordered_set<int>> CLsets;       ///< Vector of pulse injection target calibration lines
        std::set<int> notPulsedCLs;                        ///< Set of calibration lines not targeted
        std::set<int> CLset_fromDB;                        ///< Set of calibration lines for the region specified by LAr ID translator
        std::map<int, std::vector<int>> cellID_CLs_fromDB; ///< (LAr cell online ID) vs (vector of calibration lines) for the region specified by LAr ID translator
        std::set<int> outofRegionCLs;                      ///< Set of calibration lines out of the region specified by LAr ID translator
        std::set<int> notPulsedInsideCLs;                  ///< Set of calibration lines which are not pulsed in the region specified by LAr ID translator
        std::set<int> notPulsedInsideCellIDs;              ///< Set of cells without any pulsed calibration lines in the region specified by LAr ID translator

        /**
         * @brief Construct a new PulsePattern object
         * @param patternFileName Input pulse pattern file name used for configuration of calibration run (parameters.dat)
         */
        PulsePattern(const std::string& patternFileName) :
            m_patternFileName(patternFileName)
        {
            Initialize();
        }

        /**
         * @brief Construct a new PulsePattern object
         * @param patternFileName Input pulse pattern file name used for configuration of calibration run (parameters.dat)
         * @param ID_SCID_CL_fileName Input (LArcellID, SCID, CL) list txt file name
         */
        PulsePattern(const std::string& patternFileName,
                     const std::string& ID_SCID_CL_fileName) :
            m_patternFileName(patternFileName),
            m_ID_SCID_CL_fileName(ID_SCID_CL_fileName)
        {
            Initialize();
            setCellInfo();
        }

        /// Print the analysis result of input pulse patter file
        void OutputPattern();

      private:
        std::unordered_set<int> getPulsedCL(std::array<uint32_t, 4>& a);
        inline void deleteSpace(std::string& buf)
        {
            size_t pos;
            while ((pos = buf.find_first_of(" 　\t")) != std::string::npos)
                buf.erase(pos, 1);
        }
        void Initialize();
        void setCellInfo();

        std::string m_patternFileName;
        std::string m_ID_SCID_CL_fileName;
    };
} // namespace CellPropertyGenerator
  /// @}