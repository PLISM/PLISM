#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "Digitizer.h"

using namespace SequenceGenerator;

PYBIND11_MODULE(DigitizerModule, m)
{
    pybind11::class_<Digitizer>(m, "Digitizer")
      .def_readwrite("CellPropertyFileName", &Digitizer::CellPropertyFileName)
      .def_readwrite("Interpolator", &Digitizer::Interpolator)
      .def_readwrite("NBC", &Digitizer::NBC)
      .def_readwrite("mu", &Digitizer::mu)
      .def_readwrite("phase", &Digitizer::phase)
      .def_readwrite("TrainPattern", &Digitizer::TrainPattern)
      .def_readwrite("SignalPattern", &Digitizer::SignalPattern)
      .def_readwrite("OutputFileName", &Digitizer::OutputFileName)
      .def_readwrite("SignalHitSummaryFile", &Digitizer::SignalHitSummaryFile)
      .def_readwrite("LowPtPileupHitSummaryFile", &Digitizer::LowPtPileupHitSummaryFile)
      .def_readwrite("HighPtPileupHitSummaryFile", &Digitizer::HighPtPileupHitSummaryFile)
      .def_readwrite("seed", &Digitizer::seed)
      .def_readwrite("OutputBranch_SignalTrueEt", &Digitizer::OutputBranch_SignalTrueEt)
      .def_readwrite("OutputBranch_SignalTrueTau", &Digitizer::OutputBranch_SignalTrueTau)
      .def_readwrite("OutputBranch_PileupTrueEt", &Digitizer::OutputBranch_PileupTrueEt)
      .def_readwrite("OutputBranch_PileupTrueTau", &Digitizer::OutputBranch_PileupTrueTau)
      .def_readwrite("OutputBranch_PileupTrueEtSum", &Digitizer::OutputBranch_PileupTrueEtSum)
      .def_readwrite("OutputBranch_AnalogEt", &Digitizer::OutputBranch_AnalogEt)
      .def_readwrite("OutputBranch_SignalAnalogEt", &Digitizer::OutputBranch_SignalAnalogEt)
      .def_readwrite("OutputBranch_BgAnalogEt", &Digitizer::OutputBranch_BgAnalogEt)
      .def_readwrite("OutputBranch_PileupAnalogEt", &Digitizer::OutputBranch_PileupAnalogEt)
      .def_readwrite("OutputBranch_ElecNoiseAnalogEt", &Digitizer::OutputBranch_ElecNoiseAnalogEt)
      .def_readwrite("OutputBranch_Digit", &Digitizer::OutputBranch_Digit)
      .def(pybind11::init<>())
      .def("sequence", &Digitizer::sequence);
}
