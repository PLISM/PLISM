#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "OFCCaliTree.h"
#include "OFCPhysTree.h"
#include "OFCCalibrator.h"

using namespace SequenceGenerator;

PYBIND11_MODULE(OFCCalibratorModule, m)
{
    pybind11::class_<OFCCalibrator>(m, "OFCCalibrator")
      .def_readwrite("CellPropertyFileName", &OFCCalibrator::CellPropertyFileName)
      .def_readwrite("Interpolator", &OFCCalibrator::Interpolator)
      .def_readwrite("NBC", &OFCCalibrator::NBC)
      .def_readwrite("mu", &OFCCalibrator::mu)
      .def_readwrite("phase", &OFCCalibrator::phase)
      .def_readwrite("TrainPattern", &OFCCalibrator::TrainPattern)
      .def_readwrite("OutputFileName", &OFCCalibrator::OutputFileName)
      .def_readwrite("LowPtPileupHitSummaryFile", &OFCCalibrator::LowPtPileupHitSummaryFile)
      .def_readwrite("HighPtPileupHitSummaryFile", &OFCCalibrator::HighPtPileupHitSummaryFile)
      .def_readwrite("seed", &OFCCalibrator::seed)
      .def_readwrite("BCID0thSamp", &OFCCalibrator::BCID0thSamp)
      .def(pybind11::init<>())
      .def("calibrate", &OFCCalibrator::calibrate);

    pybind11::class_<OFCCaliTree>(m, "OFCCaliTree")
      .def_readwrite("NthPhase", &OFCCaliTree::NthPhase)
      .def_readwrite("OFBCshift", &OFCCaliTree::OFBCshift)
      .def_readwrite("OutputFileName", &OFCCaliTree::OutputFileName)
      .def_readwrite("CellPropertyFileName", &OFCCaliTree::CellPropertyFileName)
      .def(pybind11::init<const std::string&>())
      .def("MakeTree", &OFCCaliTree::MakeTree)
      .def("MakegFuncOFCTree", &OFCCaliTree::MakegFuncOFCTree);
    
    pybind11::class_<OFCPhysTree>(m, "OFCPhysTree")
      .def_readwrite("NthPhase", &OFCPhysTree::NthPhase)
      .def_readwrite("OFBCshift", &OFCPhysTree::OFBCshift)
      .def_readwrite("OutputFileName", &OFCPhysTree::OutputFileName)
      .def_readwrite("CellPropertyFileName", &OFCPhysTree::CellPropertyFileName)
      .def(pybind11::init<const std::string&>())
      .def("MakeTree", &OFCPhysTree::MakeTree);
}
