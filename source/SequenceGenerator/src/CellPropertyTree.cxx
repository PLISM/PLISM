#include "CellPropertyTree.h"
#include <TCanvas.h>
#include <TGraph.h>
#include <boost/math/interpolators/barycentric_rational.hpp>

using namespace SequenceGenerator;

void CellPropertyTree::setMaps(const std::string& interpolator)
{
    if (m_if_maps_set) Message::Fatal("CellPropertyMaps have already been set");

    enum class E_interpolator {
        linear,
        barycentric_rational
    };
    E_interpolator itpl;
    if (interpolator == "linear")
        itpl = E_interpolator::linear;
    else if (interpolator == "barycentric_rational")
        itpl = E_interpolator::barycentric_rational;
    else
        Message::Fatal((boost::format("Invalid interpolator specification \"%1%\"") % interpolator).str());

    Restart();
    while (Next()) {
        CellConstantMapVar[*channelId] = {*eta, *phi, *layer, *detector, *LSB, *pedADC, *noise, *BCshift};
        const auto nphases{*sampperbc};
        const auto nDACs{inputET->size()};
        if (gFunction->size() not_eq nDACs) Message::Fatal("The size of inputET and gFunction are not the same");
        const int length = gFunction->at(0).size();
        if (nDACs <= 0) Message::Fatal("nDACs <= 0. Input CellPropertyTree is bad.");
        if (nDACs > 1) {
            std::vector<std::function<double(double)>> adcs(length);
            if (itpl == E_interpolator::linear) {
                for (int i{}; i < length; ++i) { // for each point in smooth pulse shape
                    std::map<double, double> m;
                    for (std::size_t j{}; j < nDACs; ++j) // fill ramp points
                        m[inputET->at(j)] = gFunction->at(j)[i];
                    adcs[i] = [m](double e) -> double { // define ramp function
                        auto it{m.upper_bound(e)};
                        if (it == m.begin()) // lower than the first ramp point
                            return e * it->second / it->first;
                        else if (it == m.end()) // higher than the last ramp point
                            return (--it)->second;
                        const auto xh{it->first};
                        const auto yh{it->second};
                        --it;
                        const auto xl{it->first};
                        const auto yl{it->second};
                        return yl + (e - xl) * (yh - yl) / (xh - xl);
                    };
                }
            } else if (itpl == E_interpolator::barycentric_rational) {
                for (int i{}; i < length; ++i) { // for each point in smooth pulse shape
                    std::vector<double> x(inputET->begin(), inputET->end());
                    std::vector<double> y;
                    y.reserve(nDACs);
                    for (std::size_t j{}; j < nDACs; ++j) // fill ramp points
                        y.push_back(gFunction->at(j)[i]);
                    const auto xmin{x.front()};
                    const auto xmax{x.back()};
                    const auto ymini{y.front()};
                    const auto ymaxi{y.back()};

                    boost::math::barycentric_rational<double> interpolant(std::move(x), std::move(y));
                    adcs[i] = [i, xmin, xmax, ymini, ymaxi, interpolant](double e) -> double { // define ramp function
                        if (e <= xmin)
                            return e * ymini / xmin;
                        else if (e >= xmax)
                            return ymaxi;
                        else
                            return interpolant(e);
                    };
                }
            } else
                Message::Fatal("Invalid interpolator specification");

            { // output ramp file for pulse peak
                const auto dump_file_name{ReplaceFilename((boost::format("ramp_%1%.txt") % *channelId).str())};
                const auto fg{FileGenerator(dump_file_name)};
                std::ofstream ofs(dump_file_name);
                const int imax = std::distance(gFunction->begin()->begin(), std::max_element(gFunction->begin()->begin(), gFunction->begin()->end()));
                for (std::size_t j{}; j < nDACs; ++j) // fill ramp points
                    ofs << inputET->at(j) << " " << gFunction->at(j)[imax] << "\n";
                ofs << "\n";
                const double bin = inputET->back() * 1.05 / 200.;
                for (double j{}; j < 200; ++j) // fill smooth points
                    ofs << bin * j << " " << (adcs[imax])(bin * j) << "\n";
            }

            ET2SampledPulseFuncV pulse_tmp(nphases);
            for (int j{}; j < nphases; ++j) {
                pulse_tmp[j] = [adcs, nphases, length, j](double e) -> std::deque<double> {
                    std::deque<double> v;
                    for (int i{j}; i < length; i += nphases) v.push_back((adcs[i])(e));
                    return v;
                };
            }
            GDataMapVar[*channelId] = {std::move(*inputET), std::move(*gFunction), nphases, std::move(pulse_tmp)};

            /*
            TGraph* g = new TGraph();
            auto plot = [this, g, nphases, length](double e) {
            const auto& vec {GDataMapVar[*channelId].pulse};
            for (int j{}; j < nphases; ++j) {
            const auto& deq {(vec[j])(e)};
            for (int i{j}, k{}; i < length; i += nphases, ++k)
            g->SetPoint(g->GetN(), i * 25./24., deq[k]);
            }
            };
            for (const auto& it : GDataMapVar[*channelId].ET) plot(it);
            const auto size {GDataMapVar[*channelId].ET.size()};
            plot(GDataMapVar[*channelId].ET[0]/2);
            for (std::size_t i{}; i < size - 1; ++i) {
            const auto& v {GDataMapVar[*channelId].ET};
            plot((v[i] + v[i+1]) / 2.);
            }
            TCanvas* c = new TCanvas();
            g->Draw("ap");
            c->SaveAs("satValid.pdf");
            */

        } else {
            const double inputET_tmp{inputET->at(0)};
            std::deque<double> gFunction_tmp = gFunction->at(0);
            ET2SampledPulseFuncV pulse_tmp(nphases);
            for (int j{}; j < nphases; ++j) {
                pulse_tmp[j] = [inputET_tmp, gFunction_tmp, nphases, j, length](double e) -> std::deque<double> {
                    std::deque<double> v;
                    for (int i{j}; i < length; i += nphases)
                        v.push_back(gFunction_tmp[i] * e / inputET_tmp);
                    return v;
                };
            }
            GDataMapVar[*channelId] = {std::move(*inputET), std::move(*gFunction), nphases, std::move(pulse_tmp)};
        }
    }
    m_if_maps_set = true;
}
