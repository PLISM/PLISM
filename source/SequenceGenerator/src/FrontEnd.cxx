#include "FrontEnd.h"
#include <boost/progress.hpp>

using namespace SequenceGenerator;

bool FrontEnd::badStructure(const std::vector<int>& structure)
{
    const int size{static_cast<int>(structure.size())};
    if (not size) return false;
    if (size % 2) return true;
    bool bad{true};
    for (const auto& it : structure) {
        if (it < 0) return true;
        if (it) bad = false;
    }
    return bad;
}

void FrontEnd::setCellPropertyMaps()
{
    CellPropertyTree cp(CellPropertyFileName);
    cp.setMaps(Interpolator);
    m_CellConstantMap = std::move(cp.CellConstantMapVar);
    m_GDataMap        = std::move(cp.GDataMapVar);
}

std::pair<std::vector<unsigned int>, std::vector<unsigned int>>
FrontEnd::getPileupNInteraction(const double fraction)
{
    std::vector<unsigned int> highPtNIntraction(NBC), lowPtNIntraction(NBC);
    int BC{0};
    while (BC < NBC) {
        bool bEmpty{false};
        for (const auto& nBCinTrain : TrainPattern) {
            if (bEmpty) {
                BC += nBCinTrain;
            } else {
                for (int j{}; BC < NBC and j < nBCinTrain; ++BC, ++j) {
                    auto NInt{static_cast<unsigned int>(gRandom->Poisson(mu))};
                    highPtNIntraction[BC] = gRandom->Binomial(NInt, fraction);
                    lowPtNIntraction[BC]  = NInt - highPtNIntraction[BC];
                }
            }
            bEmpty = not bEmpty;
        }
    }
    return std::make_pair(highPtNIntraction, lowPtNIntraction);
}

void FrontEnd::displayCount(const int nthBC)
{
    auto message = [nthBC] { Message::Info((boost::format("%1%th BC") % nthBC).str()); };
    if (nthBC >= 1000000) {
        if (not(nthBC % 1000000)) message();
    } else if (nthBC >= 100000) {
        if (not(nthBC % 100000)) message();
    } else if (nthBC >= 10000) {
        if (not(nthBC % 10000)) message();
    } else if (nthBC >= 1000) {
        if (not(nthBC % 1000)) message();
    } else if (nthBC >= 100) {
        if (not(nthBC % 100)) message();
    } else if (nthBC >= 10) {
        if (not(nthBC % 10)) message();
    } else
        message();
}

SampledPulse FrontEnd::getSampledEventPulse(
  const std::vector<float>& vET, const std::vector<float>& vDel, const CellPropertyTree::GData& GData, const int sampperbc, const int nthSampUsed)
{
    SampledPulse sampledEventPulse;
    auto itET{std::begin(vET)};
    auto itDel{std::begin(vDel)};
    auto itET_end{vET.cend()};

    for (; itET not_eq itET_end; ++itET, ++itDel) { // intime hit loop
        SampledPulse sampledIntimePulse(GData.pulse, *itET, *itDel, sampperbc, nthSampUsed);
        sampledEventPulse += std::move(sampledIntimePulse);
    }

    return sampledEventPulse;
}

FrontEnd::SequenceDataMap FrontEnd::getSequenceMap(
  HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
  std::vector<unsigned int> crossingNumbers)
{
    SequenceDataMap outputMap;
    if (phase < 0 or phase >= 25.) Message::Fatal((boost::format("phase is set as %1%. phase must be non-negative and less than 25.") % phase).str());
    std::vector<std::vector<int>>
      pileupEvents; // BC<event<hit(index of entry in hitsummary)>>
    auto setPileupEvents = [this, &pileupEvents, &crossingNumbers] {
        pileupEvents.reserve(NBC);
        Message::Info((boost::format("m_nEventsInHitSummary = %1%") % phase).str());
        for (const auto& num : crossingNumbers) {
            std::vector<int> v;
            v.reserve(num);
            for (unsigned int j = 0; j < num; ++j)
                v.push_back(
                  std::round(gRandom->Uniform(0, m_nEventsInHitSummary - 1)));
            pileupEvents.push_back(std::move(v));
        }
    };

    auto addSequenceDataToMap = [this, &pileupEvents, &outputMap](
                                  const CellPropertyTree::GData& GData,
                                  const int sampperbc,
                                  const int nthSampUsed) {
        SampledPulse sampledTmpSequence;
        SampledPulse sampledRawSequence;
        int nthBC{};

        if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum)
            ETOut[channelId].reserve(NBC);
        if (OutputBranch_PileupTrueTau) delayOut[channelId].reserve(NBC);

        boost::progress_display show_progress(pileupEvents.size());
        for (auto&& BC : pileupEvents) { // BC loop
            std::vector<double> hitETs;
            std::vector<double> hittaus;

            if (not BC.size()) {
                if (sampledTmpSequence.pulse.size())
                    sampledTmpSequence.MoveHeadElement(sampledRawSequence);
                else
                    sampledRawSequence.pulse.push_back(0);
                if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum)
                    ETOut[channelId].push_back(hitETs);
                if (OutputBranch_PileupTrueTau)
                    delayOut[channelId].push_back(hittaus);
                ++show_progress;
                continue;
            }

            SampledPulse sampledBCPulse;
            for (auto&& event : BC) { // event loop
                const auto& vET{
                  ET->at(event)}; // intime energies in event in BC
                const auto& vDel{
                  delay->at(event)}; // intime delays in event in BC
                if (not vET.size()) continue;
                if (vET.size() not_eq vDel.size())
                    Message::Fatal("The size of energy and delay in event is not consistent! There is something wrong with HitSummary file.");
                if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum) {
                    hitETs.reserve(hitETs.size() + vET.size());
                    std::copy(vET.begin(), vET.end(), std::back_inserter(hitETs));
                }
                if (OutputBranch_PileupTrueTau) {
                    hittaus.reserve(hittaus.size() + vDel.size());
                    std::copy(vDel.begin(), vDel.end(), std::back_inserter(hittaus));
                }

                SampledPulse sampledEventPulse{getSampledEventPulse(
                  vET, vDel, GData, sampperbc, nthSampUsed)};
                if (sampledEventPulse.pulse.size())
                    sampledBCPulse += sampledEventPulse;

            } // event loop
            if (sampledBCPulse.pulse.size())
                sampledTmpSequence += sampledBCPulse;
            if (sampledTmpSequence.pulse.size())
                sampledTmpSequence.MoveHeadElement(sampledRawSequence);
            else
                sampledRawSequence.pulse.push_back(0);
            if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum)
                ETOut[channelId].push_back(hitETs);
            if (OutputBranch_PileupTrueTau)
                delayOut[channelId].push_back(hittaus);
            ++show_progress;
        } // BC loop

        outputMap[channelId] = {std::move(sampledRawSequence)};
    };

    setMap(HitSummaryDataMap, setPileupEvents, addSequenceDataToMap);
    return outputMap;
}

FrontEnd::SequencesDataMap FrontEnd::getSequencesMap(
  HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
  std::vector<bool> crossingPattern)
{
    SequencesDataMap outputMap;
    if (phase < 0 or phase >= 25.)
        Message::Fatal((boost::format("phase is set as %1%. phase must be non-negative and less than 25.") % phase).str());
    std::vector<int> signalEvents; // BC<event>
    auto setSignalEvents = [this, &signalEvents, &crossingPattern] {
        signalEvents.reserve(NBC);
        for (const auto cross : crossingPattern) {
            if (cross)
                signalEvents.push_back(
                  std::round(gRandom->Uniform(0, m_nEventsInHitSummary - 1)));
            else
                signalEvents.push_back(-1);
        }
    };

    auto addSequencesDataToMap = [this, &signalEvents, &outputMap](
                                   const CellPropertyTree::GData& GData,
                                   const int sampperbc,
                                   const int nthSampUsed) {
        SampledPulse sampledTmpSequence;
        SampledPulse sampledRawSequence;
        std::vector<double> SignalTrueEt;
        std::vector<double> SignalTrueTau;
        int nthBC{};

        boost::progress_display show_progress(signalEvents.size());
        for (auto&& event : signalEvents) { // BC loop
            if (event < 0) {
                if (sampledTmpSequence.pulse.size())
                    sampledTmpSequence.MoveHeadElement(sampledRawSequence);
                else
                    sampledRawSequence.pulse.push_back(0);
                SignalTrueEt.push_back(0.);
                SignalTrueTau.push_back(0.);
                ++show_progress;
                continue;
            }

            const auto& vET{ET->at(event)};     // intime energies in event
            const auto& vDel{delay->at(event)}; // intime delays in event
            if (not vET.size()) continue;
            if (vDel.size() > 1)
                Message::Warning("There is intime pileup in a cell in the signal HitSummary. All energies are simply summed to compute true Et for a bunch even if times (tau) are not the same.");
            if (vET.size() not_eq vDel.size())
                throw "The size of energy and delay in event is not consistent! There's something wrong with HitSummary file.";

            SampledPulse sampledEventPulse{getSampledEventPulse(vET, vDel, GData, sampperbc, nthSampUsed)};

            if (sampledEventPulse.pulse.size()) sampledTmpSequence += sampledEventPulse;
            if (sampledTmpSequence.pulse.size())
                sampledTmpSequence.MoveHeadElement(sampledRawSequence);
            else
                sampledRawSequence.pulse.push_back(0);
            SignalTrueEt.push_back(std::accumulate(vET.begin(), vET.end(), 0.));
            SignalTrueTau.push_back(std::accumulate(vDel.begin(), vDel.end(), 0.) / vDel.size());
            ++show_progress;
        } // BC loop

        outputMap[channelId] = {std::move(SignalTrueEt),
                                std::move(SignalTrueTau),
                                std::move(sampledRawSequence)};
    };

    setMap(HitSummaryDataMap, setSignalEvents, addSequencesDataToMap);
    return outputMap;
}

void FrontEnd::setMap(
  HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
  std::function<void()> setEvents,
  std::function<void(const CellPropertyTree::GData&, const int, const int)>
    addPairToMap)
{
    if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum)
        ETOut.clear();
    if (OutputBranch_PileupTrueTau) delayOut.clear();
    try {
        gInterpreter->GenerateDictionary("std::vector<std::deque<double> >",
                                         "vector;deque");
        bool bMadeCrossingPattern{};

        for (auto&& channelIt : HitSummaryDataMap) { // channelId loop
            channelId = channelIt.first;
            ET        = &channelIt.second.ET;
            delay     = &channelIt.second.delay;

            if (not bMadeCrossingPattern) {
                m_nEventsInHitSummary = ET->size();
                setEvents();
                bMadeCrossingPattern = true;
            } else if (static_cast<int>(ET->size()) not_eq m_nEventsInHitSummary)
                throw "Number of events in the SC is different from others!";
            if (static_cast<int>(delay->size()) not_eq m_nEventsInHitSummary)
                throw "The size of E and delay of input HitSummary is not consistent! There's something wrong with HitSummary file.";

            auto isc{m_GDataMap.find(channelId)};
            if (isc == m_GDataMap.end()) {
                Message::Warning((boost::format("Could not find GData for channelId %1% -> skip this cell") % channelId).str());
                continue;
            } else {
                Message::Info((boost::format("channelId %1%") % channelId).str());
            }
            const auto& GData{isc->second};

            // Apply phase information
            int nthSampUsed{-1};
            const int sampperbc{GData.sampperbc};
            const double f{sampperbc * phase};
            for (int i{}; i < sampperbc; ++i) {
                if (f >= i * 25. and f < (i + 1) * 25.) {
                    nthSampUsed = i;
                    break;
                }
            }
            if (nthSampUsed < 0 or nthSampUsed >= sampperbc)
                throw "Failed to determine nthSampUsed.";

            addPairToMap(GData, sampperbc, nthSampUsed);
            // m_GDataMap.erase(isc);
        } // channelId loop
    } catch (const std::string& str) {
        Message::Fatal(str);
    }
}
