#include "SampledPulse.h"
#include <math.h>
#include <iostream>

using namespace SequenceGenerator;

SampledPulse::SampledPulse() {}

SampledPulse::SampledPulse(const CellPropertyTree::ET2SampledPulseFuncV& funcs, const float Et, float delay, const int sampperbc, const int nthSampUsed)
{
    int delayInPhase;
    auto digitizeDelay = [&] {
        delayInPhase = static_cast<int>(round(delay * sampperbc / 25.));
    };
    if (delay < 0.) {
        delay = -delay;
        digitizeDelay();
        int i{delayInPhase + nthSampUsed};
        std::deque<double> pulse_tmp;
        if (i <= sampperbc) {
            pulse_tmp = (funcs[i])(Et);
        } // this code does not assume else
        pulse = std::move(pulse_tmp);
    } else {
        digitizeDelay();
        const int remainder      = delayInPhase % sampperbc;
        const int diff           = nthSampUsed - remainder;
        const int nthSampUsedNew = (diff < 0) ? (sampperbc - (-diff % sampperbc)) : (diff % sampperbc);
        std::deque<double> pulse_tmp{(funcs[nthSampUsedNew])(Et)};
        const int n0 = delayInPhase / sampperbc + (diff < 0);
        for (int i{}; i < n0; ++i) pulse_tmp.push_front(0);
        pulse = std::move(pulse_tmp);
    }
}

SampledPulse::SampledPulse(const std::deque<double>& g, const float Et, float delay, const int sampperbc, const int nthSampUsed)
{
    if (sampperbc <= 0) Message::Fatal("sampperbc <= 0");
    const int originalGsize = g.size();
    int delayInPhase;
    auto digitizeDelay = [&] {
        delayInPhase = static_cast<int>(round(delay * sampperbc / 25.));
    };
    if (delay < 0.) {
        delay = -delay;
        digitizeDelay();
        std::deque<double> pulse_tmp;
        int i{delayInPhase + nthSampUsed};
        if (i <= sampperbc) {
            for (int i{delayInPhase + nthSampUsed}; i < originalGsize; i += sampperbc)
                pulse_tmp.push_back(g[i] * Et);
        } // this code does not assume else
        pulse = std::move(pulse_tmp);
    } else {
        digitizeDelay();
        const int remainder      = delayInPhase % sampperbc;
        const int diff           = nthSampUsed - remainder;
        const int nthSampUsedNew = (diff < 0) ? (sampperbc - (-diff % sampperbc)) : (diff % sampperbc);
        std::deque<double> pulse_tmp(delayInPhase / sampperbc + (diff < 0));
        for (int i = nthSampUsedNew; i < originalGsize; i += sampperbc)
            pulse_tmp.push_back(g[i] * Et);
        pulse = std::move(pulse_tmp);
    }
}

SampledPulse SampledPulse::operator+=(SampledPulse theOtherPulse)
{
    int i = 0;
    if (pulse.size() < theOtherPulse.pulse.size()) {
        for (auto&& itr : pulse) theOtherPulse.pulse[i++] += itr;
        *this = std::move(theOtherPulse);
    } else {
        for (auto&& itr : theOtherPulse.pulse) pulse[i++] += itr;
    }
    return *this;
}

SampledPulse SampledPulse::operator+=(const std::deque<double>& theOtherPulse)
{
    if (pulse.size() < theOtherPulse.size()) {
        std::deque<double> pulse2 = theOtherPulse;
        auto it2                  = std::begin(pulse2);
        for (auto&& it1 : pulse) {
            *it2 += it1;
            ++it2;
        }
        pulse = std::move(pulse2);
    } else {
        auto it1 = std::begin(pulse);
        for (auto&& it2 : theOtherPulse) {
            *it1 += it2;
            ++it1;
        }
    }
    return *this;
}

void SampledPulse::MoveHeadElement(SampledPulse& theOtherPulse)
{
    theOtherPulse.pulse.push_back(pulse[0]);
    pulse.pop_front();
}

std::deque<int> SampledPulse::digitize()
{
    std::deque<int> newPulse;
    for (auto&& it : pulse)
        newPulse.push_back(static_cast<int>(round(it)));
    return newPulse;
}
