/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <deque>
#include "FrontEnd.h"
#include "HitSummaryTree.h"
#include "Message.h"
#include "SampledPulse.h"

namespace SequenceGenerator
{
    /// Class to create digit sequence tree
    class Digitizer : public FrontEnd
    {
      public:
        /**
         * @brief Construct a new Digitizer object
         */
        Digitizer() :
            FrontEnd() {}

        void sequence(); ///< Generate digit sequence tree

        std::string SignalHitSummaryFile{};        ///< Signal hit summary file name
        bool OutputBranch_SignalTrueEt{true};      ///< Whether to fill SignalTrueEt branch
        bool OutputBranch_SignalTrueTau{true};     ///< Whether to fill SignalTrueTau branch
        bool OutputBranch_AnalogEt{true};          ///< Whether to fill AnalogEt branch
        bool OutputBranch_SignalAnalogEt{true};    ///< Whether to fill SignalAnalogEt branch
        bool OutputBranch_BgAnalogEt{true};        ///< Whether to fill BgAnalogEt branch
        bool OutputBranch_PileupAnalogEt{true};    ///< Whether to fill PileupAnalogEt branch
        bool OutputBranch_ElecNoiseAnalogEt{true}; ///< Whether to fill ElecNoiseAnalogEt branch
        bool OutputBranch_Digit{true};             ///< Whether to fill Digit branch

      protected:
        virtual void memberCheck() override;

      private:
        bool m_withSignal{false};
        bool m_withPileup{false};
        std::vector<bool> getSignalInteractionPattern();
    };
} // namespace SequenceGenerator
  /// @}
