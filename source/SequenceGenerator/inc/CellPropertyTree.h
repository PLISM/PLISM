/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <deque>
#include <functional>
#include "CellPropertyTreeBase.h"

namespace SequenceGenerator
{
    /// Class to extract information from cell property tree
    class CellPropertyTree : public CellPropertyTreeBase
    {
      public:
        /**
         * @brief Vector of Et->pulse(sampled) function
         *
         * Each element of the vector corresponds to one sampling phase
         */
        typedef std::vector<std::function<std::deque<double>(double)>> ET2SampledPulseFuncV;

        /// Structure containing pulse shape information
        struct GData {
            std::vector<float> ET; ///< Input Et[event] in MeVT following order in cell property tree
            /**
             * @brief g function g[nthET][nthSample]
             *
             * nthET and nthSample follow order in cell property tree
             */
            std::vector<std::deque<double>> g;
            int sampperbc;              ///< Number of samples per BC
            ET2SampledPulseFuncV pulse; ///< @ref ET2SampledPulseFuncV object
        };
      
        /**
         * @brief Structure containing cell constant information
         *
         * One-to-one correspondence with cells
         */
        struct CellConstant {
            Float_t eta, phi;
            Int_t layer;
            Int_t detector;
            float LSB; ///< ET/ADC in MeVT
            double pedADC;
            float noise;
            int BCshift; ///< Number of BCs cut at the begining of pulse shape when making the cell property tree
        };
        typedef std::unordered_map<Int_t, GData> GDataMap;
        typedef std::unordered_map<Int_t, CellConstant> CellConstantMap;

        /**
         * @brief Construct a new CellPropertyTree object
         *
         * @param inputFileName Input cell property file name
         */
        CellPropertyTree(const std::string& inputFileName) :
            CellPropertyTreeBase(inputFileName)
        {
        }

        /**
		 * @brief Set maps that have information of cell property tree
		 * 
		 * @param interpolator Interpolator of ADC as a function of input Et for each sample and sampling phase in a pulse. "linear" or "barycentric_rational" are available.
		 */
        void setMaps(const std::string& interpolator);
        inline void setMaps() { setMaps("linear"); }

        CellConstantMap CellConstantMapVar; ///< channelId vs @ref CellConstant map
        GDataMap GDataMapVar;               ///< channelId vs @ref GData map
        bool m_if_maps_set{false};          ///< Whether @ref setMaps function has already been executed
    };
} // namespace SequenceGenerator
  /// @}
