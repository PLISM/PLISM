/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include "CellPropertyTree.h"
#include "TreeReader.h"

namespace SequenceGenerator
{
    /// Class to convert P1CalibrationProcessing OFC tree to PLISM OFC tree
    class OFCPhysTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new OFCPhysTree object
         *
         * @param InputFileName Input P1CalibrationProcessing OFC tree
         */
        OFCPhysTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "OFC_1ns_mu"),
            channelId(m_reader, "channelId"),
            Phase(m_reader, "Phase"),
            nSamples(m_reader, "nSamples"),
            OFCa(m_reader, "OFCa"),
            OFCb(m_reader, "OFCb") {}

        int NthPhase{-1};                 ///< Sampling phase
        int OFBCshift{-1};                ///< Which BC (starting from 0) to start from and perform optimal filtering
        std::string OutputFileName;       ///< Name of PLISM OFC file to be output
        std::string CellPropertyFileName; ///< Input cell property file name
        void MakeTree();                  ///< Generate PLISM OFC file
        //void MakegFuncOFCTree();          ///< Generate analysis result of optimal filtering for ideal pulse shape as tree

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<Int_t> Phase;
        TTreeReaderValue<Int_t> nSamples;
        TTreeReaderArray<Float_t> OFCa;
        TTreeReaderArray<Float_t> OFCb;

        void CheckMembers();
    };
} // namespace SequenceGenerator
  /// @}
