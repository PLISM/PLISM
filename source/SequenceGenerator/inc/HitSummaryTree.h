/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <TRandom.h>
#include "TreeReader.h"

namespace SequenceGenerator
{
    /// Class to extract infomation from hit summary tree
    class HitSummaryTree : public TreeReader
    {
      public:
        /// Structure containing Et and delay
        struct HitSummaryData {
            std::vector<std::vector<float>> ET;    ///< Et[event][hit]
            std::vector<std::vector<float>> delay; ///< delay[event][hit]
        };
        /// channelId vs @ref HitSummaryData
        typedef std::unordered_map<unsigned int, HitSummaryData> HitSummaryDataMap;

        /**
         * @brief Construct a new HitSummaryTree object
         *
         * @param inputFileName Input hit summary file name
         */
        HitSummaryTree(const std::string& inputFileName) :
            TreeReader(inputFileName, "HitSummary"),
            channelId(m_reader, "channelId"),
            ET(m_reader, "ET"),
            delay(m_reader, "delay")
        {
            Message::Info("HitSummary: " + inputFileName);
            setMap();
        }
        HitSummaryDataMap HitSummaryDataMapVar; ///< channelId vs @ref HitSummaryData

      private:
        void setMap();

        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<std::vector<std::vector<float>>> ET;
        TTreeReaderValue<std::vector<std::vector<float>>> delay;
    };
} // namespace SequenceGenerator
  /// @}
