#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class CellPropertyTreeBase;
#    pragma link C++ class SequenceGenerator::ContinuousSinglePulse;
#    pragma link C++ class SequenceGenerator::SampledPulse;
#    pragma link C++ class SequenceGenerator::CellPropertyTree;
#    pragma link C++ class SequenceGenerator::HitSummaryTree;
#    pragma link C++ class SequenceGenerator::FrontEnd;
#    pragma link C++ class SequenceGenerator::Digitizer;
#    pragma link C++ class SequenceGenerator::OFCCalibrator;
#    pragma link C++ class SequenceGenerator::OFCCaliTree;

#endif
