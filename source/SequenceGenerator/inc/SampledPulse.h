/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <deque>
#include "CellPropertyTree.h"

namespace SequenceGenerator
{
    /// Class of sampled pulse
    class SampledPulse
    {
      public:
        SampledPulse();
        /**
         * @brief Construct a new SampledPulse object
         *
         * @param funcs Pulse shapes as functions of input Et
         * @param Et Input Et
         * @param delay Input delay
         * @param sampperbc Number of phases per BC
         * @param nthSampUsed Sampling phase
         */
        SampledPulse(const CellPropertyTree::ET2SampledPulseFuncV& funcs, const float Et, float delay, const int sampperbc, const int nthSampUsed);

        /**
         * @brief Construct a new SampledPulse object
         *
         * @param g g Function
         * @param Et Input Et
         * @param delay Input delay
         * @param sampperbc Number of phases per BC
         * @param nthSampUsed Sampling phase
         */
        SampledPulse(const std::deque<double>& g, const float Et, const float delay, const int sampperbc, const int nthSampUsed);

        /**
         * @brief Add another @ref SampledPulse
         *
         * @param theOtherPulse @ref SampledPulse to be added
         */
        SampledPulse operator+=(SampledPulse theOtherPulse);

        /**
         * @brief Add sequence as @ref SampledPulse
         *
         * @param theOtherPulse Sequence to be added
         */
        SampledPulse operator+=(const std::deque<double>& theOtherPulse);

        /**
         * @brief Pop the 0th element and push it back to @ref SampledPulse
         *
         * @param theOtherPulse @ref SampledPulse to be incremented
         */
        void MoveHeadElement(SampledPulse& theOtherPulse);

        std::deque<int> digitize(); ///< Digitize the @ref SampledPulse
        std::deque<double> pulse;   ///< Main body of @ref SampledPulse
    };
} // namespace SequenceGenerator
  /// @}