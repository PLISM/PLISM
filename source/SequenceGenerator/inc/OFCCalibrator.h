/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <TMatrixD.h>
#include "FrontEnd.h"

namespace SequenceGenerator
{
    /// Class to calibrate OFCs and output OFC tree
    class OFCCalibrator : public FrontEnd
    {
      public:
        /**
         * @brief Construct a new OFCCalibrator object
         */
        OFCCalibrator() :
            FrontEnd() {}

        /**
         * @brief Calibrate OFCs and output OFC tree
         *
         * @param withNoise Whether to input electric or thermal noise
         * @param withPileup Whether to input pileup noise
         */
        void calibrate(bool withNoise, bool withPileup);

        int BCID0thSamp{}; ///< BCID of the 0th sample in input sampled pulse shape

      protected:
        virtual void memberCheck() override;

      private:
        void setGdG(std::vector<double>& g, std::vector<double>& dg, const std::deque<double>& pulse, const float phase, const int sampperbc);
        bool setRinv(TMatrixD& Rinv, const std::deque<int>& digits);
        bool setRinv(TMatrixD& Rinv, const std::deque<double>& digits);
        void setOFC(std::vector<double>& a, std::vector<double>& b, const std::vector<double>& g, const std::vector<double>& dg, const TMatrixD& Rinv);
    };
} // namespace SequenceGenerator
  /// @}
