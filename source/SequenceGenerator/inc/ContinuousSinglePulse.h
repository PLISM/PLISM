/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <deque>
#include <iostream>
#include <vector>
#include "Message.h"

namespace SequenceGenerator
{
    /// Class of smooth single pulse to be sampled
    class ContinuousSinglePulse
    {
      public:
        /**
         * @brief Construct a new ContinuousSinglePulse object
         *
         * @param g g function stored in cell property tree
         * @param E Input energy
         * @param delay Input delay
         * @param sampperbc Number of phases per BC
         */
        ContinuousSinglePulse(std::deque<double>& g, float E, float delay, int sampperbc);
        ContinuousSinglePulse();

        /**
         * @brief Return sampled pulse
         *
         * @param nthSampUsed Sampling phase
         */
        std::deque<double> sample(const int nthSampUsed);
        Int_t channelId;          ///< Channle online ID
        Int_t sampperbc;          ///< Number of phases per BC
        std::deque<double> pulse; ///< Smooth single pulse scaled and shifted according to input energy and delay
    };
} // namespace SequenceGenerator
  /// @}
