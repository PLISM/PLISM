/// @addtogroup SequenceGenerator
/// @{

#pragma once
#include <TChain.h>
#include <TFile.h>
#include <TInterpreter.h>
#include <TMath.h>
#include <TROOT.h>
#include <TRandom.h>
#include <deque>
#include <functional>
#include <numeric>
#include "CellPropertyTree.h"
#include "ContinuousSinglePulse.h"
#include "HitSummaryTree.h"
#include "SampledPulse.h"
#define PROP_LOW (382. / 383.)
#define PROP_HIGH (1. / 383.)

namespace SequenceGenerator
{
    /// Class to generate digit sequence
    class FrontEnd
    {
      public:
        struct SequenceData {
            SampledPulse rawSequence;
        };
        struct SequencesData {
            std::vector<double> SignalTrueEt;
            std::vector<double> SignalTrueTau;
            SampledPulse rawSequence;
        };
        /// channelId vs @ref SequenceData map
        typedef std::unordered_map<unsigned int, SequenceData> SequenceDataMap;
        /// channelId vs @ref SequencesData map
        typedef std::unordered_map<unsigned int, SequencesData> SequencesDataMap;

        /**
         * @brief Construct a new FrontEnd object
         *
         * @param mapFileName Input cell property file name
         */
        FrontEnd() {}
        virtual ~FrontEnd() {}

        std::string CellPropertyFileName;        ///< Input cell property file name
        std::string Interpolator{"linear"};      ///< Interpolator of ADC as a function of input Et for each sample and sampling phase in a pulse. "linear" or "barycentric_rational" are available
        int NBC{-1};                             ///< Number of BCs to be processed
        double mu{-1.};                          ///< Average number of interactions per BC
        float phase{-1.};                        ///< Sampling phase
        std::vector<int> TrainPattern;           ///< Bunch train filling pattern
        std::vector<int> SignalPattern;          ///< Signal injection pattern
        std::string OutputFileName;              ///< Output digit sequence file name
        std::string LowPtPileupHitSummaryFile;   ///< Input low pt hit summary file name
        std::string HighPtPileupHitSummaryFile;  ///< Input high pt hit summary file name
        int seed{1};                             ///< Seed of random function
        bool OutputBranch_PileupTrueEt{true};    ///< Whether to fill PileupTrueEt branch
        bool OutputBranch_PileupTrueTau{true};   ///< Whether to fill PileupTrueTau branch
        bool OutputBranch_PileupTrueEtSum{true}; ///< Whether to fill PileupTrueEtSum branch

      protected:
        bool badStructure(const std::vector<int>& structure);
        virtual void memberCheck() = 0;
        void setCellPropertyMaps();
        std::pair<std::vector<unsigned int>, std::vector<unsigned int>>
        getPileupNInteraction(const double fraction);
        inline void checkSeed()
        {
            Message::Info((boost::format("Seed is set as %1% for Digitizer") % seed).str());
            if (seed == 1)
                Message::Warning("The seed is set as 1 which is the default value. Perhaps you forgot to specify seed");
            else if (not seed)
                Message::Warning("This seed may cause inconsistency of hits in each event when you split Hitsummary with respect to cell/region, because the seed is not reset appropriately for each cell/region. Seed is recommended to be an integer larger than 0.");
            else if (seed < 0)
                Message::Fatal("Negative seed cannot be used. An integer larger than 0 is recommended.");
        }
        SequenceDataMap getSequenceMap(
          HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
          std::vector<unsigned int> crossingNumbers);
        SequencesDataMap getSequencesMap(
          HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
          std::vector<bool> crossingPattern);

        CellPropertyTree::CellConstantMap m_CellConstantMap;
        CellPropertyTree::GDataMap m_GDataMap;
        std::unordered_map<Int_t, std::vector<std::vector<double>>> ETOut; // ET in order of output tree channel<BC<hit>> (filled if OutputBranch_PileupTrueEt is true)
        std::unordered_map<Int_t, std::vector<std::vector<double>>> delayOut;

      private:
        void displayCount(const int nthBC);
        SampledPulse getSampledEventPulse(const std::vector<float>& vET,
                                          const std::vector<float>& vDel,
                                          const CellPropertyTree::GData& GData,
                                          const int sampperbc,
                                          const int nthSampUsed);
        void setMap(HitSummaryTree::HitSummaryDataMap& HitSummaryDataMap,
                    std::function<void()> setEvents,
                    std::function<void(const CellPropertyTree::GData&, const int, const int)> addPairToMap);
        int m_nEventsInHitSummary;

        Int_t channelId;
        std::vector<std::vector<float>>* ET; // ET from hit summary events<event<hit>>
        std::vector<std::vector<float>>* delay;
    };
} // namespace SequenceGenerator
  /// @}
