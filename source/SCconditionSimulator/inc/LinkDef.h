#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class Tree;
#    pragma link C++ class TreeReader;
#    pragma link C++ class CellPropertyTreeBase;
#    pragma link C++ class SCconditionSimulator::CellPropertyTree;
#    pragma link C++ class SCconditionSimulator::ReconstructedTree;

#endif
