/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include <deque>
#include "CellPropertyTreeBase.h"
#include <iostream>
#include <TInterpreter.h>

namespace SCconditionSimulator

{
    /// Class to extract information from cell property tree
    class CellPropertyTree : public CellPropertyTreeBase
    {
      public:
        /// Structure containing pulse shape information
        struct cellprop{
            int sampperbc{0};   ///< Number of samples per BC
            std::vector<float> inputEt;   ///< Input transverse energy corresponding to each elementof @ref gFunction vector [MeV]
            std::vector<std::deque<double>> gFunction; ///<  smooth pulse shapes [ADC]
        };            
        /**
         * @brief Construct a new CellPropertyTree object
         * 
         * @param inputFileName Input cell property file name
         */
        CellPropertyTree(const std::string& inputFileName) :
            CellPropertyTreeBase(inputFileName) {
                setMap();
            }
        /// channelId vs @ref cellprop
        std::unordered_map<Int_t, CellPropertyTree::cellprop> map;

      private:
        void setMap();
          
    };
} // namespace CellPropertyGenerator
/// @}
