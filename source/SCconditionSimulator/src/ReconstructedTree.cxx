#include "ReconstructedTree.h"
#include "CellPropertyTree.h"
#include "TreeReader.h"
#include <numeric>

using namespace SCconditionSimulator;

void ReconstructedTree::RecoAccuracyTree()
{
    gInterpreter->GenerateDictionary("std::vector<std::deque<double> >", "vector;deque");    
    CellPropertyTree cellprop(inputCellpropFileName);
    auto& cellpropmap = cellprop.map; 

    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("RecoAccuracy", "RecoAccuracy");
    
    Int_t sampperbc_out, nSignal;
    Double_t MeanRecEt{0.}, MeanRecTau{0.}, MeanRecEtTau{0.}, MeanReldiffRecTrueEt{0.}, RMSRecEt{0.}, RMSRecTau{0.}, RMSRecEtTau{0.}, RMSReldiffRecTrueEt{0.};
    std::vector<double> ReldiffRecTrueEt, emptyVector_d;
    std::vector<float> inputEt_out;
    std::vector<std::deque<double> > gFunction_out;
    std::vector<std::deque<double> > time_out;
    
    outputTree->Branch("channelId", &channelId);
    outputTree->Branch("eta", &eta);
    outputTree->Branch("phi", &phi);
    outputTree->Branch("layer", &layer);
    outputTree->Branch("detector", &detector);
    outputTree->Branch("LSB", &LSB);
    outputTree->Branch("pedADC", &pedADC);
    outputTree->Branch("noise", &noise);
    outputTree->Branch("ElecNoiseAnalogEt", ElecNoiseAnalogEt);
    outputTree->Branch("inputEt", &inputEt_out);
    outputTree->Branch("sampperbc", &sampperbc_out);
    outputTree->Branch("gFunction", &gFunction_out);
    outputTree->Branch("time", &time_out);
    outputTree->Branch("NBC", &NBC);
    outputTree->Branch("phase", &phase);
    outputTree->Branch("mu", &mu);
    outputTree->Branch("TrainPattern", TrainPattern);
    outputTree->Branch("SignalPattern", SignalPattern);
    outputTree->Branch("nSignal", &nSignal);
    outputTree->Branch("OFCa", &OFCa);
    outputTree->Branch("OFCb", &OFCb);
    outputTree->Branch("PileupTrueEt", OutputBranch_PileupTrueEt ? PileupTrueEt : &emptyVector_d);
    outputTree->Branch("PileupTrueTau", OutputBranch_PileupTrueTau ? PileupTrueTau : &emptyVector_d);
    outputTree->Branch("SignalTrueEt", OutputBranch_SignalTrueEt ? SignalTrueEt : &emptyVector_d);
    outputTree->Branch("SignalTrueTau", OutputBranch_SignalTrueTau ? SignalTrueTau : &emptyVector_d);
    outputTree->Branch("SignalTrueEtTau", OutputBranch_SignalTrueEtTau ? SignalTrueEtTau : &emptyVector_d);
    outputTree->Branch("RecEt", OutputBranch_RecEt ? RecEt : &emptyVector_d);
    outputTree->Branch("RecTau", OutputBranch_RecTau ? RecTau : &emptyVector_d);
    outputTree->Branch("RecEtTau", OutputBranch_RecEtTau ? RecEtTau : &emptyVector_d);
    outputTree->Branch("ReldiffRecTrueEt", &ReldiffRecTrueEt);
    outputTree->Branch("MeanRecEt", &MeanRecEt);
    outputTree->Branch("MeanRecTau", &MeanRecTau);
    outputTree->Branch("MeanRecEtTau", &MeanRecEtTau);
    outputTree->Branch("MeanReldiffRecTrueEt", &MeanReldiffRecTrueEt);
    outputTree->Branch("RMSRecEt", &RMSRecEt);
    outputTree->Branch("RMSRecTau", &RMSRecTau);
    outputTree->Branch("RMSRecEtTau", &RMSRecEtTau);
    outputTree->Branch("RMSReldiffRecTrueEt", &RMSReldiffRecTrueEt);

    Long64_t nentries{fChain->GetEntriesFast()};
    Long64_t nbytes{}, nb{};
    for (Long64_t jentry{}; jentry < nentries; ++jentry) {
        Long64_t ientry{LoadTree(jentry)};
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;

        ReldiffRecTrueEt.clear();
        ReldiffRecTrueEt.shrink_to_fit();
        gFunction_out.clear();
        gFunction_out.shrink_to_fit();
        time_out.clear();
        time_out.shrink_to_fit();

        auto itr{cellpropmap.find(channelId)};
        if (itr == cellpropmap.end()) continue;
        const auto map = cellpropmap.at(channelId);
        inputEt_out = std::vector<float>(map.inputEt.begin(), map.inputEt.end());

        sampperbc_out = map.sampperbc;
        
        const auto gwave = map.gFunction;
        gFunction_out = gwave;

        const auto n_inputEt = gwave.size();
        time_out.reserve(n_inputEt);
         
        for (int j{}; j < n_inputEt; ++j){
            const auto nsamppoints = gwave[j].size();
            std::deque<double> tmp_time;
            for (int i{}; i < nsamppoints; ++i) tmp_time.push_back(i*25./sampperbc_out);
            time_out.push_back(tmp_time);
        }
        
        const auto len_RecEt = RecEt->size();
        if(OutputBranch_ReldiffRecTrueEt){
            for (int i{}; i < len_RecEt; ++i) ReldiffRecTrueEt.push_back((RecEt->at(i)-SignalTrueEt->at(i))/SignalTrueEt->at(i));
        }
        //count signal 
        const auto bcpattern = std::accumulate(SignalPattern->begin(), SignalPattern->end(), 0);
        int signal = 0;
        for (int i=0; i<SignalPattern->size(); i+=2) signal += SignalPattern->at(i);
        const auto nsignal = signal*NBC/bcpattern;
        nSignal = nsignal;

        if (OutputBranch_MeanRecEt or OutputBranch_RMSRecEt){
            double sumEt =0;
            double sumEt_2 =0;
            for (int i{}; i<len_RecEt; ++i){
                if (SignalTrueEt->at(i) != 0.){
                    auto recEt = RecEt->at(i); 
                    sumEt += recEt;
                    sumEt_2 += recEt*recEt;
                }
            }
            if(OutputBranch_MeanRecEt) MeanRecEt = sumEt/nsignal;
            if(OutputBranch_RMSRecEt) RMSRecEt = std::sqrt(sumEt_2/nsignal);
        }

        const auto len_RecTau = RecTau->size();
        if (OutputBranch_MeanRecTau or OutputBranch_RMSRecTau){
            double sumTau =0;
            double sumTau_2 =0;
            for (int i{}; i<len_RecTau; ++i){
                if (SignalTrueEt->at(i) != 0.){
                    auto recTau = RecTau->at(i);
                    sumTau += recTau;
                    sumTau_2 += recTau*recTau;
                }
            }
            if(OutputBranch_MeanRecTau) MeanRecTau = sumTau/nsignal;
            if(OutputBranch_RMSRecTau) RMSRecTau = std::sqrt(sumTau_2/nsignal);
        }
        
        const auto len_RecEtTau = RecEtTau->size();
        if (OutputBranch_MeanRecEtTau or OutputBranch_RMSRecEtTau){
            double sumEtTau =0;
            double sumEtTau_2 =0;
            for (int i{}; i<len_RecEtTau; ++i){
                if (SignalTrueEt->at(i) != 0.){
                    auto recEtTau = RecEtTau->at(i);
                    sumEtTau += recEtTau;
                    sumEtTau_2 += recEtTau*recEtTau;
                }
            }
            if (OutputBranch_MeanRecEtTau) MeanRecEtTau = sumEtTau/nsignal;
            if (OutputBranch_RMSRecEtTau) RMSRecEtTau = std::sqrt(sumEtTau_2/nsignal);
        }

        const auto len_diff = ReldiffRecTrueEt.size();
        if (OutputBranch_MeanReldiffRecTrueEt or OutputBranch_RMSReldiffRecTrueEt){
            double sumDiff =0;
            double sumDiff_2 =0;
            for (int i{}; i<len_diff; ++i){
                if (SignalTrueEt->at(i) != 0.){
                    auto diff = ReldiffRecTrueEt[i];
                    sumDiff += diff;
                    sumDiff_2 += diff*diff;
                }
            }
            if(OutputBranch_MeanReldiffRecTrueEt) MeanReldiffRecTrueEt = sumDiff/nsignal;
            if(OutputBranch_RMSReldiffRecTrueEt) RMSReldiffRecTrueEt = std::sqrt(sumDiff_2/nsignal);
        
        }
        
        outputTree->Fill();
     
    }
    outputTree->Write();
    outputFile->Close();
}
