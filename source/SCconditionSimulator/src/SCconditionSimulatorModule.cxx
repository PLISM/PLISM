#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "ReconstructedTree.h"

using namespace SCconditionSimulator;

PYBIND11_MODULE(SCconditionSimulatorModule, m)
{
    pybind11::class_<ReconstructedTree>(m, "ReconstructedTree")
      .def_readwrite("OutputFileName", &ReconstructedTree::OutputFileName)
      .def_readwrite("inputCellpropFileName", &ReconstructedTree::inputCellpropFileName)
      .def_readwrite("OutputBranch_SignalTrueEt", &ReconstructedTree::OutputBranch_SignalTrueEt)
      .def_readwrite("OutputBranch_SignalTrueTau", &ReconstructedTree::OutputBranch_SignalTrueTau)
      .def_readwrite("OutputBranch_SignalTrueEtTau", &ReconstructedTree::OutputBranch_SignalTrueEtTau)
      .def_readwrite("OutputBranch_RecTau", &ReconstructedTree::OutputBranch_RecTau)
      .def_readwrite("OutputBranch_RecEt", &ReconstructedTree::OutputBranch_RecEt)
      .def_readwrite("OutputBranch_RecEtTau", &ReconstructedTree::OutputBranch_RecEtTau)
      .def_readwrite("OutputBranch_ReldiffRecTrueEt", &ReconstructedTree::OutputBranch_ReldiffRecTrueEt)
      .def_readwrite("OutputBranch_PileupTrueEt", &ReconstructedTree::OutputBranch_PileupTrueEt)
      .def_readwrite("OutputBranch_PileupTrueTau", &ReconstructedTree::OutputBranch_PileupTrueTau)
      .def_readwrite("OutputBranch_MeanRecEt", &ReconstructedTree::OutputBranch_MeanRecEt)
      .def_readwrite("OutputBranch_MeanRecTau", &ReconstructedTree::OutputBranch_MeanRecTau)
      .def_readwrite("OutputBranch_MeanRecEtTau", &ReconstructedTree::OutputBranch_MeanRecEtTau)
      .def_readwrite("OutputBranch_MeanReldiffRecTrueEt", &ReconstructedTree::OutputBranch_MeanReldiffRecTrueEt)
      .def_readwrite("OutputBranch_RMSRecEt", &ReconstructedTree::OutputBranch_RMSRecEt)
      .def_readwrite("OutputBranch_RMSRecTau", &ReconstructedTree::OutputBranch_RMSRecTau)
      .def_readwrite("OutputBranch_RMSRecEtTau", &ReconstructedTree::OutputBranch_RMSRecEtTau)
      .def_readwrite("OutputBranch_RMSReldiffRecTrueEt", &ReconstructedTree::OutputBranch_RMSReldiffRecTrueEt)
      .def(pybind11::init<const std::string&>())
      .def("RecoAccuracyTree", &ReconstructedTree::RecoAccuracyTree);
}
