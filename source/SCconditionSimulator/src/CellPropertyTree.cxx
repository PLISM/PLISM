#include "CellPropertyTree.h"

using namespace SCconditionSimulator;

void CellPropertyTree::setMap()
{
    Restart();
    while (Next()){
        map[*channelId] = {std::move(*sampperbc), std::move(*inputET), std::move(*gFunction)};
    }
}
