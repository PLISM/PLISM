#include "LArIDtranslator.h"

std::map<Int_t, LArIDtranslator::Position> LArIDtranslator::GetSCPositionMap()
{
    std::map<Int_t, Position> m;
    Restart();
    while (Next()) {
        if (*DET < 0 or *DET > 3) continue;
        m[*SC_ONL_ID] = {*DET, *SAM, *SCETA, *SCPHI};
    }
    return m;
}

LArIDtranslator::SCLCPositionMap LArIDtranslator::GetSCLCPositionMap()
{
    SCLCPositionMap m;
    Restart();
    while (Next()) {
        if (*DET < 0 or *DET > 3) continue;
        m[*SC_ONL_ID][*ONL_ID] = {*DET, *SAM, *ETA, *PHI};
    }
    return m;
}

LArIDtranslator::SCLCPositionMap LArIDtranslator::GetSCLCPositionMapSC()
{
    SCLCPositionMap m;
    Restart();
    while (Next()) {
        if (*DET < 0 or *DET > 3) continue;
        m[*SC_ONL_ID][*ONL_ID] = {*DET, *SAM, *SCETA, *SCPHI};
    }
    return m;
}

Int_t LArIDtranslator::GetID(Int_t DET, Int_t SAM, Float_t ETA)
{
    switch (DET) {
        case 0:
            return 0b1 | (1 << 6) | (SAM << 4);
        case 1:
            return 0b1 | (1 << (ETA < 2.5 ? 8 : 7)) | (SAM << 4);
        case 2:
            return 0b10 | (SAM << 9);
        case 3:
            return 0b100 | (SAM << 11);
        default:
            Message::Error("Unknown DET");
    }
    return 0;
}

void LArIDtranslator::SetIDEtaPhiWindows(Int_t DET, Int_t SAM, Float_t ETA, Int_t& ID, Float_t& ew, Float_t& pw)
{
    const bool outer{ETA < 2.5};
    switch (DET) {
        case 0:
            ID = 0b1 | (1 << 6) | (SAM << 4);
            pw = 0.1;
            switch (SAM) {
                case 0:
                    ew = 0.12;
                    return;
                case 1:
                case 2:
                    ew = 0.025;
                    return;
                case 3:
                    ew = 0.1;
                    return;
                default:
                    Message::Error("Unknown SAM");
            }
            return;
        case 1:
            ID = 0b1 | (1 << (outer ? 8 : 7)) | (SAM << 4);
            pw = (outer ? 0.1 : 0.2);
            switch (SAM) {
                case 0:
                case 3:
                    ew = 0.1;
                    return;
                case 1:
                    ew = (outer ? 0.125 : 0.2);
                    return;
                case 2:
                    ew = (outer ? 0.05 : 0.2);
                    return;
                default:
                    Message::Error("Unknown SAM");
            }
            return;
        case 2:
            ID = 0b10 | (SAM << 9);
            pw = 0.2;
            ew = 0.2;
            return;
        case 3:
            ID = 0b100 | (SAM << 11);
            pw = 0.4;
            ew = 0.5;
            return;
        default:
            Message::Error("Unknown DET");
    }
}

LArIDtranslator::UM4HitSummary LArIDtranslator::GetUM4HitSummary()
{
    UM4HitSummary m;
    Restart();
    while (Next()) {
        Int_t ID;
        Float_t ew, pw;
        SetIDEtaPhiWindows(*DET, *SAM, *ETA, ID, ew, pw);
        if (ID) {
            m[ID].v.push_back({*ETA, *PHI, *SC_ONL_ID});
            auto& st{m[ID]};
            if (not st.Etawindow) {
                st.Etawindow = ew;
                st.Phiwindow = pw;
            }
        } else Message::Warning("ID was not set in LArIDtranslator class");
    }
    for (auto&& el : m) {
        auto& st{el.second};
        std::sort(st.v.begin(), st.v.end());
    }
    return m;
}
