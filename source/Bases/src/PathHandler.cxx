#include "PathHandler.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <vector>

boost::filesystem::path PathHandler::ReplacePrefix(const boost::filesystem::path& path, const std::string& prefix)
{
    const auto apath{boost::filesystem::absolute(path)};
    auto parent_path{apath.parent_path()};
    auto filename{apath.filename()};
    std::vector<std::string> v;
    boost::algorithm::split(v, filename.string(), boost::is_any_of("_"));
    if (not v.size()) Message::Fatal("Empty path");
    v[0] = prefix;
    boost::filesystem::path newfilename(boost::algorithm::join(v, "_"));
    return parent_path /= newfilename;
}

bool PathHandler::HasPrefix(const boost::filesystem::path& path, const std::string& prefix)
{
    const auto apath{boost::filesystem::absolute(path)};
    const auto n{prefix.size()};
    const auto filename{apath.filename().string()};
    if (filename.substr(0, n) == prefix and filename[n] == '_')
        return true;
    else
        return false;
}
