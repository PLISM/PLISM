#include "Tree.h"

Tree::Tree(const std::string& inputFileName, const std::string& inputTreeName) :
    FileObject(inputFileName),
    fChain(0)
{
    TFile* inputFile = new TFile(inputFileName.c_str(), "read");
    TTree* tree;
    inputFile->GetObject(inputTreeName.c_str(), tree);
    fChain   = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
}

Tree::~Tree()
{
    if (not fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t Tree::GetEntry(Long64_t entry)
{
    if (not fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t Tree::LoadTree(Long64_t entry)
{
    if (not fChain) return -5;
    Long64_t centry{fChain->LoadTree(entry)};
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() not_eq fCurrent) {
        fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}
