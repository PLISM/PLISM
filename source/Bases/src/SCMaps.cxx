#define SCMaps_cxx
#include "SCMaps.h"

unsigned int SCMaps::getID(AtlasID AtlasID, EMSubsystem subsystem, EMLayer layer)
{
    if (AtlasID == LArEM) return (0b0001 | (1 << (6 + subsystem)) | layer << 4);
    Message::Error("Unknown region");
    return 0;
}

unsigned int SCMaps::getID(AtlasID AtlasID, int region)
{
    if (AtlasID == LArHEC && region >= 0 && region < 4)
        return (0b0010 | (region << 9));
    if (AtlasID == LArFCal && region > 0 && region < 4)
        return (0b0100 | (region << 11));
    Message::Error("Unknown region");
    return 0;
}

unsigned int SCMaps::getID(AtlasID AtlasID, TileSubsystem subsystem, TileSample sampling)
{
    if (AtlasID == LArTile)
        return (0b1000 | (1 << (16 + subsystem)) | (sampling << 13));
    Message::Error("Unknown region");
    return 0;
}

void SCMaps::fill(ChannelIdMap& theMap)
{
    Int_t channelId = 0;
    Float_t eta;
    Float_t phi;
    unsigned int ID;
    auto add = [&] {
        ++channelId;
        unsigned int* i = &theMap[ID][phi][eta];
        if (*i)
            Message::Error((boost::format("Overlap: ID=%1%, phi=%2%, eta=%3%") % ID % phi % eta).str());
        else
            *i = channelId;
    };
    auto iterate = [&](Float_t etaBegin, Float_t etaWidth, int etaNum) {
        Int_t ieta = 0;
        for (ieta = 0, eta = etaBegin; ieta < etaNum; ++ieta, eta += etaWidth)
            add();
        Float_t etaTmp = eta;
        for (ieta = 0, eta = -eta; ieta < etaNum; ++ieta, eta += etaWidth)
            add();
        eta = std::move(etaTmp);
    };
    Float_t phiWidth;
    Int_t iphi;

    for (iphi = 0, phi = -3.2, phiWidth = .1; iphi <= 63;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, EMB, presampler);
        iterate(0., .1, 14);
        iterate(eta, .12, 1);
        ID = getID(LArEM, EMB, front);
        iterate(0., .025, 56);
        iterate(eta, .025, 3);
        ID = getID(LArEM, EMB, middle);
        iterate(0., .025, 56);
        iterate(eta, .075, 1);
        ID = getID(LArEM, EMB, back);
        iterate(0., .1, 13);
        iterate(eta, .05, 1);
        ID = getID(LArEM, OuterEMEC, presampler);
        iterate(1.5, .1, 3);
        ID = getID(LArEM, OuterEMEC, front);
        iterate(1.375, .125, 1);
        iterate(eta, .025, 11);
        iterate(eta, .2 / 6., 6);
        iterate(eta, .025, 16);
        iterate(eta, .1, 1);
        ID = getID(LArEM, OuterEMEC, middle);
        iterate(1.375, .05, 1);
        iterate(eta, .025, 3);
        iterate(eta, .025, 12);
        iterate(eta, .025, 28);
        ID = getID(LArEM, OuterEMEC, back);
        iterate(1.5, .1, 3);
        iterate(eta, .1, 7);
        ID = getID(LArHEC, 0);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 1);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 2);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 3);
        iterate(1.5, .1, 10);
    }
    for (iphi = 0, phi = -3.2, phiWidth = 0.2; iphi <= 31;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, InnerEMEC, front);
        iterate(2.5, .2, 39);
        iterate(eta, .1, 1);
        ID = getID(LArEM, InnerEMEC, middle);
        iterate(2.5, .2, 47);
        iterate(eta, .1, 1);
        ID = getID(LArHEC, 0);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 1);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 2);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 3);
        iterate(2.5, 0.2, 14);
    }
}

void SCMaps::fill(ChannelIdVector& theVector)
{
    Int_t channelId = 0;
    Float_t eta;
    Float_t phi;
    unsigned int ID;
    Float_t phiWidth;
    auto add = [&](Float_t etaWidth) {
        ++channelId;
        if (std::round(phi * 1000.) == -3200)
            theVector.push_back(
              {channelId, ID, static_cast<Float_t>(eta + etaWidth * .5), -3.12});
        else if (std::round(phi * 1000.) == 3100)
            theVector.push_back(
              {channelId, ID, static_cast<Float_t>(eta + etaWidth * .5), 3.12});
        else
            theVector.push_back({channelId, ID, static_cast<Float_t>(eta + etaWidth * .5), static_cast<Float_t>(phi + phiWidth * .5)});
    };
    auto iterate = [&](Float_t etaBegin, Float_t etaWidth, int etaNum) {
        Int_t ieta = 0;
        for (ieta = 0, eta = etaBegin; ieta < etaNum; ++ieta, eta += etaWidth)
            add(etaWidth);
        Float_t etaTmp = eta;
        for (ieta = 0, eta = -eta; ieta < etaNum; ++ieta, eta += etaWidth)
            add(etaWidth);
        eta = std::move(etaTmp);
    };
    Int_t iphi;

    for (iphi = 0, phi = -3.2, phiWidth = .1; iphi <= 63;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, EMB, presampler);
        iterate(0., .1, 14);
        iterate(eta, .12, 1);
        ID = getID(LArEM, EMB, front);
        iterate(0., .025, 56);
        iterate(eta, .025, 3);
        ID = getID(LArEM, EMB, middle);
        iterate(0., .025, 56);
        iterate(eta, .075, 1);
        ID = getID(LArEM, EMB, back);
        iterate(0., .1, 13);
        iterate(eta, .05, 1);
        ID = getID(LArEM, OuterEMEC, presampler);
        iterate(1.5, .1, 3);
        ID = getID(LArEM, OuterEMEC, front);
        iterate(1.375, .125, 1);
        iterate(eta, .025, 11);
        iterate(eta, .2 / 6., 6);
        iterate(eta, .025, 16);
        iterate(eta, .1, 1);
        ID = getID(LArEM, OuterEMEC, middle);
        iterate(1.375, .05, 1);
        iterate(eta, .025, 3);
        iterate(eta, .025, 12);
        iterate(eta, .025, 28);
        ID = getID(LArEM, OuterEMEC, back);
        iterate(1.5, .1, 3);
        iterate(eta, .1, 7);
        ID = getID(LArHEC, 0);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 1);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 2);
        iterate(1.5, .1, 10);
        ID = getID(LArHEC, 3);
        iterate(1.5, .1, 10);
    }
    for (iphi = 0, phi = -3.2, phiWidth = .2; iphi <= 31;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, InnerEMEC, front);
        iterate(2.5, .2, 39);
        iterate(eta, .1, 1);
        ID = getID(LArEM, InnerEMEC, middle);
        iterate(2.5, .2, 47);
        iterate(eta, .1, 1);
        ID = getID(LArHEC, 0);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 1);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 2);
        iterate(2.5, 0.2, 14);
        ID = getID(LArHEC, 3);
        iterate(2.5, 0.2, 14);
    }
}

void SCMaps::fill(ChannelIdAngleMap& theMap)
{
    Int_t channelId = 0;
    Float_t eta;
    Float_t phi;
    Float_t etaWidth;
    Float_t phiWidth;
    auto add = [&] {
        theMap[channelId++] = {static_cast<Float_t>(eta + etaWidth * 0.5),
                               static_cast<Float_t>(phi + phiWidth * 0.5)};
    };

    unsigned int ID{};
    Int_t ieta;
    Int_t iphi;
    ieta = ID; // just to remove compilation warning

    for (iphi = 0, phi = -3.2, phiWidth = 0.1; iphi <= 63;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, EMB, presampler);
        for (ieta = 0, eta = 0, etaWidth = 0.1; ieta <= 13;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.12; ieta <= 14; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, EMB, front);
        for (ieta = 0, eta = 0, etaWidth = 0.025; ieta <= 55;
             ++ieta, eta += etaWidth)
            add();
        for (; ieta <= 58; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, EMB, middle);
        for (ieta = 0, eta = 0, etaWidth = 0.025; ieta <= 55;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.075; ieta <= 56; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, EMB, back);
        for (ieta = 0, eta = 0, etaWidth = 0.1; ieta <= 12;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.05; ieta <= 13; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, OuterEMEC, presampler);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 2;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArEM, OuterEMEC, front);
        for (ieta = 0, eta = 1.375, etaWidth = 0.125; ieta <= 0;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.025; ieta <= 12; ++ieta, eta += etaWidth) add();
        for (etaWidth = (0.2 / 6.0); ieta <= 18; ++ieta, eta += etaWidth) add();
        for (etaWidth = 0.025; ieta <= 34; ++ieta, eta += etaWidth) add();
        for (etaWidth = 0.1; ieta <= 35; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, OuterEMEC, middle);
        for (ieta = 0, eta = 1.375, etaWidth = 0.05; ieta <= 0;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.025; ieta <= 3; ++ieta, eta += etaWidth) add();
        for (; ieta <= 15; ++ieta, eta += etaWidth) add();
        for (; ieta <= 43; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, OuterEMEC, back);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 2;
             ++ieta, eta += etaWidth)
            add();
        for (; ieta <= 9; ++ieta, eta += etaWidth) add();
        ID = getID(LArHEC, 0);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 9;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 1);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 9;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 2);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 9;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 3);
        for (ieta = 0, eta = 1.5, etaWidth = 0.1; ieta <= 9;
             ++ieta, eta += etaWidth)
            add();
    }

    for (iphi = 0, phi = -3.2, phiWidth = 0.2; iphi <= 31;
         ++iphi, phi += phiWidth) {
        ID = getID(LArEM, InnerEMEC, front);
        for (ieta = 36, eta = 2.5, etaWidth = 0.2; ieta <= 38;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.1; ieta <= 39; ++ieta, eta += etaWidth) add();
        ID = getID(LArEM, InnerEMEC, middle);
        for (ieta = 44, eta = 2.5, etaWidth = 0.2; ieta <= 46;
             ++ieta, eta += etaWidth)
            add();
        for (etaWidth = 0.1; ieta <= 47; ++ieta, eta += etaWidth) add();
        ID = getID(LArHEC, 0);
        for (ieta = 10, eta = 2.5, etaWidth = 0.2; ieta <= 13;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 1);
        for (ieta = 10, eta = 2.5, etaWidth = 0.2; ieta <= 13;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 2);
        for (ieta = 10, eta = 2.5, etaWidth = 0.2; ieta <= 13;
             ++ieta, eta += etaWidth)
            add();
        ID = getID(LArHEC, 3);
        for (ieta = 10, eta = 2.5, etaWidth = 0.2; ieta <= 13;
             ++ieta, eta += etaWidth)
            add();
    }
}
