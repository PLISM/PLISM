#pragma once
#include <boost/format.hpp>
#include <iostream>

class Message
{
  public:
    static inline void Fatal(const std::string& str)
    {
        std::cerr << "FATAL: " << str << "\n";
        std::exit(EXIT_FAILURE);
    }
    static inline void Error(const std::string& str) { std::cerr << "ERROR: " << str << "\n"; }
    static inline void Warning(const std::string& str) { std::cout << "WARNING: " << str << "\n"; }
    static inline void Info(const std::string& str) { std::cout << "INFO: " << str << "\n"; }
    static inline void Debug(const std::string& str) { std::cout << "DEBUG: " << str << "\n"; }
    static inline void Verbose(const std::string& str) { std::cout << "VERBOSE: " << str << "\n"; }
};
