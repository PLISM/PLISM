#pragma once
#include <deque>
#include "TreeReader.h"

class CellPropertyTreeBase : public TreeReader
{
  public:
    CellPropertyTreeBase(const std::string& inputFileName) :
        TreeReader(inputFileName, "CellProperty"),
        channelId(m_reader, "channelId"),
        eta(m_reader, "eta"),
        phi(m_reader, "phi"),
        layer(m_reader, "layer"),
        detector(m_reader, "detector"),
        inputET(m_reader, "inputET"),
        sampperbc(m_reader, "sampperbc"),
        gFunction(m_reader, "gFunction"),
        LSB(m_reader, "LSB"),
        pedADC(m_reader, "pedADC"),
        noise(m_reader, "noise"),
        BCshift(m_reader, "BCshift") {}

  protected:
    TTreeReaderValue<Int_t> channelId;
    TTreeReaderValue<Float_t> eta;
    TTreeReaderValue<Float_t> phi;
    TTreeReaderValue<Int_t> layer;
    TTreeReaderValue<Int_t> detector;
    TTreeReaderValue<std::vector<float>> inputET;
    TTreeReaderValue<Int_t> sampperbc;
    TTreeReaderValue<std::vector<std::deque<double>>> gFunction;
    TTreeReaderValue<Float_t> LSB;
    TTreeReaderValue<Double_t> pedADC;
    TTreeReaderValue<Float_t> noise;
    TTreeReaderValue<Int_t> BCshift;
};
