#pragma once
#include <boost/filesystem.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include "FileGenerator.h"
#include "PathHandler.h"

class FileObject
{
  public:
    FileObject() {}
    FileObject(const std::string& inputFileName) :
        m_path(inputFileName)
    {
        if (not boost::filesystem::exists(m_path)) Message::Fatal(m_path.string() + " is not found");
        m_path = boost::filesystem::absolute(m_path);
        Message::Info("Opening input file " + m_path.string());
    }
    virtual ~FileObject()
    {
        if (not m_path.empty()) Message::Info("Closed input file " + m_path.string());
    }

    inline boost::filesystem::path path() const { return m_path; }
    inline boost::filesystem::path AddPrefix(const std::string& prefix) const { return PathHandler::AddPrefix(m_path, prefix); }
    inline boost::filesystem::path ReplaceFilename(const std::string& filename) const { return PathHandler::ReplaceFilename(m_path, filename); }
    inline boost::filesystem::path ReplacePrefix(const std::string& prefix) const { return PathHandler::ReplacePrefix(m_path, prefix); }
    inline boost::filesystem::path ReplaceExtension(const std::string& ex) const { return PathHandler::ReplaceExtension(m_path, ex); }
    inline bool HasPrefix(const std::string& prefix) const { return PathHandler::HasPrefix(m_path, prefix); }

  protected:
    boost::filesystem::path m_path;
};
