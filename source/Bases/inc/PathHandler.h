#pragma once
#include <boost/filesystem.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include "Message.h"

class PathHandler
{
  public:
    static inline boost::filesystem::path AddPrefix(const boost::filesystem::path& path, const std::string& prefix)
    {
        auto apath{boost::filesystem::absolute(path)};
        return apath.parent_path() /= boost::filesystem::path(prefix + "_") += apath.filename();
    }
    static inline boost::filesystem::path ReplaceFilename(const boost::filesystem::path& path, const std::string& filename)
    {
        auto apath{boost::filesystem::absolute(path)};
        return apath.parent_path() /= boost::filesystem::path(filename);
    }
    static boost::filesystem::path ReplacePrefix(const boost::filesystem::path& path, const std::string& prefix);
    static inline boost::filesystem::path ReplaceExtension(const boost::filesystem::path& path, const std::string& ex)
    {
        auto apath{boost::filesystem::absolute(path)};
        return apath.parent_path() /= apath.stem() += boost::filesystem::path("." + ex);
    }
    static bool HasPrefix(const boost::filesystem::path& path, const std::string& prefix);
};
