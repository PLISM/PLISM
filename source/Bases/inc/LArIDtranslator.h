#pragma once
#include "TreeReader.h"

class LArIDtranslator : public TreeReader
{
  public:
    LArIDtranslator(const std::string& InputFileName) :
        TreeReader(InputFileName, "LIT"),
        ONL_ID(m_reader, "ONL_ID"),
        DET(m_reader, "DET"),
        SAM(m_reader, "SAM"),
        ETA(m_reader, "ETA"),
        PHI(m_reader, "PHI"),
        SC_ONL_ID(m_reader, "SC_ONL_ID"),
        SCETA(m_reader, "SCETA"),
        SCPHI(m_reader, "SCPHI") {}

    struct Position {
        Int_t DET, SAM;
        Float_t ETA, PHI;
    };
    typedef std::map<Int_t, std::map<Int_t, Position>> SCLCPositionMap;
    typedef std::unordered_map<Float_t, std::unordered_map<Float_t, Int_t>> Phi_Eta_SC_map;
    typedef std::pair<Float_t, Float_t> EtaPhiPair_t;
    std::map<Int_t, Position> GetSCPositionMap();
    SCLCPositionMap GetSCLCPositionMap();
    SCLCPositionMap GetSCLCPositionMapSC();
    Int_t GetID(Int_t DET, Int_t SAM, Float_t ETA);

    struct EtaPhiSCID {
        Float_t ETA, PHI;
        Int_t SCID;
        bool operator<(const EtaPhiSCID& rhs) const { return ETA == rhs.ETA ? PHI < rhs.PHI : ETA < rhs.ETA; }
    };
    struct Struct4HitSummay {
        std::vector<EtaPhiSCID> v;
        Float_t Etawindow{};
        Float_t Phiwindow{};
    };
    typedef std::unordered_map<Int_t, Struct4HitSummay> UM4HitSummary;
    UM4HitSummary GetUM4HitSummary();

  private:
    void SetIDEtaPhiWindows(Int_t DET, Int_t SAM, Float_t ETA, Int_t& ID, Float_t& ew, Float_t& pw);

    TTreeReaderValue<Int_t> ONL_ID;
    TTreeReaderValue<Int_t> DET;
    TTreeReaderValue<Int_t> SAM;
    TTreeReaderValue<Float_t> ETA;
    TTreeReaderValue<Float_t> PHI;
    TTreeReaderValue<Int_t> SC_ONL_ID;
    TTreeReaderValue<Float_t> SCETA;
    TTreeReaderValue<Float_t> SCPHI;
};
