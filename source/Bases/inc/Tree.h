#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <deque>
#include <map>
#include <unordered_map>
#include <vector>
#include "FileObject.h"

class Tree : public FileObject
{
  public:
    Tree() :
        FileObject(){};
    Tree(const std::string& inputFileName, const std::string& inputTreeName);

    virtual ~Tree();

  protected:
    TTree* fChain;
    Int_t fCurrent;
    void Setup(const std::string& inputFileName, const std::string& inputTreeName);
    Int_t GetEntry(Long64_t entry);
    Long64_t LoadTree(Long64_t entry);
    void Init(TTree* tree);
};
