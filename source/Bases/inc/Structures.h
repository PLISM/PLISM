#pragma once
#include <deque>
#include <list>
#include <map>
#include <unordered_map>
#include <unordered_set>

struct SC {
    Int_t channelId;
    unsigned int ID;
    Float_t eta;
    Float_t phi;
};
struct angle {
    Float_t eta;
    Float_t phi;
};
struct GOFCData {
    std::vector<float> ET;
    std::vector<std::deque<float> > g;  // g[nthET][nthSample]
    std::vector<std::vector<float> > a; // a[nthET][nthOFC]
    std::vector<std::vector<float> > b; // b[nthET][nthOFC]
    int sampperbc;
};
typedef std::unordered_map<unsigned int,
                           std::map<float, std::map<float, unsigned int> > >
  ChannelIdMap;
typedef std::vector<SC> ChannelIdVector;
typedef std::map<unsigned int, angle> ChannelIdAngleMap;
typedef std::unordered_map<unsigned int, GOFCData> GOFCDataMap;
typedef std::unordered_set<unsigned int> ChannelIdSet;
