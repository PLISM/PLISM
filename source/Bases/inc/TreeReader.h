#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <deque>
#include <fstream>
#include <map>
#include <unordered_map>
#include <vector>
#include "FileObject.h"

class TreeReader : public FileObject
{
  public:
    TreeReader(const std::string& inputFileName, const std::string& inputTreeName) :
        FileObject(inputFileName),
        m_file(TFile::Open(inputFileName.c_str())),
        m_reader(TTreeReader(inputTreeName.c_str(), m_file)),
        m_tree_name(inputTreeName.c_str())
    {
        std::ifstream ifs(inputFileName);
        m_configured = ifs.is_open();
    }

    inline bool operator()() const { return m_configured; }
    inline const TFile* GetFile() const { return m_file; }
    inline std::string GetTreeName() const { return m_tree_name; }
    inline void Restart() { m_reader.Restart(); }
    inline Bool_t Next() { return m_reader.Next(); }

  protected:
    TFile* m_file;
    TTreeReader m_reader;
    std::string m_tree_name;
    bool m_configured{false};
};
