#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class Message;
#    pragma link C++ class PathHandler;
#    pragma link C++ class FileGenerator;
#    pragma link C++ class FileObject;
#    pragma link C++ class Tree;
#    pragma link C++ class TreeReader;
#    pragma link C++ class SCMaps;
#    pragma link C++ class LArIDtranslator;
#    pragma link C++ class CellPropertyTreeBase;

#endif
