#!/bin/sh

export LAR_SIMLATOR_MAIN_DIR=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)

myhost=`hostname`
if [[ $myhost == lxplus*.cern.ch || $myhost == pcatutt28 ]] ; then
    export LITDB=/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db
else
    export LITDB=$LAR_SIMLATOR_MAIN_DIR/run/storage/CellProperties/LArId.db
    if [ ! -e $LITDB ]; then
	scp lxplus:/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db $LAR_SIMLATOR_MAIN_DIR/run/storage/CellProperties/ || \
	    echo "Failed to copy LAr ID translator database from lxplus. Please copy /afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db from lxplus to $LAR_SIMLATOR_MAIN_DIR/run/storage/CellProperties/"
    fi
fi

if [[ $myhost == login*.icepp.jp || $myhost == lxplus*.cern.ch || $myhost == pcatutt28 ]] ; then
    setupATLAS || {
	export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    }
fi
