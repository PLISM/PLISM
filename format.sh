#!/bin/bash

echo "Formatting Code..."
find ./source/Bases -type f -name "*.cxx" -or -name "*.h" | xargs clang-format -i -style=file
find ./source/CellPropertyGenerator -type f -name "*.cxx" -or -name "*.h" | xargs clang-format -i -style=file
find ./source/HitSummarizer -type f -name "*.cxx" -or -name "*.h" | xargs clang-format -i -style=file 
find ./source/Reconstructor -type f -name "*.cxx" -or -name "*.h" | xargs clang-format -i -style=file
find ./source/SequenceGenerator -type f -name "*.cxx" -or -name "*.h" | xargs clang-format -i -style=file
echo "done!"
